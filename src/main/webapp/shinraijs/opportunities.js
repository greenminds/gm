function editOpp(id){
	if (id != null) {
		document.getElementById("editOppId").value = id;
		document.getElementById("formEdit").submit();
	}
}
function deleteOpp(id){
	if (id != null) {
		document.getElementById("deleteOppId").value = id;
		document.getElementById("formDelete").submit();
	}
}
function getSearchVal(val){
	if(val.value.length>1)
		AjaxController.getSearchVal(val.value,dispGetSearchVal);
	else
			AjaxController.getSearchVal("all",dispGetSearchVal);
	
}
function dispGetSearchVal(res){
	var result="";
	if(res.length>0){
		for(var i=0;i<res.length;i++){
			result+='<tr><td>'+res[i].email+'</td><td>'+res[i].firstName+'</td><td>'
					+res[i].phoneNumber+'</td><td>'+res[i].phoneNumber+'</td><td>'+res[i].assignedTo+'</td><td>'
					+res[i].status+'</td><td><a onclick="editOpp('+res[i].id+')" class="glyphicon glyphicon-edit edtbtn"></a>&nbsp; <a onclick="deleteOpp('
					+res[i].id+')" class="glyphicon glyphicon-remove edtbtn"></a></td></tr>';
		}
		document.getElementById("myTable").innerHTML=result;
	}else{
		document.getElementById("myTable").innerHTML="";
	}
}
function getStatusVal(val){
	if(val.value.length>2){
		alert(val.value);
		AjaxController.getStatusVal(val.value,dispGetSearchVal);
	}
}
