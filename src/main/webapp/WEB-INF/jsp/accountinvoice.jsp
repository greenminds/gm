
<script src="js/addreceipt.js"></script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	
	 <!-- main content start-->

<div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Accounts / Invoice </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2"> Invoice </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">

                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="#" class="btn btn-warning crtbtn" data-tooltip="Create" data-toggle="modal" data-target="#invccreate"> <span class="glyphicon glyphicon-plus"></span> </a> </div>
                  <div class="col-md-2 col-sm-1 col-xs-3">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="createInvoiceSheet.mm">XLS</a></li>
											<!--      <li><a href="#">WORD</a></li> -->
										</ul>
                    
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="accountinvoice.mm" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3 col-sm-1 col-xs-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">3</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#" onclick="pagination(10)">10</a></li>
                        <li class="active"><a  href="#" onclick="pagination(25)">25</a></li>
                        <li><a href="#" onclick="pagination(50)">50</a></li>
                        <li><a href="#" onclick="pagination(100)">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">No</th>
                  <th class="tblthd">Client</th>
                  <th class="tblthd">Date</th>
                  <th class="tblthd">Amount</th>
                  <th class="tblthd">Mode</th>
                  <th class="tblthd">TR.ID</th>
                  <th class="tblthd">Description</th>
                  <th class="tblthd">Coordinators</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                 
                  <td>
                  <input type="text" onkeyup="Searchinvoice(this,'id')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="Searchinvoice(this,'email')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="Searchinvoice(this,'date')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="Searchinvoice(this,'amount')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="Searchinvoice(this,'paymentMode')" class="form-control">
                  </td>
                  <td>
                  <input type="text"onkeyup="Searchinvoice(this,'transactionNo')" class="form-control">                  
                  </td>
                   <td>
                  <input type="text" onkeyup="Searchinvoice(this,'description')" class="form-control">
                  </td>
                  
                  
                <td><select  onchange="Searchinvoice(this,'coordinator')" class="form-control" >                           
								<option>Select Coordinators</option>
                           	<c:forEach var="coordinate" items="${coordinators}">
								<option value="${coordinate.emp_firstname}">${coordinate.emp_firstname} </option>
							</c:forEach>
                        
                          </select>
                  </td>
                  <td></td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <c:forEach var="invo" items="${invoicer}">
               <tr>
                  <td>${invo.id}</td>
                  <td>${invo.email}</td>
                  <td>${invo.date}</td>
                  <td>${invo.amount}</td>
                  <td>${invo.paymentMode}</td>
                  <td>${invo.transactionNo}</td>
                  <td>${invo.description}</td>
                  <td>${invo.coordinator}</td>

                  <td>
                  <ul class="actnul">
                  <li><a href="#" data-tooltip="Edit" class="actn" data-toggle="modal" data-target="#invcedit"><span class="glyphicon glyphicon-edit edtbtn" onclick="editInvoice(${invo.id})"></span></a></li>&nbsp;&nbsp;
                  <li><a href="#" data-tooltip="Go to Approval" class="actn"><span class="glyphicon glyphicon-ok edtbtn" onclick="approvalInvo(${invo.id})"></span></a></li>
                  </ul>
                  </td>
                </tr>
                 </c:forEach>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  <!--footer-->
  <div class="footer">
    <p>&copy; 2016 Admin Panel. All Rights Reserved | Design by <a href="https://maestroinfotech.in/" target="_blank">Maestro Infotech System</a></p>
  </div>
  <!--//footer--> 
</div>
<!-- Modal -->
<div id="invccreate" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="form-horizontal" role="form" action="invoice.mm" method="post">
    <div class="modal-content">
      <div class="modal-header mbt">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mdlhdr">Create Invoice</h4>
         <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Client</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="email" value="" onchange="isEmail(this)" placeholder="Enter Email"  >
      <span id="isEmail" style="color:red"></span>
      </div>
    </div>
      </div>
      <div class="modal-body">
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Date</label>
      <div class="col-sm-3">
        <input type="text" class="form-control datepicker" name="" value="">
      </div>
      <label class="control-label col-sm-2 lblfnt">Amount</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="amount" value="" onchange="isAmount(this)" placeholder="Enter Amount" >
        <span id="isAmount" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Pay Mode</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="paymentMode" value="" onchange="isPayment(this)" placeholder="Enter PaymentMode"  >
        <span id="isPayment" style="color:red"></span>
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Transaction ID</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="transactionNo" value="" onchange="isTransaction(this)" placeholder="Enter TransactionNo"  >
    <span id="isTransaction" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Coordinators</label>
      <div class="col-sm-8">
      <select class="form-control" name="coordinator">
      <option>Select Coordinators</option>
                           	<c:forEach var="coordinate" items="${coordinators}" >
								<option value="${coordinate.emp_firstname}" >${coordinate.emp_firstname}</option>
							</c:forEach>
                          </select>
      </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Description</label>
    <div class="col-md-8">
    <textarea class="form-control" name=""onchange="isPayment1(this)" placeholder="Enter Description"  ></textarea>
    <span id="isPayment1" style="color:red"></span>
    </div>
    </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
 </form>
  </div>
</div>
<!------edit--------->
<div id="invcedit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="form-horizontal" role="form" action="editInvoice.mm">
    <div class="modal-content">
      <div class="modal-header mbt">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mdlhdr">Edit Invoice</h4>
         <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Client</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="email" value="" id="editInEmail" onchange="isEmail(this)" placeholder="Enter Email"  disabled>
      </div>
    </div>
      </div>
      <div class="modal-body">
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Date</label>
      <div class="col-sm-3">
        <input type="text" class="form-control datepicker" name="date" value="" id="editInDate">
      </div>
     <input type="hidden" id="invoiceId" name="id" value="" />
      <label class="control-label col-sm-2 lblfnt">Amount</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="amount" value="" id="editInAmount" onchange="isAmount(this)" placeholder="Enter Amount" >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Pay Mode</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="paymentMode" value="" id="editInPaymode" onchange="isPayment(this)" placeholder="Enter PaymentMode" >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Transaction ID</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="transactionNo" value="" id="editInTransaction" onchange="isTransaction(this)" placeholder="Enter TransactionNo" >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Coordinators</label>
      <div class="col-sm-8">
      <select class="form-control"  name="coordinator"  id="editInCoordinator">
     <option>Select Coordinators</option>
                           	<c:forEach var="coordinate" items="${coordinators}">
								<option value="${coordinate.emp_firstname}">${coordinate.emp_firstname}</option>
							</c:forEach>
                          </select>
      </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Description</label>
    <div class="col-md-8">
    <textarea class="form-control" name="description" id="editInDescription" onchange="isPayment(this)" placeholder="Enter Description"></textarea>
    </div>
    </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
 </form>
  </div>
</div>
<!-- Classie --> 
<script src="js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
 <script>
 $.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 7,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">�</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">�</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
  	pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
      	pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

$(document).ready(function(){
    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
    
});
 </script>
<!--scrolling js--> 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.js"> </script>