<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Croyez - Career</title>

<!-- Bootstrap -->
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
</script>
 
<script>            
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.crunchify-top').fadeIn(duration);
			} else {
				jQuery('.crunchify-top').fadeOut(duration);
			}
		});
 
		jQuery('.crunchify-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
</script>

<script type="text/javascript">
$(document).ready(function () {
 //called when key is pressed in textbox
  $("#exampleInputEmail").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
//         $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>

<style>
/* Carousel */
ul.nav li.dropdown:hover > ul.dropdown-menu {
    display: block;    
}

#quote-carousel .carousel-control {
	background: none;
	color: #CACACA;
	font-size: 2.3em;
	text-shadow: none;
	margin-top: 30px;
}
#quote-carousel .carousel-control.left {
	left: -60px;
}
#quote-carousel .carousel-control.right {
	right: -60px;
}
#quote-carousel .carousel-indicators {
	right: 50%;
	top: auto;
	bottom: 0px;
	margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
	width: 50px;
	height: 50px;
	margin: 5px;
	cursor: pointer;
	border: 4px solid #CCC;
	border-radius: 50px;
	opacity: 0.4;
	overflow: hidden;
	transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
	background: #333333;
	width: 128px;
	height: 128px;
	border-radius: 100px;
	border-color: #f33;
	opacity: 1;
	overflow: hidden;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
 
.crunchify-top {
	display: none;
	position: fixed;
	bottom: 1rem;
	right: 1rem;
	width: 3.2rem;
	height: 3.2rem;
	line-height: 3.2rem;
	font-size: 1.4rem;
	color: #fff;
	background-color: rgba(0,0,0,0.3);
	text-decoration: none;
	border-radius: 3.2rem;
	text-align: center;
	cursor: pointer;
}
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
/*-----------form--------------*/
.aplyp {
	font-family: 'Open Sans', sans-serif;
	font-size: 14px;
	color: #fff;
	text-align: center;
	margin-bottom: 2%;
}
.subbtn {
	background: #fd9324;
	border: 2px solid #fd9324;
	padding: 5px 3%;
	border-radius: 0px;
	color: #fff;
	font-family: 'Open Sans', sans-serif;
	font-size: 14px;
	margin-top: 1%;
	margin-bottom: 2em;
}
.subbtn:hover {
	background: #fd9324;
	color: #fff;
	box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
}
.form-control {
	border: none !important;
	border-radius: 0px !important;
	font-style: italic;
	border: 1px solid #533050 !important;
}
#custom-search-form {
	margin: 0;
	margin-top: 0px;
	padding: 0;
}
#custom-search-form .search-query {
	padding-right: 3px;
	padding-right: 26px;
	padding-left: 3px;
	padding-left: 5px;
	/* IE7-8 doesn't have border-radius, so don't indent the padding */
 
	margin-bottom: 0;
	-webkit-border-radius: 0px;
	-moz-border-radius: 0px;
	border-radius: 0px;
	border: none;
	height: 30px;
	background-color: #a59b8f;
	color: #fff;
}
#custom-search-form button {
	border: 0;
	background-color: #533050;
	padding: 2px 5px;
	margin-top: -3px;
	position: relative;
	left: -1px;
	margin-bottom: 0;
	-webkit-border-radius: 3px;
	-moz-border-radius: 3px;
	border-radius: inherit !important;
	height: 30px;
}
.search-query:focus + button {
	z-index: 3;
}
.enqery {
    width: 100%;
    position: absolute;
    z-index: 9999;
    max-height: 442px;
}
.loginhh {
	font-family: 'Open Sans', sans-serif;
	font-size: 25px;
	color: #FD9324;
	margin: 0.5em 0;
	text-align: center;
}
.bxshdw {
	box-shadow: 0 0px 7px rgba(0, 0, 0, 0.25);
	background-color: rgba(255, 255, 255, .6);
	margin-top: 1%;
padding-bottom: 1%;
}
.sbmtbtn {
	background-color: #FD9324;
	padding: 2% 9%;
	border: 1px solid #FD9324;
	color: rgb(255, 255, 255);
	margin: 2% auto;
	font-family: 'Open Sans', sans-serif;
}
.recentbg {
	background: #666;
	padding: 15px;
}
</style>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body>
 <a href="#" class="crunchify-top">↑</a>
<!--------------reaponsive nav------------------->
<div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="https://in.linkedin.com/in/croyez-immigration-823671124
" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph">+91 9543690385</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
             <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="homePage.mm"><img src="images/demo-new-site/croyez2.png" width="250" class="img-responsive"></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="homePage.mm">Home</a></li>
            <li><a href="about.mm">About Us</a></li>
           <li class="dropdown"><a href="canada.mm" class="dropdown-toggle" data-toggle="dropdown">Canada<b class="caret"></b></a>
          <ul class="dropdown-menu">
           <li><a href="canada.mm">Canada</a></li>
              <li><a href="expressentry.mm">Express Entry</a></li>
              <li><a href="qubecskiled.mm">Quebec Skilled </a></li>
              <li><a href="familysponsorship.mm">Family Sponsorship </a></li>
              <li><a href="businessimmigration.mm">Business Immigration</a></li>
              <li><a href="provincial nominee.mm">Provincial Nominee </a></li>
            </ul>
          </li>
            <li><a href="australia.mm">Australia</a></li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Immigration <b class="caret"></b> </a>
            <ul class="dropdown-menu">
                <li><a href="#">Germany</a></li>
                <li><a href="newzealand.mm">New zealand</a></li>
                <li><a href="#">Hong Kong</a></li>
                <li><a href="#">Equador</a></li>
              </ul>
          </li>
            <li><a href="blog.mm">Blog</a></li>
            <li class="active"><a href="career.mm">Careers</a></li>
            <li><a href="contact.mm">Contact</a></li>
            <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
          </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
        </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav> 
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="row">
        <div class="col-md-8 col-sm-6">
          <div class="row">
  <div class="row">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active"> <img src="images/bg.png" alt="Chania">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <div class="item"> <img src="images/bg.png" alt="Chania">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <div class="item"> <img src="images/bg.png" alt="Flower">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <div class="item"> <img src="images/bg.png" alt="Flower">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
      </div>
      
      <!-- Left and right controls --
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>--> </div>
  </div>
           </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="col-md-12 bxshdw">
            <div class="col-md-12">
              <h4 class="loginhh">Enquiry</h4>
            </div>
            <c:if test="${enq eq 'true'}">
            <span style="color:#0dbd0d; font-weight:700; padding-left: 15px;"> Submitted Enquiry Successfully</span>
            </c:if>
            <div class="col-md-12">
              <form action="enquiry.mm" method="post">
                <div class="form-group">
                  <input type="text" class="form-control" id="exampleInputEmail1" name="firstName" placeholder="Name">
                </div>
          <div class="form-group">
										<input type="email" name="email" class="form-control"
											placeholder="Email Address" id="email-id" required="required"
											onchange="emailCheck(this)">
										<div class="registrationFormAlert" id="divCheckPasswordMatch"
											style="color: red;"></div>
										<div class="registrationFormAlert" id="divCheckMatch"
											style="color: green;"></div>
									</div>
                <div class="form-group">
                  <input type="tel" class="form-control" required="required"
											minlength="10" maxlength="10" placeholder="Mobile Number"
											id="exampleInputEmail" name="phoneNumber">
											<input type ="hidden" name="hidden" value="career">
                </div>
          <div class="form-group">
            <select name="country" class="form-control">
           <option value="">SELECT PROFESSION</option>
								<option value="ACCOUNTANT">ACCOUNTANT</option>
								<option value="CIVIL ENGG">CIVIL ENGG</option>
								<option value="DENTIST">DENTIST</option>
								<option value="FRESHERS">FRESHERS</option>
								<option value="IT ENGG">IT ENGG</option>
								<option value="MECHANICAL ENGG">MECHANICAL ENGG</option>
								<option value="OTHERS">OTHERS</option>
								<option value="STUDENT">STUDENT</option>
										</select>
           
          </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Enquiry">
                  
                </div>
                <!-- <div class="checkbox">
    <label>
      <input type="checkbox">Remember me
    </label>
  </div>-->
                <div class="col-md-12">
                  <button type="submit" class="sbmtbtn center-block">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="container-fluid boutmargn">
    <div class="container">
	<div class="col-md-12">
    <h3 class="abouthed">Careers </h3>
    <h4 class="aboutpara">Migrate to a new country is a big undertaking and the immigration process can sometimes be a daunting task.CROYEZ immigration services private limited is one of the fastest growing immigration company in india, having its head office at chennai,tamilnadu.We evaluate client profile with legal advisers before we file the application.</h4>
    </div>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-6">
    <div class="col-md-12">
     <h3 class="career">Careers </h3>
     <p class="skilpara"><img src="images/demo-new-site/icons/login.png"> Tele Caller</p>
     <p class="skilpara"><img src="images/demo-new-site/icons/login.png"> SEO</p>
     <p class="skilpara"></p>
     <p class="skilpara"></p>
     <p class="skilpara"></p>
    </div>
    </div>
    <div class="col-md-6">
    <div class="col-md-12">
    <img src="images/Cycles.png" class="img-responsive">
    </div>
    </div>
    </div>
    </div>
  </div>
</div>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/croyez.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">To Know more about Croyez and get updates about immigration services kindly follow us on social websites.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('f','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="f"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('3','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="3"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://in.linkedin.com/in/croyez-immigration-823671124" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('4','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="4"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('5','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="5"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> 2floor ,sons complex,near sangamam hotel,Ashok Nagar, chennai - 600083 </p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">+91 9543690385</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">info@croyezimmigration.com</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.mm" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.mm" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.mm" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Testimonial </h4>
            </div>
            <div class="row">
              <div class="col-md-12 column">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel"> 
                  <!-- Bottom Carousel Indicators --> 
                  
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner text-center"> 
                    
                    <!-- Quote 1 -->
                    <div class="item active">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 2 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 3 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--1--> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">&#169; 2016 Croyez. All rights reserved</h3>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>