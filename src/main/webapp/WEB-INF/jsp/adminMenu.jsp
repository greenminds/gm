  <!--left-fixed -navigation-->
      <div class=" sidebar" role="navigation">
    <div class="navbar-collapse">
          <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <ul class="nav" id="side-menu">
              <li class="active"> <a href="adminDashboard.mm" ><i class="fa fa-home nav_icon"></i>Dashboard</a> </li>
              <li> <a href="#"><i class="fa fa-thumbs-up nav_icon"></i>Pre-Sales<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li  class="active"> <a href="#">Opportunities/Leads<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="opportunities.mm">Opportunities</a> </li>
                      <li class="sidbr"> <a class="lisd" href="enquiries.mm">Enquiries</a> </li>
                      <li class="sidbr"> <a class="lisd" href="refferals.mm">Refferals</a> </li>
                    </ul>
                     
                 </li>
                </ul>
            <!-- /nav-second-level --> 
          </li>
              <li> <a href="#"><i class="fa fa-users nav_icon"></i>Clients<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="addClient.mm">Add Client</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clients.mm">Clients</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clientfileupload.mm">File Uploads</a> </li>
                  <li class="sidbr"> <a class="lisd" href="agreementupload.mm">Agreement Upload</a> </li>
                  <li class="sidbr"> <a class="lisd" href="documentapproval.mm">Document Approval</a></li>
                  <li class="sidbr"> <a class="lisd" href="agreementapproval.mm">Agreement Approval</a></li>
                  <li class="sidbr"> <a class="lisd" href="invoice.mm">Initial Agreement</a></li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-inr nav_icon"></i>Accounts<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="reciept.mm">Reciepts</a> </li>
                  <li class="sidbr"> <a class="lisd" href="installment.mm">Installment Breakup</a> </li>
                  <li class="sidbr"> <a class="lisd" href="accountinvoice.mm">Invoice</a> </li>
                  <li class="sidbr"> <a class="lisd" href="miscfee.mm">Misc Fee</a></li>
                  <li class="sidbr"> <a class="lisd" href="refunds.mm">Refunds</a></li>
                  <li class="sidbr"> <a class="lisd" href="onlinepayments.mm">Online Payments</a></li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-filter nav_icon"></i>Assessment<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="e">Express Assessment</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Assessment Point</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Assessment Contents</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Pending Assessment</a></li>
                  <li class="sidbr"> <a class="lisd" href="#">Send Assessment</a></li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-envelope nav_icon"></i>Communication<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Mailer<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Email Sync</a> </li>
                      <li class="sidbr"> <a class="lisd" href="mail.mm">Simple Mail</a> </li>
                      <li class="sidbr"> <a class="lisd" href="bulkmail.mm">Bulk Mail</a> </li>
                    </ul>
              </li>
              <li> <a href="#">Messenger<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Simple SMS</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Bulk SMS</a> </li>
                    </ul>
              </li>
              <li> <a href="courier.html">Courier</a></li>
              <li> <a href="newsupdates.html" >News Updates</a> </li>
                </ul>
            <!-- /nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-question nav_icon"></i>Helpdesk/Updates<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="supporttickets.mm">Support Tickets</a> </li>
                  <li class="sidbr"> <a class="lisd" href="officialupdates.html">Official Updates</a> </li>
                  <li class="sidbr"> <a class="lisd" href="faq.html">FAQ</a> </li>
                  <li class="sidbr"> <a class="lisd" href="blog.html">Blog</a> </li>
                </ul>
            <!-- //nav-second-level --> 
          </li>	
          <li> <a href="#"><i class="fa fa-bar-chart nav_icon"></i>Report<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="#">Task Report</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Express Report</a> </li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
              <li> <a href="#"><i class="fa fa-cogs nav_icon"></i>System<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Administration<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="role.mm">Role</a> </li>
                      <li class="sidbr"> <a class="lisd" href="userdetails.mm">User Details</a> </li>
                      <li class="sidbr"> <a class="lisd" href="accesscontrol.mm">Access Control</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">Comm. Settings<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">SMTP Config</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Mailing List</a> </li>
                     <li class="sidbr"> <a class="lisd" href="#">Email Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Back.Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Post Agg. Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">SMS Config</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">General Masters<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="countrymaster.mm">Country Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Branch</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Module</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Doc Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Support Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Update Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clientcategories.mm">Client Category</a> </li>
                  <li class="sidbr"> <a class="lisd" href="assignto.mm">Designation</a></li>
                  <li class="sidbr"> <a class="lisd" href="#">Agreement Template</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Application Forms</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Client Status</a> </li>
                  <li class="sidbr"> <a class="lisd" href="source.mm">Source Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Document Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Document Mapping</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">Account Master<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="program.html">Program</a> </li>
                      <li class="sidbr"> <a class="lisd" href="feemaster.html">Fee Master</a> </li>
                    </ul>
              </li>
                </ul>
            <!-- /nav-second-level --> 
          </li>
            </ul>
        <!-- //sidebar-collapse --> 
      </nav>
        </div>
  </div>
      <!--left-fixed -navigation--> 