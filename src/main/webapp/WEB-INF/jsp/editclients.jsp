<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="js/addclient.js"></script>


<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
  <!-- main content start-->
  <input type="hidden" id="clientId" name="clientId" value="${clientId}" />
  <div id="page-wrapper">
    <div class="main-page general">
      <div class="panel-info rothdr"> Clients / Clients </div>
      <div class="row">
        <div class="col-md-4">
          <div class="col-md-12">
            <div class="panel cltprf"> <img src="http://placehold.it/150x90" class="img-rounded">
              <p class="cltnm">${user.fName}</p>
              <p class="cltmil">${user.email}</p>
            </div>
          </div>
          <div class="col-md-12">
            <nav class="nav-sidebar widget-shadow">
              <ul class="nav tabs">
              <li class="libtmbdr"><a href="#followup" data-toggle="tab">Follow Ups</a></li>
                <li class="active libtmbdr"><a href="#personal" data-toggle="tab">Personal Info</a></li>
                <li class="libtmbdr"><a href="#contact" data-toggle="tab">Contact Info</a></li>
                <li class="libtmbdr"><a href="#coordinator" data-toggle="tab">Coordinators</a></li>
                <li class="libtmbdr"><a href="#document" data-toggle="tab">Document</a></li>
                <li class="libtmbdr"><a href="#file" data-toggle="tab">File Details</a></li>
                <li class="libtmbdr"><a href="#permanent" data-toggle="tab">Permanent Residency</a></li>
                <li class="libtmbdr"><a href="#tempporary" data-toggle="tab">Tempporary Foregin Worker</a></li>
                <li class="libtmbdr"><a href="#account" data-toggle="tab">Accounts</a></li>
                <li class=""><a href="#logs" data-toggle="tab">Logs</a></li>
              </ul>
            </nav>
          </div>
        </div>
        <div class="col-md-8 widget-shadow">
          <form class="form-horizontal" action="updatecClient.mm" method="post">
            <div class="tab-content">
            <div class="tab-pane text-style" id="followup">
           <h4 class="title2 general">Profile Received Followup</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">1</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="profileNote1" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="profileDate1" placeholder="">
                  </div>
                </div>
                 <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">2</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="profileNote2" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="profileDate2" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">3</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                     <input type="text" class="form-control" value="" name="" id="profileNote3" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="profileDate3" placeholder="">
                  </div>
                </div>
      	      <div class="form-group">
                <label class="control-label lblfnt col-md-3"><input type="checkbox" name="" id="callYou1" >We wil call you</label>
                 <label class="control-label lblfnt col-md-3"><input type="checkbox" name="" id="profileReceipt1" >Profile Received</label>
                 <div class="col-md-2"> <a href="#" class="btn btn-success" onclick="profileFollowup()">Submit</a> </div>
                <label class="control-label lblfnt col-md-3" id="profileResult" style="color:green"></label>
                </div>
                
                 <h4 class="title2 general">Registration Followup</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">1</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="regFollowNote1" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="regFollowDate1" placeholder="">
                  </div>
                </div>
                 <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">2</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="regFollowNote2" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="regFollowDate2" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">3</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="regFollowNote3" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="regFollowDate3" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                <label class="control-label lblfnt col-md-3"><input type="checkbox" name="" id="callYou2" >We wil call you</label>
                 <label class="control-label lblfnt col-md-3"><input type="checkbox" name="" id="profileReceived2" >Profile Received</label>
                <div class="col-md-2"> <a href="#" class="btn btn-success" onclick="RegistrationFolloup()">Submit</a> </div>
                <label class="control-label lblfnt col-md-3" id="regResult" style="color:green"></label>
                </div>
                 <h4 class="title2 general">Invoice Followup</h4>
                <div class="bdr"></div>
                 <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">1</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="invoiceNote1" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="invoiceDate1" placeholder="">
                  </div>
                </div>
                 <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">2</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="invoiceNote2" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="invoiceDate2" placeholder="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">3</label>
                  <label class="control-label col-md-2 lblfnt">Notes</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="" name="" id="invoiceNote3" placeholder="">
                  </div>
                  <label class="control-label col-md-1 lblfnt">Date</label>
                  <div class="col-md-3">
                    <input type="text" class="form-control datepicker" value="" name="" id="invoiceDate3" placeholder="">
                  </div>
                </div>
                 <div class="form-group">
                <label class="control-label lblfnt col-md-3"><input type="checkbox" name="" id="callYou3" >We wil call you</label>
                 <label class="control-label lblfnt col-md-3"><input type="checkbox" name="" id="profileReceipt3" >Profile Received</label>
                <div class="col-md-2"> <a href="#" class="btn btn-success" onclick="InvoiceFolloup()">Submit</a> </div>
                <label class="control-label lblfnt col-md-3" id="invoiceResult" style="color:green"></label>
                </div>
            </div>
              <div class="tab-pane active text-style" id="personal">
                <h4 class="title2 general">Personal Info</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Email</label>
                  <div class="col-md-4">
                    <input type="email" class="form-control"  name="" placeholder="Email" value="${clt.email}" disabled>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Phone</label>
                  <div class="col-md-4">
                    <input type="number" class="form-control" value="${clt.phoneNumber}" onchange="isPhoner(this)" name="" placeholder="Phone">
                    <span id="isPhoner" style="color:red"></span>	
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Salutation</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" value="${clt.salutation}"
											onchange="isAlphabet(this)" id="salutation" name="" placeholder="Salutation">
											<span id="isAlphabet" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">First Name</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" id="firstName" 
											 onchange="isAlphabet1(this)" placeholder="First Name" value="${clt.firstName}">
											<span id="isAlphabet1" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Middle Name</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control"  name="" id="middleName" value="${clt.middleName}"
												onchange="isAlphabet2(this)" placeholder="Middle Name">
										<span id="isAlphabet2" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Last Name</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control"  name="" id="lastName" value="${clt.lastName}"
											onchange="isAlphabet3(this)"	placeholder="Last Name">
										<span id="isAlphabet3" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Date of Birth</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker"  name="" id="dob" value="${clt.dob}"
												onchange="isDob(this)" placeholder="Date of Birth">
										<span id="isDob" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Passport Exp Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker"  name="" id="ped" value="${clt.passportExpDate}"
											placeholder="Passport Exp Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Birth Country</label>
                 <div  class="col-md-4">
											<select id="birthCountry" class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
												<c:if test="${clt.birthCountry eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
                  <label class="control-label col-md-2 lblfnt">Residence Country</label>
                  <div class="col-md-4">
											<select id="residencyCountry" class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
												<c:if test="${clt.residenCountry eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Citizenship</label>
                  <div class="col-md-4">
											<select id=citizenCountry class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
												<c:if test="${clt.citizenshipCountry eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
                  <label class="control-label col-md-2 lblfnt">Preffered Language</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control"  name="" id="preferredLanguage" value="${clt.prefferedLanguage}"
												onchange="isAlphabet4(this)" placeholder="Preffered Language">
										<span id="isAlphabet4" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Date of Jioning</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker"  name="" id="doj" value="${clt.dateOfJoin}"
											 placeholder="Date of Joining">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success" onclick="saveFirstStep()">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="contact">
                <h4 class="title2 general">Contact Info</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Address 1</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="address1" value="${clt.address1}"
												onchange="isAddress(this)" placeholder="Address1">
										<span id="isAddress" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Address 2</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="address2" value="${clt.address2}"
											onchange="isAddress1(this)"	placeholder="Address2">
										<span id="isAddress1" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">City</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="city" value="${clt.city}"
											onchange="isAlphabet5(this)"	placeholder="City">
										<span id="isAlphabet5" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">State</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="state" value="${clt.state}"
										onchange="isAlphabet6(this)"		placeholder="State">
										<span id="isAlphabet6" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Country</label>
                <div class="col-md-4">
											<select id="country" class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
													<c:if test="${clt.country eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
                  <label class="control-label col-md-2 lblfnt">Zip Code</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="zipcode" value="${clt.zipcode}"
												onchange="isZibcode(this)"	placeholder="Zip Code">
												<span id="isZibcode" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Mobile</label>
                  <div class="col-md-4">
                    <input type="number" class="form-control" name="" id="mobile" value="${clt.mobileNumber}"
									onchange="isPhoner1(this)"			placeholder="Mobile">
										<span id="isPhoner1" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Phone</label>
                  <div class="col-md-4">
                    <input type="number" class="form-control" name="" id="phone1" value="${clt.phoneNumber1}"
									onchange="isPhoner2(this)"			placeholder="Phone">
									<span id="isPhoner2" style="color:red"></span>	
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Email</label>
                  <div class="col-md-4">
                    <input type="email" class="form-control" name="" id="otherEmail" value="${clt.alternateEmail}"
										onchange="isEmail(this)"		placeholder="Other Email">
										<span id="isEmail" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">FAX</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name=""   id="fax" value="${clt.fax}"
								onchange="isFax(this)"		placeholder="FAX">
										<span id="isFax" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Contact Method</label>
                 <div class="col-md-4">
											<select id="contactMethod" class="form-control">
											<option>Select Contact Method</option>
												<option value="Online">Online</option>
												<option value="News Paper">News Paper</option>
											</select>
										</div>
                  <label class="control-label col-md-2 lblfnt">Special Instructions</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="specialInst" value="${clt.specialInstructions}"
										onchange="isAddress2(this)"		placeholder="Special Instructions">
										<span id="isAddress2" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="coordinator">
                <h4 class="title2 general">Coordinators Info</h4>
                <div class="bdr"></div>
                <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="tblthd">Type</th>
                        <th class="tblthd">Coordinator</th>
                      </tr>
                    </thead>
                    <tbody class="tbdy">
                      <tr>
                        <td>SALES</td>
                       <td><select id="sales" class="form-control">
															<option>Select Sales Coordinator</option>
															<c:forEach var="salco" items="${salemp}">
												<option value="${salco.e_id}">${salco.emp_firstname}</option>
												</c:forEach>
													</select></td>
                      <tr>
                      <td>RELATIONSHIP MANAGER</td>
                       <td><select id="relationshipManager" name="relationshipManager" class="form-control">
															<option>Select relationship manager</option>
															<c:forEach var="relation" items="${relatoinmanList}">
												<option value="${relation.e_id}">${relation.emp_firstname}</option>
												</c:forEach>
													</select></td>
                      </tr>
                      <tr>
                        <td>PROCESSING OFFICER</td>
                       	<td><select id="processingOfficer" class="form-control">
															<option>Select processing Officer </option>
															<c:forEach var="process" items="${processingemp}">
												<option value="${process.e_id}">${process.emp_firstname}</option>
												</c:forEach>
													</select></td>
                      </tr>
                      <tr>
                        <td>PROCESSING COORDINATOR</td>
                      <td><select id="processingCoordinator" class="form-control">
															<option>Select Processing Coordinator</option>
															<c:forEach var="processcor" items="${processingempcor}">
												<option value="${processcor.e_id}">${processcor.emp_firstname}</option>
												</c:forEach>
													</select></td>
                      </tr>
                      <tr>
                        <td>PROCESSING CONSULTANT</td>
													<td><select id="processingConsultant" class="form-control">
															<option>Select Processing Consultant</option>
															<c:forEach var="processconsult" items="${processingConsultantemp}">
												<option value="${processconsult.e_id}">${processconsult.emp_firstname}</option>
												</c:forEach>
													</select></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="document">
                <h4 class="title2 general">Agreements</h4>
                <div class="bdr"></div>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">Agreement</th>
                          <th class="tblthd">Upload Date</th>
                          <th class="tblthd">Status</th>
                          <th class="tblthd">Approver</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="2">Agreement File</td>
                          <td>Not Uploaded</td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Documents</h4>
                <div class="bdr"></div>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">Document</th>
                          <th class="tblthd">File</th>
                          <th class="tblthd">Status</th>
                          <th class="tblthd">Remove</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="4">
                          <a href="addclientdocument.html" class="btn btn-warning crtbtn" data-tooltip="Add Document"> <span class="glyphicon glyphicon-plus"></span> </a> 
                          <a href="approvaclientdocumtnel.html" class="btn btn-warning" data-tooltip="Approve Document"><span class="glyphicon glyphicon-ok"></span></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Applications</h4>
                <div class="bdr"></div>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">Name</th>
                          <th class="tblthd">Applications</th>
                          <th class="tblthd">Status</th>
                          <th class="tblthd">Remove</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="4"><a href="#" class="btn btn-warning" data-tooltip="Add Application"><span class="glyphicon glyphicon-plus"></span></a> <a href="#" class="btn btn-warning" data-tooltip="Approve Application"><span class="glyphicon glyphicon-ok"></span></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Document Emails</h4>
                <div class="bdr"></div>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">Sl.No.</th>
                          <th class="tblthd">Subject</th>
                          <th class="tblthd"></th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr> </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="file">
                <h4 class="title2 general">File Details</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Program</label>
                  <div class="col-md-4">
                    <select class="form-control" name="">
                   <option>Select Program</option>
												
												<option value="program1">progream1</option>
												<option value="program2">progream2</option>
												<option value="program3">progream3</option>
                    </select>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Visa Posts</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="visaPosts" value="${clt.visaPosts}"
											onchange="isLetter(this)"	placeholder="Visa Posts">
										<span id="isLetter" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">File No.</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name=""  id="fileNo" value="${clt.fileNo}"
										onchange="isPhoner3(this)"		placeholder="File No.">
										<span id="isPhoner3" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">File No. Other</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name=""  id="fileNoOther" value="${clt.fileNoOther}"
											onchange="isPhoner4(this)"	placeholder="File No. Other">
										<span id="isPhoner4" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">File Status</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" id="fileStatus" value="${clt.fileStatus}"
										onchange="isAlphabet7(this)"		placeholder="File Status">
										<span id="isAlphabet7" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Application Submission Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="appSubmissionDate" value="${clt.appSubmissionDate}"
											placeholder="Application Submission Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Medical Exp. Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="medExpDate" value="${clt.medExpDate}"
												placeholder="Medical Exp. Date">
                  </div>
                  <label class="control-label col-md-2 lblfnt">Interview Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="interviewDate"
												value="${clt.interviewDate}"  placeholder="Interview Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Interview Time</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.interviewTime}" id="interviewTime"
												onchange="isTime(this)" placeholder="Interview Time">
										<span id="isTime" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Interview Location</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.interviewLocation}" id="interviewLocation"
												 onchange="isAddress3(this)"placeholder="Interview Location">
										<span id="isAddress3" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Additional Info</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.addInfo}" id="addInfo"
												onchange="isAddress4(this)" placeholder="Additional Info">
										<span id="isAddress4" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Medical Sent By</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.medSentBy}" id="medSentBy"
											onchange="isLetter1(this)"	placeholder="Medical Sent By">
										<span id="isLetter1" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Medical Waybill No.</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name=""  value="${clt.medBillNo}" id="medBillNo"
											onchange="isPhoner5(this)"	placeholder="Medical Waybill No.">
										<span id="isPhoner5" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Visa/Permit Sent By</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.visaSentBy}" id="visaSentBy"
											onchange="isLetter2(this)"	placeholder="Visa/Permit Sent By">
										<span id="isLetter2" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Visa/Permit Waybill No.</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name=""   value="${clt.visaWaybillNo}" id="visaWaybillNo"
											onchange="isPhoner6(this)"	placeholder="Visa/Permit Waybill No.">
										<span id="isPhoner6" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="permanent">
                <h4 class="title2 general">Permanent Residency</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">PR Issue Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="prIssueDate"
												value="${clt.prIssueDate}" placeholder="PR Issue Date">
                  </div>
                  <label class="control-label col-md-2 lblfnt">PR Validity Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="prValidityDate"
												value="${clt.prValidityDate}" placeholder="PR Validity Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Landing Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="landingDate"
												value="${clt.landingDate}" placeholder="Landing Date">
                  </div>
                  <label class="control-label col-md-2 lblfnt">Citizenship Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="citizenshipDate"
												value="${clt.citizenshipDate}" placeholder="Citizenship Date">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="tempporary">
                <h4 class="title2 general">Temporary Foreign Worker</h4>
                <div class="bdr"></div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Employer</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.employer}" id="employer"
											onchange="isAlphabet8(this)"	placeholder="Employer">
										<span id="isAlphabet8" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Employer Location</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name=""  value="${clt.employerLocation}" id="employerLocation"
											onchange="isAddress5(this)"	placeholder="Employer Location">
										<span id="isAddress5" style="color:red"></span>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">LMO Number</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" alue="${clt.lmoNumber}" id="lmoNumber"
												onchange="isZibcode1(this)"placeholder="LMO Number">
										<span id="isZibcode1" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">WP Issue Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="wpIssueDate"
												value="${clt.wpIssueDate}" placeholder="WP Issue Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">WP Validity Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name=""  id="wpValidityDate"
												value="${clt.wpValidityDate}" placeholder="WP Validity Date">
                  </div>
                  <label class="control-label col-md-2 lblfnt">WP Interview Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="wpInterviewDate"
												value="${clt.wpInterviewDate}" placeholder="WP Interview Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">WP Extension Granted Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name=""  id="wpExtGrantDate"
												value="${clt.wpExtGrantDate}" placeholder="WP Extension Granted Date">
                  </div>
                  <label class="control-label col-md-2 lblfnt">WP Interview Waiver Reciept Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="wpInterviewWaiverReciptDate"
												value="${clt.wpInterviewWaiverReciptDate}" placeholder="WP Interview Waiver Reciept Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Temp. Renewals and Extensions</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control" name="" value="${clt.tempRenExt}" id="tempRenExt"
											onchange="isAddress6(this)"	placeholder="Temp. Renewals and Extensions">
										<span id="isAddress6" style="color:red"></span>
                  </div>
                  <label class="control-label col-md-2 lblfnt">Arrival Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name=""  id="arrivalDate"
												value="${clt.arrivalDate}" placeholder="Arrival Date">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2 lblfnt">Departure Date</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control datepicker" name="" id="departureDate"
												value="${clt.departureDate}" placeholder="Departure Date">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-3"> <a href="clients.mm" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>
              </div>
              <div class="tab-pane text-style" id="account">
                <h4 class="title2 general"> Fee</h4>
                <div class="bdr"></div>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">Application Fee</th>
                          <th class="tblthd">Consultation Fee</th>
                          <th class="tblthd">Effective Date</th>
                          <th class="tblthd">Person</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td>(corresponding currency to country)</td>
                          <td></td>
                          <td></td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Receipts</h4>
                <div class="bdr"></div>
                <a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create New Receipts"> <span class="glyphicon glyphicon-plus"></span> </a>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd"></th>
                          <th class="tblthd">ID</th>
                          <th class="tblthd">Date</th>
                          <th class="tblthd">Description</th>
                          <th class="tblthd">Amount</th>
                          <th class="tblthd">Payment Mode</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr> </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Installments Info</h4>
                <div class="bdr"></div>
                <a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Add InstallmentBreakup"> <span class="glyphicon glyphicon-plus"></span> </a>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">Breakup Details</th>
                          <th class="tblthd">Amount</th>
                          <th class="tblthd">Paid</th>
                          <th class="tblthd">Due</th>
                          <th class="tblthd">Status</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="2">Total Fee : 0</td>
                          <td colspan="2">Total Paid : 0</td>
                          <td>Total Outstanding : 0</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Invoices</h4>
                <div class="bdr"></div>
                <a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create New Invoice"> <span class="glyphicon glyphicon-plus"></span> </a>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">#</th>
                          <th class="tblthd">Date</th>
                          <th class="tblthd">Description</th>
                          <th class="tblthd">Amount</th>
                          <th class="tblthd">Tax</th>
                          <th class="tblthd">Total</th>
                          <th class="tblthd">Payment mode</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="5">Total Amount</td>
                          <td colspan="2">0</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Old Invoices</h4>
                <div class="bdr"></div>
                <a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Add more Old Invoices"> <span class="glyphicon glyphicon-plus"></span> </a>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">#</th>
                          <th class="tblthd">Date</th>
                          <th class="tblthd">Description</th>
                          <th class="tblthd">Amount</th>
                          <th class="tblthd">Tax</th>
                          <th class="tblthd">Total</th>
                          <th class="tblthd">Payment mode</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="5">Total Amount</td>
                          <td colspan="2">0</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Misc. Receipts</h4>
                <div class="bdr"></div>
                <a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create Misc Receipt"> <span class="glyphicon glyphicon-plus"></span> </a>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd"></th>
                          <th class="tblthd">ID</th>
                          <th class="tblthd">Date</th>
                          <th class="tblthd">Description</th>
                          <th class="tblthd">Amount</th>
                          <th class="tblthd">Payment mode</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr> </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <h4 class="title2 general">Expenses</h4>
                <div class="bdr"></div>
                <div class="tables">
                  <div class="table-responsive bs-example">
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="tblthd">ID</th>
                          <th class="tblthd">Date</th>
                          <th class="tblthd">Description</th>
                          <th class="tblthd">Amount</th>
                          <th class="tblthd">Payment mode</th>
                        </tr>
                      </thead>
                      <tbody class="tbdy">
                        <tr>
                          <td colspan="3">Total Amount</td>
                          <td colspan="2">0</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <div class="col-md-3"> <a href="clients.html" class="btn btn-default bcktlst">Back To List</a> </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-success">Submit</a> </div>
                </div>--> 
              </div>
              <div class="tab-pane text-style" id="logs">
                <h4 class="title2 general">Logs</h4>
                <div class="bdr"></div>
                <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                    <thead>
                    <th class="tblthd">#</th>
                      <th class="tblthd">Action</th>
                      <th class="tblthd">Date/Time</th>
                      <th class="tblthd">System IP</th>
                      <th class="tblthd">Platform</th>
                      <th class="tblthd">Device</th>
                        </thead>
                    <tbody class="tbdy">
                    <td>1</td>
                      <td>InitAgreement</td>
                      <td>20-May-2016 / 04:01:44 PM</td>
                      <td>125.17.0.50</td>
                      <td> Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36</td>
                      <td>Computer/Laptop</td>
                        </tbody>
                  </table>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
 