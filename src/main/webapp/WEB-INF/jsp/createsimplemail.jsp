<!DOCTYPE HTML>
<html>
<head>
<title>Create Mail</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shinrai" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/responsive.css">
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
<script>
		 new WOW().init();
	</script>
    <script>
	initSample();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<!-------date picker---------->
<link href="css/datepicker.css" type="text/css" rel="stylesheet">
<script src="js/datepicker.js" type="text/javascript"></script>
<script>
	 $(function() {
    $("#datepicker1").datepicker();
    $("#datepicker2").datepicker();
});
</script>
<style>
.rothdr {
	width: 36% !important;
}
.panel-info.widget-shadow {
	padding: 0.7em 0.1em;
}
ul.dropdown-menu {
    padding: 0;
    min-width: 148px;
    top: 95%;
}
.charts, .row {
    margin:inherit !important;
}
.general h4.title2 {
    font-size: 17px;
    margin: 3px;
    color: #777777;
    padding-left: 10px;
    font-family: 'Open Sans', sans-serif;
}
</style>
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  <div class=" sidebar" role="navigation">
    <div class="navbar-collapse">
      <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <ul class="nav" id="side-menu">
          <li> <a href="index.html" ><i class="fa fa-home nav_icon"></i>Dashboard</a> </li>
          <li> <a href="#"><i class="fa fa-thumbs-up nav_icon"></i>Pre-Sales<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="#">Opportunities/Leads<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li  class="sidbr"> <a href="opportunities.html">Opportunities</a> </li>
                  <li class="sidbr"> <a href="enquiries.html">Enquiries</a> </li>
                  <li class="sidbr"> <a href="refferals.html">Refferals</a> </li>
                </ul>
              </li>
            </ul>
            <!-- /nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-users nav_icon"></i>Clients<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a href="addclient.html">Add Client</a> </li>
                  <li class="sidbr"> <a href="clients.html">Clients</a> </li>
                  <li class="sidbr"> <a href="clientfileupload.html">File Uploads</a> </li>
                  <li class="sidbr"> <a href="agreementupload.html">Agreement Upload</a> </li>
                  <li class="sidbr"> <a href="documentapproval.html">Document Approval</a></li>
                  <li class="sidbr"> <a href="agreementapproval.html">Agreement Approval</a></li>
                  <li class="sidbr"> <a href="invoice.html">Initial Agreement</a></li>
             </ul>
            <!-- //nav-second-level --> 
          </li>
           <li  class="active"> <a href="#"><i class="fa fa-thumbs-up nav_icon"></i>Communication <span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li  class="active"> <a href="#">Mailer<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li  class="active sidbr"> <a href="#">Simple Mail</a> </li>
                  <li class="sidbr"> <a href="#">Enquiries</a> </li>
                  <li class="sidbr"> <a href="#">Refferals</a> </li>
                </ul>
              </li>
            </ul>
            <!-- /nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-cogs nav_icon"></i>System<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="#"><i class="fa fa-book nav_icon"></i>Administration<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li> <a href="role.html">Role</a> </li>
                  <li> <a href="userdetails.html">User Details</a> </li>
                  <li> <a href="accesscontrol.html">Access Control</a> </li>
                  <li> <a href="pagelevelaccess.html">Page Level Access</a> </li>
                  <li> <a href="userrename.html.html">User Rename</a> </li>
                </ul>
              </li>
              <li> <a href="#"><i class="fa fa-book nav_icon"></i>Comm. Settings<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li> <a href="#">SMTP Config</a> </li>
                  <li> <a href="typography.html">Mailing List</a> </li>
                  <li> <a href="typography.html">Email Templates</a> </li>
                  <li> <a href="typography.html">Back.Templates</a> </li>
                  <li> <a href="typography.html">Post Agg. Templates</a> </li>
                  <li> <a href="typography.html">SMS Config</a> </li>
                </ul>
              </li>
              <li> <a href="#"><i class="fa fa-book nav_icon"></i>General Masters<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Country Master</a> </li>
                  <li> <a href="typography.html">Branch</a> </li>
                  <li> <a href="typography.html">Module</a> </li>
                  <li> <a href="typography.html">Doc Type</a> </li>
                  <li> <a href="typography.html">Support Type</a> </li>
                  <li> <a href="typography.html">Update Type</a> </li>
                  <li> <a href="typography.html">Client Category</a> </li>
                  <li> <a href="typography.html">Agreement Template</a> </li>
                  <li> <a href="typography.html">Application Forms</a> </li>
                  <li> <a href="typography.html">Client Status</a> </li>
                  <li> <a href="typography.html">Source Master</a> </li>
                  <li> <a href="typography.html">Document Master</a> </li>
                  <li> <a href="typography.html">Document Mapping</a> </li>
                </ul>
              </li>
              <li> <a href="#"><i class="fa fa-book nav_icon"></i>Account Master<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li> <a href="typography.html">Program</a> </li>
                  <li> <a href="typography.html">Fee Master</a> </li>
                  <li> <a href="typography.html">Promo Codes</a> </li>
                </ul>
              </li>
              <li> <a href="#" ><i class="fa fa-home nav_icon"></i>Performance</a> </li>
            </ul>
            <!-- /nav-second-level --> 
          </li>
        </ul>
        <!-- //sidebar-collapse --> 
      </nav>
    </div>
  </div>
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
      <button id="showLeftPush"><i class="fa fa-bars"></i></button>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"> <a href="index.html">
        <h1>Shinrai</h1>
        <span>Immigration</span> </a> </div>
      <!--//logo-->
      
      <div class="clearfix"> </div>
    </div>
    <div class="header-right"> 
      <!--search-box-->
      <div class="search-box"> 
        <!--<form class="input">
						<input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
						<label class="input__label" for="input-31">
							<svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
								<path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
							</svg>
						</label>
					</form>--->
        <form action="" class="search-form">
          <div class="form-group has-feedback">
            <label for="search" class="sr-only">Search</label>
            <input type="text" class="form-control" name="search" id="search" placeholder="Search">
            <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
        </form>
      </div>
      <!--//end-search-box-->
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 3 new messages</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="user_img"><img src="images/1.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li class="odd"><a href="#">
                <div class="user_img"><img src="images/2.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li><a href="#">
                <div class="user_img"><img src="images/3.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
          </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-filter"></i><span class="badge">3</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 3 new messages</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="user_img"><img src="images/1.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li class="odd"><a href="#">
                <div class="user_img"><img src="images/2.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li><a href="#">
                <div class="user_img"><img src="images/3.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
          </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 3 new notification</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="user_img"><img src="images/2.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li class="odd"><a href="#">
                <div class="user_img"><img src="images/1.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li><a href="#">
                <div class="user_img"><img src="images/3.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all notifications</a> </div>
              </li>
            </ul>
          </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">15</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 8 pending task</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Database update</span><span class="percentage">40%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar yellow" style="width:40%;"></div>
                </div>
                </a></li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar green" style="width:90%;"></div>
                </div>
                </a></li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar red" style="width: 33%;"></div>
                </div>
                </a></li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar  blue" style="width: 80%;"></div>
                </div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all pending tasks</a> </div>
              </li>
            </ul>
          </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
      <!--notification menu end -->
      <div class="profile_details">
        <ul>
          <li class="dropdown profile_details_drop"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <div class="profile_img"> <span class="prfil-img"><img src="images/a.png" alt=""> </span>
              <div class="user-name">
                <p>Wikolia</p>
                <span>Administrator</span> </div>
              <i class="fa fa-angle-down lnr"></i> <i class="fa fa-angle-up lnr"></i>
              <div class="clearfix"></div>
            </div>
            </a>
            <ul class="dropdown-menu drp-mnu">
              <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
              <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
              <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
  <!-- //header-ends --> 
  <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
      <div class="panel-info rothdr"> Communication / Mailer / Simple Mail </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2">Create Mail</h4>
        <div class="bdr"></div>
        <div class="col-md-12">
        <form class="form-horizontal" role="form">
        <div class="col-md-12">
   		<div class="row">
        <div class="col-md-6">
        <div class="col-md-12">
         <div class="form-group">
       <label class="control-label lblfnt" for="fstnm">Email Template (Choose if you want to send from templates)</label>
      <select class="form-control">
      <option>Select Templates</option>
      <option></option>
      <option></option>
      <option></option>
      </select>
      </div>
      </div>
        </div>
        <div class="col-md-6">
        <div class="col-md-12">
         <div class="form-group">
       <label class="control-label lblfnt" >Subject</label>
      <input type="text" class="form-control" name="" value="">
      </div>
      </div>
        </div>
        </div>
    </div>
    <div class="col-md-12">
   		<div class="row">
        <div class="col-md-6">
        <div class="col-md-12">
         <div class="form-group">
       <label class="control-label lblfnt" for="fstnm">Mailing List</label>
      <select class="form-control">
      <option>Select Mailing List</option>
      <option></option>
      <option></option>
      <option></option>
      </select>
      </div>
      </div>
        </div>
        <div class="col-md-6">
        <div class="col-md-12">
         <div class="form-group">
       <label class="control-label lblfnt">Recipient</label>
      <input type="text" class="form-control" name="" value="">
      </div>
      </div>
        </div>
        </div>
    </div>
    <div class="col-md-12">
    <div class="form-group">
    <label class="control-label lblfnt" for="fstnm">Recipient</label>
    <textarea class="ckeditor" name="description2"
													id="description2Edit" required> </textarea>
    </div>
    </div>
    <div class="form-group">
    <div class="col-md-12">  
    <div class="col-md-10">
      <!--<a href="clients.html" class="btn btn-default bcktlst">Back To List</a>-->
      </div>      
      <div class="col-sm-1">
        <a href="#" type="submit" class="btn btn-warning">Save</a>
      </div>
      </div>
    </div>
  </form>
  </div>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!--footer-->
  <div class="footer">
    <p>&copy; 2016 Admin Panel. All Rights Reserved | Design by <a href="https://maestroinfotech.in/" target="_blank">Maestro Infotech System</a></p>
  </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
  <script>
  $("#StartDate, #EndDate").datepicker();

$("#EndDate").change(function () {
    var startDate = document.getElementById("StartDate").value;
    var endDate = document.getElementById("EndDate").value;
 
    if ((Date.parse(endDate) <= Date.parse(startDate))) {
        alert("End date should be greater than Start date");
        document.getElementById("EndDate").value = "";
    }
});
  </script>
<!--scrolling js--> 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.js"> </script>
</body>
</html>