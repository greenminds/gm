<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Croyez - Home</title>

<!-- Bootstrap -->
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/windows.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js"></script>
	<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
    <script src="js/jQuery v2.2.4.js"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<!-- Crunchify's Scroll to Top Script -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>            
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.crunchify-top').fadeIn(duration);
			} else {
				jQuery('.crunchify-top').fadeOut(duration);
			}
		});
 
		jQuery('.crunchify-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
</script>

<script type="text/javascript">
$(document).ready(function () {
 //called when key is pressed in textbox
  $("#exampleInputEmail").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
//         $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script type="text/javascript">
	var statusOfEmailId = false;
	function emailCheck(email) {

		var eml = email.value;
		if (eml.length != 0) {
			

			AjaxController.checkEmail(eml, returnedEmail);
		}
		function returnedEmail(value) {
			if (value == true) {
				document.getElementById("divCheckPasswordMatch").innerHTML = "Entered Email is already an user";
				document.getElementById("divCheckMatch").innerHTML = "";
				document.getElementById("email-id").value="";
				
			} else {
				document.getElementById("divCheckPasswordMatch").innerHTML = "";
				statusOfEmailId = true;
			}
		}

	}
</script>



<script type="text/javascript">
	var statusOfEmailId = false;
	function emailCheck1(emailId) {
			
		var eml = emailId.value;
		if (eml.length != 0) {
			

			AjaxController.emailCheck1(eml, returnedEmail);
		}
		function returnedEmail(value) {
			if (value == true) {
				document.getElementById("divCheckPasswordMatch1").innerHTML = "Entered Email is already an user";
				document.getElementById("divCheckMatch").innerHTML = "";
				document.getElementById("email-id1").value="";
				
			} else {
				document.getElementById("divCheckPasswordMatch1").innerHTML = "";
				statusOfEmailId = true;
			}
		}

	}
</script>


<script type="text/javascript">
$(document).ready(function () {
 //called when key is pressed in textbox
  $("#mobile").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
//         $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script src="js/prefixfree.min.js"></script>
<style>
/* Carousel */


#quote-carousel .carousel-control {
	background: none;
	color: #CACACA;
	font-size: 2.3em;
	text-shadow: none;
	margin-top: 30px;
}
#quote-carousel .carousel-control.left {
	left: -60px;
}
#quote-carousel .carousel-control.right {
	right: -60px;
}
#quote-carousel .carousel-indicators {
	right: 50%;
	top: auto;
	bottom: 0px;
	margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
	width: 50px;
	height: 50px;
	margin: 5px;
	cursor: pointer;
	border: 4px solid #CCC;
	border-radius: 50px;
	opacity: 0.4;
	overflow: hidden;
	transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
	background: #333333;
	width: 128px;
	height: 128px;
	border-radius: 100px;
	border-color: #f33;
	opacity: 1;
	overflow: hidden;
}

.no-padding {
	padding: 3px !important;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
.crunchify-top {
	display: none;
	position: fixed;
	bottom: 1rem;
	right: 1rem;
	width: 3.2rem;
	height: 3.2rem;
	line-height: 3.2rem;
	font-size: 1.4rem;
	color: #fff;
	background-color: rgba(0, 0, 0, 0.3);
	text-decoration: none;
	border-radius: 3.2rem;
	text-align: center;
	cursor: pointer;
}
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
.enqery {
	width: 100%;
	position: absolute;
	z-index: 9999;
	max-height: 442px;
}
.loginhh {
	font-family: 'Open Sans', sans-serif;
	font-size: 25px;
	color: #FD9324;
	margin: 0.5em 0;
	text-align: center;
}
.bxshdw {
	box-shadow: 0 0px 7px rgba(0, 0, 0, 0.25);
	background-color: rgba(255, 255, 255, .6);
	margin-top: 1%;
padding-bottom: 1%;
}
.sbmtbtn {
	background-color: #FD9324;
	padding: 2% 9%;
	border: 1px solid #FD9324;
	color: rgb(255, 255, 255);
	margin: 2% auto;
	font-family: 'Open Sans', sans-serif;
}
.recentbg {
	background: #666;
	padding: 15px;
}
.austl {
	 height: 402px;
}
.canda {
	height:167px;
	width:100%;
}
section.video {
position: relative;
height: 800px;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
color: white;
}
.video.v-center, .video .v-center {
display: table;
width: 100%;
}
.video .hero-unit {
text-align: center;
padding: 0;
margin: 0;
background-color: transparent;
}


.video .container {
position: relative;
z-index: 2;
}

section * {
z-index: 2;
}

section {
font-weight:100;
}

section.video {
position:relative;
height:800px;
-webkit-background-size:cover;
-moz-background-size:cover;
-o-background-size:cover;
background-size:cover;
color:#FFF;
}


video {
left: 0px;
bottom: 0px;
right: 0px;
z-index: -100;
width: 1422px;
height: 799.875px;
}
.video #bgVideo {
overflow: hidden;
}

.video #bgVideo > video {
position: absolute !important;
top: 0 !important;
}
.video > div, .video .v-center > div {
display: table-cell;
vertical-align: middle;
margin-top: 0;
margin-bottom: 0;
float: none;
}

.background {
position: absolute;
left: 0;
top: 0;
right: 0;
bottom: 0;
background: 50% 50% no-repeat;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;

}
.vidoht {
	height: 586px !important; 
	background-position: 50% 58px;
}
</style>
</head>
<body>
<a href="#" class="crunchify-top">&uarr;��</a>
<div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="https://in.linkedin.com/in/croyez-immigration-823671124" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph">+91 9543690385</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
              <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="homePage.mm"><img src="images/demo-new-site/croyez2.png" width="250" class="img-responsive"></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
          <li class="active"><a href="homePage.mm">Home</a></li>
          <li><a href="about.mm">About Us</a></li>
          <li class="dropdown"><a href="canada.mm" class="dropdown-toggle" data-toggle="dropdown">Canada<b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="canada.mm">Canada</a></li>
              <li><a href="#Express">Express Entry</a></li>
              <li><a href="qubecskiled.mm">Quebec Skilled </a></li>
              <li><a href="familysponsorship.mm">Family Sponsorship </a></li>
              <li><a href="businessimmigration.mm">Business Immigration</a></li>
              <li><a href="provincialnominee.mm">Provincial Nominee </a></li>
            </ul>
          </li>
          <li><a href="australia.mm">Australia</a></li>
          <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Immigration <b class="caret"></b> </a>
            <ul class="dropdown-menu">
              <li><a href="#">Germany</a></li>
              <li><a href="newzealand.mm">New zealand</a></li>
              <li><a href="#">Hong Kong</a></li>
              <li><a href="#">Ecuador</a></li>
            </ul>
          </li>
          <li><a href="blog.mm">Blog</a></li>
          <li><a href="career.mm">Careers</a></li>
          <li><a href="contact.mm">Contact</a></li>
          <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
        </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
        </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="row">
        <div class="col-md-8 col-sm-6">
          <div class="row">
  <div class="row">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active"> <img src="images/bnrs/bg1.png" alt="Chania">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <div class="item"> <img src="images/bnrs/bg2.png" alt="Chania">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <div class="item"> <img src="images/bnrs/bg3.png" alt="Flower">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
        <div class="item"> <img src="images/bnrs/bg4.png" alt="Flower">
          <div class="carousel-caption">
            <h3></h3>
            <p></p>
          </div>
        </div>
      </div>
      
      <!-- Left and right controls --
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>--> </div>
  </div>
           </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="col-md-12 bxshdw">
            <div class="col-md-12">
              <h4 class="loginhh">Enquiry</h4>
            </div>
            <c:if test="${enq eq 'true' }">
            <span style="color:#0dbd0d; font-weight:700; padding-left: 15px;"> Submitted Enquiry Successfully</span>
            </c:if>
            <span > </span>
            <div class="col-md-12">
              <form action="enquiry.mm" method="post">
                <div class="form-group">
                  <input type="text" class="form-control" id="exampleInputEmail1" name="firstName" placeholder="Name">
                </div>
          <div class="form-group">
										<input type="email" name="email" class="form-control"
											placeholder="Email Address" id="email-id" required="required"
											onchange="emailCheck(this)">
										<div class="registrationFormAlert" id="divCheckPasswordMatch"
											style="color: red;"></div>
										<div class="registrationFormAlert" id="divCheckMatch"
											style="color: green;"></div>
									</div>
                <div class="form-group">
                  <input type="tel" class="form-control" required="required"
											minlength="10" maxlength="10" placeholder="Mobile Number"
											id="exampleInputEmail" name="phoneNumber">
											<input type ="hidden" name="hidden" value="homepage">
                </div>
          <div class="form-group">
            <select name="country" class="form-control">
           <option value="">SELECT PROFESSION</option>
								<option value="ACCOUNTANT">ACCOUNTANT</option>
								<option value="CIVIL ENGG">CIVIL ENGG</option>
								<option value="DENTIST">DENTIST</option>
								<option value="FRESHERS">FRESHERS</option>
								<option value="IT ENGG">IT ENGG</option>
								<option value="MECHANICAL ENGG">MECHANICAL ENGG</option>
								<option value="OTHERS">OTHERS</option>
								<option value="STUDENT">STUDENT</option>
										</select>
           
          </div>
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Enquiry">
                  
                </div>
                <!-- <div class="checkbox">
    <label>
      <input type="checkbox">Remember me
    </label>
  </div>-->
                <div class="col-md-12">
                  <button type="submit" class="sbmtbtn center-block">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid boutmargn" id"Express">
  <div class="container">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-8">
          <h3 class="abouthed">CROYEZ Immigration</h3>
          <h4 class="aboutpara">Migrate to a new country is a big undertaking and the immigration process can sometimes be a daunting task.CROYEZ immigration services private limited is one of the fastest growing immigration company in india, having its head office at chennai,tamilnadu.We evaluate client profile with legal advisers before we file the application. We offer assistance to corporate and individuals in the field of immigration  to various markets including Australia, Canada, New Zealand, Hong kong,Ecuador and Germany. We operate through a network of business associates who are qualified immigration lawyers/certified immigration attorneys thus enabling our clients an access to seamless immigration services all over the world.The combination of specialized knowledge, Superior technical capabilities, expertise and comprehensive support facilities, enables our firm to maintain its high success rate in the processing of all immigration cases.</h4>
        </div>
        <div class="col-md-4">
          <div class="row">
            <div class="col-md-12 mrggntp">
              <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators -
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>--> 
                  
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active"><a href="expressentry.mm"> <img src="images/city_of_love-wallpaper.jpg" alt="Chania" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3 >Express Entry</h3>
                      </div>
                      </a> </div>
                    <div class="item"><a href="qubecskiled.mm"> <img src="images/city_of_love-wallpaper.jpg" alt="Chania" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Quebec Skilled Worker</h3>
                      </div>
                      </a> </div>
                    <div class="item"><a href="familysponsorship.mm"> <img src="images/city_of_love-wallpaper.jpg" alt="Flower" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Family Sponsorship Immigration</h3>
                      </div>
                      </a> </div>
                    <div class="item"><a href="businessimmigration.mm"> <img src="images/city_of_love-wallpaper.jpg" alt="Flower" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Business Immigration</h3>
                      </div>
                      </a> </div>
                    <div class="item"><a href="provincialnominee.mm"> <img src="images/city_of_love-wallpaper.jpg" alt="Flower" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Provincial Nominee Program</h3>
                      </div>
                      </a> </div>
                  </div>
                  
                  <!-- Left and right controls --
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>--> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
<section class="video v-center vidoht">
                <div id="bgVideo" class="background" style="height: auto;"><video id="video_background" preload="auto" autoplay="autoplay" loop="loop" style="position: fixed; top: -203px; left: 0px; bottom: 0px; right: 0px; z-index: -100; width: 1422px; height: 799.875px;"><source src="http://cineagency.s3.amazonaws.com/test.mp4" type="video/mp4"><source src="http://cineagency.s3.amazonaws.com/test.webm" type="video/webm">bgvideo</video></div>
                <div>
	                
	                    <div class="hero-unit">
                        <div class="container">
          <h1 class="mnsrvc">Countries We Offer</h1>
  <div class="row">
  <div id="theArt">
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork">
  <img src="images/countrys/Canada Flag.jpg" class="img-responsive canda">
   
			<div class="detail">
				  <h3>Canada</h3>
				<p>Canada is one of the world's top immigration destinations with over 200,000 new arrivals coming under the Canada immigration system every year.</p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/austrila.png" class="img-responsive austl">
			<div class="detail">
				<h3>Australia</h3>
				<p>Australia is one among the top immigration destinations in the world.Australia is a culturally diverse, racially tolerant society that welcomes and offers opportunity to those who wish to settle here.</p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/newzalnd.png" class="img-responsive">
			<div class="detail">
				<h3>New zealand</h3>
				<p>Discover why New Zealand is not just a great place to work but how we Kiwis enjoy life outside of work, which we call work-life balance. </p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/germany.png" class="img-responsive">
			<div class="detail">
				<h3>Germany</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae harum non, repellat aut, voluptate distinctio perspiciatis excepturi totam culpa </p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/hong.png" class="img-responsive">
			<div class="detail">
				<h3>Hong Kong</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae harum non, repellat aut, voluptate distinctio perspiciatis excepturi totam culpa</p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/flg.png" class="img-responsive">
			<div class="detail">
				<h3>Ecuador</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae harum non, repellat aut, voluptate distinctio perspiciatis excepturi totam culpa </p>
			</div>
		</div>
	</div>
  </div>
  </div>
  </div>
  </div>
      </div>
      
     
	                </div>
	            </div>
</section></div>
<!-- <div class="container-fluid recentbg">
  <div class="container">
    <h3 class="mnsrvc">Countries We Offer </h3>
  </div>
  <div class="col-md-12">
  <div class="row">
  <div id="theArt">
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork">
  <img src="images/countrys/Canada Flag.jpg" class="img-responsive canda">
   
			<div class="detail">
				  <h3>Canada</h3>
				<p>Canada is one of the world's top immigration destinations with over 200,000 new arrivals coming under the Canada immigration system every year.</p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/austrila.png" class="img-responsive austl">
			<div class="detail">
				<h3>Australia</h3>
				<p>Australia is one among the top immigration destinations in the world.Australia is a culturally diverse, racially tolerant society that welcomes and offers opportunity to those who wish to settle here.</p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/newzalnd.png" class="img-responsive">
			<div class="detail">
				<h3>New zealand</h3>
				<p>Discover why New Zealand is not just a great place to work but how we Kiwis enjoy life outside of work, which we call work-life balance. </p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/germany.png" class="img-responsive">
			<div class="detail">
				<h3>Germany</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae harum non, repellat aut, voluptate distinctio perspiciatis excepturi totam culpa </p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/hong.png" class="img-responsive">
			<div class="detail">
				<h3>Hong Kong</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae harum non, repellat aut, voluptate distinctio perspiciatis excepturi totam culpa</p>
			</div>
		</div>
	</div>
  </div>
  </div>
  <div class="col-md-4">
  <div class="col-md-12">
  <div class="artGroup slide">
		<div class="artwork"> <img src="images/countrys/flg.png" class="img-responsive">
			<div class="detail">
				<h3>Ecuador</h3>
				<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod, recusandae harum non, repellat aut, voluptate distinctio perspiciatis excepturi totam culpa </p>
			</div>
		</div>
	</div>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="col-md-12">
  <div class="row">
  <div class="col-md-9 no-padding">
  <div class="col-md-12">
  <div class="row">
  <div class="col-md-4 no-padding">
  <div class="col-md-12 covr">
  <h4 class="cntsofer">New zealand</h4>
        <img src="images/countrys/newzalnd.png" class="img-responsive"></div>
        </div>
  <div class="col-md-4 no-padding">
  <div class="col-md-12 covr">
  <h4 class="cntsofer ">Hong Kong</h4>
        <img src="images/countrys/hong.png" class="img-responsive"></div>
        </div>
  <div class="col-md-4 no-padding"> 
  <div class="col-md-12 covr">
  <h4 class="cntsofer">Ecuador</h4>
        <img src="images/countrys/flg.png" class="img-responsive"></div>
        </div>
  </div>
  </div>
  <div class="col-md-12">
  <div class="row">
  <div class="col-md-8 no-padding">
  <div class="col-md-12 covr">
     <h4 class="cntsofer">Canada</h4>
  <img src="images/countrys/Canada Flag.jpg" class="img-responsive canda">
     
        </div>
        </div>
  <div class="col-md-4  no-padding">
  <div class="col-md-12 covr">
   <h4 class="cntsofer">Germany</h4>
        <img src="images/images/home-2_04.png" class="img-responsive"> </div>
        </div>
  </div>
  </div>
  </div>
  <div class="col-md-3 no-padding cntr">
  <div class="col-md-12 covr">
   <h4 class="cntsofer">Australia</h4>
        <img src="images/countrys/austrila.png" class="img-responsive austl">
  </div>
  </div>
  </div>
  </div>
  -<div class="col-md-12">
    <div class="row">
      <div class="col-md-2 no-padding"> <img src="images/countrys/canada.png" class="img-responsive img1">
        <h4 class="cntsofer">Canada</h4>
      </div>
      <div class="col-md-2"> </div>
      <div class="col-md-2 no-padding">
        <h4 class="cntsofer">New zealand</h4>
        <img src="images/countrys/newzalnd.png" class="img-responsive img1"> </div>
      <div class="col-md-2"> </div>
      <div class="col-md-2 no-padding">
        <h4 class="cntsofer ">Hong Kong</h4>
        <img src="images/countrys/hong.png" class="img-responsive img1"> </div>
      <div class="col-md-2"> </div>
    </div>
    <div class="row">
      <div class="col-md-2"> </div>
      <div class="col-md-2 no-padding">
        <h4 class="cntsofer">Australia</h4>
        <img src="images/countrys/aust.png" class="img-responsive img1"> </div>
      <div class="col-md-2"> </div>
      <div class="col-md-2 no-padding">
        <h4 class="cntsofer">Germany</h4>
        <img src="images/images/home-2_04.png" class="img-responsive img1"> </div>
      <div class="col-md-2"> </div>
      <div class="col-md-2 no-padding">
        <h4 class="cntsofer">Ecuador</h4>
        <img src="images/countrys/flg.png" class="img-responsive img1"> </div>
    </div>
  </div>
  <div class="row">
    <div class="wrap">
  	
		<div class="metro-big photo">
      <img src="http://lorempixel.com/200/95/animals" class="img-delay-1" />
			<span class="label bottom">Photos</span>
		</div>
        <div class="metro-big chat">
			<div class="icon-chat"></div>
			<span class="label bottom">Mail</span>
		</div>
		
		<div class="metro-big music">
			<div class="icon-music"></div>
			<span class="label bottom">Musics</span>
		</div>
        <div class="metro-big ie">
			<div class="icon-ie"></div>
			<span class="label bottom">Internet Explorer</span>
		</div>
		<div class="metro-big photo">
      <img src="http://lorempixel.com/200/95/animals" class="img-delay-1" />
			<span class="label bottom">Photos</span>
		</div>
		<div class="metro-big video">
			<div class="icon-video"></div>
			<span class="label bottom">Videos</span>
		</div>
		<div class="metro-big music">
			<div class="icon-music"></div>
			<span class="label bottom">Musics</span>
		</div>
		<div class="metro-big photo">
      <img src="http://lorempixel.com/200/95/animals" class="img-delay-1" />
			<span class="label bottom">Photos</span>
		</div>
		<div class="metro-big music">
			<div class="icon-music"></div>
			<span class="label bottom">Musics</span>
		</div>
		<div class="metro-big ps">
			<div class="icon-ps"></div>
			<span class="label bottom">Adobe Photoshop</span>
		</div>
	</div>
    
  </div> 
</div> -->
<div class="container-fluid">
  <div class="container">
    <div class="col-md-12 col-sm-12">
      <h3 class="mnsrvc">Services</h3>
    </div>
    <div class="col-md-12 col-sm-12">
      <p class="mnssrvcp">At croyez,we serve clients interested in filing Student visa,Temporary Resident visa,Business visa,Investor visa,family sponsorship visa to many of your dream destination including Canada, Australia,New Zealand,Hong Kong,Germany,Ecuador etc., We have associate with the legal advisers for all countries We have a expert team of CRCIC,ICCRC ,Who will do evaluate clients profile initially before filing application in the visa department.We provide professional guidance not only for immigration application filing but also gives assistance in post landing services and job search </p>
    </div>
    <div class="col-md-12 srvcbbrdr nonepdng"> 
      <!--section 1-->
      <div class="block animatable bounceInLeft">
        <div class="col-md-3 col-sm-6 srvcbrdr">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/register.png"  width="68px" height='68px'  class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">Legal registration </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp">We do services with the legal advisors</p>
            </div>
          </div>
        </div>
      </div>
      <!--section 1--> 
      <!--section 2-->
      <div class="block animatable moveUp">
        <div class="col-md-3 col-sm-6 srvcbrdr">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/job.jpg"  width="68px" height='68px'  class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">Job assistance </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp">Will give 100 % job assistance</p>
            </div>
          </div>
          <!--<div class="row">
        <div class="col-md-12 nonepdng"> <img src="images/icon/srvc-3.png" class="center-block srvcimgmrgn rote"> </div>
        <div class="col-md-12 nonepdng">
            <h4 class="msrvch">Job assistance </h4>
            
          </div>
        <div class="col-md-12 nonepdng">
         <p class="mnssrvcp">Will give 100 % job assistance</p>
          </div>
      </div>--></div>
      </div>
      <!--section 2-->
      <div class="block animatable moveUp">
        <div class="col-md-3 col-sm-6">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/message.png"  width="68px" height='68px'  class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">Post landing assistance </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp">We do give post landing assistance with the local vendors</p>
            </div>
          </div>
        </div>
      </div>
      <!--section 3-->
      <div class="block animatable bounceInRight">
        <div class="col-md-3 col-sm-6">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/clear.png"  width="68px" height='68px'  class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">Clear evaluation </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp"> We evaluate clients profile before start process with legal representatives</p>
            </div>
          </div>
        </div>
      </div>
      <!--section 3--> 
    </div>
    <div class="col-md-12 srvcbbrdr nonepdng"> 
      <!--section 1-->
      <div class="block animatable bounceInLeft">
        <div class="col-md-3 col-sm-6 srvcbrdr">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/world.png" width="68px" height='68px' class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch"  id="show_load">World class web platform </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp">With the help of our CRM .anyone can do process from anywhere</p>
            </div>
          </div>
        </div>
      </div>
      <!--section 1--> 
      <!--section 2-->
      <div class="block animatable moveUp">
        <div class="col-md-3 col-sm-6 srvcbrdr">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/refund.png"  width="68px" height='68px' class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">100% money refund </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp"> Registration fee will be refundable if client profile not meet the eligible criteria</p>
            </div>
          </div>
        </div>
      </div>
      <!--section 2--> 
      <!--section 3-->
      <div class="block animatable moveUp">
        <div class="col-md-3 col-sm-6">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/help.png" width="68px" height='68px' class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">Legal help </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp">At every step client will get legal advice and representation from legal advisors</p>
            </div>
          </div>
        </div>
      </div>
      <div class="block animatable bounceInRight">
        <div class="col-md-3 col-sm-6">
          <div class="row">
            <div class="col-md-12 nonepdng"> <img src="images/icon/document.png" width="68px" height='68px'class="center-block srvcimgmrgn rote"> </div>
            <div class="col-md-12 nonepdng">
              <h4 class="msrvch">Documentation </h4>
            </div>
            <div class="col-md-12 nonepdng">
              <p class="mnssrvcp">Our processing department will give you assistance in collecting submitting and verifying documents</p>
            </div>
          </div>
        </div>
      </div>
      <!--section 3--> 
    </div>
  </div>
</div>
<div class="container-fluid aplybg" >
  <div class="container nonepdng">
    <div class="col-md-12">
      <h3 class="rcrnrthead">Apply Now</h3>
    </div>
    <div class="col-md-12">
      <p class="aplyp">Sign up for daily email affirmations by entering your information below</p>
      
       <c:if test="${enq1 eq 'true' }">
            <span><p class="aplyp" style="color:#0dbd0d; font-weight:700; align:center;">Successfully Submitted</p></span>
            </c:if>
    </div>
    
      <form  action="onEnquiry.mm" method="post">
        <div class="col-md-3 col-sm-1"> </div>
        <div class="col-md-3 col-sm-5">
          <div class="form-group">
            <input type="text" name="fName" class="form-control"
								required="required" id="exampleInputEmail1"
								placeholder="First Name">
          </div>
          <div class="form-group">
            <input type="tel" name="mobile" class="form-control"
								required="required" maxlength="10" minlength="10" id="mobile"
								placeholder="Mobile Number">
          </div>
        </div>
        <div class="col-md-3 col-sm-5">
          <div class="form-group">
            <input type="text" name="lName" class="form-control"
								id="exampleInputPassword1" placeholder="Last Name">
          </div>
                    <div class="form-group">
							<input type="email" name="emailId" class="form-control"
								id="email-id1" required="required" onchange="emailCheck1(this)"
								placeholder="Email Address">
							<div class="registrationFormAlert" id="divCheckPasswordMatch1"
								style="color: red;"></div>
							<div class="registrationFormAlert" id="divCheckMatch"
								style="color: green;"></div>
						</div>
        </div>
        <div class="col-md-3 col-sm-1"> </div>
        <div class="col-md-12 col-sm-12">
          <button type ="submit" id="show_success" class="subbtn center-block">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!--<div class="container-fluid">
    <div class="container">
    <div class="col-md-12 col-sm-12">
        <h3 class="mnsrvc">Working Feature</h3>
      </div>
    <div class="col-md-12 col-sm-12">
        <p class="wrkp">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>
        Ut enim ad minim veniam, exercitationullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in </p>
      </div>
    <div class="col-md-12 col-sm-12 col-xs-12 btmcol-md">
        <div class="col-md-1"></div>
        <div class="col-md-10 col-xs-12 nonepdng"> 
        <!---------step 1---------
        <div class="col-md-4 col-sm-4 col-xs-12 nonepdng">
            <div class="col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                <div class="row"> <img src="images/demo-new-site/technology-wallpaper-1920x1080.jpg" class="img-responsive center-block"> </div>
              </div>
                <div class="col-md-12 col-xs-12 stepbrdr nonepdng">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                    <h4 class="steprvch">Eligibility</h4>
                  </div>
                  </div>
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                    <p class="stepsrvcp">Lorem ipsum dolor sit amet consectetur 
                        adipisicing elit sed don eiusmod tempor enim
                        minim veniam quis notrud exercitation</p>
                  </div>
                  </div>
              </div>
              </div>
          </div>
          </div>
        <!--------step 2---------
        <div class="col-md-4 col-sm-4 col-xs-12 nonepdng">
            <div class="col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                <div class="row"> <img src="images/demo-new-site/technology-wallpaper-1920x1080.jpg" class="img-responsive center-block"> </div>
              </div>
                <div class="col-md-12 col-xs-12 stepbrdr nonepdng">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                    <h4 class="steprvch">Documentation</h4>
                  </div>
                  </div>
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                    <p class="stepsrvcp">Lorem ipsum dolor sit amet consectetur 
                        adipisicing elit sed don eiusmod tempor enim
                        minim veniam quis notrud exercitation</p>
                  </div>
                  </div>
              </div>
              </div>
          </div>
          </div>
        <!--------step 3-----------------
        <div class="col-md-4 col-sm-4 col-xs-12 nonepdng">
            <div class="col-md-12 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                <div class="row"> <img src="images/demo-new-site/fullscreen-slider.jpg" class="img-responsive center-block"> </div>
              </div>
                <div class="col-md-12 col-xs-12 stepbrdr nonepdng">
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                    <h4 class="steprvch">Submission</h4>
                  </div>
                  </div>
                <div class="col-md-12 col-xs-12">
                    <div class="row">
                    <p class="stepsrvcp">Lorem ipsum dolor sit amet consectetur 
                        adipisicing elit sed don eiusmod tempor enim
                        minim veniam quis notrud exercitation</p>
                  </div>
                  </div>
              </div>
              </div>
          </div>
          </div>
        <!-----step- 3----
      </div>
        <div class="col-md-1"></div>
      </div>
  </div>
  </div>-->

<div class="container-fluid extrsrvc">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                  <!-- Indicators -
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>--> 
                  
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                    <div class="item active"> <img src="images/bnrs/fotrbnr1.png" alt="Chania" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Settle In Canada</h3>
                        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                      </div>
                    </div>
                    <div class="item"> <img src="images/bnrs/fotrbnr2.png" alt="Chania" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Work</h3>
                        <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                      </div>
                    </div>
                    <div class="item"> <img src="images/bnrs/fotrbnr3.png" alt="Flower" width="100%" height="100%">
                      <div class="carousel-caption">
                        <h3>Study</h3>
                        <p>Beatiful flowers in Kolymbari, Crete.</p>
                      </div>
                    </div>
                  </div>
                  
                  <!-- Left and right controls --
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>--> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item" width="640" height="480" src="video/Protect Your Canadian Dream.mp4" frameborder="0" allowfullscreen></iframe>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/croyez.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">To Know more about Croyez and get updates about immigration services kindly follow us on social websites.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('face','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="face"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('gplus','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="gplus"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://in.linkedin.com/in/croyez-immigration-823671124
" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('skyp','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="skyp"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('twter','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="twter"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> 2floor ,sons complex,near sangamam hotel,Ashok Nagar, chennai - 600083</p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">+91 9543690385</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">info@croyezimmigration.com</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.mm" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.mm" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.mm" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Testimonial </h4>
            </div>
            <div class="row">
              <div class="col-md-12 column">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel"> 
                  <!-- Bottom Carousel Indicators --> 
                  
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner text-center"> 
                    
                    <!-- Quote 1 -->
                    <div class="item active">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 2 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 3 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--1--> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">&#169; 2016 Croyez. All rights reserved</h3>
</div>
<script src="js/index.js"></script> 
<script src="js/index2.js"></script> 
<script>
///////////test the success meaaasage///////
$(document).ready(function() {
  $('#show_success').click(function(){
    $('#show_message').show();
  });
});
</script>
<script>
$(document).ready(function() {
  $('#media').carousel({
    pause: true,
    interval: false,
  });
});
</script> 
 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script>
   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 70;
			 if ($(window).scrollTop() > navHeight) {
				 $('nav').addClass('fixed');
			 }
			 else {
				 $('nav').removeClass('fixed');
			 }
		});
	});
</script> 
<script>
$(function(){
    $('.dropdown').hover(function() {
        $(this).addClass('open');
    },
    function() {
        $(this).removeClass('open');
    });
});
</script> 
<script>
		$(function () {
		if ( $('html').hasClass('csstransforms3d') ) {
			$('.artGroup').removeClass('slide').addClass('flip');
			$('.artGroup.flip').on('mouseenter',
				function () {
					$(this).find('.artwork').addClass('theFlip');
				});
			$('.artGroup.flip').on('mouseleave',
				function () {
					$(this).find('.artwork').removeClass('theFlip');
				});
		} else {
			$('.artGroup').on('mouseenter',
				function () {
					$(this).find('.detail').stop().animate({bottom:0}, 500, 'easeOutCubic');
				});
			$('.artGroup').on('mouseleave',
				function () {
					$(this).find('.detail').stop().animate({bottom: ($(this).height() + -1) }, 500, 'easeOutCubic');
				});
		}
	});
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>