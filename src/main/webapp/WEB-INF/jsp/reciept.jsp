<script src="js/addreceipt.js"></script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
  <!-- main content start-->
  <body  class="cbp-spmenu-push">
  <div id="page-wrapper">
    <div class="main-page general">
    <form>
     <input type="hidden" id="txtPageSizeH" value="5">
      <div class="panel-info rothdr"> Accounts / Reciept </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2"> Reciept </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="#" class="btn btn-warning crtbtn" data-tooltip="Create" data-toggle="modal" data-target="#reciept"> <span class="glyphicon glyphicon-plus"></span> </a> </div>
                  <div class="col-md-2 col-sm-1 col-xs-3">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
											<li><a href="createReceiptSheet.mm">XLS</a></li>
											<!--      <li><a href="#">WORD</a></li> -->
										</ul>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="reciept.mm" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3 col-sm-1 col-xs-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">3</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li class="active" ><a href="#" onclick="pagination(10)">10</a></li>
                        <li><a href="#" onclick="pagination(25)">25</a></li>
                        <li><a href="#" onclick="pagination(50)">50</a></li>
                        <li><a href="#" onclick="pagination(100)">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">Id</th>
                  <th class="tblthd">Client</th>
                  <th class="tblthd">Date</th>
                  <th class="tblthd">Amount</th>
                  <th class="tblthd">Mode</th>
                  <th class="tblthd">TR.ID</th>
                  <th class="tblthd">Description</th>
                  <th class="tblthd">Coordinators</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                 
                  <td>
                  <input type="text"  onkeyup="SearchReceipt(this,'id')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchReceipt(this,'email')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchReceipt(this,'date')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchReceipt(this,'amount')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchReceipt(this,'paymentMode')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchReceipt(this,'transactionNo')" class="form-control">
                  </td>
                   <td>
                  <input type="text" onkeyup="SearchReceipt(this,'description')" class="form-control">
                  </td>
                  <td>
                 <select class="form-control">
                 <option>Select Coordinators</option>
												<c:forEach var="coordinate" items="${coordinators}">
												<option value="${coordinate.e_id}">${coordinate.emp_firstname}</option>
												</c:forEach>
                 </select>
                  </td>				
                  <td></td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <c:forEach var="reciepter" items="${reciept}">
                <tr>
                  <td>${reciepter.id}</td>
                  <td>${reciepter.email}</td>
                  <td>${reciepter.date}</td>
                  <td>${reciepter.amount}</td>
                  <td>${reciepter.paymentMode}</td>
                  <td>${reciepter.transactionNo}</td>
                  <td>${reciepter.description}</td>
                  <td></td>
                  <td>
                  <ul class="actnul">
                  <li><a href="#" data-tooltip="Go to Approval" class="actn" onclick="approvalRec(${reciepter.id})" ><span class="glyphicon glyphicon-ok edtbtn"></span></a></li>
                  </ul>
                  </td>
                </tr>
                 </c:forEach>
                           </tbody>
            </table>
            <div class="col-md-12" id="result">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  <!--footer-->
  <div class="footer">
    <p>&copy; 2016 Admin Panel. All Rights Reserved | Design by <a href="https://maestroinfotech.in/" target="_blank">Maestro Infotech System</a></p>
  </div>
  <!--//footer--> 
</div>
<input type="hidden" id="noofrec" value="" />
<!-- Modal -->
<div id="reciept" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="form-horizontal" role="form" action="createreciept.mm" method="post">
    <div class="modal-content">
      <div class="modal-header mbt">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mdlhdr">Create Reciept</h4>
         <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Client</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="email" value=""  onchange="isEmail(this)" placeholder="Enter Email"  >
      <span id="isEmail" style="color:red"></span>
      </div>
    </div>
      </div>
      <div class="modal-body">
      
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Assessment Program</label>
      <div class="col-sm-8">
        <select name="assProgram" class="form-control">
        <option></option>
        <option></option>
        <option></option>
        </select>
      </div>
    </div>
    <div class="form-group">
     <label class="control-label col-sm-3 lblfnt">Date</label>
      <div class="col-sm-3">
        <input type="text"  class="form-control datepicker" name="date" name="" value="">
      </div>
      <label class="control-label col-sm-2 lblfnt">Amount</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="amount" value=""  onchange="isAmount(this)" placeholder="Enter Amount" >
        <span id="isAmount" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Payment Mode</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="paymentMode" value=""  onchange="isPayment(this)" placeholder="Enter PaymentMode"  >
        <span id="isPayment" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Transaction No</label>
    <div class="col-md-8">
    <input type="text" class="form-control" name="transactionNo" value="" onchange="isTransaction(this)" placeholder="Enter TransactionNo"  >
    <span id="isTransaction" style="color:red"></span>
    </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Description</label>
    <div class="col-md-8">
    <textarea class="form-control" name="description" value=""    onchange="isPayment1(this)" placeholder="Enter Description"  ></textarea>
    <span id="isPayment1" style="color:red"></span>
    </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Promo Code</label>
      <div class="col-sm-8">
        <select name="promoCode" class="form-control">
        <option></option>
        <option></option>
        <option></option>
        </select>
      </div>
    </div>
 
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
 </form>
  </div>
</div>

<!---------Edit--------->
<div id="editreciept" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="form-horizontal" role="form">
    <div class="modal-content">
      <div class="modal-header mbt">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mdlhdr">Edit Reciept</h4>
         <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Client</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name=""  value="" >
      </div>
    </div>
      </div>
      <div class="modal-body">
      
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Assessment Program</label>
      <div class="col-sm-8">
        <select class="form-control">
        <option></option>
        <option></option>
        <option></option>
        </select>
      </div>
    </div>
    <div class="form-group">
     <label class="control-label col-sm-3 lblfnt">Date</label>
      <div class="col-sm-3">
        <input type="text" class="form-control datepicker" name="" value="">
      </div>
      <label class="control-label col-sm-2 lblfnt">Amount</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Payment Mode</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
    <div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Transaction No</label>
    <div class="col-md-8">
    <input type="text" class="form-control" name="" value="">
    </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-3 lblfnt" >Description</label>
    <div class="col-md-8">
    <textarea class="form-control" name="" value=""></textarea>
    </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Promo Code</label>
      <div class="col-sm-8">
        <select class="form-control">
        <option></option>
        <option></option>
        <option></option>
        </select>
      </div>
    </div>
 
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
 </form>
  </div>
</div>

<!-- Classie --> 
<script src="js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
 <script>
 
 $.fn.pageMe = function(opts){
	    var $this = this,
	        defaults = {
	            perPage: 7,
	            showPrevNext: false,
	            hidePageNumbers: false
	        },
	        settings = $.extend(defaults, opts);
	    
	    var listElement = $this;
	    var perPage = settings.perPage; 
	    var children = listElement.children();
	    var pager = $('.pager');
		
	    if (typeof settings.childSelector!="undefined") {
	        children = listElement.find(settings.childSelector);
	    }
	    
	    if (typeof settings.pagerSelector!="undefined") {
	        pager = $(settings.pagerSelector);
	    }
	    var noofrec = document.getElementById("noofrec").value;
	    var numItems = 0;
	    if(noofrec == null || noofrec == "")
	    numItems = children.size();
	    else
	    	numItems = noofrec;
	    var numPages = Math.ceil(numItems/perPage);

	    pager.data("curr",0);
	    
	    if (settings.showPrevNext){
	        $('<li><a href="#" class="prev_link">�</a></li>').appendTo(pager);
	    }
	    
	    var curr = 0;
	    while(numPages > curr && (settings.hidePageNumbers==false)){
	        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
	        curr++;
	    }
	    
	    if (settings.showPrevNext){
	        $('<li><a href="#" class="next_link">�</a></li>').appendTo(pager);
	    }
	    
	    pager.find('.page_link:first').addClass('active');
	    pager.find('.prev_link').hide();
	    if (numPages<=1) {
	        pager.find('.next_link').hide();
	    }
	  	pager.children().eq(1).addClass("active");
	    
	    children.hide();
	    children.slice(0, perPage).show();
	    
	    pager.find('li .page_link').click(function(){
	        var clickedPage = $(this).html().valueOf()-1;
	        goTo(clickedPage,perPage);
	        return false;
	    });
	    pager.find('li .prev_link').click(function(){
	        previous();
	        return false;
	    });
	    pager.find('li .next_link').click(function(){
	        next();
	        return false;
	    });
	 
	    
	    	     function previous(){
	        var goToPage = parseInt(pager.data("curr")) - 1;
	        goTo(goToPage);
	    }
	     
	    function next(){
	        goToPage = parseInt(pager.data("curr")) + 1;
	        goTo(goToPage);
	    }
	    
	    function goTo(page){
	        var startAt = page * perPage,
	            endOn = startAt + perPage;
	        
	        children.css('display','none').slice(startAt, endOn).show();
	        
	        if (page>=1) {
	            pager.find('.prev_link').show();
	        }
	        else {
	            pager.find('.prev_link').hide();
	        }
	        
	        if (page<(numPages-1)) {
	            pager.find('.next_link').show();
	        }
	        else {
	            pager.find('.next_link').hide();
	        }
	        
	        pager.data("curr",page);
	      	pager.children().removeClass("active");
	        pager.children().eq(page+1).addClass("active");
	    
	    }
	};

	$(document).ready(function(){
		<!--var pageSize=document.getElementById("txtPageSizeH").value;-->
	  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
	    
	});
	 </script>

<!--scrolling js--> 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.js"> </script>
