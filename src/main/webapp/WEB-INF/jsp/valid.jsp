<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript">
	function resetPassword(form) {
		if (form.password.value != ""
				&& form.password.value == form.confirmPassword.value) {
			return true;
		}
		document.getElementById("pswd").value = "";
		document.getElementById("cnfpswd").value = "";

		document.getElementById("divCheckPasswordMatch").innerHTML = "Password doesn't match";

		return false;

	}
</script>

</head>
<body>
	<form action="valid1.mm" method="post" name="loginForm" id="login" onsubmit="return resetPassword(this)">
		Enter new password:<input name="password" id="pswd" tabindex="1" type="password"placeholder="Password" minlength="6" required="required" maxlength="15" required="required" />
			<br><br> Confirm Password:<input name="confirmPassword" id="cnfpswd" minlength="6" tabindex="2" type="password"
			placeholder="Confirm Password" required="required" 	maxlength="15" required="required" /> 
			<input type="hidden" value="${email}" name="email" id="email">
			 <input	type="hidden" value="${token}" name="token" id="token">
		<div class="registrationFormAlert" id="divCheckPasswordMatch"
			style="color: red;"></div>
		<input type="hidden" name="check" value="check">
		 <input	type="submit" class="btn btn-default" value="Submit">
	</form>



	

</body>
</html>