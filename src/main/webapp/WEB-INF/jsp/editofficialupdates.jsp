<!DOCTYPE HTML>
<html>
<head>
<title>Edit Official Updates</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="shinrai" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/responsive.css">
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="ckeditor/ckeditor.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
 <link href='https://fonts.googleapis.com/css?family=Open+Sans|Noto+Sans' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
<script>
		 new WOW().init();
	</script>
  
<!--//end-animate-->
<link href="css/datepicker.css" type="text/css" rel="stylesheet">
<script src="js/datepicker.js" type="text/javascript"></script>
<script>
	 $(function() {
    $(".datepicker").datepicker();
});
</script>
<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
.panel-info.widget-shadow {
	padding: 0.7em 0.1em;
}
ul.dropdown-menu {
    padding: 0;
    min-width: 148px;
    top: 95%;
}
.charts, .row {
    margin:inherit !important;
}
.general h4.title2 {
    font-size: 17px;
    margin: 3px;
    color: #777777;
    padding-left: 10px;
    font-family: 'Open Sans', sans-serif;
}
.btn-primary {
    color: #fff;
    background-color: #00BCD4;
    border-color: #00BCD4 !important;
}
</style>
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  <div class=" sidebar" role="navigation">
    <div class="navbar-collapse">
      <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <ul class="nav" id="side-menu">
          <li> <a href="index.html" ><i class="fa fa-home nav_icon"></i>Dashboard</a> </li>
          <li> <a href="#"><i class="fa fa-thumbs-up nav_icon"></i>Pre-Sales<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="#">Opportunities/Leads<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li  class="sidbr"> <a class="lisd" href="opportunities.html">Opportunities</a> </li>
                  <li class="sidbr"> <a class="lisd" href="enquiries.html">Enquiries</a> </li>
                  <li class="sidbr"> <a class="lisd" href="refferals.html">Refferals</a> </li>
                </ul>
              </li>
            </ul>
            <!-- /nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-users nav_icon"></i>Clients<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="addclient.html">Add Client</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clients.html">Clients</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clientfileupload.html">File Uploads</a> </li>
                  <li class="sidbr"> <a class="lisd" href="agreementupload.html">Agreement Upload</a> </li>
                  <li class="sidbr"> <a class="lisd" href="documentapproval.html">Document Approval</a></li>
                  <li class="sidbr"> <a class="lisd" href="agreementapproval.html">Agreement Approval</a></li>
                  <li class="sidbr"> <a class="lisd" href="invoice.html">Initial Agreement</a></li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-inr nav_icon"></i>Accounts<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="reciept.html">Reciepts</a> </li>
                  <li class="sidbr"> <a class="lisd" href="installment.html">Installment Breakup</a> </li>
                  <li class="sidbr"> <a class="lisd" href="accountinvoice.html">Invoice</a> </li>
                  <li class="sidbr"> <a class="lisd" href="miscfee.html">Misc Fee</a></li>
                  <li class="sidbr"> <a class="lisd" href="refunds.html">Refunds</a></li>
                  <li class="sidbr"> <a class="lisd" href="onlinepayments.html">Online Payments</a></li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-filter nav_icon"></i>Assessment<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="#">Express Assessment</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Assessment Point</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Assessment Contents</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Pending Assessment</a></li>
                  <li class="sidbr"> <a class="lisd" href="#">Send Assessment</a></li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-envelope nav_icon"></i>Communication<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Mailer<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Email Sync</a> </li>
                      <li class="sidbr"> <a class="lisd" href="mail.html">Simple Mail</a> </li>
                      <li class="sidbr"> <a class="lisd" href="bulkmail.html">Bulk Mail</a> </li>
                    </ul>
              </li>
              <li> <a href="#">Messenger<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Simple SMS</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Bulk SMS</a> </li>
                    </ul>
              </li>
              <li> <a href="courier.html">Courier</a></li>
              <li> <a href="newsupdates.html" >News Updates</a> </li>
                </ul>
            <!-- /nav-second-level --> 
          </li>
          <li  class="active" > <a href="#"><i class="fa fa-question nav_icon"></i>Help/Updates<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li class="sidbr"> <a class="lisd" href="supporttickets.html"t>Support Tickets</a> </li>
              <li class="active sidbr"> <a class="lisd" href="officialupdates.html">Official Updates</a> </li>
              <li class="sidbr"> <a class="lisd" href="faq.html">FAQ</a> </li>
              <li class="sidbr"> <a class="lisd" href="blog.html">Blog</a> </li>
            </ul>
            <!-- //nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-bar-chart nav_icon"></i>Report<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="#">Task Report</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Express Report</a> </li>
                </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-cogs nav_icon"></i>System<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Administration<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="role.html">Role</a> </li>
                      <li class="sidbr"> <a class="lisd" href="userdetails.html">User Details</a> </li>
                      <li class="sidbr"> <a class="lisd" href="accesscontrol.html">Access Control</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">Comm. Settings<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">SMTP Config</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Mailing List</a> </li>
                     <li class="sidbr"> <a class="lisd" href="#">Email Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Back.Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Post Agg. Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">SMS Config</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">General Masters<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Country Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Branch</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Module</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Doc Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Support Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Update Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clientcategory.html">Client Category</a> </li>
                  <li class="sidbr"> <a class="lisd" href="assignto.html">Designation</a></li>
                  <li class="sidbr"> <a class="lisd" href="#">Agreement Template</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Application Forms</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Client Status</a> </li>
                  <li class="sidbr"> <a class="lisd" href="sources.html">Source Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Document Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Document Mapping</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">Account Master<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="program.html">Program</a> </li>
                      <li class="sidbr"> <a class="lisd" href="feemaster.html">Fee Master</a> </li>
                    </ul>
              </li>
                </ul>
            <!-- /nav-second-level --> 
          </li>
        </ul>
        <!-- //sidebar-collapse --> 
      </nav>
    </div>
  </div>
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
      <button id="showLeftPush"><i class="fa fa-bars"></i></button>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"> <a href="index.html">
       <img src="images/logo-4.png" class="img-responsive center-block">
       </a> </div>
      <!--//logo-->
      
      <div class="clearfix"> </div>
    </div>
    <div class="header-right"> 
      <!--search-box-->
      <div class="search-box"> 
        <!--<form class="input">
						<input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
						<label class="input__label" for="input-31">
							<svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
								<path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
							</svg>
						</label>
					</form>--->
        <form action="" class="search-form">
          <div class="form-group has-feedback">
            <label for="search" class="sr-only">Search</label>
            <input type="text" class="form-control" name="search" id="search" placeholder="Search">
            <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
        </form>
      </div>
      <!--//end-search-box-->
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 3 new messages</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="user_img"><img src="images/1.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li class="odd"><a href="#">
                <div class="user_img"><img src="images/2.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li><a href="#">
                <div class="user_img"><img src="images/3.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
          </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-filter"></i><span class="badge">3</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 3 new messages</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="user_img"><img src="images/1.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li class="odd"><a href="#">
                <div class="user_img"><img src="images/2.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li><a href="#">
                <div class="user_img"><img src="images/3.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
          </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 3 new notification</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="user_img"><img src="images/2.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li class="odd"><a href="#">
                <div class="user_img"><img src="images/1.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li><a href="#">
                <div class="user_img"><img src="images/3.png" alt=""></div>
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet </p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all notifications</a> </div>
              </li>
            </ul>
          </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">15</span></a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>You have 8 pending task</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Database update</span><span class="percentage">40%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar yellow" style="width:40%;"></div>
                </div>
                </a></li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar green" style="width:90%;"></div>
                </div>
                </a></li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar red" style="width: 33%;"></div>
                </div>
                </a></li>
              <li><a href="#">
                <div class="task-info"> <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                  <div class="clearfix"></div>
                </div>
                <div class="progress progress-striped active">
                  <div class="bar  blue" style="width: 80%;"></div>
                </div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all pending tasks</a> </div>
              </li>
            </ul>
          </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
      <!--notification menu end -->
      <div class="profile_details">
        <ul>
          <li class="dropdown profile_details_drop"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <div class="profile_img"> <span class="prfil-img"><img src="images/a.png" alt=""> </span>
              <div class="user-name">
                <p>Wikolia</p>
                <span>Administrator</span> </div>
              <i class="fa fa-angle-down lnr"></i> <i class="fa fa-angle-up lnr"></i>
              <div class="clearfix"></div>
            </div>
            </a>
            <ul class="dropdown-menu drp-mnu">
              <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
              <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
              <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
            </ul>
          </li>
        </ul>
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
  <!-- //header-ends --> 
  <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
      <div class="panel-info rothdr"> Help/Updates / Official Updates </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2">Edit Official Updates </h4>
        <div class="bdr"></div>
        <form class="form-horizontal" role="form">
    <div class="form-group">
      <label class="control-label col-sm-2 lblfnt">Client ID</label>
      <div class="col-sm-6">
        <select class="form-control">
        <option></option>
        <option></option>
        <option></option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 lblfnt">Title</label>
      <div class="col-sm-6">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-2 lblfnt">Content</label>
    <div class="col-md-9">
   <textarea class="ckeditor" name="description2" id="description2Edit" required> </textarea>
    </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 lblfnt">Visible to Client</label>
      <div class="col-sm-6">
        <input type="checkbox" data-toggle="toggle" data-on="On" data-off="Off">
      </div>
    </div>
    <div class="form-group">
    <div class="col-md-12">  
    <div class="col-md-2 col-sm-2 col-xs-5">
      <a href="officialupdates.html" class="btn btn-default bcktlst">Back To List</a>
      </div>      
      <div class="col-sm-1 col-sm-1 col-xs-1">
        <button type="submit" class="btn btn-warning">Submit</button>
      </div>
      </div>
    </div>
  </form>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>
  <!--footer-->
  <div class="footer">
    <p>&copy; 2016 Admin Panel. All Rights Reserved | Design by <a href="https://maestroinfotech.in/" target="_blank">Maestro Infotech System</a></p>
  </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="js/classie.js"></script>
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.js"> </script>
</body>
</html>