<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>My Dashboard</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Raleway|Noto+Sans|Lato' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    
	<link href="css/mainstyle.css" type="text/css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#step").click(function(){
        $("#walk").animate({left: '+=6%',top:'-=6.5%'});
    });
});
</script>
<style>
.pldr {
border-radius: 4px !important;
}
.navbar-nav {
    margin: 7.5px -15px;
}
</style>    
  </head>
  <body>
    <!-- Second navbar for search -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand" href="dashBoard.mm"><img src="images/demo-new-site/croyezlogo2.png" width="100px" class="img-responsive"></a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-3">
          <ul class="nav navbar-nav">
        <li  class="active active1 nvmnu"><a href="dashBoard.mm">My Dashboard</a></li>
        <li class="nvmnu"><a href="myInfo.mm">My Info</a></li>
        <li class="nvmnu"><a href="refer.mm">Refer a Friend</a></li>
        <li class="nvmnu"><a href="myDocuments.mm">My Documents</a></li>
        <li class="nvmnu"><a href="evaluation.mm">Evaluation Report</a></li>
        <li class="nvmnu"><a href="retainerAgreement.mm">Retainer Agreement</a></li>
        <li class="nvmnu"><a href="fileDetails.mm">File Details</a></li>
        <li class="nvmnu"><a href="helpDesk.mm">Helpdesk</a></li>
          </ul>
           <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img style="margin: auto; height: 34px; width: 34px;border-radius: 50%;"  src="image.mm?id=${user.userId}">  ${user.fName}  <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="myInfo.mm"> Profile</a></li>
						<li><a href="login.mm">Logout</a></li>
						</ul>
					</li>
          </ul>
          <div class="collapse nav navbar-nav nav-collapse slide-down" id="nav-collapse3">
            <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" />
              </div>
              <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
          </div>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
	<!-------------main body----------->
	<div class="container-fluid">
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-2">
   <div style=" position:absolute;top:0px;left:0;">

<img src="images/step.jpg" >
<img id="walk" style="position:absolute; bottom: 10%; left: 2%;" src="images/walk.png" width="auto" height="auto">
<button style="display:none;" id="step">Next Step</button>
</div>
    </div>
    <div class="col-md-10">
   	<div class="panel">
    <div class="panel-heading"></div>
    <div class="panel-body">
    <div class="col-md-12">
    <div class="row">
    <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel talk1">
					<div class="row no-padding">
                        <h4 class="complthdr">Lorem ipsum</h4>
					</div>
				</div>
            </div>
    <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel talk2">
					<div class="row no-padding">
                        <h4 class="complthdr">Lorem ipsum</h4>
					</div>
				</div>
            </div>
    <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel talk3">
					<div class="row no-padding">
                        <h4 class="complthdr">Lorem ipsum</h4>
					</div>
				</div>
            </div>
    </div>
    </div>
   <div class="col-md-12">
   <div class="row">
   <div class="col-xs-12 col-md-6 col-lg-4">
				<div class="panel pnl panel-widget pnrds pldr ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-4 widget-left uplod">
                        <span class="	glyphicon glyphicon-cloud-upload"></span>
						</div>
						<div class="col-sm-9 col-lg-8 widget-right">
                        <h4 class="indxhdr">Upload Resume</h4>
						</div>
					</div>
				</div>
			</div>
   <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#">
            <div class="panel pnl panel-widget pnrds pldr complt">
					<div class="row no-padding">
						
                        <h4 class="complthdr">Pending</h4>
					</div>
				</div></a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel talk1">
					<div class="row no-padding">
                        <h4 class="complthdr">Lorem ipsum</h4>
					</div>
				</div>
            </div>
   </div>
   </div>
   <div class="col-md-12">
   <div class="row">
   <div class="col-xs-12 col-md-6 col-lg-4">
				<div class="panel pnl panel-widget pnrds pldr ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-4 widget-left minfo">
                        <span class="glyphicon glyphicon-user"></span>
						</div>
						<div class="col-sm-9 col-lg-8 widget-right">
                        <h4 class="indxhdr">My Info</h4>
						</div>
					</div>
				</div>
			</div>
            <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#">
            <div class="panel pnl panel-widget pnrds pldr complt">
					<div class="row no-padding">
                        <h4 class="complthdr">Pending</h4>
					</div>
				</div></a>
            </div>
        <div class="col-xs-12 col-md-6 col-lg-4">
            <div class="panel 	paymt">
					<div class="row no-padding">
                    <div class="col-md-12 totlamt col-xs-12">
                    <div class="row">
                    <div class="col-md-6 col-xs-6">
                    <marquee scrolldelay="250">
                    <h5 class="hdr">Total Amount</h5>
                    <span> 0</span>
                    </marquee>
                    </div>
                    <div class="col-md-6 col-xs-6">
                    <marquee scrolldelay="250">
                    <h5 class="hdr">Pending Amount</h5>
                    <span>0</span>
                    </marquee>
                    </div>
                    </div>
                    </div>
                    <div class="col-md-12 col-xs-12">
                        <a href="#"><h4 class="pmt hdr1">Payment</h4></a>
					</div>
                    </div>
				</div>
            </div>    
   </div>
   </div>
   <div class="col-md-12">
   <div class="row">
   <div class="col-xs-12 col-md-6 col-lg-4">
				<div class="panel pnl panel-widget pnrds pldr ">
					<div class="row no-padding">
						<div class="col-sm-3 col-lg-4 widget-left reg">
                        <span class="glyphicon glyphicon-tag"></span>
						</div>
						<div class="col-sm-9 col-lg-8 widget-right">
                        <h4 class="indxhdr">Registration</h4>
						</div>
					</div>
				</div>
			</div>
   <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#">
            <div class="panel pnl panel-widget pnrds pldr complt">
					<div class="row no-padding">
						
                        <h4 class="complthdr">Pending</h4>
					</div>
				</div></a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-4">
            <a href="#">
            <div class="panel talk">
					<div class="row no-padding">
                        <h4 class="complthdr">Talk to Expert</h4>
					</div>
				</div></a>
            </div>
   </div>
   </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6">
    <div class="panel">
    <div class="panel-heading">
     <h4 class="lsthdr">LATEST UPDATES</h4></div>
    <div class="panel-body">
    <marquee behavior="scroll" direction="up" scrolldelay="250" onmouseover="this.stop();" onmouseout="this.start();">
    <p class="lastp">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> <p class="lastp">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
    
    </marquee>
    </div>
    </div>
    </div>
    <div class="col-md-6">
    <div class="panel">
    <div class="panel-heading">
   <h4 class="lsthdr">NEWS UPDATES</h4></div>
    <div class="panel-body">
  <div class='row'>
      <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#quote-carousel" data-slide-to="1"></li>
          <li data-target="#quote-carousel" data-slide-to="2"></li>
        </ol>
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
        
          <!-- Quote 1 -->
          <div class="item active">
          <h5 class="teminhdr">Lorem ipsum </h5>
                 <p class="tsmin">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <!-- Quote 2 -->
          <div class="item">
           <h5 class="teminhdr">Lorem ipsum </h5>
                  <p class="tsmin">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
          <!-- Quote 3 -->
          <div class="item">
           <h5 class="teminhdr">Lorem ipsum </h5>
                <p class="tsmin">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          </div>
        </div>
        
        <!-- Carousel Buttons Next/Prev -->
        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
      </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script>
    // When the DOM is ready, run this function
$(document).ready(function() {
  //Set the carousel options
  $('#quote-carousel').carousel({
    pause: true,
    interval: 4000,
  });
});
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>