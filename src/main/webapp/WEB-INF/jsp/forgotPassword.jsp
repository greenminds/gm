<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script>
	function checkEmail() {
		var email = document.getElementById("user-email-id").value;
		if (email != "") {
			AjaxController.verifyEmail(email, returnedResult);
		}
		function returnedResult(value) {
			if (value == true) {
				//alert("value:"+value);
				AjaxController.forgotPassword(email);

				window.location.href = "forgotMailSuccess.mm";
			} else {
				document.getElementById("divCheck").innerHTML = "Your Email-Id doesn't exist";

			}
		}
	}
</script>

</head>
<body>
	<br>
	<b>Enter your Email-Id</b>
	<br>
	<input type="email" name="email" id="user-email-id" required="required" />
	<input type="submit" name="submit" onclick="checkEmail()" />
	<div class="registrationFormAlert" id="divCheck" style="color: red;"></div>
</body>
</html>