	<script src="js/addclient.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
  <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
      <form>
        <div class="panel-info rothdr"> Clients / Agreement Approval </div>
        <div class="panel-info widget-shadow">
          <h4 class="title2">Agreement Approval </h4>
          <div class="col-md-12 crtbtnbtm">
            <div class="row">
              <div class="col-md-8">
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#home" onclick="filteragreeaprove('pending')" aria-controls="home" role="tab" data-toggle="tab" class="tbbdr stp1">Agreement Not Uploaded</a></li>
                  <li role="presentation"><a href="#profile" onclick="filteragreeaprove('Approve')" aria-controls="profile" role="tab" data-toggle="tab" class="tbbdr stp2">Agreement Uploaded</a></li>
                </ul>
              </div>
              <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"> <!--<a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a>--> </div>
                  <div class="col-md-2">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="createAgreementApprovalSheet.mm">XLS</a></li>
											<!--      <li><a href="#">WORD</a></li> -->
										</ul>
                    </div>
                  </div>
                  <div class="col-md-2"> <a href="agreementapproval.mm" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">3</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                          <li class="active" ><a href="#" onclick="pagination(10)">10</a></li>
                        <li><a href="#" onclick="pagination(25)">25</a></li>
                        <li><a href="#" onclick="pagination(50)">50</a></li>
                        <li><a href="#" onclick="pagination(100)">100</a></li>

                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
              <div class="tables">
                <div class="table-responsive bs-example">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="tblthd">Id</th>
                        <th class="tblthd">Name</th>
                        <th class="tblthd">Email</th>
                        <th class="tblthd">Contact</th>
                        <th class="tblthd">Coordinators</th>
                        <th class="tblthd">Action</th>
                      </tr>
                      <tr>
                        <td><input type="text" onkeyup="AgreeApprove(this,'userId')" class="form-control"></td>
                        <td><input type="text" onkeyup="AgreeApprove(this,'fName')" class="form-control"></td>
                        <td><input type="text" onkeyup="AgreeApprove(this,'email')" class="form-control"></td>
                        <td><input type="text" onkeyup="AgreeApprove(this,'mobile')" class="form-control"></td>
                        <input type="hidden" id="UserStatus" value="" />
                         <td><select onchange="SearchUserAACo(this,'coordinator')"  class="form-control">
                            <option>Select Coordinators</option>
                           <c:forEach var="coordinate" items="${coordinators}">
							<option value="${coordinate.emp_firstname}">${coordinate.emp_firstname}</option>
							</c:forEach>
                          </select></td>
                        <td><input type="text" class="form-control"></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody class="tbdy" id="myTable">
                        <c:forEach var="agreementApproval" items="${agreementApprovalList}">
                <tr>
                  <td>${agreementApproval.userId}</td>
                  <td>${agreementApproval.fName}</td>
                  <td>${agreementApproval.email}</td>
                  <td>${agreementApproval.mobile}</td>
                  <td>${agreementApproval.coordinator} </td>
                  <td>
                   <a href="#" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                </c:forEach >                  
                  </tbody>
                  </table>
      <%--       <div class="col-md-12">
                    <ul class="pagination pagination-lg pager pull-right" id="myPager">
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
              <div class="tables">
                <div class="table-responsive bs-example">
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="tblthd">Id</th>
                        <th class="tblthd">Email</th>
                        <th class="tblthd">Name</th>
                        <th class="tblthd">Coordinators</th>
                        <th class="tblthd">Agreement Waiting</th>
                        <th class="tblthd">Action</th>
                      </tr>
                      <tr>
                        <td><input type="text" class="form-control"></td>
                        <td><input type="text" class="form-control"></td>
                        <td><input type="text" class="form-control"></td>
                         <td><select class="form-control">
                            <option>Select Coordinators</option>
                            <option></option>
                            <option></option>
                            <option></option>
                          </select></td>
                        <td><input type="text" class="form-control"></td>
                        <td></td>
                      </tr>
                    </thead>
                    <tbody class="tbdy" id="myTable">
                     <c:forEach var="agreementApproval" items="${agreementApprovalList}">
                <tr>
                  <td>${agreementApproval.userId}</td>
                  <td>${agreementApproval.email}</td>
                  <td>${agreementApproval.fName}</td>
                  <td>${agreementApproval.mobile}</td>
                  <td> </td>
                  <td>
                   <a href="#" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                </c:forEach >                  
                  </tbody>
                  </table>
                  <div class="col-md-12">
                    <ul class="pagination pagination-lg pager" id="myPager">
                    </ul>
                  </div>
  		  --%> 	</div>
              </div>
            </div>
          </div>
          <div class="clearfix"> </div>
        </div>
      </form>
    </div>
  </div>
 