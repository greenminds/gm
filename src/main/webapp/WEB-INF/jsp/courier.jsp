<!DOCTYPE HTML>
<html>
<head>
	<title>Courier</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Shinrai" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- Bootstrap Core CSS -->
	<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
	<!-- Custom CSS -->
	<link href="css/style.css" rel='stylesheet' type='text/css' />
	<link rel="stylesheet" href="css/responsive.css">
	<!-- font CSS -->
	<!-- font-awesome icons -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- //font-awesome icons -->
	<!-- js-->
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<!--webfonts-->
	<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans|Noto+Sans' rel='stylesheet' type='text/css'>
    <!--//webfonts-->
	<!--animate-->
	<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
	<script src="js/wow.min.js"></script>
	<script>
		 new WOW().init();
	</script>
	<!--//end-animate-->
	<!-- Metis Menu -->
	<script src="js/metisMenu.min.js"></script>
	<script src="js/custom.js"></script>
	<link href="css/custom.css" rel="stylesheet">
	<!--//Metis Menu -->
    <!---------date picker---------->
    <link href="css/datepicker.css" type="text/css" rel="stylesheet">
<script src="js/datepicker.js" type="text/javascript"></script>
<script>
	 $(function() {
    $(".datepicker").datepicker();
});
</script>
<style>
.panel-info.widget-shadow {
	padding: 0.7em 0.1em;
}
ul.dropdown-menu {
    padding: 0;
    min-width: 116px;
    top: 95%;
}
.charts, .row {
    margin:inherit !important;
}
.general h4.title2 {
    font-size: 17px;
    margin: 3px;
    color: #777777;
    padding-left: 10px;
    font-family: 'Open Sans', sans-serif;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 2;
    cursor: default;
    background: #e9ecf2;
    color: #000;
    border-color: #ccc;
}
.pager li > a, .pager li > span {
    display: inline-block;
    padding: 5px 14px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius:0px;
	font-size:16px;
	font-family: 'Open Sans', sans-serif;
}
.pagination-lg > li:last-child > a, .pagination-lg > li:last-child > span {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.pager {
	margin:inherit;
}  
[data-tooltip]:before {
    position: absolute;
    bottom: 150%;
    left: inherit;
    margin-bottom: 5px;
    margin-left: -23px;
    padding: 6px;
    width: auto;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    background-color: #000;
    background-color: hsla(0, 0%, 20%, 0.9);
    color: #fff;
    content: attr(data-tooltip);
    text-align: center;
    font-size: 13px;
    line-height: 1.2;
}
</style>
	</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
      <!--left-fixed -navigation-->
      <div class=" sidebar" role="navigation">
    <div class="navbar-collapse">
      <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
        <ul class="nav" id="side-menu">
          <li> <a href="index.html" ><i class="fa fa-home nav_icon"></i>Dashboard</a> </li>
          <li> <a href="#"><i class="fa fa-thumbs-up nav_icon"></i>Pre-Sales<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
              <li> <a href="#">Opportunities/Leads<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                  <li  class="sidbr"> <a href="opportunities.html">Opportunities</a> </li>
                  <li class="sidbr"> <a href="enquiries.html">Enquiries</a> </li>
                  <li class="sidbr"> <a href="refferals.html">Refferals</a> </li>
                </ul>
              </li>
            </ul>
            <!-- /nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-users nav_icon"></i>Clients<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a href="addclient.html">Add Client</a> </li>
                  <li class="sidbr"> <a href="clients.html">Clients</a> </li>
                  <li class="sidbr"> <a href="clientfileupload.html">File Uploads</a> </li>
                  <li class="sidbr"> <a href="agreementupload.html">Agreement Upload</a> </li>
                  <li class="sidbr"> <a href="documentapproval.html">Document Approval</a></li>
                  <li class="sidbr"> <a href="agreementapproval.html">Agreement Approval</a></li>
                  <li class="sidbr"> <a href="invoice.html">Initial Agreement</a></li>
             </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-inr nav_icon"></i>Accounts<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="reciept.html">Reciepts</a> </li>
                  <li class="sidbr"> <a class="lisd" href="installment.html">Installment Breakup</a> </li>
                  <li class="sidbr"> <a class="lisd" href="accountinvoice.html">Invoice</a> </li>
                  <li class="sidbr"> <a class="lisd" href="miscfee.html">Misc Fee</a></li>
                  <li class="sidbr"> <a class="lisd" href="refunds.html">Refunds</a></li>
                  <li class="sidbr"> <a class="lisd" href="onlinepayments.html">Online Payments</a></li>
            </ul>
            <!-- //nav-second-level --> 
          </li>
           <li> <a href="#"><i class="fa fa-filter nav_icon"></i>Assessment<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="#">Express Assessment</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Assessment Point</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Assessment Contents</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Pending Assessment</a></li>
                  <li class="sidbr"> <a class="lisd" href="#">Send Assessment</a></li>
             </ul>
            <!-- //nav-second-level --> 
          </li>
          <li class="active"> <a href="#"><i class="fa fa-envelope nav_icon"></i>Communication<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Mailer<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Email Sync</a> </li>
                      <li class="sidbr"> <a class="lisd" href="mail.html">Simple Mail</a> </li>
                      <li class="sidbr"> <a class="lisd" href="bulkmail.html">Bulk Mail</a> </li>
                    </ul>
              </li>
              <li> <a href="#">Messenger<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Simple SMS</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Bulk SMS</a> </li>
                </ul>
              </li>
              <li class="active"> <a href="courier.html">Courier</a></li>
              <li> <a href="newsupdates.html" >News Updates</a> </li>
            </ul>
            <!-- /nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-question nav_icon"></i>Help/Updates<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="supporttickets.html">Support Tickets</a> </li>
                  <li class="sidbr"> <a class="lisd" href="officialupdates.html">Official Updates</a> </li>
                  <li class="sidbr"> <a class="lisd" href="faq.html">FAQ</a> </li>
                  <li class="sidbr"> <a class="lisd" href="blog.html">Blog</a> </li>
            </ul>
            <!-- //nav-second-level --> 
          </li>	
          <li> <a href="#"><i class="fa fa-bar-chart nav_icon"></i>Report<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li class="sidbr"> <a class="lisd" href="#">Task Report</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Express Report</a> </li>
            </ul>
            <!-- //nav-second-level --> 
          </li>
          <li> <a href="#"><i class="fa fa-cogs nav_icon"></i>System<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                  <li> <a href="#">Administration<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="role.html">Role</a> </li>
                      <li class="sidbr"> <a class="lisd" href="userdetails.html">User Details</a> </li>
                      <li class="sidbr"> <a class="lisd" href="accesscontrol.html">Access Control</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">Comm. Settings<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">SMTP Config</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Mailing List</a> </li>
                     <li class="sidbr"> <a class="lisd" href="#">Email Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Back.Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">Post Agg. Templates</a> </li>
                      <li class="sidbr"> <a class="lisd" href="#">SMS Config</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">General Masters<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="#">Country Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Branch</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Module</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Doc Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Support Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Update Type</a> </li>
                  <li class="sidbr"> <a class="lisd" href="clientcategory.html">Client Category</a> </li>
                  <li class="sidbr"> <a class="lisd" href="assignto.html">Designation</a></li>
                  <li class="sidbr"> <a class="lisd" href="#">Agreement Template</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Application Forms</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Client Status</a> </li>
                  <li class="sidbr"> <a class="lisd" href="sources.html">Source Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Document Master</a> </li>
                  <li class="sidbr"> <a class="lisd" href="#">Document Mapping</a> </li>
                    </ul>
              </li>
                  <li> <a href="#">Account Master<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                      <li class="sidbr"> <a class="lisd" href="program.html">Program</a> </li>
                      <li class="sidbr"> <a class="lisd" href="feemaster.html">Fee Master</a> </li>
                    </ul>
              </li>
                </ul>
            <!-- /nav-second-level --> 
          </li>
        </ul>
        <!-- //sidebar-collapse --> 
      </nav>
    </div>
  </div>
      <!--left-fixed -navigation--> 
      <!-- header-starts -->
      <div class="sticky-header header-section ">
    <div class="header-left"> 
          <!--toggle button start-->
          <button id="showLeftPush"><i class="fa fa-bars"></i></button>
          <!--toggle button end--> 
          <!--logo -->
          <div class="logo"> <a href="index.html">
            <img src="images/logo-4.png" class="center-block img-responsive"></a>
      </div>
          <!--//logo-->
          
          <div class="clearfix"> </div>
        </div>
    <div class="header-right"> 
          <!--search-box-->
          <div class="search-box"> 
        <form action="" class="search-form">
              <div class="form-group has-feedback">
            <label for="search" class="sr-only">Search</label>
            <input type="text" class="form-control" name="search" id="search" placeholder="search">
            <span class="glyphicon glyphicon-search form-control-feedback"></span> </div>
            </form>
      </div>
          <!--//end-search-box-->
          <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
              <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">3</span></a>
            <ul class="dropdown-menu">
                  <li>
                <div class="notification_header">
                      <h3>You have 3 new messages</h3>
                    </div>
              </li>
                  <li><a href="#">
                    <div class="user_img"><img src="images/1.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet</p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li class="odd"><a href="#">
                    <div class="user_img"><img src="images/2.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet </p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li><a href="#">
                    <div class="user_img"><img src="images/3.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet </p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
                </ul>
          </li>
              <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-filter"></i><span class="badge">3</span></a>
            <ul class="dropdown-menu">
                  <li>
                <div class="notification_header">
                      <h3>You have 3 new messages</h3>
                    </div>
              </li>
                  <li><a href="#">
                    <div class="user_img"><img src="images/1.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet</p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li class="odd"><a href="#">
                    <div class="user_img"><img src="images/2.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet </p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li><a href="#">
                    <div class="user_img"><img src="images/3.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet </p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
                </ul>
          </li>
              <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
            <ul class="dropdown-menu">
                  <li>
                <div class="notification_header">
                      <h3>You have 3 new notification</h3>
                    </div>
              </li>
                  <li><a href="#">
                    <div class="user_img"><img src="images/2.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet</p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li class="odd"><a href="#">
                    <div class="user_img"><img src="images/1.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet </p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li><a href="#">
                    <div class="user_img"><img src="images/3.png" alt=""></div>
                    <div class="notification_desc">
                    <p>Lorem ipsum dolor amet </p>
                    <p><span>1 hour ago</span></p>
                  </div>
                    <div class="clearfix"></div>
                    </a></li>
                  <li>
                <div class="notification_bottom"> <a href="#">See all notifications</a> </div>
              </li>
                </ul>
          </li>
              <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">15</span></a>
            <ul class="dropdown-menu">
                  <li>
                <div class="notification_header">
                      <h3>You have 8 pending task</h3>
                    </div>
              </li>
                  <li><a href="#">
                    <div class="task-info"> <span class="task-desc">Database update</span><span class="percentage">40%</span>
                    <div class="clearfix"></div>
                  </div>
                    <div class="progress progress-striped active">
                    <div class="bar yellow" style="width:40%;"></div>
                  </div>
                    </a></li>
                  <li><a href="#">
                    <div class="task-info"> <span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
                    <div class="clearfix"></div>
                  </div>
                    <div class="progress progress-striped active">
                    <div class="bar green" style="width:90%;"></div>
                  </div>
                    </a></li>
                  <li><a href="#">
                    <div class="task-info"> <span class="task-desc">Mobile App</span><span class="percentage">33%</span>
                    <div class="clearfix"></div>
                  </div>
                    <div class="progress progress-striped active">
                    <div class="bar red" style="width: 33%;"></div>
                  </div>
                    </a></li>
                  <li><a href="#">
                    <div class="task-info"> <span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
                    <div class="clearfix"></div>
                  </div>
                    <div class="progress progress-striped active">
                    <div class="bar  blue" style="width: 80%;"></div>
                  </div>
                    </a></li>
                  <li>
                <div class="notification_bottom"> <a href="#">See all pending tasks</a> </div>
              </li>
                </ul>
          </li>
            </ul>
        <div class="clearfix"> </div>
      </div>
          <!--notification menu end -->
          <div class="profile_details">
        <ul>
              <li class="dropdown profile_details_drop"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <div class="profile_img"> <span class="prfil-img"><img src="images/a.png" alt=""> </span>
                <div class="user-name">
                    <p>Wikolia</p>
                    <span>Administrator</span> </div>
                <i class="fa fa-angle-down lnr"></i> <i class="fa fa-angle-up lnr"></i>
                <div class="clearfix"></div>
              </div>
                </a>
            <ul class="dropdown-menu drp-mnu">
                  <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li>
                  <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li>
                  <li> <a href="#"><i class="fa fa-sign-out"></i> Logout</a> </li>
                </ul>
          </li>
            </ul>
      </div>
          <div class="clearfix"> </div>
        </div>
    <div class="clearfix"> </div>
  </div>
      <!-- //header-ends --> 
      <!-- main content start-->
      <div id="page-wrapper">
    <div class="main-page general">
          <div class="panel-info rothdr"> Communication / Courier </div>
          <div class="panel-info widget-shadow">
        <h4 class="title2">Courier Details</h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"> <a href="createcourier.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a> </div>
                  <div class="col-md-2">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
            </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">Type</th>
                  <th class="tblthd">Consignment no</th>
                  <th class="tblthd">File Name</th>
                  <th class="tblthd">Date</th>
                  <th class="tblthd">Courier Name</th>
                  <th class="tblthd">Active</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                   <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td></td>
                </tr>
              </thead>
                <tbody class="tbdy" id="myTable">
                <tr>
                  <td>Table cell</td>
                  <td>Table cell</td>
                  <td>Table cell</td>
                  <td>lvishnu15@gmail.com</td>
                  <td>9876543210</td>
                  <td> <input type="checkbox" data-toggle="toggle" data-on="On" data-off="Off"></td>
                  <td>
                   <a href="editcourier.html" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                 <tr>
                  <td>Table cell</td>
                  <td>Table cell</td>
                  <td>Table cell</td>
                  <td>lvishnu@gmail.com</td>
                  <td>9876543210</td>
                  <td> <input type="checkbox" data-toggle="toggle" data-on="On" data-off="Off"></td>
                  <td>
                   <a href="editcourier.html" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
        </div>
  </div>
      <!--footer-->
      <div class="footer">
    <p>&copy; 2016 Admin Panel. All Rights Reserved | Design by <a href="https://maestroinfotech.in/" target="_blank">Maestro Infotech System</a></p>
  </div>
      <!--//footer--> 
</div>
<!-- Classie --> 
<script src="js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<script>
 $.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 7,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
  	pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
      	pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

$(document).ready(function(){
    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
    
});
 </script>
<!--scrolling js--> 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.js"> </script>
</body>
</html>