
<script src="js/addclient.js"></script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
  <!-- main content start-->
  
  <div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Clients / Client File Uploads </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2">Client File Uploads </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"> <!--<a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a>--> </div>
                  <div class="col-md-2">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="createFileUploadSheet.mm">XLS</a></li>
											<!--      <li><a href="#">WORD</a></li> -->
										</ul>
                    </div>
                  </div>
                  <div class="col-md-2"> <a href="clientfileupload.mm" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">3</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li  class="active" ><a href="#" onclick="pagination(3)">3</a></li>
                        <li><a href="#" onclick="pagination(4)">4</a></li>
                        <li><a href="#" onclick="pagination(5)">5</a></li>
                        <li><a href="#" onclick="pagination(6)">6</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
                      </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">ID</th>
                  <th class="tblthd">Name</th>
                  <th class="tblthd">Email</th>
                  <th class="tblthd">Contact</th>
                  <th class="tblthd">Coordinators</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                  <td>
                  <input type="text" onkeyup="SearchDocupload(this,'userId')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchDocupload(this,'fName')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchDocupload(this,'email')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchDocupload(this,'mobile')" class="form-control">
                  </td>
                  <td>
                 <select onchange="SearchUserFUCo(this,'coordinator')" class="form-control">
                 <option>Select Coordinators</option>
                 	<c:forEach var="coordinate" items="${coordinators}">
					<option value="${coordinate.emp_firstname}">${coordinate.emp_firstname}</option>
					</c:forEach>
                 </select>
                  </td>
                  <td></td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <c:forEach var="fileupload" items="${fileuploadList}">
                <tr>
                  <td>${fileupload.userId}</td>
                  <td>${fileupload.fName}</td>
                  <td>${fileupload.email}</td>
                  <td>${fileupload.mobile}</td>
                  <td>${fileupload.coordinator} </td>
                  <td>
                   <a href="#" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                </c:forEach >
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
       <input type="hidden" id="noofrec" value="" />
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  
  </div>
 
 <!--  <!-- Classie --> 
<script src="js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
 <script>
 
 $.fn.pageMe = function(opts){
	    var $this = this,
	        defaults = {
	            perPage: 7,
	            showPrevNext: false,
	            hidePageNumbers: false
	        },
	        settings = $.extend(defaults, opts);
	    
	    var listElement = $this;
	    var perPage = settings.perPage; 
	    var children = listElement.children();
	    var pager = $('.pager');
		
	    if (typeof settings.childSelector!="undefined") {
	        children = listElement.find(settings.childSelector);
	    }
	    
	    if (typeof settings.pagerSelector!="undefined") {
	        pager = $(settings.pagerSelector);
	    }
	    var noofrec = document.getElementById("noofrec").value;
	    var numItems = 0;
	    if(noofrec == null || noofrec == "")
	    numItems = children.size();
	    else
	    	numItems = noofrec;
	    var numPages = Math.ceil(numItems/perPage);

	    pager.data("curr",0);
	    
	    if (settings.showPrevNext){
	        $('<li><a href="#" class="prev_link">�</a></li>').appendTo(pager);
	    }
	    
	    var curr = 0;
	    while(numPages > curr && (settings.hidePageNumbers==false)){
	        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
	        curr++;
	    }
	    
	    if (settings.showPrevNext){
	        $('<li><a href="#" class="next_link">�</a></li>').appendTo(pager);
	    }
	    
	    pager.find('.page_link:first').addClass('active');
	    pager.find('.prev_link').hide();
	    if (numPages<=1) {
	        pager.find('.next_link').hide();
	    }
	  	pager.children().eq(1).addClass("active");
	    
	    children.hide();
	    children.slice(0, perPage).show();
	    
	    pager.find('li .page_link').click(function(){
	        var clickedPage = $(this).html().valueOf()-1;
	        goTo(clickedPage,perPage);
	        return false;
	    });
	    pager.find('li .prev_link').click(function(){
	        previous();
	        return false;
	    });
	    pager.find('li .next_link').click(function(){
	        next();
	        return false;
	    });
	 
	    
	    	     function previous(){
	        var goToPage = parseInt(pager.data("curr")) - 1;
	        goTo(goToPage);
	    }
	     
	    function next(){
	        goToPage = parseInt(pager.data("curr")) + 1;
	        goTo(goToPage);
	    }
	    
	    function goTo(page){
	        var startAt = page * perPage,
	            endOn = startAt + perPage;
	        
	        children.css('display','none').slice(startAt, endOn).show();
	        
	        if (page>=1) {
	            pager.find('.prev_link').show();
	        }
	        else {
	            pager.find('.prev_link').hide();
	        }
	        
	        if (page<(numPages-1)) {
	            pager.find('.next_link').show();
	        }
	        else {
	            pager.find('.next_link').hide();
	        }
	        
	        pager.data("curr",page);
	      	pager.children().removeClass("active");
	        pager.children().eq(page+1).addClass("active");
	    
	    }
	};

	$(document).ready(function(){
		<!--var pageSize=document.getElementById("txtPageSizeH").value;-->
	  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
	    
	});
	 </script> -->

<!--scrolling js 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
//scrolling js 
Bootstrap Core JavaScript 
<script src="js/bootstrap.js"> </script>
   
  -->