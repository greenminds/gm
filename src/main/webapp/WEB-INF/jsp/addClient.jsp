	<script src="js/addclient.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>

<style>
.panel-info.widget-shadow {
	padding: 0.7em 0.1em;
}

ul.dropdown-menu {
	padding: 0;
	min-width: 148px;
	top: 95%;
}

.charts,.row {
	margin: inherit !important;
}

.general h4.title2 {
	font-size: 17px;
	margin: 3px;
	color: #777777;
	padding-left: 10px;
	font-family: 'Open Sans', sans-serif;
}

.wizard {
	background: #fff;
}

.wizard .nav-tabs {
	position: relative;
	margin-bottom: 0;
	border-bottom-color: #e0e0e0;
}

.wizard>div.wizard-inner {
	position: relative;
}

.connecting-line {
	height: 1px;
	background: #e0e0e0;
	position: absolute;
	width: 80%;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: 50%;
	z-index: 1;
}

.wizard .nav-tabs>li.active>a,.wizard .nav-tabs>li.active>a:hover,.wizard .nav-tabs>li.active>a:focus
	{
	color: #555555;
	cursor: default;
	border: 0;
	border-bottom-color: transparent;
}

span.round-tab {
	width: 60px;
	height: 60px;
	line-height: 60px;
	display: inline-block;
	border-radius: 100px;
	background: #fff;
	border: 2px solid #e0e0e0;
	z-index: 2;
	position: absolute;
	left: 0;
	text-align: center;
	font-size: 25px;
}

span.round-tab i {
	color: #5bc0de;
}

.wizard li.active span.round-tab {
	background: #fff;
	border: 2px solid rgba(27, 255, 0, 0.35);
}

.wizard li.active span.round-tab i {
	color: #febe29;
}

span.round-tab:hover {
	color: #333;
	border: 2px solid #A9D86E;
}

.wizard .nav-tabs>li {
	width: 16%;
}

.wizard li:after {
	content: " ";
	position: absolute;
	left: 46%;
	opacity: 0;
	margin: 0 auto;
	bottom: 0px;
	border: 5px solid transparent;
	border-bottom-color: #5bc0de;
	transition: 0.1s ease-in-out;
}

.wizard li.active:after {
	content: " ";
	position: absolute;
	left: 44%;
	opacity: 1;
	margin: 0 auto;
	bottom: 0px;
	border: 10px solid transparent;
	border-bottom-color: #febe29;
}

.wizard .nav-tabs>li a {
	width: 60px;
	height: 60px;
	margin: 11px auto;
	border-radius: 100%;
	padding: 0;
}

.wizard .nav-tabs>li a:hover {
	background: transparent;
}

.wizard .tab-pane {
	position: relative;
	padding-top: 0px;
}

.wizard h3 {
	margin-top: 0;
}

.step1 .row {
	margin-bottom: 10px;
}

.step_21 {
	border: 1px solid #eee;
	border-radius: 5px;
	padding: 10px;
}

.step33 {
	border: 1px solid #ccc;
	border-radius: 5px;
	padding-left: 10px;
	margin-bottom: 10px;
}

.dropselectsec {
	width: 68%;
	padding: 6px 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	color: #333;
	margin-left: 10px;
	outline: none;
	font-weight: normal;
}

.dropselectsec1 {
	width: 74%;
	padding: 6px 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	color: #333;
	margin-left: 10px;
	outline: none;
	font-weight: normal;
}

.mar_ned {
	margin-bottom: 10px;
}

.wdth {
	width: 25%;
}

.birthdrop {
	padding: 6px 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	color: #333;
	margin-left: 10px;
	width: 16%;
	outline: 0;
	font-weight: normal;
}
/* according menu */
#accordion-container {
	font-size: 13px
}

.accordion-header {
	font-size: 13px;
	background: #ebebeb;
	margin: 5px 0 0;
	padding: 7px 20px;
	cursor: pointer;
	color: #fff;
	font-weight: 400;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px
}

.unselect_img {
	width: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

.active-header {
	-moz-border-radius: 5px 5px 0 0;
	-webkit-border-radius: 5px 5px 0 0;
	border-radius: 5px 5px 0 0;
	background: #F53B27;
}

.active-header:after {
	content: "\f068";
	font-family: 'FontAwesome';
	float: right;
	margin: 5px;
	font-weight: 400
}

.inactive-header {
	background: #333;
}

.inactive-header:after {
	content: "\f067";
	font-family: 'FontAwesome';
	float: right;
	margin: 4px 5px;
	font-weight: 400
}

.accordion-content {
	display: none;
	padding: 20px;
	background: #fff;
	border: 1px solid #ccc;
	border-top: 0;
	-moz-border-radius: 0 0 5px 5px;
	-webkit-border-radius: 0 0 5px 5px;
	border-radius: 0 0 5px 5px
}

.accordion-content a {
	text-decoration: none;
	color: #333;
}

.accordion-content td {
	border-bottom: 1px solid #dcdcdc;
}

@media ( max-width : 585px ) {
	.wizard {
		width: 90%;
		height: auto !important;
	}
	span.round-tab {
		font-size: 16px;
		width: 50px;
		height: 50px;
		line-height: 50px;
	}
	.wizard .nav-tabs>li a {
		width: 50px;
		height: 50px;
		line-height: 50px;
	}
	.wizard li.active:after {
		content: " ";
		position: absolute;
		left: 35%;
	}
}
</style>
<body onload="onload()">
<div id="page-wrapper">
	<div class="main-page general">
		<div class="panel-info rothdr">Clients / Add Clients</div>
		<div class="panel-info widget-shadow">
			<h4 class="title2">Add Clients</h4>
			<div class="bdr"></div>
			<!--------------detail tabs-------------->
			<section>
				<div class="wizard">
					<div class="wizard-inner">
						<div class="connecting-line"></div>
						<ul class="nav nav-tabs" role="tablist">
							<li id="des5" role="presentation" class="active"><a
								href="#step1" data-toggle="tab" aria-controls="step1" role="tab"
								title="Personal Details"> <span class="round-tab"> <i
										class="glyphicon glyphicon-user"></i>
								</span>
							</a></li>
							<li id="des4" role="presentation" class="disabled"><a
								href="#step2" data-toggle="tab" aria-controls="step2" role="tab"
								title="Contact Details"> <span class="round-tab"> <i
										class="glyphicon glyphicon-earphone"></i>
								</span>
							</a></li>
							<li id="des3" role="presentation" class="disabled"><a
								href="#step3" data-toggle="tab" aria-controls="step3" role="tab"
								title="File Details"> <span class="round-tab"> <i
										class="glyphicon glyphicon-folder-open"></i>
								</span>
							</a></li>
							<li id="des2" role="presentation" class="disabled"><a
								href="#step4" data-toggle="tab" aria-controls="step4" role="tab"
								title="PR & TFW"> <span class="round-tab"> <i
										class="glyphicon glyphicon-tags"></i>
								</span>
							</a></li>
							<li id="des1" role="presentation" class="disabled"><a
								href="#step5" data-toggle="tab" aria-controls="step5" role="tab"
								title="Processing"> <span class="round-tab"> <i
										class="glyphicon glyphicon-flash"></i>
								</span>
							</a></li>
							<li id="des0" role="presentation" class="disabled"><a href="#complete"
								data-toggle="tab" aria-controls="complete" role="tab"
								title="System"> <span class="round-tab"> <i
										class="glyphicon glyphicon glyphicon-erase"></i>
								</span>
							</a></li>
						</ul>
					</div>
					<form role="form" class="form-horizontal">
						<div class="tab-content">
							<div class="tab-pane active" role="tabpanel" id="step1">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Email</label>
										<div class="col-md-4">
										<c:choose>
										<c:when test="${clt.email ne null}">
										<input type="email" id="clientEmail" class="form-control"
												name="" value="${clt.email}" onchange=""
												placeholder="Email" disabled>
							
										</c:when>
										<c:otherwise>
							<input type="email" id="clientEmail" class="form-control"
												name="" value="${clt.email}" onchange="emailValid(this)"
												placeholder="Email">
										
										</c:otherwise>
										</c:choose>
												<span id="email" style="color:red"></span>
										</div>
										
										<label class="control-label col-md-2 lblfnt">Phone</label>
										<div class="col-md-4">
											<input type="text" class="form-control"
											 name="" id="phone" value="${clt.phoneNumber}" onchange="isPhoner(this)"
												 placeholder="Phone">
											<span id="isPhoner" style="color:red"></span>	 	
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Salutation</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="salutation" value="${clt.salutation}"
											onchange="isAlphabet(this)"	placeholder="Salutation">
										<span id="isAlphabet" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">First
											Name</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="firstName" 
											 onchange="isAlphabet1(this)" placeholder="First Name" value="${clt.firstName}">
											<span id="isAlphabet1" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Middle
											Name</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="middleName" value="${clt.middleName}"
												onchange="isAlphabet2(this)" placeholder="Middle Name">
										<span id="isAlphabet2" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Last Name</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="lastName" value="${clt.lastName}"
											onchange="isAlphabet3(this)"	placeholder="Last Name">
										<span id="isAlphabet3" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Date Of
											Birth</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" id="dob" value="${clt.dob}"
												onchange="isDob(this)" placeholder="Date of Birth">
										<span id="isDob" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Passport
											No.</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="passportNo" value="${clt.passportNo}"
												onchange="isPassport(this)" placeholder="Passport No">
										<span id="isPassport" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Passport
											Exp Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" id="ped" value="${clt.passportExpDate}"
											placeholder="Passport Exp Date">
										</div>
										<label class="control-label col-md-2 lblfnt">Birth
											Country</label>
										<div  class="col-md-4">
											<select id="birthCountry" class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
												<c:if test="${clt.birthCountry eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Residence
											Country</label>
										<div class="col-md-4">
											<select id="residencyCountry" class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
												<c:if test="${clt.residenCountry eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
										<label class="control-label col-md-2 lblfnt">Citizenship
											Country</label>
										<div class="col-md-4">
											<select id=citizenCountry class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
												<c:if test="${clt.citizenshipCountry eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Preffered
											Language</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="preferredLanguage" value="${clt.prefferedLanguage}"
												onchange="isAlphabet4(this)" placeholder="Preffered Language">
										<span id="isAlphabet4" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Date Of
											Join</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" id="doj" value="${clt.dateOfJoin}"
											 placeholder="Date of Joining">
										</div>
									</div>
								</div>
								<ul class="list-inline pull-right">
									<li>
										<span id="valueind" ></span>	
										<button type="button" class="btn btn-warning next-step"
											id="save5" onclick="saveFirstStep()"
											>Save and
											continue</button>
											
									</li>
								</ul>
							</div>
							<div class="tab-pane" role="tabpanel" id="step2">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Address 1</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="address1" value="${clt.address1}"
												onchange="isAddress(this)" placeholder="Address1">
										<span id="isAddress" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Address 2</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="address2" value="${clt.address2}"
											onchange="isAddress1(this)"	placeholder="Address2">
										<span id="isAddress1" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">City</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="city" value="${clt.city}"
											onchange="isAlphabet5(this)"	placeholder="City">
										<span id="isAlphabet5" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">State</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="state" value="${clt.state}"
										onchange="isAlphabet6(this)"		placeholder="State">
										<span id="isAlphabet6" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Country</label>
										<div class="col-md-4">
											<select id="country" class="form-control">
												<option>Select Country</option>
												<c:forEach var="ctry" items="${countries}">
													<c:if test="${clt.country eq  ctry.country_name}">
												<option value="${ctry.id}" selected>${ctry.country_name}</option>
												</c:if>
												<option value="${ctry.id}">${ctry.country_name}</option>
												</c:forEach>
											</select>
										</div>
										<label class="control-label col-md-2 lblfnt">Zip Code</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="zipcode" value="${clt.zipcode}"
												onchange="isZibcode(this)"	placeholder="Zip Code">
												<span id="isZibcode" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Mobile</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="mobile" value="${clt.mobileNumber}"
									onchange="isPhoner1(this)"			placeholder="Mobile">
										<span id="isPhoner1" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Phone</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="phone1" value="${clt.phoneNumber1}"
									onchange="isPhoner2(this)"			placeholder="Phone">
									<span id="isPhoner2" style="color:red"></span>	
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Other
											Email</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="otherEmail" value="${clt.alternateEmail}"
										onchange="isEmail(this)"		placeholder="Other Email">
										<span id="isEmail" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">FAX</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="fax" value="${clt.fax}"
								onchange="isFax(this)"		placeholder="FAX">
										<span id="isFax" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Contact
											Method</label>
										<div class="col-md-4">
											<select id="contactMethod" class="form-control">
											<option>Select Contact Method</option>
												<option value="Online">Online</option>
												<option value="News Paper">News Paper</option>
											</select>
										</div>
										<label class="control-label col-md-2 lblfnt">Special
											Instructions</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="specialInst" value="${clt.specialInstructions}"
										onchange="isAddress2(this)"		placeholder="Special Instructions">
										<span id="isAddress2" style="color:red"></span>
										</div>
									</div>
								</div>
								<ul class="list-inline pull-left">
									<li>
										<button type="button"
											class=" prvbtn btn btn-default prev-step">Previous</button>
									</li>
								</ul>
								<ul class="list-inline pull-right">
									<li>
										<button type="button" class="btn btn-warning next-step"
											onclick="saveSecondStep()">Save and continue</button>
									</li>
								</ul>
							</div>
							<div class="tab-pane" role="tabpanel" id="step3">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Program</label>
										<div class="col-md-4">
											<select id="program" class="form-control">
												<option>Select Country</option>
												
												<option value="program1">progream1</option>
												<option value="program2">progream2</option>
												<option value="program3">progream3</option>
												
											</select>
										</div>
										<label class="control-label col-md-2 lblfnt">Visa
											Posts</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="visaPosts" value="${clt.visaPosts}"
											onchange="isLetter(this)"	placeholder="Visa Posts">
										<span id="isLetter" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">File No.</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="fileNo" value="${clt.fileNo}"
										onchange="isPhoner3(this)"		placeholder="File No.">
										<span id="isPhoner3" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">File No.
											Other</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="fileNoOther" value="${clt.fileNoOther}"
											onchange="isPhoner4(this)"	placeholder="File No. Other">
										<span id="isPhoner4" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">File
											Status</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="fileStatus" value="${clt.fileStatus}"
										onchange="isAlphabet7(this)"		placeholder="File Status">
										<span id="isAlphabet7" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Application
											Submission Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="appSubmissionDate" value="${clt.appSubmissionDate}"
											placeholder="Application Submission Date">
										
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Medical
											Exp. Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" id="medExpDate" value="${clt.medExpDate}"
												placeholder="Medical Exp. Date">
										</div>
										<label class="control-label col-md-2 lblfnt">Interview
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="interviewDate"
												value="${clt.interviewDate}"  placeholder="Interview Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Interview
											Time</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.interviewTime}" id="interviewTime"
												onchange="isTime(this)" placeholder="Interview Time">
										<span id="isTime" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Interview
											Location</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.interviewLocation}" id="interviewLocation"
												 onchange="isAddress3(this)"placeholder="Interview Location">
										<span id="isAddress3" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Additional
											Info</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.addInfo}" id="addInfo"
												onchange="isAddress4(this)" placeholder="Additional Info">
										<span id="isAddress4" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Medical
											Sent By</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.medSentBy}" id="medSentBy"
											onchange="isLetter1(this)"	placeholder="Medical Sent By">
										<span id="isLetter1" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Medical
											Wellbill No.</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.medBillNo}" id="medBillNo"
											onchange="isPhoner5(this)"	placeholder="Medical Waybill No.">
										<span id="isPhoner5" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Visa/Permit
											Sent BY</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.visaSentBy}" id="visaSentBy"
											onchange="isLetter2(this)"	placeholder="Visa/Permit Sent By">
										<span id="isLetter2" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Visa/Permit
											Waybill No.</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.visaWaybillNo}" id="visaWaybillNo"
											onchange="isPhoner6(this)"	placeholder="Visa/Permit Waybill No.">
										<span id="isPhoner6" style="color:red"></span>
										</div>
									</div>
								</div>
								<ul class="list-inline pull-left">
									<li>
										<button type="button" class="prvbtn btn btn-default prev-step">Previous</button>
									</li>
								</ul>
								<ul class="list-inline pull-right">
									<li>
										<button type="button"
											class="btn btn-warning btn-info-full next-step"
											onclick="saveThirdStep()">Save and continue</button>
									</li>
								</ul>
							</div>
							<div class="tab-pane" role="tabpanel" id="step4">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">PR Issue
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="prIssueDate"
												value="${clt.prIssueDate}" placeholder="PR Issue Date">
										</div>
										<label class="control-label col-md-2 lblfnt">PR
											Validity Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="prValidityDate"
												value="${clt.prValidityDate}" placeholder="PR Validity Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Landing
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="landingDate"
												value="${clt.landingDate}" placeholder="Landing Date">
										</div>
										<label class="control-label col-md-2 lblfnt">Citizenship
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="citizenshipDate"
												value="${clt.citizenshipDate}" placeholder="Citizenship Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Employer</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.employer}" id="employer"
											onchange="isAlphabet8(this)"	placeholder="Employer">
										<span id="isAlphabet8" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Employer
											Location</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.employerLocation}" id="employerLocation"
											onchange="isAddress5(this)"	placeholder="Employer Location">
										<span id="isAddress5" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">LMO
											Number</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.lmoNumber}" id="lmoNumber"
												onchange="isZibcode1(this)"placeholder="LMO Number">
										<span id="isZibcode1" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">WP Issue
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="wpIssueDate"
												value="${clt.wpIssueDate}" placeholder="WP Issue Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">WP
											Validity Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="wpValidityDate"
												value="${clt.wpValidityDate}" placeholder="WP Validity Date">
										</div>
										<label class="control-label col-md-2 lblfnt">WP
											Interview Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="wpInterviewDate"
												value="${clt.wpInterviewDate}" placeholder="WP Interview Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">WP
											Extension Granted Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="wpExtGrantDate"
												value="${clt.wpExtGrantDate}" placeholder="WP Extension Granted Date">
										</div>
										<label class="control-label col-md-2 lblfnt">WP
											Interview Waiver Reciept Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="wpInterviewWaiverReciptDate"
												value="${clt.wpInterviewWaiverReciptDate}" placeholder="WP Interview Waiver Reciept Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Temp.
											Renewals and Extensions</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" value="${clt.tempRenExt}" id="tempRenExt"
											onchange="isAddress6(this)"	placeholder="Temp. Renewals and Extensions">
										<span id="isAddress6" style="color:red"></span>
										</div>
										<label class="control-label col-md-2 lblfnt">Arrival
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="arrivalDate"
												value="${clt.arrivalDate}" placeholder="Arrival Date">
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Departure
											Date</label>
										<div class="col-md-4">
											<input type="text" class="form-control datepicker" name="" id="departureDate"
												value="${clt.departureDate}" placeholder="Departure Date">
										</div>
									</div>
								</div>
								<ul class="list-inline pull-left">
									<li>
										<button type="button" class="prvbtn btn btn-default prev-step">Previous</button>
									</li>
								</ul>
								<ul class="list-inline pull-right">
									<li>
										<button type="button" class="btn btn-warning next-step"
											onclick="saveFourthStep()">Save and continue</button>
									</li>
								</ul>
							</div>
							<div class="tab-pane" role="tabpanel" id="step5">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered table-striped">
											<thead>
												<tr>
													<th class="tblthd">Type</th>
													<th class="tblthd">Coordinator</th>
												</tr>
											</thead>
											<tbody class="tbdy">
												<tr>
													<td>SALES</td>
													<td><select id="sales" class="form-control">
															<option>Select Sales Coordinator</option>
															<c:forEach var="salco" items="${salemp}">
												<option value="${salco.e_id}">${salco.emp_firstname}</option>
												</c:forEach>
													</select></td>
												</tr>
												<tr>
													<td>RELATIONSHIP MANAGER</td>
													<td><select id="relationshipManager" name="relationshipManager" class="form-control">
															<option>Select relationship manager</option>
															<c:forEach var="relation" items="${relatoinmanList}">
												<option value="${relation.e_id}">${relation.emp_firstname}</option>
												</c:forEach>
													</select></td>
												</tr>
												<tr>
													<td>PROCESSING OFFICER</td>
													<td><select id="processingOfficer" class="form-control">
															<option>Select processing Officer </option>
															<c:forEach var="process" items="${processingemp}">
												<option value="${process.e_id}">${process.emp_firstname}</option>
												</c:forEach>
													</select></td>
												</tr>						 <tr>
										<td>PROCESSING COORDINATOR</td>
														<td><select id="processingCoordinator" class="form-control">
															<option>Select Processing Coordinator</option>
															<c:forEach var="processcor" items="${processingempcor}">
												<option value="${processcor.e_id}">${processcor.emp_firstname}</option>
												</c:forEach>
													</select></td>
												</tr>
												<tr>
													<td>PROCESSING CONSULTANT</td>
													<td><select id="processingConsultant" class="form-control">
															<option>Select Processing Consultant</option>
															<c:forEach var="processconsult" items="${processingConsultantemp}">
												<option value="${processconsult.e_id}">${processconsult.emp_firstname}</option>
												</c:forEach>
													</select></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<ul class="list-inline pull-left">
									<li>
										<button type="button" class="prvbtn btn btn-default prev-step">Previous</button>
									</li>
								</ul>
								<ul class="list-inline pull-right">
									<li>
										<button type="button" class="btn btn-warning next-step"
											onclick="saveFifthStep()">Save and continue</button>
									</li>
								</ul>
							</div>
							<div class="tab-pane" role="tabpanel" id="complete">
								<div class="col-md-12">
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Client
											Category</label>
										<div class="col-md-4">
											<select id="clientCategory" class="form-control">
												<option>Select Client Category</option>
												<option id="category1">category1</option>
												<option id="category2">category2</option>
												<option id="category3">category3</option>
											</select>
										</div>
										<label class="control-label col-md-2 lblfnt">Client
											Status</label>
										<div class="col-md-4">
											<select id="clientStatus" class="form-control">
												<option>Client Status</option>
												<option id="registered">Registered</option>
												<option id="process started"> Process Started</option>
												<option id="unpaid"> Unpaid</option>
												<option id="closing"> Closing</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-2 lblfnt">Source</label>
										<div id="source" class="col-md-4">
											<select class="form-control">
												<option>Select Source</option>
												<option id="source1">source1</option>
												<option id="source2">source2</option>
												<option id="source3">source3</option>
											</select>
										</div>
										<label class="control-label col-md-2 lblfnt">Closing</label>
										<div class="col-md-4">
											<input type="text" class="form-control" name="" id="closing"
										onchange="isAlphabet9(this)"		placeholder="Closing" value="${clt.closing}">
										<span id="isAlphabet9" style="color:red"></span>
										</div>
									</div>
								</div>
								<ul class="list-inline pull-left">
									<li>
										<button type="button" class="prvbtn btn btn-default prev-step">Previous</button>
									</li>
								</ul>
								<ul class="list-inline pull-right">
									<li>
										<button type="button"
											class="btn btn-warning btn-info-full next-step" onclick="saveFinalStep()">Finish</button>
									</li>
								</ul>
							</div>
						</div>
						<div class="clearfix"></div>
				</div>
				</form>
				<form action="addClient.mm" method="post" id="preForm">
				<input type="hidden" name="prePop" id="prePop" value="" />
				</form>
		</div>
		</section>
		<div class="clearfix"></div>
	</div>
</div>
</div>
</body>