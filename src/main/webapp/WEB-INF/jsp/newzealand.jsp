<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Croyez - New zealand</title>

<!-- Bootstrap -->
<link href="css/service.css" type="text/css" rel="stylesheet">
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Lato|Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
 
<script>            
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.crunchify-top').fadeIn(duration);
			} else {
				jQuery('.crunchify-top').fadeOut(duration);
			}
		});
 
		jQuery('.crunchify-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
</script>
<script src="js/prefixfree.min.js"></script>
<style>
/* Carousel */
ul.nav li.dropdown:hover > ul.dropdown-menu {
    display: block;    
}

#quote-carousel .carousel-control {
	background: none;
	color: #CACACA;
	font-size: 2.3em;
	text-shadow: none;
	margin-top: 30px;
}
#quote-carousel .carousel-control.left {
	left: -60px;
}
#quote-carousel .carousel-control.right {
	right: -60px;
}
#quote-carousel .carousel-indicators {
	right: 50%;
	top: auto;
	bottom: 0px;
	margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
	width: 50px;
	height: 50px;
	margin: 5px;
	cursor: pointer;
	border: 4px solid #CCC;
	border-radius: 50px;
	opacity: 0.4;
	overflow: hidden;
	transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
	background: #333333;
	width: 128px;
	height: 128px;
	border-radius: 100px;
	border-color: #f33;
	opacity: 1;
	overflow: hidden;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
 .crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
 
.crunchify-top {
	display: none;
	position: fixed;
	bottom: 1rem;
	right: 1rem;
	width: 3.2rem;
	height: 3.2rem;
	line-height: 3.2rem;
	font-size: 1.4rem;
	color: #fff;
	background-color: rgba(0,0,0,0.3);
	text-decoration: none;
	border-radius: 3.2rem;
	text-align: center;
	cursor: pointer;
}
.panel-heading {
    padding: inherit;
    border-bottom: inherit;
	font-family: 'Open Sans', sans-serif;
font-size: 17px;
}
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
.carousel-caption {
	bottom: 40px !important;
	padding-bottom: 0px !important;
}

</style>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body>
<a href="#" class="crunchify-top">↑</a>
<!--------------reaponsive nav------------------->
 <div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="https://in.linkedin.com/in/croyez-immigration-823671124
" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph">+91 9543690385</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
             <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="homePage.mm"><img src="images/demo-new-site/croyez2.png" width="250" class="img-responsive"></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="homePage.mm">Home</a></li>
            <li><a href="about.mm">About Us</a></li>
            <li class="dropdown"><a href="canada.mm" class="dropdown-toggle" data-toggle="dropdown">Canada<b class="caret"></b></a>
          <ul class="dropdown-menu">
           <li><a href="canada.mm">Canada</a></li>
              <li><a href="expressentry.mm">Express Entry</a></li>
              <li><a href="qubecskiled.mm">Quebec Skilled </a></li>
              <li><a href="familysponsorship.mm">Family Sponsorship </a></li>
              <li><a href="businessimmigration.mm">Business Immigration</a></li>
              <li><a href="provincialnominee.mm">Provincial Nominee </a></li>
            </ul>
          </li>
            <li><a href="australia.mm">Australia</a></li>
            <li class="dropdown active"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Immigration <b class="caret"></b> </a>
            <ul class="dropdown-menu">
                <li><a href="#">Germany</a></li>
                <li class="active"><a href="#">New zealand</a></li>
                <li><a href="#">Hong Kong</a></li>
                <li><a href="#">Ecuador</a></li>
              </ul>
          </li>
            <li><a href="blog.mm">Blog</a></li>
            <li><a href="career.mm">Careers</a></li>
            <li><a href="contact.mm">Contact</a></li>
            <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
          </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
            <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" />
              </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
          </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav> 
 
<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-12 mainmrgn">
        <div class="row">
          <div class="col-md-5">
            <div class="row">
              <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active"> <img src="images/countrys/newzealand/newbg1.png"
 alt="Chania" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Settle In New zealand</h3>
                      <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                    </div>
                  </div>
                  <div class="item"> <img src="images/countrys/newzealand/newbg2.png"
 alt="Chania" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Work</h3>
                      <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                    </div>
                  </div>
                  <div class="item"> <img src="images/countrys/newzealand/newbg3.png"
 alt="Flower" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Study</h3>
                      <p>Beatiful flowers in Kolymbari, Crete.</p>
                    </div>
                  </div>
                </div>
                
                <!-- Left and right controls --
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>--> 
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <h3 class="srvched">Why New Zealand?</h3>
            <p class="srvcpara">Discover why New Zealand is not just a great place to work but how we Kiwis enjoy life outside of work, which we call work-life balance. It is also a fantastic environment to bring up children as well as feeling safe and secure..New Zealand is around the same size as Japan or Great Britain.  New Zealand has just over four million people, know affectionately as 'Kiwis', who are easy going, warm and welcoming to their neighbours and to those who travel to experience all that is New Zealand. New Zealand also has an international reputation as a provider of quality education. </p>
          </div>
          <div class="col-md-12">
      <div class="row">
      <div class="col-md-6">
      <div class="col-md-12">
       <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Safe yet modern: This country of just over 4 million people is an easy going and one of the safest places on this earth with high quality living conditions and a modern lifestyle.</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; British based education system:The New Zealand education programs and degrees are based on the worlds most recognized and accredited education system- The British System- without the same expense.</p>
      </div>
      </div>
      <div class="col-md-6">
      <div class="col-md-12">
       <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Competitive Costs:New Zealand offers very affordable low cost of living represents a good value for your money compared with many other countries around the world.</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Balanced lifestyle: Get a career. And a life. New Zealand's work-life balance is envied around the world.For the record, New Zealand was rated second in the world for work-life balance in HSBC's 2015 Expat Explorer survey. </p>
      </div>
      </div>
      </div>
      </div>
        </div>
      </div>
    </div>
  </div>
</div>
 <div class="container-fluid recentbgb1">
 <div class="container">
 <h3 class="cndaswmhdr">Entrepreneur visas</h3>
  <h4 class="skilreq">  Investing in New Zealand is a smart business choice. And a lifestyle to match</h4>
<p class="entrvisa">If you're an experienced businessperson interested in being self-employed in your own business, our entrepreneur visas could be for you.New Zealand's business migration categories are designed to enable experienced business people to buy or establish businesses in New Zealand and contribute to economic growth</p>
 </div>
 <div class="col-md-12">
 <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#default1" data-toggle="tab">With this visa you can</a></li>
                            <li><a href="#default2" data-toggle="tab">Entry permission</a></li>
                            <li><a href="#default3" data-toggle="tab">Work</a></li>
                              <li><a href="#default4" data-toggle="tab">Capital investment</a></li>
                            <li><a href="#default5" data-toggle="tab">New jobs</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="default1">
                         <p class="skilreqparagrp">
             Continue to operate your own business in New Zealand. </p>	
             <p class="skilreqparagrp">Live, work and study in New Zealand.</p>
             <p class="skilreqparagrp">Include your partner, and dependent children aged 24 and under, in your visa application.</p>
             </div>
                        <div class="tab-pane fade" id="default2">
                         <p class="skilreqparagrp ">
           You must apply for entry permission when you arrive in New Zealand. You can do this by completing an arrival card, which you'll be given on the way to New Zealand.
                </p>
                        </div>
                        <div class="tab-pane fade" id="default3">
                         <p class="skilreqparagrp ">
               If you've been operating your business for less than 2 years, you must keep working in your own business for 2 years.

                </p>
                        </div>
                        <div class="tab-pane fade" id="default4">
                         <p class="skilreqparagrp ">
                If you've been operating your business for less than 2 years, you must keep your nominated capital invested in your business for 2 years.
                </p>
                        </div>
                        <div class="tab-pane fade" id="default5">
                         <p class="skilreqparagrp ">
               If you've operated your business for less than 2 years, you must continue to employ people in the new jobs created, for a minimum of 2 years.
                </p>
                        </div>
                    </div>
                </div>
            </div>
 </div>
 <div class="col-md-12">
                <div class="row">
                <div class="col-md-6">
                <h4 class="skilreq">Requirements for this visa</h4>
                <p class="nezalndrq">Minimum capital investment of NZ$100,000 (excluding working capital)</p>
                <p class="nezalndrq">120 points or more, with points awarded for factors about the likely success of the business and its value to New Zealand</p>
                <p class="nezalndrq">Clear business plan</p>
                <p class="nezalndrq">Clean recent history of bankruptcy, business failure and fraud</p>
                <p class="nezalndrq">Health, character and English language requirements.</p>
                </div>
                <div class="col-md-6">
                <h4 class="skilreq">What are the Key Benefits for applicants under the Entrepreneur Policies?</h4>
                <p class="nezalndrq">You can chose a business that matches your entrepreneurial background</p>
                <p class="nezalndrq">Your business can be located anywhere in New Zealand</p>
                <p class="nezalndrq">You will not have to find a job and overcome the Catch22 hurdle</p>
                
                <p class="nezalndrq">Relatively low minimum investment of NZ$100.00 required </p>
                
                <p class="nezalndrq">Minimum investment can be waived</p>
                
                <p class="nezalndrq">Your partner will obtain an open Work Visa that entitles him/her to work for any business in New Zealand</p>
                
                <p class="nezalndrq">Your partner can also be self employed</p>
                
                <p class="nezalndrq">Primary and secondary school children will be classified as domestic students, so that high international student fees can be avoided</p>
                <p class="nezalndrq">No age limit</p>
                <p class="nezalndrq">Relatively low English language requirements.</p>
                </div>
                </div>
                </div>
 </div>
 <div class="container-fluid">
 <div class="container">
 <h4 class="prcss">The process: Your way to residence in New Zealand will be in two steps</h4>
 </div>
 <div class="col-md-12">
 
                <div class="row">
                <div class="col-md-6">
                <h4 class="nezreq">Step 1. The Entrepreneur Work Visa</h4>
                <p class="sklrqpara">Establish a business or buy a business that suits your expectations and experience</p>
                <p class="sklrqpara">Write a business plan which will satisfy Immigration New Zealand</p>
                <p class="sklrqpara">Prepare and lodge your Entrepreneur Work Visa application according to New Zealand immigration law requirements.</p>
                </div>
                <div class="col-md-6">
                <h4 class="nezreq">Step 2:  The Entrepreneur Residence Visa </h4>
                <p class="sklrqpara">After you have run your business successfully for at least two years, you Can apply for a Entrepreneur Residence Visa. There is a fast track option: You can apply after six months, provided you have invested at least NZ $ 500,000 in your business and created at least three full time jobs for New Zealanders (investment and employment must be maintained for two years). </p>
                </div>
                </div>
                </div>
                <div class="col-md-12">
                <div class="table-responsive">
                <table class="table table-bordered table-hover">
                <thead>
                <tr>
                <th colspan="2" class="tblbl">Business Qualification</th>
                </tr>
                <tr>
                <th colspan="2 " class="tblbl">Business criteria to be met</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td>
                <label class="control-label tblbl">Successful Establishment</label>
                </td>
                <td>
                <label class="control-label tblbl">You have successfully established a business in New Zealand if</label>
                <ul class="tblcnt">
                <li>•	You have established or purchased, or made a substantial investment (a minimum of 25% of the shareholding of a business) in a business operating in New Zealand;</li>
                <li>•	The business has been established for at least 2 years; and</li>
                <li>•	You have been lawfully working in New Zealand in that business for at least 2 years.</li>
                </ul>
                </td>
                </tr>
                 <tr>
                <td>
                <label class="control-label tblbl">Beneficial to New Zealand</label>
                </td>
                <td>
                <label class="control-label tblbl">Your business must be benefiting New Zealand's economic growth by</label>
                <ul class="tblcnt">
                <li>•	Introducing new or enhancing existing technology, management or technical skills;</li>
                <li>•	Introducing new or enhancing existing products, services or export markets;</li>
                <li>•	Creating new or expanding existing export markets;</li>
                <li>•	Creating new job opportunities; or</li>
                <li>•	Revitalising an existing business.</li>
                </ul>
                </td>
                </tr>
                </tbody>
                </table>
                </div>
                </div>
 </div>
 <div class="container-fluid recentbgb">
 <div class="container">
 <h3 class="cndaswmhdr">Skilled Immigration</h3>
  <h4 class="skilreq">  Investing in New Zealand is a smart business choice. And a lifestyle to match</h4>
<p class="entrvisa">We invite people who have the skills to contribute to New Zealand's economic growth to apply for this visa. </p>
 </div>
 <div class="container">
 <h4 class="sklnez">NZ Skilled Visa Requirements?</h4>
 <p class="sklnzpara">You no longer need to have a job offer or sponsorship to be eligible for a New Zealand Skilled Migration Visa, but if you do have a job from a New Zealand employer, you will get additional points to your visa application.
To be eligible for this New Zealand Work Visa you will need score at least 100 points in the skills test. Visit the Eligibility tab for more information. The basic requirements are:
</p>
 </div>
 <div class="col-md-12">
 <div class="row">
 <div class="col-md-6">
 <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Age - must be under 56 years old.</p>
  <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">You must meet the character and health requirements.</p>
   <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Occupation - must be on the skills list.</p>
 </div>
 <div class="col-md-6">
  <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Qualifications - degree or certificates in your occupation</p>
   <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Experience - at least 3 years experience relevant to your qualification.</p>
    <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">English language - must be fluent in English.</p>
 </div>
 </div>
 </div>
  <div class="container">
        <div class="row">
        <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading1">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">How is Your Application Assessed
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                                <div class="panel-body">
                                    <p class="skilreqparagrp">Your eligibility for a New Zealand Skilled Migration Visa is based on a skills test in which you must score at least 100 points to be able to submit your expression of interest. You get extra points if you have a partner who also wants to migrate to New Zealand and has enough points	</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">Points in the Skills Test are Based on
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                <div class="panel-body">
                                   <p class="skilreqparagrp">Employability</p>
                                   <p class="skilreqparagrp">Work experience</p>
                                   <p class="skilreqparagrp">Qualifications</p>
                                   <p class="skilreqparagrp">Family ties</p>
                                   <p class="skilreqparagrp">Age</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading3">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">With this visa you can
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                <div class="panel-body">
                                   <p class="skilreqparagrp">Live, work and study in New Zealand</p>
                                   <p class="skilreqparagrp">Include your partner, and dependent children aged 24 and under, in your residence application.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading4">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">Things to note
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                <div class="panel-body">
                                   <p class="skilreqparagrp">We use a points-based system to assess expressions of interest</p>
                                   <p class="skilreqparagrp">This visa isn't designed for people who are self-employed. If you want to work in your own business, you may wish to apply for an Entrepreneur Resident Visa instead.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
 </div>
 <div class="container-fluid spousebg">
 <div class="container">
  <h3 class="cndaswmhdr">Spouse</h3>
  <p class="spousepra">If your ultimate goal is to live together permanently in NZ, the overseas partner will need to obtain a resident visa. There is a specific residence category available to such applicants, called Partnership policy.</p>
 </div>
 <div class="col-md-12">
 <h4 class="sklnez">To be eligible you will need to prove you</h4>
 <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Are living together (and have been for a minimum of 12 months) in a genuine and stable relationship;</p>
  <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Are both aged 18 years or older (or can provide evidence of parental/guardian/other consent if either of you are 16 or 17 years of age);</p>
   <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Met each other prior to your application being made; and</p>
   <p class="nezalndrq"><img src="images/demo-new-site/icons/login.png">Are not close relatives.</p>
 </div>
 </div>
 <div class="container-fluid">
 <div class="container">
  <h3 class="cndaswmhdr">Investor Visa</h3>
  <p class="invet">If you are wishing to invest funds in to a business in New Zealand you can apply for residence with a New Zealand investment visa under the following categories; Investor and Investor Plus. You are also able to come to New Zealand for a period of two years under the Temporary Retirement Category.</p>
 </div>
 <div class="col-md-12">
 <div class="row">
 <div class="col-md-1"></div>
 <div class="col-md-10">
 <div class="table-responsive">
 <table class="table  table-striped">
 <thead>
 <tr>
 <th class="tblbl"><label class="control-label">-</label></th>
 <th class="tblbl"><label class="control-label">Investor Plus</label></th>
 <th class="tblbl"><label class="control-label">Investor</label></th>
 </tr>
 </thead>
 <tbody>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Minimum investment funds</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">NZ$ 10 million invested in NZ for threeyears</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">NZ$ 1.5 million invested in NZ for fouryears</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Business experience requirement</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">No requirement</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">Minimum three years</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Maximum age</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">No requirement</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">65 or younger</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Settlement funds</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">No requirement</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">NZ$ 1 million (transfer not required)</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Principal applicant's English language ability</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">No requirement</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght"><p>English speaking background or;</p>
 <p>International English Language Testing System (IELTS) test score 3+ or;</p>
 <p>Competent user of English</p>
</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Family member's English language ability</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">No requirement</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">Same as principal applicant or pre-purchase ESOL tuition</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Minimum time in NZ</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">44 days in NZ in each of the last two years of the three-year investment period</label></td>
 <td class="tblcnt1"><label class="control-label tblfnwght">146 days in NZ in each of the last three years of the four-year investment period</label></td>
 </tr>
 <tr>
 <td class="tblcnt1"><label class="control-label tblfnwght">Health and character</label></td>
 <td class="tblcnt1" colspan="2"><label class="control-label tblfnwght">All applicants must meet requirements</label></td>
 </tr>
 </tbody>
 </table>
 </div>
 </div>
 <div class="col-md-1"></div>
 </div>
 </div>
 </div>


<!-----footer------>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/croyez.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">To Know more about Croyez and get updates about immigration services kindly follow us on social websites.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('f','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="f"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('3','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="3"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://in.linkedin.com/in/croyez-immigration-823671124" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('4','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="4"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('5','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="5"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> 

2floor ,sons complex,near sangamam hotel,Ashok Nagar, chennai - 600083
</p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">

+91 9543690385
</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">

info@croyezimmigration.com
</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.mm" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.mm" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.mm" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Testimonial </h4>
            </div>
            <div class="row">
              <div class="col-md-12 column">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel"> 
                  <!-- Bottom Carousel Indicators --> 
                  
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner text-center"> 
                    
                    <!-- Quote 1 -->
                    <div class="item active">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 2 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 3 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--1--> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">&#169; 2016 Croyez. All rights reserved</h3>
</div>
<!-- <script>
     videojs.autoSetup();

    videojs('my_video_1').ready(function(){
      console.log(this.options()); //log all of the default videojs options
      
       // Store the video object
      var myPlayer = this, id = myPlayer.id();
      // Make up an aspect ratio
      var aspectRatio = 264/640; 

      function resizeVideoJS(){
        var width = document.getElementById(id).parentElement.offsetWidth;
        myPlayer.width(width).height( width * aspectRatio );

      }
      
      // Initialize resizeVideoJS()
      resizeVideoJS();
      // Then on resize call resizeVideoJS()
      window.onresize = resizeVideoJS; 
    });
    </script>  -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>