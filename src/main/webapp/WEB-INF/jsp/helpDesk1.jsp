<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Helpdesk</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Raleway|Noto+Sans' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    
	<link href="css/mainstyle.css" type="text/css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Second navbar for search -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand" href="dashBoard.mm"><img src="images/logo-3.png" width="100px" class="img-responsive"></a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-3">
          <ul class="nav navbar-nav">
        <li class="nvmnu"><a href="dashBoard.mm">My Dashboard</a></li>
        <li class="nvmnu"><a href="myInfo.mm">My Info</a></li>
        <li class="nvmnu"><a href="referFriend.mm">Refer a Friend</a></li>
        <li class="nvmnu"><a href="myDocumemts.mm">My Documents</a></li>
        <li class="nvmnu"><a href="evaluation.mm">Evaluation Report</a></li>
        <li class="nvmnu"><a href="retainerAgreement.mm">Retainer Agreement</a></li>
        <li class="nvmnu"><a href="fileDetails.mm">File Details</a></li>
        <li class="active active8 nvmnu"><a href="helpDesk.mm" class="ex3">Helpdesk</a></li>
          </ul>
           <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img style="margin: auto; height: 34px; width: 34px;border-radius: 50%;"  src="image.mm?id=${user.userId}"> ${user.fName} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
            <li><a href="myInfo.mm"> Profile</a></li>
             <li><a href="logout.mm">Logout</a></li>
			  </ul>
			 </li>
          </ul>
          <div class="collapse nav navbar-nav nav-collapse slide-down" id="nav-collapse3">
            <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" />
              </div>
              <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
          </div>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
	<!-------------main body----------->
	<div class="container-fluid">
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-1 side">
    <h3>side bar</h3>
    </div>
    <div class="col-md-11">
      <div class="panel panel-default pnlbdr8">
					<div class="panel-heading pnlhdr8">Helpdesk</div>
					<div class="panel-body">
                    <form class="form-horizontal">
                    <div class="form-group">
                    <div class="col-md-1"></div>
                    <div class="col-md-8">
                    <p class="ablty">Once you start the process you will get acccess to helpdesk department.</p>
                    </div>
                    <div class="col-md-2">
                    <a href="#" class="btn btn-info">Talk to Expert</a>
                    </div>
                    </div>
                    </form>
					</div>
				</div>
    </div>
    </div>
    </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>