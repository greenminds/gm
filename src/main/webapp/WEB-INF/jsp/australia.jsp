<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Croyez - Australia</title>

<!-- Bootstrap -->
<link href="css/service.css" type="text/css" rel="stylesheet">
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Lato|Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
 
<script>            
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.crunchify-top').fadeIn(duration);
			} else {
				jQuery('.crunchify-top').fadeOut(duration);
			}
		});
 
		jQuery('.crunchify-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
</script>
<script src="js/prefixfree.min.js"></script>

  <style>
  /* Carousel */


#quote-carousel .carousel-control {
	background: none;
	color: #CACACA;
	font-size: 2.3em;
	text-shadow: none;
	margin-top: 30px;
}
#quote-carousel .carousel-control.left {
	left: -60px;
}
#quote-carousel .carousel-control.right {
	right: -60px;
}
#quote-carousel .carousel-indicators {
	right: 50%;
	top: auto;
	bottom: 0px;
	margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
	width: 50px;
	height: 50px;
	margin: 5px;
	cursor: pointer;
	border: 4px solid #CCC;
	border-radius: 50px;
	opacity: 0.4;
	overflow: hidden;
	transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
	background: #333333;
	width: 128px;
	height: 128px;
	border-radius: 100px;
	border-color: #f33;
	opacity: 1;
	overflow: hidden;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
  .crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
 
.crunchify-top {
	display: none;
	position: fixed;
	bottom: 1rem;
	right: 1rem;
	width: 3.2rem;
	height: 3.2rem;
	line-height: 3.2rem;
	font-size: 1.4rem;
	color: #fff;
	background-color: rgba(0,0,0,0.3);
	text-decoration: none;
	border-radius: 3.2rem;
	text-align: center;
	cursor: pointer;
}
.crslft {
	top: inherit !important;
	bottom: 50%;
}
.crslhght {
	height:700px;
}
.tct {
	height:inherit !important;
}
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
.carousel-caption {
	bottom: 40px !important;
	padding-bottom: 0px !important;
}
.carousel-inner.onebyone-carosel { margin: auto; width: 90%; }
.onebyone-carosel .active.left { left: -25%; }
.onebyone-carosel .active.right { left: 25%; }
.onebyone-carosel .next { left: 25%; }
.onebyone-carosel .prev { left: -25%; }

.text-center {
    text-align: center;
    height: 240px;
}
.carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next, .carousel-control .icon-prev {
	top: 10px;
}
.panel-heading {
    padding: inherit;
    border-bottom: inherit;
	font-family: 'Open Sans', sans-serif;
font-size: 17px;
}
@media screen and (max-width:880px){

.svrcrolwdth {
	width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
    display: none !important;
}
.carousel-control {
	height:inherit !important;
}
}
@media screen and (max-width:768px){

.svrcrolwdth {
	width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
    display: none !important;
}
.carousel-control {
	height:inherit !important;
}
}
@media screen and (max-width:360px){

.svrcrolwdth {
	width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
    display: none !important;
}
.carousel-control {
	height:inherit !important;
}
}
@media screen and (max-width:320px){

.svrcrolwdth {
	width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
    display: none !important;
}
.carousel-control {
	height:inherit !important;
}
}

ul.nav li.dropdown:hover > ul.dropdown-menu {
    display: block;    
}
</style>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body>
 <a href="#" class="crunchify-top">↑</a>
<!--------------reaponsive nav------------------->
 <div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="https://in.linkedin.com/in/croyez-immigration-823671124
" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph">+91 9543690385</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
             <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="index.mm"><img src="images/demo-new-site/croyez2.png" width="250" class="img-responsive"></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="homePage.mm">Home</a></li>
            <li><a href="about.mm">About Us</a></li>
             <li class="dropdown"><a href="canada.mm" class="dropdown-toggle" data-toggle="dropdown">Canada<b class="caret"></b></a>
          <ul class="dropdown-menu">
           <li><a href="canada.mm">Canada</a></li>
              <li><a href="expressentry.mm">Express Entry</a></li>
              <li><a href="qubecskiled.mm">Quebec Skilled </a></li>
              <li><a href="familysponsorship.mm">Family Sponsorship </a></li>
              <li><a href="businessimmigration.mm">Business Immigration</a></li>
              <li><a href="provincialnominee.mm">Provincial Nominee </a></li>
            </ul>
          </li>
            <li class="active"><a href="australia.mm">Australia</a></li>
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Immigration <b class="caret"></b> </a>
            <ul class="dropdown-menu">
                <li><a href="#">Germany</a></li>
                <li><a href="newzealand.mm">New zealand</a></li>
                <li><a href="#">Hong Kong</a></li>
                <li><a href="#">Equador</a></li>
              </ul>
          </li>
            <li><a href="blog.mm">Blog</a></li>
            <li><a href="career.mm">Careers</a></li>
            <li><a href="contact.mm">Contact</a></li>
            <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
          </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
            <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" />
              </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
          </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav> 
 
<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-12 mainmrgn">
        <div class="row">
          <div class="col-md-5">
            <div class="row">
              <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active"> <img src="images/countrys/australia/ausrtbg1.png"
 alt="Chania" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Settle In Australia</h3>
                      <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                    </div>
                  </div>
                  <div class="item"> <img src="images/countrys/australia/ausrtbg2.png"
 alt="Chania" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Work</h3>
                      <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                    </div>
                  </div>
                  <div class="item"> <img src="images/countrys/australia/ausrtbg3.png" alt="Flower" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Study</h3>
                      <p>Beatiful flowers in Kolymbari, Crete.</p>
                    </div>
                  </div>
                </div>
                
                <!-- Left and right controls --
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>--> 
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <h3 class="srvched">Why Australia?</h3>
            <p class="srvcpara">Australia is one among the top immigration destinations in the world.Australia is a culturally diverse, racially tolerant society that welcomes and offers opportunity to those who wish to settle here. Since 1945, nearly 6.5 million people from over 150 countries have come to call Australia home. Currently Australia has a population of 23 million people, but this number is growing constantly. Approximately 1 in 4 Australian residents are born outside Australia.There was a time when majority of migrants in Australia came from European countries but now the scenario has changed. Now Australia attracts people mainly from Asia mainly China and India because Australia is one of the most prospering countries for immigration.</p>
          </div>
        </div>
      </div>
      <div class="col-md-12">
      <div class="row">
      <div class="col-md-6">
      <div class="col-md-12">
       <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Long tradition of welcoming emigrants.</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Friendly, familiar, English speaking culture.</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Low cost of living, especially the cost of property</p>
      </div>
      </div>
      <div class="col-md-6">
      <div class="col-md-12">
       <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Great job opportunities, especially for tradespersons and workers with jobs on the MODL and SOL.</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; World-class education and a great place to raise a family.</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; Plenty of Australian visa options for skilled migrants.</p>
      </div>
      </div>
      </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid recentbgb">
 <div class="col-md-12">
 <h3 class="cndaswmhdr">Reasons To Move To Australia</h3>
 </div>
   <div class="container">
        <div class="row">
            <div class="span12">
                    <div id="myCarousel" class="carousel fdi-Carousel slide">
                     <!-- Carousel items -->
                        <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
                            <div class="carousel-inner onebyone-carosel">
                                <div class="item active">
                                    <div class="col-md-3">
                                       <img src="images/countrys/australia/fresh1.png"
 class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Fresh Air</h4>
                                        <p class="rncntpara">Australia has one of the lowest air pollution levels in the world.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/unspoilt2.png"
 class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Unspoilt Nature</h4>
                                        <p class="rncntpara">Australia has a large variety of beautiful natural ecosystems. We have golden sandy beaches with clean water, tropical rainforests, arid deserts and snowy mountains. There are over 500 national parks in Australia and 14 world heritage areas</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/society.png"
 class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Multicultural Society</h4>
                                        <p class="rncntpara">Australia is truly a multicultural society. 43% of Australians were either born overseas or have a parent who was born overseas. Each year we celebrate our diversity on Harmony Day (21 March).</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                       <img src="images/countrys/australia/low.png"
 class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Low population density</h4>
                                        <p class="rncntpara">Australia has the lowest population density of any continent in the world with only 6.4 people per square mile.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/climate.png" class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Climate</h4>
                                        <p class="rncntpara">Australia has a temperate climate with mild weather all year round and more than its fair share of sunshine.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                       <img src="images/countrys/australia/health.png" class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Healthcare system</h4>
                                        <p class="rncntpara">Australia has one of the most efficient healthcare systems in the world. Medicare provides basic health cover for all Australians and there is a private healthcare system as well. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/jobs.png" class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Job opportunities</h4>
                                        <p class="rncntpara">Australians pride themselves on perfecting a work-life balance. With a strong economy and low unemployment rate (around 5%) Australia is a good place to find a new job.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/liftstyle.png"
 class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Laid back lifestyle</h4>
                                        <p class="rncntpara">Australians have a reputation for being laid back and friendly and what's more it is actually true. People are much more likely to give you the time of day in Australian cities compared with other cities around the world. There's also a comparatively low crime rate.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/greateplace.png" class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Great place to travel</h4>
                                        <p class="rncntpara">
                                        Australia is such a vast country that you can spend all your holidays exploring it. Domestic travel has it all from rainforests, beaches, to ski-resorts and stunning ancient landscapes. 
                                        </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-3">
                                        <img src="images/countrys/australia/education.png"
 class="img-responsive center-block">
                                        <div class="text-center">
                                        <h4 class="mtoaust">Great opportunities for study</h4>
                                        <p class="rncntpara">
                                        Australia has an impressive number of study options for international students with more than 1,200 institutions and 22,000 courses to choose from. Australia has the third highest number of international students in the world behind the UK and US even though we have a much smaller population than those countries.
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="crslft left carousel-control svrcrolwdth" href="#eventCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left hidden-xs"></span></a>
                            <a class="crslft right carousel-control svrcrolwdth" href="#eventCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right  hidden-xs"></span></a>
                        </div>
                        <!--/carousel-inner-->
                    </div><!--/myCarousel-->
            </div>
        </div>
</div>
   </div>
   
   <div class="container-fluid recentbgb1">
    <div id="custom_carousel" class="carousel slide" data-ride="carousel" data-interval="2500">
    <div class="controls">
            <ul class="nav" style="border: 1px solid;">
                <li data-target="#custom_carousel" data-slide-to="0" class="active"><a href="#" class="skilhdr">SKILLED - INDEPENDENT VISA(subclass 189)</a></li>
                <li data-target="#custom_carousel" data-slide-to="1"><a href="#" class="skilhdr">SKILLED - NOMINATED VISA(Subclass 190)</a> </li>
                <li data-target="#custom_carousel" data-slide-to="2"><a href="#" class="skilhdr">Skilled Regional Nominated (Provisional)<br> (Subclass 489)</a>
              
                </li>
                <li data-target="#custom_carousel" data-slide-to="3"><a href="#" class="skilhdr">Skilled Regional Subclass 887</a>
            
                </li>
            </ul>
        </div>
        <div class="col-md-12">
 <h3 class="cndaswmhdr">Skilled Immigration</h3>
 </div>
        <!-- Wrapper for slides -->
        <div class="carousel-inner crslhght">
            <div class="item active">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3"><img src="images/countrys/australia/189.png"
 class="img-responsive"></div>
                        <div class="col-md-9">
                            <h4 class="mtoaust">SKILLED - INDEPENDENT VISA(subclass 189)</h4>
                           <p class="skilpara">
                The Skilled - Independent Visa (subclass189) is the most popular skilled migration category is gearedto skilled workers who are in high demand and do not require a state or family sponsor.
                </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                <div class="row">
                <div class="col-md-3">
                <h4 class="skilreq">Requirements</h4>
                <p class="sklrqpara">Must not be more than fifty years of age</p>
                <p class="sklrqpara">Forward an intent of interest to Australian Immigration and Citizenship Department</p>
                <p class="sklrqpara">Possess a skill as mentioned in the list of skilled occupation as available with immigration department</p>
                <p class="sklrqpara">Possess relevant assessments of this skill</p>
                <p class="sklrqpara">Meet required English language knowledge as demanded by Immigration Department</p>
                <p class="sklrqpara">Conform to necessary character and health preconditions</p>
                <p class="sklrqpara">Must score 60 or more in points test conducted by this department</p>
                </div>
                <div class="col-md-6">
                <div class="col-md-12">
                <h4 class="skilreq"> Visa Process</h4>
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Stage 1</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Stage 2</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Stage 3</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                         <p class="skilreqparagrp">
               The first stage of the application process involves completing a skills assessment with the relevant government body. Your skills assessment enables the Department of Immigration and Border Protection (DIBP) to judge if you have the required level of skill in your chosen occupation to practice in Australia
                </p>
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                         <p class="skilreqparagrp ">
           Once you have received a positive skills assessment, you will need to submit an Expression of Interest through SkillSelect. You can do this in or outside Australia.
                </p>
                        </div>
                        <div class="tab-pane fade" id="tab3default">
                         <p class="skilreqparagrp ">
                Once you have been issued an Invitation To Apply (ITA), you can continue to the DIBP stage of the application, where you can submit your final, complete application to be approved by DIBP so your visa can be granted. 
                </p>
                        </div>
                    </div>
                </div>
            </div>
                </div>
               </div>
                <div class="col-md-3">
                <h4 class="skilreq">Benefits</h4>
                <p class="sklrqpara">Stay in Australia indefinitely</p>
                <p class="sklrqpara">Work and study in Australia</p>
                <p class="sklrqpara">Enrol in Medicare, Australia's health care scheme</p>
                <p class="sklrqpara">Apply for Australian citizenship once they are eligible</p>
                <p class="sklrqpara">Sponsor eligible relatives for permanent residence</p>
                <p class="sklrqpara">Travel to and from Australia for five years from the date the visa is granted (after that time, you will need a resident return visa or another visa to return to Australia).</p>
                </div>
                </div>
                </div>            
            </div> 
            <div class="item">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3"><img src="images/countrys/australia/190.png"
 class="img-responsive"></div>
                        <div class="col-md-9">
                             <h4 class="mtoaust">SKILLED - NOMINATED VISA(Subclass 190)</h4>
                            <p class="skilpara">The Skilled Nominated visa (subclass 190) is a permanent residence visa for points-test skilled workers who want to work and live in Australia after being nominated by an Australian state or territory government agency.</p>
                        </div>
                    </div>
                </div> 
                <div class="col-md-12">
                <div class="row">
                <div class="col-md-3">
                <h4 class="skilreq">Requirements</h4>
                <p class="sklrqpara">Not be more than 50 years of age at the time of issuance of invitation</p>
                <p class="sklrqpara">Must be nominated by Australian State/ Territory</p>
                <p class="sklrqpara">Forward an intent of interest for nomination</p>
                <p class="sklrqpara">Possess an occupational skill that is included in the occupation list (CSOL – Consolidated Skilled occupation list) published by Australian Immigration and Citizenship Department</p>
                <p class="sklrqpara">Have relevant skills assessment of declared occupation as in intent of interest</p>
                <p class="sklrqpara">Conform to required knowledge of English</p>
                <p class="sklrqpara">Satisfy character and health pre-requisites</p>
                <p class="sklrqpara">Score 60 in Skill Select points test</p>
                </div>
                <div class="col-md-6">
                <div class="col-md-12">
                <h4 class="skilreq"> Visa Process</h4>
                <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabdefault" data-toggle="tab">Stage 1</a></li>
                            <li><a href="#tabdefault" data-toggle="tab">Stage 2</a></li>
                            <li><a href="#tabdefault" data-toggle="tab">Stage 3</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tabdefault">
                         <p class="skilreqparagrp">
               The first stage of the application process involves completing a skills assessment with the relevant government body. Your skills assessment enables the Department of Immigration and Border Protection (DIBP) to judge if you have the required level of skill in your chosen occupation to practice in Australia.
                </p>
                        </div>
                        <div class="tab-pane fade" id="tabdefault">
                         <p class="skilreqparagrp ">
        Once you have received a positive skills assessment, you will need to submit an Expression of Interest through SkillSelect. You can do this in or outside Australia. You will then also need to apply for state sponsorship with an Australian state / territory. This will require that you meet the sponsorship requirements and have work experience in a position listed on one of their State Migration Plans. 
                </p>
                        </div>
                        <div class="tab-pane fade" id="tabdefault">
                         <p class="skilreqparagrp ">
              Once you have been issued an Invitation To Apply (ITA), you can continue to the DIBP stage of the application, where you can submit your final, complete application to be approved by DIBP so your visa can be granted. Priority processing arrangements apply to this visa. They determine the order in which the department considers applications regardless of when they have been lodged.
                </p>
                        </div>
                    </div>
                </div>
            </div>
                </div>
               </div>
                <div class="col-md-3">
                <h4 class="skilreq">Benefits</h4>
                  <p class="sklrqpara">Stay in Australia indefinitely</p>
                    <p class="sklrqpara">Work and study in Australia</p>
                      <p class="sklrqpara">Enrol in Medicare, Australia's scheme for health-related care and expenses</p>
                        <p class="sklrqpara">Apply for Australian citizenship (if you are eligible)</p>
                          <p class="sklrqpara">Sponsor eligible relatives for permanent residence</p>
                            <p class="sklrqpara">Travel to and from Australia for five years from the date the visa is granted (after that time, you will need a resident return visa or another visa to return to Australia).</p>
                </div>
                </div>
                </div>           
            </div> 
            <div class="item">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3"><img src="images/countrys/australia/487.png"
 class="img-responsive"></div>
                        <div class="col-md-9">
                            <h4 class="mtoaust">Skilled Regional Nominated (Provisional) (Subclass 489)</h4>
                          <p class="skilpara">
                The Skilled Regional (Provisional) Subclass 489 visa is a 4-year provisional visa which requires holders to live and work in a regional area to obtain permanent residence. To qualify, you would need be sponsored by either a relative living in a designated area or a State or Territory Government.
                </p>
                        </div>
                    </div>
                </div> 
                <div class="col-md-12">
                <div class="row">
                <div class="col-md-3">
                <h4 class="skilreq">Requirements</h4>
                 <p class="sklrqpara">Meet the general skilled migration requirements</p>
                    <p class="sklrqpara">Meet the required benchmark for the points assessment test</p>
                      <p class="sklrqpara">Nominate a skilled occupation from the relevant list</p>
                        <p class="sklrqpara">Be under 50 years of age</p>
                          <p class="sklrqpara">Submit a valid EOI</p>
                           <p class="sklrqpara">Await an invitation to apply for the visa</p>
                    <p class="sklrqpara">Obtain a suitable skills assessment</p>
                      <p class="sklrqpara">Nomination by a state/territory government or from an eligible relative living in a designated area</p>
                </div>
                <div class="col-md-6">
                <div class="col-md-12">
                <h4 class="skilreq"> Visa Process</h4>
                <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabrg1" data-toggle="tab">Stage 1</a></li>
                            <li><a href="#tabrg2" data-toggle="tab">Stage 2</a></li>
                            <li><a href="#tabrg3" data-toggle="tab">Stage 3</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tabrg1">
                         <p class="skilreqparagrp">
              The first stage of the application process involves completing a skills assessment with the relevant government body. Your skills assessment enables the Department of Immigration and Border Protection (DIBP) to judge if you have the required level of skill in your chosen occupation to practice in Australia.
                </p>
                        </div>
                        <div class="tab-pane fade" id="tabrg2">
                         <p class="skilreqparagrp ">
      Once you have received a positive skills assessment, you will need to submit an Expression of Interest through SkillSelect. You can do this in or outside Australia. You will then also need to apply for state sponsorship with an Australian state / territory. This will require that you meet the sponsorship requirements and have work experience in a position listed on one of their State Migration Plans.
                </p>
                        </div>
                        <div class="tab-pane fade" id="tabrg3">
                         <p class="skilreqparagrp ">
            Once you have been issued an Invitation To Apply (ITA), you can continue to the DIBP stage of the application, where you can submit your final, complete application to be approved by DIBP so your visa can be granted. Priority processing arrangements apply to this visa. They determine the order in which the department considers applications regardless of when they have been lodged. 
                </p>
                        </div>
                    </div>
                </div>
            </div>
                </div>
               </div>
                <div class="col-md-3">
                <h4 class="skilreq">Benefits</h4>
                 <p class="sklrqpara">Stay in Australia for up to four years</p>
                    <p class="sklrqpara">Work and study in a designated regional area of Australia</p>
                      <p class="sklrqpara">Bring eligible family members with you to Australia</p>
                        <p class="sklrqpara">Travel in and out of Australia while the visa is valid</p>
                          <p class="sklrqpara">Seek permanent residence by applying for a subclass 887 visa</p>
                </div>
                </div>
                </div>          
            </div> 
            <div class="item">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3"><img src="images/countrys/australia/887.png" class="img-responsive"></div>
                        <div class="col-md-9">
                            <h4 class="mtoaust">Skilled Regional Subclass 887</h4>
                           <p class="skilpara">
                The 887 visa is a permanent residency visa for people who have certain provisional visas, including the Skilled Regional (Provisional) visa (subclass 489), and have lived for at least two years and worked for at least one year in a specified regional. area of Australia
                </p>
                        </div>
                    </div>
                </div> 
                <div class="col-md-12">
                <div class="row">
                <div class="col-md-3">
                <h4 class="skilreq">Requirements</h4>
                <p class="sklrqpara">Resided in a Regional Australian territory for two years</p>
                    <p class="sklrqpara">Worked for a minimum term of twelve months on full term basis</p>
                      <p class="sklrqpara">Complied to all preconditions of first tier immigration which was essentially provisional 
or temporary in nature
</p>
                </div>
                <div class="col-md-6">
                <div class="col-md-12">
                <h4 class="skilreq"> Visa Process</h4>
                 <p class="skilpara">
                Nominees are selected by participating States/Territories in Australia to meet the skills shortages in those States/Territories. The current participating States/Territories are Western Australia, South Australia, Victoria, Queensland, the Northern Territory and Tasmania.
                </p>
                <h4 class="skilreq">If you were successful in applying under this category, you would have to undertake the following obligations:</h4>
                <p class="sklrqpara">Live in the nominating State/Territory for at least 2 years;</p>
                    <p class="sklrqpara">Keep immigration authorities informed of changes to your address; and</p>
                      <p class="sklrqpara">Take part in surveys as required
</p>
                </div>
               </div>
                <div class="col-md-3">
                <h4 class="skilreq">Benefits</h4>
                 <p class="sklrqpara">Work and dwell in this country as permanent residents</p>
                    <p class="sklrqpara">Undertake any study program, vocational training from an institute of your choice</p>
                      <p class="sklrqpara">Have access to social security benefits
</p>
 <p class="sklrqpara">Receive medical benefits through PBS (Pharmaceutical Benefits Scheme) and Medicare</p>
                    <p class="sklrqpara">Sponsor immigrants for permanent residency in this country</p>
                      <p class="sklrqpara">Apply for Australian citizenship
</p>
                </div>
                </div>
                </div>          
            </div> 
        <!-- End Item -->
        </div>
        <!-- End Carousel Inner -->
    </div>
    <!-- End Carousel -->
</div>
<div class="container-fluid">
    <div class="container">
 <h3 class="cndaswmhdr">Business Immigration</h3>
 <p class="skilreqparagrp">Australia's business skills migration program encourages successful business people (business owners, investors and senior executives) to settle permanently in Australia and develop new or existing businesses in Australia.</p>
 </div>
 <div class="col-md-12">
     <div class="row">
                <div class="fancy-collapse-panel">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">The Business Talent (Migrant) visa (Subclass 132)
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <p class="skilreqparagrp">This is a state-nominated permanent residence visa for business owners and entrepreneurs. The 132 visa is a permanent residency visa for people who want to establish or develop a new or existing business in Australia.You must be sponsored by a state or territory government.</p> 
                                    <div class="col-md-12">
                                    <div class="row">
                                    <div class="col-md-6">
                                    <div class="col-md-12" style="background:url(images/australia-bg.png)">
                                    <h4 class="busreq">Requirements</h4>
                                    <p class="busreqpara">Be less than 55 years old (NB: exceptions will be made if the regional authority considers that the proposed business will be of exceptional economic benefit to the region)</p>
                                    <p class="busreqpara">Have an overall successful business career</p>
                                    <p class="busreqpara">Be sponsored by an appropriate regional authority of a state or territory government</p>
                                    <p class="busreqpara">Have net assets in a qualifying business of AU$400,000 for at least two of the four fiscal years immediately before you apply (you an your partner combined)</p>
                                    <p class="busreqpara">Have total assets with a net value of at least AU$1,500,000 that have been legally acquired and are capable of being transferred to Australia within two years of your visa being granted</p>
                                    <p class="busreqpara">Have a commitment to maintain an ownership interest in a business in Australia and direct and continuous involvement in the management of that business</p>
                                    <p class="busreqpara">Have no record of any involvement in unacceptable business activities (applies also to your partner)</p>
                                    <p class="busreqpara">Have (combined with your partner) owned at least 51% of a business where turnover is less than AU$400,000 annually; at least 30% of a business where turnover is equal to or more than AU$400,000 annually; or 10% if the business is a publically listed company</p>
                                    <p class="busreqpara">Be of good health and character</p>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="col-md-12" style="background:url(images/blog-3.png)">
                                    <h4 class="busreq">Benefits</h4>
                                    <p class="busreqpara">"Establish, develop and manage a new or existing business in Australia</p>
                                    <p class="busreqpara">Stay in Australia indefinitely</p>
                                    <p class="busreqpara">Work and study in Australia</p>
                                    <p class="busreqpara">Enrol in Medicare, Australia's scheme for health-related care and expenses</p>
                                    <p class="busreqpara">Apply for Australian citizenship (if you are eligible)</p>
                                    <p class="busreqpara">Sponsor eligible relatives for permanent residence</p>
                                    <p class="busreqpara">Travel to and from Australia for five years from the date the visa is granted (after that  time, you will need a resident return visa or another visa to return to Australia)"</p>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Business Innovation and Investment (Provisional) visa (subclass 188)
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                     <p class="skilreqparagrp">Business Innovation & Investment Subclass 188 visas are for investors and business owners who wish to establish business operations in Australia. The visa is valid for 4 years.</p>
                                     <div class="col-md-12">
                                     <div class="row">
                                      <div class="col-md-6">
                                    <div class="col-md-12 strms">
                                    <h4 class="busreq">This visa has three streams</h4>
                                    <p class="busreqpara">Business innovation stream: for people who want to own and manage a new or existing business in Queensland</p>
                                      <p class="busreqpara">Investor stream: for people who want to make a designated investment in Queensland Treasury Corporation bonds and want to maintain business and investment activity in Queensland</p>
                                      <p class="busreqpara">Significant investor stream: for people who are willing to invest at least AUD$5million into complying investments.</p>
                                    </div>
                                    </div>
                                      <div class="col-md-6">
                                    <div class="col-md-12" style="background:url(images/blog-3.png)">
                                    <h4 class="busreq">Requirements</h4>
                                     <p class="busreqpara">Nomination by a state or territory government</p>
                                      <p class="busreqpara">Invitation to apply through SkillSelect</p>
                                      <p class="busreqpara">Under 55 years of age, unless the nominating state or territory certifies that you will make an exceptional economic benefit</p>
                                       <p class="busreqpara">Meet the pass mark in the Business Innovation and Investment Points test (currently 65)</p>
                                      <p class="busreqpara">Have an overall successful career in business or investment</p>
                
                                    </div>
                                    </div>
                                     </div>
                                     </div>
                                     
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Business Innovation and Investment (Permanent) (subclass 888) visa
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <p class="skilreqparagrp">The Subclass 888 Business Innovation and Investment (Residence) visa is the permanent stage for thesubclass 188 Business Innovation and Investment visa. There are two streams depending on which stream was applied for at the provisional visa stage</p> 
                                    <div class="col-md-12">
                                     <div class="row"><p class="busrb">Business Innovation Stream: requires you to have established a business in Australia which meets certain asset and turnover requirements</p>
                                      <p class="busrb">Investor Stream: you must have held an investment in Australia bonds for 4 years</p>
                                      <h4>Requirements:</h4>
                                      <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;Are the primary holder of a current Business Innovation and Investment (Provisional) Visa (subclass 188); </p>
                                      <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;Have met all the requirements of the stream under which you first applied for the current visa*;</p>
                                      <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;Have not been involved in any unacceptable business or investment activities;</p>
                                       <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;Have a satisfactory record of complying with Australian laws;</p>
                                      <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;Have been nominated by a state or territory;</p>
                                      <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;Have a genuine commitment to continue maintaining your business or investment activity in Australia;</p>
                                       <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;and your secondary applicants pass health examinations; and</p>
                                      <p class="busrb"><img src="images/demo-new-site/icons/login.png"> &nbsp;and your secondary applicants pass police checks for each country you and your secondary applicants have lived in for 12 months or more over the last 10 years since turning 16 years of age. </p>
                                     </div>
                                     </div>
                                    <p class="sklrqpara"></p>
                                    <p class="sklrqpara"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="container-fluid recentbgb spusmrgn">
 <div class="col-md-12">
 <h3 class="cndaswmhdr">Spouse Immigration</h3>
  <p class="skilreqparagrp1">If you are married to an Australian citizen, Australian permanent resident or eligible New Zealand citizen, plan to marry one, or you are in a in a relationship with one, you can apply for and obtain an Australian immigration partner visa.The applicant must be sponsored by their Australian independent or de-facto partner aged over 18 years, and the sponsorship must be for a minimumperiod of 2 years.
</p>
 </div>
 <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3"><img src="images/images/home-2_06.png" class="img-responsive"></div>
                        <div class="col-md-9">
                            <h4 class="mtoaust">Partner Temporary Visa and Permanent Visa(Subclass 820 and 801)</h4>
                           <p class="skilpara">
               The Partner Visa  – Subclasses 820 and 801  allows the spouse or de facto partner (opposite and same-sex) of an Australian citizen, permanent resident or eligible New Zealand citizen, to live in Australia.
                </p>
                        </div>
                        <div class="col-md-12">
                                    <div class="row">
                                    <div class="col-md-6">
                                    <div class="col-md-12">
                                    <h4 class="busreq">Requirements</h4>
                                    <p class="busreqpara">If your partner is an Australian citizen, permanent resident or an eligible New Zealander</p>
                                    <p class="busreqpara">If you're inside Australia at the time of application.</p>
                                    <p class="busreqpara">You already hold another visa type, e.g. student visa, visitor visa or working holiday visa.</p>
                                    <p class="busreqpara">You do not have an "8503 – NO FURTHER STAY" condition.</p>
                                    <p class="busreqpara">You are married or can show that you have lived together for 12 months.</p>
                                    </div>
                                    </div>
                                    <div class="col-md-6">
                                    <div class="col-md-12">
                                    <h4 class="busreq">Benefits</h4>
                                    <p class="busreqpara">Remain in Australian until a decision has been made about your permanent Partner visa;</p>
                                    <p class="busreqpara">Work in Australia;</p>
                                    <p class="busreqpara">Study in Australia, but with no access to government funding for tertiary study;</p>
                                    <p class="busreqpara">Be a part of Australia's scheme for health-related care and expense, by enrolling in Medicare; and</p>
                                    <p class="busreqpara">After approximately two (2) years after you lodge your Provisional visa application, you will be assessed for the subclass 801 (Permanent) visa. If this is granted you can:</p>
                                    <p class="busreqpara">Stay in Australia indefinitely, and travel to and from Australia without restrictions;</p>
                                    <p class="busreqpara">Work and study in Australia;</p>
                                     <p class="busreqpara">Apply for Australian citizenship once you become eligible;</p>
                                      <p class="busreqpara">Sponsor relatives; and</p>
                                       <p class="busreqpara">Receive some social security payments.</p>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                    </div>
                </div>
 </div>
<!---<div class="container-fluid">
  <div class="container cntnr">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="tabbable-panel">
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                <li class="active"> <a href="#tab_default_1" data-toggle="tab"> What We Offer</a> </li>
                <li> <a href="#tab_default_2" data-toggle="tab"> What We Offer</a> </li>
                <li> <a href="#tab_default_3" data-toggle="tab"> What We Offer</a> </li>
                <li> <a href="#tab_default_4" data-toggle="tab"> What We Offer</a> </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Duis autem eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="tab-pane" id="tab_default_2">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. </p>
                </div>
                <div class="tab-pane" id="tab_default_3">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="tab-pane" id="tab_default_4">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <video id="my_video_1" class="video-js vjs-default-skin" controls preload="auto" 
  data-setup='{ "asdf": true }' poster="http://video-js.zencoder.com/oceans-clip.png" >
            <source src="http://vjs.zencdn.net/v/oceans.mp4" type='video/mp4'>
            <source src="http://vjs.zencdn.net/v/oceans.webm" type='video/webm'>
          </video>
        </div>
      </div>
    </div>
  </div>
</div>-->

<!-----footer------>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/croyez.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">To Know more about Croyez and get updates about immigration services kindly follow us on social websites.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('f','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="f"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('3','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="3"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://in.linkedin.com/in/croyez-immigration-823671124" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('4','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="4"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://twitter.com/croyezmigration" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('5','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="5"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> 2floor ,sons complex,near sangamam hotel,Ashok Nagar, chennai - 600083</p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">+91 9543690385</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">info@croyezimmigration.com</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.mm" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.mm" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.mm" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Testimonial </h4>
            </div>
            <div class="row">
              <div class="col-md-12 column">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel"> 
                  <!-- Bottom Carousel Indicators --> 
                  
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner text-center tct"> 
                    
                    <!-- Quote 1 -->
                    <div class="item active">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 2 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 3 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">&#169; 2016 Croyez. All rights reserved</h3>
</div>
<script>
$(document).ready(function(ev){
    $('#custom_carousel').on('slide.bs.carousel', function (evt) {
      $('#custom_carousel .controls li.active').removeClass('active');
      $('#custom_carousel .controls li:eq('+$(evt.relatedTarget).index()+')').addClass('active');
    })
});
</script>
<script>
$(document).ready(function () {
    $('#myCarousel').carousel({
        interval: 10000
    })
    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
        
         if (next.next().next().length > 0) {
            next.next().next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
});
</script>
<!-- <script>
     videojs.autoSetup();

    videojs('my_video_1').ready(function(){
      console.log(this.options()); //log all of the default videojs options
      
       // Store the video object
      var myPlayer = this, id = myPlayer.id();
      // Make up an aspect ratio
      var aspectRatio = 264/640; 

      function resizeVideoJS(){
        var width = document.getElementById(id).parentElement.offsetWidth;
        myPlayer.width(width).height( width * aspectRatio );

      }
      
      // Initialize resizeVideoJS()
      resizeVideoJS();
      // Then on resize call resizeVideoJS()
      window.onresize = resizeVideoJS; 
    });
    </script>  -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>