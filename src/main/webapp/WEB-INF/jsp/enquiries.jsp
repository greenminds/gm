<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html><head>
	<script src="js/addclient.js"></script>

<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script> 
  <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Pre-Sales / Opportunities/Leads / Enquiries </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2">Enquiries </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"> <!--<a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a>--> </div>
                  <div class="col-md-2">
                    <!--<div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-th-list"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>-->
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="#" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3 col-sm-2 col-xs-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
                      </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                <th class="tblthd">Name</th>
                  <th class="tblthd">Email</th>
                  <th class="tblthd">Phone</th>
                  <th class="tblthd">Date</th>
                  <th class="tblthd">Message</th>
                  <th class="tblthd">Action</th>
                </tr>
               <tr>
										<td><input type="text" class="form-control" onchange="getSearchVal(this)"></td>
										<td><input type="text" class="form-control"onkeyup="getSearchVal(this)"></td>
										<td><input type="text" class="form-control" onkeyup="getSearchVal(this)"></td>
										<td><input type="text" class="form-control"
											id="datepicker1" name="datepicker1" onkeyup="getSearchVal(this)"></td>
										<td><input type="text" class="form-control"onkeyup="getSearchVal(this)"></td>
										<td></td>
									</tr>
                </thead>
                <tbody class="tbdy" id="myTable">
						<c:forEach var="enq" items="${enq}">
									<tr>
										<td>${enq.name}</td>
										<td>${enq.email}</td>
										<td>${enq.phoneNumber}</td>
										<td>${enq.date}</td>
										<td>${enq.message }</td>
										<td><a onclick="callEditEnquiries(${enq.enqId})"
										class="glyphicon glyphicon-edit edtbtn"></a>&nbsp; <a
											href="#" class="glyphicon glyphicon-remove edtbtn"></a></td>
									</tr>
									</c:forEach>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  <form action="editenquiries.mm" method="post" id="editEnquiriesForm">
	<input type="hidden" name="enqId" id="editEnquiriesId" value="">
	</form>

</body>
</html>