
  <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Accounts / Installment Breakup </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2"> Installment Breakup </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-2">
        </div>
         <div class="col-md-6">
        <input type="text" class="form-control" name="" placeholder="Search Client">
        </div>
        <div class="col-md-4">
                <div class="row">
                  <div class="col-md-2 col-sm-1 col-xs-3"><!--- <a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a>--></div>
                  <div class="col-md-2 col-sm-1 col-xs-3">
                    <!--<div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>-->
                  </div>
                  <div class="col-md-3 col-sm-1 col-xs-3">
                   <!-- <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>-->
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="#" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd" colspan="2">Installment Visiblity</th>
                  <th class="tblthd"><input type="checkbox" data-toggle="toggle" data-on="On" data-off="Off"></th>
                  <th class="tblthd" colspan="2">
                  <a href="#" class="btn btn-warning">Change Visibility</a>
                  <a href="#" class="btn btn-warning">Re-Arrange All Installment</a>
                  </th>
                </tr>
                <tr>
                  <th class="tblthd" colspan="2">Total Fee : 250000.00</th>
                  <th class="tblthd"colspan="2">Total Paid : 0.00</th>
                  <th class="tblthd">	Total Outstanding : 250000.00</th>
                 
                </tr>
                <tr>
                  <th class="tblthd">Breakup Details</th>
                  <th class="tblthd">Amount</th>
                  <th class="tblthd">Paid</th>
                  <th class="tblthd">Due</th>
                  <th class="tblthd">Status</th>
                  <th class="tblthd">	Action</th>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <tr>
                  <td>installment 1</td>
                  <td>75000</td>
                  <td>0.00</td>
                  <td>75000</td>
                  <td>cleared</td>
                  <td>
                  <ul class="actnul">
                  <li><a href="editopportunity.html" data-tooltip="Edit" class="actn"><span class="glyphicon glyphicon-edit edtbtn"></span></a></li>&nbsp;&nbsp;
                  <li><a href="#" data-tooltip="Remove" class="actn"><span class="glyphicon glyphicon-remove edtbtn"></span></a></li>
                  </ul>
                  </td>
                </tr>
                </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  