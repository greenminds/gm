
<script src="js/addclient.js"></script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

      <!-- main content start-->
      <div id="page-wrapper">
    <div class="main-page general">
          <div class="panel-info rothdr"> System / Administration / User Details </div>
          <div class="panel-info widget-shadow">
        <h4 class="title2">User Details</h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"> <a href="createnewuser.mm" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a> </div>
                  <div class="col-md-2">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="createEmployeeSheet.mm">XLS</a></li>
                        
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-2"> <a href="userdetails.mm" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">ID</th>
                  <th class="tblthd">Name</th>
                  <th class="tblthd">Email</th>
                  <th class="tblthd">Phone</th>
                  <th class="tblthd">Action</th>
                </tr>
                 <tr>
                  <td>
                  <input type="text"  onkeyup="SearchThis(this,'e_id')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchThis(this,'emp_firstname')" class="form-control" >
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchThis(this,'emp_email')" class="form-control" >
                  </td>
                  <td>
                  <input type="text"  onkeyup="SearchThis(this,'emp_mobile')" class="form-control">
                  </td>
                
                  <td></td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <c:forEach var="emp" items="${emp}">
                <tr id="row${emp.e_id}">
                  <td>${emp.e_id}</td>
                  <td>${emp.emp_firstname}</td>
                  <td>${emp.emp_email}</td>
                  <td>${emp.emp_mobile}</td>
                  <td>
                   <a href="javascript:edit(${emp.e_id});" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="javascript:deletThis(${emp.e_id});" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
        </div>
  </div>
    <form action="createnewuser.mm" method="post"  id="editData" name="editData"> 
  <input type="hidden" name="empid" id="empid" value="">
</form>
 