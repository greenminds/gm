<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<style>
.panel-info.widget-shadow {
	padding: 0.7em 0.1em;
}

ul.dropdown-menu {
	padding: 0;
	min-width: 148px;
	top: 95%;
}

.charts,.row {
	margin: inherit !important;
}

.general h4.title2 {
	font-size: 17px;
	margin: 3px;
	color: #777777;
	padding-left: 10px;
	font-family: 'Open Sans', sans-serif;
}

.wizard {
	background: #fff;
}

.wizard .nav-tabs {
	position: relative;
	margin-bottom: 0;
	border-bottom-color: #e0e0e0;
}

.wizard>div.wizard-inner {
	position: relative;
}

.connecting-line {
	height: 1px;
	background: #e0e0e0;
	position: absolute;
	width: 80%;
	margin: 0 auto;
	left: 0;
	right: 0;
	top: 50%;
	z-index: 1;
}

.wizard .nav-tabs>li.active>a,.wizard .nav-tabs>li.active>a:hover,.wizard .nav-tabs>li.active>a:focus
	{
	color: #555555;
	cursor: default;
	border: 0;
	border-bottom-color: transparent;
}

span.round-tab {
	width: 60px;
	height: 60px;
	line-height: 60px;
	display: inline-block;
	border-radius: 100px;
	background: #fff;
	border: 2px solid #e0e0e0;
	z-index: 2;
	position: absolute;
	left: 0;
	text-align: center;
	font-size: 25px;
}

span.round-tab i {
	color: #5bc0de;
}

.wizard li.active span.round-tab {
	background: #fff;
	border: 2px solid rgba(27, 255, 0, 0.35);
}

.wizard li.active span.round-tab i {
	color: #febe29;
}

span.round-tab:hover {
	color: #333;
	border: 2px solid #A9D86E;
}

.wizard .nav-tabs>li {
	width: 16%;
}

.wizard li:after {
	content: " ";
	position: absolute;
	left: 46%;
	opacity: 0;
	margin: 0 auto;
	bottom: 0px;
	border: 5px solid transparent;
	border-bottom-color: #5bc0de;
	transition: 0.1s ease-in-out;
}

.wizard li.active:after {
	content: " ";
	position: absolute;
	left: 44%;
	opacity: 1;
	margin: 0 auto;
	bottom: 0px;
	border: 10px solid transparent;
	border-bottom-color: #febe29;
}

.wizard .nav-tabs>li a {
	width: 60px;
	height: 60px;
	margin: 11px auto;
	border-radius: 100%;
	padding: 0;
}

.wizard .nav-tabs>li a:hover {
	background: transparent;
}

.wizard .tab-pane {
	position: relative;
	padding-top: 0px;
}

.wizard h3 {
	margin-top: 0;
}

.step1 .row {
	margin-bottom: 10px;
}

.step_21 {
	border: 1px solid #eee;
	border-radius: 5px;
	padding: 10px;
}

.step33 {
	border: 1px solid #ccc;
	border-radius: 5px;
	padding-left: 10px;
	margin-bottom: 10px;
}

.dropselectsec {
	width: 68%;
	padding: 6px 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	color: #333;
	margin-left: 10px;
	outline: none;
	font-weight: normal;
}

.dropselectsec1 {
	width: 74%;
	padding: 6px 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	color: #333;
	margin-left: 10px;
	outline: none;
	font-weight: normal;
}

.mar_ned {
	margin-bottom: 10px;
}

.wdth {
	width: 25%;
}

.birthdrop {
	padding: 6px 5px;
	border: 1px solid #ccc;
	border-radius: 3px;
	color: #333;
	margin-left: 10px;
	width: 16%;
	outline: 0;
	font-weight: normal;
}
/* according menu */
#accordion-container {
	font-size: 13px
}

.accordion-header {
	font-size: 13px;
	background: #ebebeb;
	margin: 5px 0 0;
	padding: 7px 20px;
	cursor: pointer;
	color: #fff;
	font-weight: 400;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px
}

.unselect_img {
	width: 18px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

.active-header {
	-moz-border-radius: 5px 5px 0 0;
	-webkit-border-radius: 5px 5px 0 0;
	border-radius: 5px 5px 0 0;
	background: #F53B27;
}

.active-header:after {
	content: "\f068";
	font-family: 'FontAwesome';
	float: right;
	margin: 5px;
	font-weight: 400
}

.inactive-header {
	background: #333;
}

.inactive-header:after {
	content: "\f067";
	font-family: 'FontAwesome';
	float: right;
	margin: 4px 5px;
	font-weight: 400
}

.accordion-content {
	display: none;
	padding: 20px;
	background: #fff;
	border: 1px solid #ccc;
	border-top: 0;
	-moz-border-radius: 0 0 5px 5px;
	-webkit-border-radius: 0 0 5px 5px;
	border-radius: 0 0 5px 5px
}

.accordion-content a {
	text-decoration: none;
	color: #333;
}

.accordion-content td {
	border-bottom: 1px solid #dcdcdc;
}

@media ( max-width : 585px ) {
	.wizard {
		width: 90%;
		height: auto !important;
	}
	span.round-tab {
		font-size: 16px;
		width: 50px;
		height: 50px;
		line-height: 50px;
	}
	.wizard .nav-tabs>li a {
		width: 50px;
		height: 50px;
		line-height: 50px;
	}
	.wizard li.active:after {
		content: " ";
		position: absolute;
		left: 35%;
	}
}
</style>
  <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Pre-Sales / Opportunities/Leads / Opportunity </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2">Opportunities </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="createopportunitie.mm" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a> </div>
                  <div class="col-md-2 col-sm-1 col-xs-3">
                    <div class="dropdown">
                      <button class="btn btn-primary dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Import"><i class="glyphicon glyphicon-import"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="#" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3 col-sm-1 col-xs-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">Email</th>
                  <th class="tblthd">Name</th>
                  <th class="tblthd">Phone</th>
                  <th class="tblthd">Join Date</th>
                  <th class="tblthd">Assigned To</th>
                  <th class="tblthd">Status</th>
                  <th class="tblthd">Action</th>
                </tr>
<tr>
											<td><input type="text" class="form-control" onkeyup="getSearchVal(this)"></td>
											<td><input type="text" class="form-control" onkeyup="getSearchVal(this)"></td>
											<td><input type="text" class="form-control" onkeyup="getSearchVal(this)"></td>
											<td><input type="text" class="form-control" onkeyup="getSearchVal(this)"></td>
											<td><input type="text" class="form-control" onkeyup="getSearchVal(this)"></td>
											<td><input type="text" class="form-control" onkeyup="getStatusVal(this)"></td>
											<td></td>
										</tr>
                </thead>
                <tbody class="tbdy" id="myTable">
<c:forEach items="${opp}" var="opp">
											<tr>
												<td>${opp.email}</td>
												<td>${opp.firstName }</td>
												<td>${opp.phoneNumber }</td>
												<td>${opp.phoneNumber }</td>
												<td>${opp.assignedTo }</td>
												<td>${opp.status }</td>
												<td><a onclick="editOpp(${opp.id})"
													class="glyphicon glyphicon-edit edtbtn"></a>&nbsp; <a
													onclick="deleteOpp(${opp.id})" class="glyphicon glyphicon-remove edtbtn"></a></td>
											</tr>
										</c:forEach>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  
    <form method="post" action="editopportunity.mm" id="formEdit">
			<input type="hidden" name="oppId" id="editOppId" value="">
		</form>
		<form method="post" action="deleteopportunity.mm" id="formDelete">
			<input type="hidden" name="deleteOppId" id="deleteOppId" value="">
		</form>
  
</html>