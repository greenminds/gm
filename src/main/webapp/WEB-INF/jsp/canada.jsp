<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Croyez - Canada</title>

<!-- Bootstrap -->
<link href="css/service.css" type="text/css" rel="stylesheet">
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<!-- Crunchify's Scroll to Top Script -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
</script>
<script>            
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.crunchify-top').fadeIn(duration);
			} else {
				jQuery('.crunchify-top').fadeOut(duration);
			}
		});
 
		jQuery('.crunchify-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		})
	});
</script>
<style>
/* Carousel */
ul.nav li.dropdown:hover > ul.dropdown-menu {
    display: block;    
}

#quote-carousel .carousel-control {
	background: none;
	color: #CACACA;
	font-size: 2.3em;
	text-shadow: none;
	margin-top: 30px;
}
#quote-carousel .carousel-control.left {
	left: -60px;
}
#quote-carousel .carousel-control.right {
	right: -60px;
}
#quote-carousel .carousel-indicators {
	right: 50%;
	top: auto;
	bottom: 0px;
	margin-right: -19px;
}
#quote-carousel .carousel-indicators li {
	width: 50px;
	height: 50px;
	margin: 5px;
	cursor: pointer;
	border: 4px solid #CCC;
	border-radius: 50px;
	opacity: 0.4;
	overflow: hidden;
	transition: all 0.4s;
}
#quote-carousel .carousel-indicators .active {
	background: #333333;
	width: 128px;
	height: 128px;
	border-radius: 100px;
	border-color: #f33;
	opacity: 1;
	overflow: hidden;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
.crunchify-top:hover {
	color: #fff !important;
	background-color: #ed702b;
	text-decoration: none;
}
.crunchify-top {
	display: none;
	position: fixed;
	bottom: 1rem;
	right: 1rem;
	width: 3.2rem;
	height: 3.2rem;
	line-height: 3.2rem;
	font-size: 1.4rem;
	color: #fff;
	background-color: rgba(0, 0, 0, 0.3);
	text-decoration: none;
	border-radius: 3.2rem;
	text-align: center;
	cursor: pointer;
}
.crslft {
	top: inherit !important;
	bottom: 50%;
}
.crslhght {
	height:700px;
}
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
.carousel-caption {
	bottom: 40px !important;
	padding-bottom: 0px !important;
}
.carousel-inner.onebyone-carosel {
	margin: auto;
	width: 90%;
}
.onebyone-carosel .active.left {
	left: -25%;
}
.onebyone-carosel .active.right {
	left: 25%;
}
.onebyone-carosel .next {
	left: 25%;
}
.onebyone-carosel .prev {
	left: -25%;
}
.carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-next, .carousel-control .icon-prev {
	top: 10px;
}
.panel-heading {
	padding: inherit;
	border-bottom: inherit;
	font-family: 'Open Sans', sans-serif;
	font-size: 17px;
}
@media screen and (max-width:880px) {
 .svrcrolwdth {
 width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
 display: none !important;
}
.carousel-control {
 height:inherit !important;
}
}
@media screen and (max-width:768px) {
 .svrcrolwdth {
 width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
 display: none !important;
}
.carousel-control {
 height:inherit !important;
}
}
@media screen and (max-width:360px) {
 .svrcrolwdth {
 width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
 display: none !important;
}
.carousel-control {
 height:inherit !important;
}
}
@media screen and (max-width:320px) {
 .svrcrolwdth {
 width: 7%;
top: 0 !important;
bottom: 0;
}
.hidden-xs {
 display: none !important;
}
.carousel-control {
 height:inherit !important;
}
}
</style>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body>
<a href="#" class="crunchify-top">&uarr;��</a> 
<!--------------reaponsive nav------------------->
<div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="https://in.linkedin.com/in/croyez-immigration-823671124" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="https://twitter.com/croyezmigration
" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph">+91 9543690385</p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
              <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="homePage.mm"><img src="images/demo-new-site/croyez2.png" width="250" class="img-responsive"></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="homePage.mm">Home</a></li>
          <li><a href="about.mm">About Us</a></li>
          <li class="active dropdown" onclick('windows.location.href="canada.mm"')><a href="canada.html" class="dropdown-toggle" data-toggle="dropdown">Canada<b class="caret"></b></a>
          <ul class="dropdown-menu">
           <li class="active"><a href="canada.mm">Canada</a></li>
              <li><a href="expressentry.mm">Express Entry</a></li>
              <li><a href="qubecskiled.mm">Quebec Skilled </a></li>
              <li><a href="familysponsorship.mm">Family Sponsorship </a></li>
              <li><a href="businessimmigration.mm">Business Immigration</a></li>
              <li><a href="provincial nominee.mm">Provincial Nominee </a></li>
            </ul>
          </li>
          <li><a href="australia.mm">Australia</a></li>
          <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Immigration <b class="caret"></b> </a>
            <ul class="dropdown-menu">
              <li><a href="#">Germany</a></li>
              <li><a href="newzealand.mm">New zealand</a></li>
              <li><a href="#">Hong Kong</a></li>
              <li><a href="#">Ecuador</a></li>
            </ul>
          </li>
          <li><a href="blog.mm">Blog</a></li>
          <li><a href="career.mm">Careers</a></li>
          <li><a href="contact.mm">Contact</a></li>
          <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
        </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
        </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav>
<div class="container-fluid">
  <div class="container">
    <div class="row">
      <div class="col-md-12 mainmrgn">
        <div class="row">
          <div class="col-md-5">
            <div class="row">
              <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                  <li data-target="#myCarousel" data-slide-to="1"></li>
                  <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active"> <img src="images/portada.jpg" alt="Chania" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Settle In Canada</h3>
                      <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                    </div>
                  </div>
                  <div class="item"> <img src="images/fullscreen-slider.jpg" alt="Chania" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Work</h3>
                      <p>The atmosphere in Chania has a touch of Florence and Venice.</p>
                    </div>
                  </div>
                  <div class="item"> <img src="images/countrys/canada/cand1.png" alt="Flower" width="100%" height="100%">
                    <div class="carousel-caption">
                      <h3>Study</h3>
                      <p>Beatiful flowers in Kolymbari, Crete.</p>
                    </div>
                  </div>
                </div>
                
                <!-- Left and right controls --
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>--> 
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <h3 class="srvched">Why Canada?</h3>
            <p class="srvcpara">Canada is one of the world's top immigration destinations with over 200,000 new arrivals coming under the Canada immigration system every year. In terms of standards of living, the United Nations has voted Canada as one of the best places in the world to live.Canada can be considered as a heaven, especially for the people from the developing countries. Between now and 2021, a million jobs are expected to go unfilled across Canada.  Ottawa is making reforms to the immigration system but isn't going far enough. We need to radically boost immigration numbers. Deciding to immigrate to Canada is a significant step in the lives of those who pursue this dream. Canada was among the very first countries to have introduced immigration programs against a Point Based System. The programs have attracted hundreds and thousands of immigrants over the last few decades.The immigration rules are not as stringent when compared to other countries such as Australia and hence, are easier to qualify. </p>
            <!-- <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; The generated Lorem Ipsum</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; The generated Lorem Ipsum</p>
            <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> &nbsp; The generated Lorem Ipsum</p>--> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid recentbgb">
  <div class="col-md-12">
    <h3 class="cndaswmhdr">Why Canada is Awesome?</h3>
  </div>
  <div class="container">
    <div class="row">
      <div class="span12">
        <div id="myCarousel" class="carousel fdi-Carousel slide"> 
          <div class="carousel fdi-Carousel slide" id="eventCarousel" data-interval="0">
            <div class="carousel-inner onebyone-carosel">
              <div class="item active">
                <div class="col-md-3"> <img src="images/images/home-2_06.png" class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">Canada Rated the BEST Place to Live</h4>
                    <p class="rncntpara">Every year since 1994, the United Nations has rated Canada as the best country in the world to live in. And also Canada is the world's second biggest country, rich in natural resources including oil reserves second only to Saudi Arabia.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3"> <img src="images/countrys/canada/income.png" class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">Income</h4>
                    <p class="rncntpara">Canada's minimum wage as of October 1, 2015 is $10.50. So it has one of the best salary benefits for workers around the world with good working conditions.  Canadians earn an average annual income of approximately $40,000.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3"> <img src="images/countrys/canada/Health-Insurance.jpg" class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">Health</h4>
                    <p class="rncntpara">This country also offers the most supporting social security benefits program for immigrants and their families. Canada's educational and health facilities are among the best in the world and are highly subsidized. Canada's health care is publicly (tax-payer) funded: payment is generally not required for medical treatment, although, depending on the province you live in.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3"> <img src="images/countrys/canada/educt.png"2 class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">Top Quality Education</h4>
                    <p class="rncntpara">Offering free primary and secondary education and subsidized post-secondary studies, Canada spends more on education than any industrialized nation. Canadian universities and colleges have a strong international reputation for high quality.If you are bringing children to Canada, it's likely their education will be important to you. Students in Canadian schools performed better than students from any other English speaking country.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3"> <img src="images/countrys/canada/marketing.png" class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">Growing Job Market</h4>
                    <p class="rncntpara">At 6.8%, Canada's unemployment rate is the lowest it's been since 1976, with hundreds of thousands of new jobs created each year.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3"> <img src="images/countrys/canada/safe.png" class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">Safe and Secure</h4>
                    <p class="rncntpara">Community policing, strict gun control laws, and a fair justice system make Canada safe and secure for everyone. Just 1.3% of Canadians said they were assaulted over the one-year period leading up to the survey. That's well below the OECD average of 4%. Crime rates are among the lowest in the world, and continue to decline.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="col-md-3"> <img src="images/countrys/canada/funnly.png" class="img-responsive center-block">
                  <div class="text-center">
                    <h4 class="mtoaust">And funnily</h4>
                    <p class="rncntpara">Canada is likely the only place in the world where you can crash into someone, and they will apologize to you. They are actually the politest people ever existing on earth because they are happy folks.</p>
                  </div>
                </div>
              </div>
            </div>
            <a class="crslft left carousel-control svrcrolwdth" href="#eventCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left hidden-xs"></span></a> <a class="crslft right carousel-control svrcrolwdth" href="#eventCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right  hidden-xs"></span></a> </div>
        </div>      </div>
    </div>
  </div>
</div>
<!--<div class="container-fluid gird">
  <div class="container orftrmrgn">
    <h3 class="cndaswmhdr">Why Canada is Awesome?</h3>
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-1">
    <div class="clnmrond">
    <span>01</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">Canada Rated the BEST Place to Live:</h4>
    <p class="candasw">Every year since 1994, the United Nations has rated Canada as the best country in the world to live in. And also Canada is the world's second biggest country, rich in natural resources including oil reserves second only to Saudi Arabia.</p>
    </div>
    <div class="col-md-1">
     <div class="clnmrond">
    <span>02</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">Income:</h4>
    <p class="candasw">Canada's minimum wage as of October 1, 2015 is $10.50. So it has one of the best salary benefits for workers around the world with good working conditions.  Canadians earn an average annual income of approximately $40,000.</p>
    </div>
    </div>
    <div class="row">
    <div class="col-md-1">
     <div class="clnmrond">
    <span>03</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">Health:</h4>
    <p class="candasw">This country also offers the most supporting social security benefits program for immigrants and their families. Canada's educational and health facilities are among the best in the world and are highly subsidized. Canada's health care is publicly (tax-payer) funded: payment is generally not required for medical treatment, although, depending on the province you live in.</p>
    </div>
    <div class="col-md-1">
     <div class="clnmrond">
    <span>04</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">Top Quality Education:</h4>
    <p class="candasw">Offering free primary and secondary education and subsidized post-secondary studies, Canada spends more on education than any industrialized nation. Canadian universities and colleges have a strong international reputation for high quality.If you are bringing children to Canada, it's likely their education will be important to you. Students in Canadian schools performed better than students from any other English speaking country.</p>
    </div>
    </div>
    <div class="row">
    <div class="col-md-1">
     <div class="clnmrond">
    <span>05</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">Growing Job Market:</h4>
    <p class="candasw">At 6.8%, Canada's unemployment rate is the lowest it's been since 1976, with hundreds of thousands of new jobs created each year.</p>
    </div>
    <div class="col-md-1">
     <div class="clnmrond">
   <span>06</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">Safe and Secure:</h4>
    <p class="candasw">Community policing, strict gun control laws, and a fair justice system make Canada safe and secure for everyone. Just 1.3% of Canadians said they were assaulted over the one-year period leading up to the survey. That's well below the OECD average of 4%. Crime rates are among the lowest in the world, and continue to decline.</p>
    </div>
    </div>
    <div class="row">
    <div class="col-md-1">
     <div class="clnmrond">
   <span>07</span>
    </div>
    </div>
    <div class="col-md-5">
    <h4 class="candswhdr">And funnily: </h4>
    <p class="candasw">Canada is likely the only place in the world where you can crash into someone, and they will apologize to you. They are actually the politest people ever existing on earth because they are happy folks.</p>
    </div>
    </div>
    </div>
  </div>
</div>-->

<!--<div class="container-fluid">
<div class="container">
<h3 class="cndaswmhdr">Saskatchewan Immigrant Nominee Program (SINP)</h3>
<p class="extrypara">Saskatchewan is one of the Prairie provinces of Western Canada. Its economy is primarily based on agriculture and important natural resources industries like forestry and fishing. It boasts two major cities, Saskatoon and Regina, as well as vast expanses of pristine wilderness.The Saskatchewan Immigrant Nominee Program (SINP) is an immigration program, supervised by the province that operates under an agreement with the <a href="#">federal government</a>. This program issues with a speedy means of entry into Canada; it allows Saskatchewan to nominate applicants to the federal government for permanent residence.Successful applicants under the Saskatchewan Immigrant Nominee Program (SINP) will receive a provincial nomination certificate that will speed up the process of obtaining a <a href="#">Canada Visa</a> and help you on your way to obtaining Canadian <a href="#">permanent residence</a>. </p>
</div>
<div class="col-md-12">
<div class="row">
<div class="col-md-6">
<h4 class="exptryhdr4">
The SINP accepts applications under the following categories
</h4>
<div class="col-md-12">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-10">
<p class="exptrypgrm"><img src="images/demo-new-site/icons/login.png"> International Skilled Worker Category.</p>
<p class="exptrypgrm"><img src="images/demo-new-site/icons/login.png"> Saskatchewan Experience Category.</p>
<p class="exptrypgrm"><img src="images/demo-new-site/icons/login.png"> Entrepreneur and Farm Category.</p>
</div>
</div>
</div>
</div>
<div class="col-md-6">
<div class="col-md-12">
<div class="row">
<div class="col-md-2"></div>
<div class="col-md-10">
<img src="images/saskatche.jpg" class="mxmwdth">
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container-fluid">
  <div class="container cntnr">
    <hr>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-3">
          <h3 class="srvchdr">Federal Skilled Worker Program</h3>
          <div class="col-md-12"><div class="row"><img src="images/demo-new-site/technology-wallpaper-1920x1080.jpg" class="img-responsive center-block"></div></div>
          <p class="srvcpara">The Federal Skilled Worker (FSW) program is for immigrants to Canada who are selected as permanent residents based on their ability to become economically established in Canada.</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Point System</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Eligibility Criteria</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Selection Factors</p>
        </div>
        <div class="col-md-3">
          <h3 class="srvchdr">Federal Skilled Trades Program</h3>
          <div class="col-md-12"><div class="row"><img src="images/demo-new-site/technology-wallpaper-1920x1080.jpg" class="img-responsive center-block"></div></div>
          <p class="srvcpara">The Federal Skilled Trades Program is for the applicants who are qualified trade workers and want to migrate to Canada permanently after receiving a offer of employment.</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Eligibility Criteria</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Selection Factors</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Eligible Skilled Trades</p>
        </div>
        <div class="col-md-3">
          <h3 class="srvchdr">Canadian Experience Class</h3>
          <div class="col-md-12"><div class="row"><img src="images/demo-new-site/technology-wallpaper-1920x1080.jpg" class="img-responsive center-block"></div></div>
          <p class="srvcpara">Individuals who have worked in Canada and who wish to settle permanently in Canada with their accompanying dependants may qualify to apply for Permanent Residence under the Canadian Experience Class.anada Experience Class program recognizes the benefits to Canada by candidates who have spent significant amounts of time pursuing their studies and working careers in Canada.</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Eligibility Criteria</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Qualifying work Experience</p>
        </div>
        <div class="col-md-3">
          <h3 class="srvchdr">Quebec skilled worker</h3>
          <div class="col-md-12"><div class="row"><img src="images/demo-new-site/technology-wallpaper-1920x1080.jpg" class="img-responsive center-block"></div></div>
          <p class="srvcpara">Quebec is Canada's largest province by area and its second-largest administrative division.Montreal is the largest city in the province of Quebec and consistently rated as one of the world's most livable cities, which offers numerous employment opportunities.The Quebec Immigration selection system for Quebec Skilled Workers is designed to indicate which applicants are likely to become economically established upon immigration to Quebec.Quebec Skilled Worker Immigration program is not restricted to a list of occupations, and a job offer from a Canadian employer is not needed for eligibility.</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Eligibility Criteria</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Selection Factors</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Application Process</p>
          <p class="srvcpara"><img src="images/demo-new-site/icons/login.png"> Benefits</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container cntnr">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6">
          <div class="tabbable-panel">
            <div class="tabbable-line">
              <ul class="nav nav-tabs ">
                <li class="active"> <a href="#tab_default_1" data-toggle="tab"> What We Offer</a> </li>
                <li> <a href="#tab_default_2" data-toggle="tab"> What We Offer</a> </li>
                <li> <a href="#tab_default_3" data-toggle="tab"> What We Offer</a> </li>
                <li> <a href="#tab_default_4" data-toggle="tab"> What We Offer</a> </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="tab_default_1">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Duis autem eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="tab-pane" id="tab_default_2">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. </p>
                </div>
                <div class="tab-pane" id="tab_default_3">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="tab-pane" id="tab_default_4">
                  <h4 class="tabhdr"> Recusandae Perferendis </h4>
                  <p class="tabpara"> Duis autem vel eum iriure dolor in hendrerit in vulputate. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <video id="my_video_1" class="video-js vjs-default-skin" controls preload="auto" 
  data-setup='{ "asdf": true }' poster="http://video-js.zencoder.com/oceans-clip.png" >
            <source src="http://vjs.zencdn.net/v/oceans.mp4" type='video/mp4'>
            <source src="http://vjs.zencdn.net/v/oceans.webm" type='video/webm'>
          </video>
        </div>
      </div>
    </div>
  </div>
</div>--> 

<!-----footer------>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/croyez.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">To Know more about Croyez and get updates about immigration services kindly follow us on social websites.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="https://www.facebook.com/Croyez-Immigration-1110677735661580/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('f','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="f"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('3','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="3"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://in.linkedin.com/in/croyez-immigration-823671124" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('4','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="4"></a></div>
              <div class="scl ftrgtmrgn"><a href="https://twitter.com/croyezmigration
" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('5','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="5"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> 2floor ,sons complex,near sangamam hotel,Ashok Nagar, chennai - 600083 </p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">+91 9543690385</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">info@croyezimmigration.com</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.mm" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.mm" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.mm" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Testimonial </h4>
            </div>
            <div class="row">
              <div class="col-md-12 column">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel"> 
                  <!-- Bottom Carousel Indicators --> 
                  
                  <!-- Carousel Slides / Quotes -->
                  <div class="carousel-inner text-center"> 
                    
                    <!-- Quote 1 -->
                    <div class="item active">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 2 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                    <!-- Quote 3 -->
                    <div class="item">
                      <p class="ftrlinkp">"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua". </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--1--> 
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">&#169; 2016 Croyez. All rights reserved</h3>
</div>
<script>
$(document).ready(function () {
    $('#myCarousel').carousel({
        interval: 10000
    })
    $('.fdi-Carousel .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        if (next.next().length > 0) {
            next.next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
        
         if (next.next().next().length > 0) {
            next.next().next().children(':first-child').clone().appendTo($(this));
        }
        else {
            $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
        }
    });
});
</script> 
<script>
     videojs.autoSetup();

    videojs('my_video_1').ready(function(){
      console.log(this.options()); //log all of the default videojs options
      
       // Store the video object
      var myPlayer = this, id = myPlayer.id();
      // Make up an aspect ratio
      var aspectRatio = 264/640; 

      function resizeVideoJS(){
        var width = document.getElementById(id).parentElement.offsetWidth;
        myPlayer.width(width).height( width * aspectRatio );

      }
      
      // Initialize resizeVideoJS()
      resizeVideoJS();
      // Then on resize call resizeVideoJS()
      window.onresize = resizeVideoJS; 
    });
    </script> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>