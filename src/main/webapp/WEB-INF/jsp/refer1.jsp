<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
  <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Refer a Friend</title>
	<script type="text/javascript">
function checksubmit(form){
	
	document.getElementById("divCheckMatch").innerHTML = "you have reffered";
	
	return true;
}
</script>
<script type="text/javascript">
$(document).ready(function () {
 //called when key is pressed in textbox
  $("#phone").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
//         $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});
</script>
<script type="text/javascript">
	var statusOfEmailId = false;
	function emailCheck(email) {
		
		var eml = email.value;
		if (eml.length != 0) {
			
			AjaxController.checkEmail(eml, returnedEmail);
		}
		function returnedEmail(value) {
			if (value == true) {
				document.getElementById("divCheckPasswordMatch").innerHTML = "Referred email-id is already an user";
				document.getElementById("email-id").innerHTML = "";
				document.getElementById("email-id").value="";
			} else {
				
				document.getElementById("divCheckPasswordMatch").innerHTML = "";
				statusOfEmailId = true;
			}
		}

	}
</script>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans|Roboto|Raleway|Noto+Sans' rel='stylesheet' type='text/css'>
    <!-- Bootstrap -->
    
	<link href="css/mainstyle.css" type="text/css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Second navbar for search -->
    <nav class="navbar navbar-inverse">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand" href="dashBoard.mm"><img src="images/logo-3.png" width="100px" class="img-responsive"></a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-3">
          <ul class="nav navbar-nav">
        <li class="nvmnu"><a href="dashBoard.mm">My Dashboard</a></li>
        <li class="nvmnu"><a href="myInfo.mm">My Info</a></li>
        <li  class="active active3 nvmnu"><a href="refer.mm">Refer a Friend</a></li>
        <li class="nvmnu"><a href="myDocuments.mm">My Documents</a></li>
        <li class="nvmnu"><a href="evaluation.mm">Evaluation Report</a></li>
        <li class="nvmnu"><a href="retainerAgreement.mm">Retainer Agreement</a></li>
        <li class="nvmnu"><a href="fileDetails.mm">File Details</a></li>
        <li class="nvmnu"><a href="helpDesk.mm">Helpdesk</a></li>
          </ul>
           <ul class="nav navbar-nav navbar-right">
            
            <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img style="margin: auto; height: 34px; width: 34px;border-radius: 50%;"  src="image.mm?id=${user.userId}">  ${user.fName} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="myInfo.mm"> Profile</a></li>
							<li><a href="logout.mm">Logout</a></li>
						</ul>
					</li>
          </ul>
          <div class="collapse nav navbar-nav nav-collapse slide-down" id="nav-collapse3">
            <form class="navbar-form navbar-right" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Search" />
              </div>
              <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
            </form>
          </div>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
	<!-------------main body----------->
	<div class="container-fluid">
    <div class="col-md-12">
    <div class="row">
    <div class="col-md-1 side">
    <h3>side bar</h3>
    </div>
    <div class="col-md-11">
      <div class="panel panel-default pnlbdr3">
					<div class="panel-heading pnlhdr3">Refer a Friend</div>
					<div class="panel-body">
                    <form action="referNow.mm" method="post" class="form-horizontal"
							onsubmit="return checksubmit(this)">
                    <div class="form-group">
                    <label class="control-label col-md-2">Client Name</label>
					<div class="col-md-3">
                    <input type="text" class="form-control" placeholder="First Name" name="refferedName"
										required="required">
                    </div>
                    <div class="col-md-3">
                    <input type="text" name="refferedMname"
										 class="form-control" placeholder="Middle Name">
                    </div>
                    <div class="col-md-3">
                    <input type="text" name="refferedLname"
										required="required" class="form-control" placeholder="Last Name">
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-2">Email</label>
                    <div class="col-md-9">
                    <input type="email" class="form-control" name="email" required="required" id="email-id" onchange="emailCheck(this)">
                    <div class="registrationFormAlert" id="divCheckPasswordMatch"
										style="color: red;"></div>
									<div class="registrationFormAlert" id="divCheckMatch"
										style="color: green;"></div>
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-2">Phone</label>
                    <div class="col-md-9">
                    <input type="tel" class="form-control" required="required"
										name="phoneNumber" id="phone" minlength="10" maxlength="10">
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="control-label col-md-2">Relation</label>
                    <div class="col-md-9">
                    <input type="text" class="form-control" required="required"
										name="relation">
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                    <button type="submit" class="btn btn-success">Refer Now</button>
                    </div>
                    
								<div class="registrationFormAlert" id="divCheckMatch"
									style="color: green;"></div>
                    <div class="col-md-5"></div>
                    </div>
                    </form>
					</div>
				</div>
    </div>
    </div>
    </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>