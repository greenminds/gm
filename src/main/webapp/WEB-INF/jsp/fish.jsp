<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Dry Fish</title>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="js/jquery-1.7.1.min.js"></script>
<script src="js/jquery-1.9.1.min.js"></script>
<!-- <script src="/js/jquery.validate.min.js"></script>
<script src="js/jquery.validate.js"></script> -->
<script src="js/registration-form-validation.js"></script>
<!-- Style -->
<link
	href='https://fonts.googleapis.com/css?family=Play|Oxygen|Dosis|Arimo'
	rel='stylesheet' type='text/css'>
<link href="style.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link rel="icon" href="images/fevicon-icon/android-icon-48x48.png"
	type="image/x-icon" />
<body>
${pro}
</body>
</html>