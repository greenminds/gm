<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<title><tiles:insertAttribute name="title" /></title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/bootstrap-responsive.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/tutoring-portal-main.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/tutoring-portal-form.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/elements.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="mediaqueries/mediaqueries.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="mediaqueries/mediaqueries-tablet.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="mediaqueries/mediaqueries-desktop.css" rel="stylesheet"
	type="text/css" media="all" />
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
	
</script>
<script type="text/javascript" src="js/jquery.simplyscroll.js"></script>
<link rel="stylesheet" href="css/jquery.simplyscroll.css" media="all"
	type="text/css">

</head>
<body onunload="myFunction()">
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />
</body>
<script type="text/javascript">
	function myFunction() {

		AjaxController.logout();
	}
	/* (function($) {
	 $(function() {
	 $("#scroller").simplyScroll();
	 });
	 })(jQuery); */
</script>
</html>