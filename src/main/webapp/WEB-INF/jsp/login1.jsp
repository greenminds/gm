<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Shinrai - Log in</title>
<script type="text/javascript">
		function validateLoginForm() {
			var uName = document.getElementById("email").value;
			var pwd = document.getElementById("password").value;
 			if(document.getElementById("myCheck").checked){
 			document.getElementById("myCheck").value="yes";
			}
 			else{
 				document.getElementById("myCheck").value="no";
			}
			if (uName != "" && pwd != "" && uName.length != 0
					&& uName.length != 0
					&& document.getElementById("myCheck").checked) {
				document.getElementById(submit);
			}
		}
	</script>
	<script type="text/javascript">
		function getCookie() {

			AjaxController.getUserLoginCookie(displayCookie);

		}
		function displayCookie(val) {
			if (val != null) {
				var values = val.split("_");
				//var uname = values[0];
				//var pwd = values[1];
				document.getElementById("email").value = values[0];
				document.getElementById("password").value = values[1];
				document.getElementById("myCheck").checked = true;
			}

		}
	</script>
	<script type="text/javascript">
		function checkEmail(email) {
			var eml = email.value;
			if (eml.length != 0) {
				AjaxController.emailidCheck(eml, returnemail);

			}
			function returnemail(value) {
				if (value == true) {
					document.getElementById("divCheckMatch").innerHTML = "";
				} else
					document.getElementById("divCheckMatch").innerHTML = "Email-id incorrect";

			}
		}
	</script>
<script type="text/javascript">
function changeCheck(){
if(!document.getElementById("myCheck").checked){
	document.getElementById("myCheck").value="yes";
	}else{
		document.getElementById("myCheck").value="no";
	}
}
</script>
<!-- Bootstrap -->
<!-- Bootstrap -->
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/login.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
 <style>
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
</style>
 <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
 </script>
</head>
<body onload="getCookie()">


<!--------------reaponsive nav------------------->
<div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a>
       	     <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph"><a href="students.html" class="phlr"><img src="images/icon/graduate-student-avatar.png"> &nbsp; Students </a>&nbsp; &nbsp; &nbsp; <a href="register.mm" class="phlr"><img src="images/icon/icon.png"> &nbsp; Partner with us</a></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
             <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="homePage.mm"><img src="images/demo-new-site/logo.png" width="250" class="img-responsive"/></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="homePage.mm">Home</a></li>
          <li><a href="about.html">About</a></li>
          <li><a href="service.html">Services</a></li>
          <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Dropdown <b class="caret"></b> </a>
            <ul class="dropdown-menu">
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </li>
          <li><a href="blog.html">Blog</a></li>
          <li><a href="contact.html">Contact</a></li>
          <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
        </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
        </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="col-md-12 mrgnpdng">
    <div class="col-md-4"></div>
    <div class="col-md-4 bxshdw">
      <div class="col-md-12">
        <h4 class="loginhh">Log In</h4>
      </div>
      <div class="col-md-12">
        <form action="verifyLogin.mm" method="post" name="loginForm">
          <div class="form-group">
            <input type="email" name="email" class="form-control" required="required" id="email" placeholder="Email" onchange="checkEmail(this)">
          </div>
          <div class="form-group">
            <input type="password" class="form-control" name="password" value="" required="required"
								 id="password" placeholder="Password">
          </div>
          <div class="checkbox">
            <label>
              <input type="checkbox" id="myCheck" name="checkbox" checked="checked" value="no" onchange="changeCheck(this)">
              Remember me </label>
          </div>
          <div class="registrationFormAlert" id="divCheckMatch"
							style="color: red;"></div>
          <div class="col-md-12">
            <button type="submit" class="sbmtbtn center-block" onclick="validateLoginForm()">Submit</button>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <div class="row"><a href="forgotPassword.mm">
                  <p class="frgt">Forgot Password ?</p>
                </a></div>
              </div>
              <div class="col-md-6">
                <div class="row"> <a href="register.mm">
                  <p class="rgstr">Register</p>
                  </a> </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-4"></div>
  </div>
</div>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/logo.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">Lorem ipsum dolor sit amet, consectetur sed
                do eiusmod tempor incididunt ut labore et 
                dolore magna aliqua.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('15','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="15"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('16','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="16"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('17','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="17"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('18','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="18"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> Lorem ipsum dolor sit amet consectetur,<br>
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">+ 91 0123456789</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">toshinri@gmail.com</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.html" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.html" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.html" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Latest News</h4>
            </div>
            <!--1-->
            <marquee  behavior="scroll" direction="up" scrolldelay="250" behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/social-twitter-circular-button.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">Lorem ipsum dolor sit amet 
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/social-twitter-circular-button.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">Lorem ipsum dolor sit amet 
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/social-twitter-circular-button.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">Lorem ipsum dolor sit amet 
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            </marquee>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">© 2016 Shinrai. All rights reserved</h3>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>

</body>
</html>