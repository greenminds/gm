
<script src="js/addreceipt.js"></script>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="shinarai" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/responsive.css">
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans|Noto+Sans' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="js/wow.min.js"></script>
<script>
		 new WOW().init();
	</script>
<!--//end-animate-->
<!-- Metis Menu -->
<!-------date picker---------->
<link href="css/datepicker.css" type="text/css" rel="stylesheet">
<script src="js/datepicker.js" type="text/javascript"></script>
<script>
	 $(function() {
    $(".datepicker").datepicker();
});
</script>
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
.panel-info.widget-shadow {
	padding: 0.7em 0.1em;
}
ul.dropdown-menu {
    padding: 0;
    min-width: 116px;
    top: 95%;
}
.charts, .row {
    margin:inherit !important;
}
.general h4.title2 {
    font-size: 17px;
    margin: 3px;
    color: #777777;
    padding-left: 10px;
    font-family: 'Open Sans', sans-serif;
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 2;
    cursor: default;
    background: #e9ecf2;
    color: #000;
    border-color: #ccc;
}
.pager li > a, .pager li > span {
    display: inline-block;
    padding: 5px 14px;
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius:0px;
	font-size:16px;
	font-family: 'Open Sans', sans-serif;
}
.pagination-lg > li:last-child > a, .pagination-lg > li:last-child > span {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.pager {
	margin:inherit;
}  
[data-tooltip]:before {
    position: absolute;
    bottom: 150%;
    left: inherit;
    margin-bottom: 5px;
    margin-left: -23px;
    padding: 6px;
    width: auto;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    background-color: #000;
    background-color: hsla(0, 0%, 20%, 0.9);
    color: #fff;
    content: attr(data-tooltip);
    text-align: center;
    font-size: 13px;
    line-height: 1.2;
}
</style>
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
    <!-- main content start-->
  <div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Accounts / Misc Fee </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2"> Misc Fee </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">

                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="#" class="btn btn-warning crtbtn" data-tooltip="Create" data-toggle="modal" data-target="#create"> <span class="glyphicon glyphicon-plus"></span> </a> </div>
                  <div class="col-md-2 col-sm-1 col-xs-3">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="createMiscfeeSheet.mm">XLS</a></li>
											<!--      <li><a href="#">WORD</a></li> -->
										</ul>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-3"> <a href="miscfee.mm" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3 col-sm-1 col-xs-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">3</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#" onclick="pagination(10)">10</a></li>
                        <li class="active"><a href="#"  onclick="pagination(25)">25</a></li>
                        <li><a href="#" onclick="pagination(50)">50</a></li>
                        <li><a href="#" onclick="pagination(100)">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">Id</th>
                  <th class="tblthd">Client</th>
                  <th class="tblthd">Date</th>
                  <th class="tblthd">Fee Name</th>
                  <th class="tblthd">Amount</th>
                  <th class="tblthd">Pay Mode</th>
                  <th class="tblthd">TR.ID</th>
                  <th class="tblthd">Description</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                  <td>
                 
                  </td>
                 
                  <td>
                  <input type="text" onkeyup="SearchMiscfee(this,'client')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchMiscfee(this,'date')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchMiscfee(this,'feeName')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchMiscfee(this,'amonut')" class="form-control">
                  </td>
                  <td>
                  <input type="text" onkeyup="SearchMiscfee(this,'payMode')" class="form-control">
                  </td>
                  <td>
                <input type="text" onkeyup="SearchMiscfee(this,'transation')" class="form-control">
                  </td>
                  <td>
                   <input type="text" onkeyup="SearchMiscfee(this,'description')" class="form-control">
                  </td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <c:forEach var="miscfeer" items="${miscfee}">
                <tr>
                  <td>${miscfeer.id}</td>
                  <td>${miscfeer.client}</td>
                  <td>${miscfeer.date}</td>
                  <td>${miscfeer.feeName}</td>
                  <td>${miscfeer.amonut}</td>
                  <td>${miscfeer.payMode}</td>
                  <td>${miscfeer.transation}</td>
                  <td>${miscfeer.description}</td>
                  <td>
                  <ul class="actnul">
                  <li><a href="#" data-tooltip="Go to Approval" class="actn"><span class="glyphicon glyphicon-ok edtbtn" onclick="approvalMisc(${miscfeer.id})" ></span></a></li>
                  </ul>
                  </td>
                </tr>
                 </c:forEach>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  <!--footer-->
  <div class="footer">
    <p>&copy; 2016 Admin Panel. All Rights Reserved | Design by <a href="https://maestroinfotech.in/" target="_blank">Maestro Infotech System</a></p>
  </div>
  <!--//footer--> 
</div>

<!-- Modal -->
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="form-horizontal" role="form" action="createmisc.mm" method="post">
    <div class="modal-content">
      <div class="modal-header mbt">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mdlhdr">Create Misc Fee</h4>
         <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Client</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="client" value=""onchange="isEmail(this)" placeholder="Enter Email"  >
      <span id="isEmail" style="color:red"></span>
      </div>
    </div>
      </div>
      <div class="modal-body">
      <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Fee Name</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="feeName" value=""  onchange="isPayment1(this)" placeholder="Enter PaymentMode"  >
        <span id="isPayment1" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Date</label>
      <div class="col-sm-3">
        <input type="text" class="form-control datepicker" name="" value="">
      </div>
      <label class="control-label col-sm-2 lblfnt">Amount</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="amonut" value="" onchange="isAmount(this)" placeholder="Enter Amount" >
        <span id="isAmount" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Pay Mode</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="payMode" value="" onchange="isPayment(this)" placeholder="Enter PaymentMode"  >
        <span id="isPayment" style="color:red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Transaction ID</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="transation" value="" onchange="isTransaction(this)" placeholder="Enter TransactionNo"  >
    <span id="isTransaction" style="color:red"></span>
      </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Description</label>
    <div class="col-md-8">
    <textarea class="form-control" name="description"onchange="isPayment1(this)" placeholder="Enter Description"  ></textarea>
    <span id="isPayment1" style="color:red"></span>
    </div>
    </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
 </form>
  </div>
</div>
<!------edit--------->
<div id="edit" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <form class="form-horizontal" role="form">
    <div class="modal-content">
      <div class="modal-header mbt">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title mdlhdr">Edit Misc Fee</h4>
         <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Client</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
      </div>
      <div class="modal-body">
      <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Fee Name</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Date</label>
      <div class="col-sm-3">
        <input type="text" class="form-control datepicker" name="" value="">
      </div>
      <label class="control-label col-sm-2 lblfnt">Amount</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Pay Mode</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3 lblfnt">Transaction ID</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="" value="">
      </div>
    </div>
   	<div class="form-group">
    <label class="control-label col-sm-3 lblfnt">Description</label>
    <div class="col-md-8">
    <textarea class="form-control" name=""></textarea>
    </div>
    </div>
      </div>
      <div class="modal-footer">
      <button type="submit" class="btn btn-success">Save</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
 </form>
  </div>
</div>
<!-- Classie --> 
<script src="js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
 <script>
 $.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 7,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">�</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">�</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
  	pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
      	pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

$(document).ready(function(){
    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
    
});
 </script>
<!--scrolling js--> 
<script src="js/jquery.nicescroll.js"></script> 
<script src="js/scripts.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="js/bootstrap.js"> </script>
