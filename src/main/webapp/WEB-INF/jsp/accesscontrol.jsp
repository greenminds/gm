
      <!-- main content start-->
      <div id="page-wrapper">
    <div class="main-page general">
          <div class="panel-info rothdr"> System / Administration / Access Control </div>
          <div class="panel-info widget-shadow fstpnl ">
        <h4 class="title2">Access Control</h4>
        <div class="col-md-12 crtbtnbtm"></div>
        <form class="form-horizontal">
        <div class="col-md-12">
        <div class="row">
        <div class="col-md-5">
        <div class="form-group">
        <select class="form-control">
        <option></option>
        <option></option>
        <option></option>
        </select>
        </div>
        <div class="form-group chckbdr ">
        <div class="col-md-4"><label class="control-label lblfnt"><input type="checkbox">Select All</label></div>
        <div class="col-md-8"><input type="text" class="form-control" name="" placeholder="Search The user"></div>
        <div class="col-md-12"><label class="control-label lblfnt"><input type="checkbox">Select</label></div>
        <div class="col-md-12"><label class="control-label lblfnt"><input type="checkbox">Select</label></div>
        <div class="col-md-12"><label class="control-label lblfnt"><input type="checkbox">Select</label></div>
        <div class="col-md-12"><label class="control-label lblfnt"><input type="checkbox">Select</label></div>
        <div class="col-md-12"><label class="control-label lblfnt"><input type="checkbox">Select</label></div>
        <div class="col-md-12"><label class="control-label lblfnt"><input type="checkbox">Select</label></div>
        </div>
        </div>
        <div class="col-md-7">
        <div class="tables">
        <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
        <tr>
        <th class="tblthd">Pages</th>
        <th class="tblthd">Permission</th>
        </tr>
        </thead>
        <tbody>
        <tr>
        <td>Opportunities</td>
        <td>
        <label class="control-label lblfnt"><input type="checkbox">View</label>
         <label class="control-label lblfnt"><input type="checkbox">Edit</label>
          <label class="control-label lblfnt"><input type="checkbox">Add</label>
           <label class="control-label lblfnt"><input type="checkbox">All</label>
        </td>
        </tr>
        <tr>
        <td>Enquiries</td>
        <td>
        <label class="control-label lblfnt"><input type="checkbox">View</label>
         <label class="control-label lblfnt"><input type="checkbox">Edit</label>
          <label class="control-label lblfnt"><input type="checkbox">Add</label>
           <label class="control-label lblfnt"><input type="checkbox">All</label>
        </td>
        </tr>
        <tr>
        <td>Referal</td>
        <td>
        <label class="control-label lblfnt"><input type="checkbox">View</label>
         <label class="control-label lblfnt"><input type="checkbox">Edit</label>
          <label class="control-label lblfnt"><input type="checkbox">Add</label>
           <label class="control-label lblfnt"><input type="checkbox">All</label>
        </td>
        </tr>
        </tbody>
        </table>
        </div>
        </div>
        </div>
        </div>
        </div>
        </form>
        <div class="clearfix"> </div>
      </div>
        </div>
        
        <div class="main-page general">
          <div class="panel-info"></div>
          <div class="panel-info widget-shadow">
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"><!--<a href="createuserdetails.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a>--> </div>
                  <div class="col-md-2">
                    <!--<div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>-->
                  </div>
                  <div class="col-md-2"> <a href="#" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
        </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">Group Name</th>
                  <th class="tblthd">User Name</th>
                  <th class="tblthd">Pages</th>
                  <th class="tblthd">Permission</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td></td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
                <tr>
                  <td>Table cell</td>
                  <td>Table cell</td>
                  <td>lvishnu15@gmail.com</td>
                  <td>9876543210</td>
                  <td>
                   <a href="#" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                 <tr>
                  <td>Table cell</td>
                  <td>Table cell</td>
                  <td>lvishnu@gmail.com</td>
                  <td>9876543210</td>
                  <td>
                   <a href="#" class="glyphicon glyphicon-edit edtbtn actn"></a>&nbsp;
                  <a href="#" class="glyphicon glyphicon-remove edtbtn actn"></a>
                  </td>
                </tr>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
        </div>
  </div>
     