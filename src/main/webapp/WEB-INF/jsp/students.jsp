<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Shinrai - Students</title>

<!-- Bootstrap -->

<link href="css/students.css" rel="stylesheet" type="text/css">
<link href="css/main-style.css" type="text/css" rel="stylesheet">
<link href="css/responsive.css" type="text/css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
/* Note: Try to remove the following lines to see the effect of CSS positioning */
.affix {
	top: 0;
	width: 100%;
	z-index: 999;
}
.affix + .container-fluid {
	padding-top: 70px;
}
.carousel-indicators {
	position: absolute;
	bottom: 10px;
	left: 50% !important;
	z-index: 15;
	width: 60%;
	padding-left: 0;
	margin-left: -30%;
	text-align: center;
	list-style: none;
}
.carousel-control {
    position: absolute;
    top: 42em !important;
    bottom: 0;
    left: 0;
    width: 5% !important;
    font-size: 20px;
    color: #fff;
    text-align: center;
    text-shadow: inherit;
    background-color: #fff !important;
    filter: inherit !important;
    opacity: .5;
}
.dropdown:hover .dropdown-menu {
    display: block;
    margin-top: 4%; // remove the gap so it doesn't close
 }
</style>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body>
<div class="container-fluid mrgntp">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="row"> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image47','','images/demo-new-site/icons/f-h.png',1)"><img src="images/demo-new-site/icons/f.png" alt="" width="30" height="30" id="Image47"></a>
       	     <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('dgdfgd','','images/demo-new-site/icons/g-h.png',1)"><img src="images/demo-new-site/icons/f-g.png" alt="" width="30" height="30" id="dgdfgd"></a> <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image49','','images/demo-new-site/icons/s-h.png',1)"><img src="images/demo-new-site/icons/s.png" alt="" width="30" height="30" id="Image49"></a><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image50','','images/demo-new-site/icons/t-h.png',1)"><img src="images/demo-new-site/icons/t.png" alt="" width="30" height="30" id="Image50"></a> </div>
          </div>
          <div class="col-md-4 col-sm-5 col-xs-12">
            <div class="row">
              <p class="ph"><a href="students.mm" class="phlr"><img src="images/icon/graduate-student-avatar.png"> &nbsp; Students </a>&nbsp; &nbsp; &nbsp; <a href="register.html" class="phlr"><img src="images/icon/icon.png"> &nbsp; Partner with us</a></p>
            </div>
          </div>
          <div class="col-md-4 col-sm-3 col-xs-12">
            <div class="row">
             <p><a href="register.mm" class="login"> Register </a> <a href="login.mm" class="login"> Log In </a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <hr class="mainhr">
  </div>
</div>
<nav class="navbar navbar-inverse" data-spy="affix" data-offset-top="197">
  <div class="container mrgnmnubtm">
    <div class="row"> 
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="homePage.mm"><img src="images/demo-new-site/logo.png" width="250" class="img-responsive"></a> </div>
      
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="homePage.mm">Home</a></li>
          <li><a href="about.mm">About</a></li>
          <li><a href="service.mm">Services</a></li>
          <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Dropdown <b class="caret"></b> </a>
            <ul class="dropdown-menu">
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
              <li><a href="#">Link</a></li>
            </ul>
          </li>
          <li><a href="blog.mm">Blog</a></li>
          <li><a href="contact.mm">Contact</a></li>
          <li> <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Search</a> </li>
        </ul>
        <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
          <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search" />
            </div>
            <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
          </form>
        </div>
      </div>
      <!-- /.navbar-collapse --> 
    </div>
  </div>
</nav>
<div class="container-fluid">
  <div class="row">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active"> <img src="images/bg-1.png" alt="Chania" height="350">
          <div class="carousel-caption">
            <h3>What We Offering</h3>
            <!--<p>The atmosphere in Chania has a touch of Florence and Venice.</p>-->
          </div>
        </div>
        <div class="item"> <img src="images/bg-1.png" alt="Chania" height="350">
          <div class="carousel-caption">
            <h3>Study With</h3>
             <!--<p>The atmosphere in Chania has a touch of Florence and Venice.</p>-->
          </div>
        </div>
        <div class="item"> <img src="images/bg-1.png" alt="Flower" height="350">
          <div class="carousel-caption">
            <h3>Canada</h3>
             <!--<p>Beatiful flowers in Kolymbari, Crete.</p>-->
          </div>
        </div>
      </div>
      
      <!-- Left and right controls --
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>--> 
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="container">
    <div class="col-md-12 col-sm-12">
      <h3 class="mnsrvc">What We Offer</h3>
    </div>
    <div class="col-md-12 col-sm-12">
      <p class="wrkp">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br>
        Ut enim ad minim veniam, exercitationullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in </p>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class='row'>
        <div class='col-md-12'>
          <div class="carousel slide media-carousel" id="media">
            <div class="carousel-inner">
              <div class="item  active">
                <div class="row">
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="row">
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                      <h4 class="stdntsp">Study In Germany</h4>
                    </div>
                    <div class="col-md-12"> <img src="images/students-bg.png" class="img-responsive center-block"> </div>
                    <div class="col-md-12">
                      <p class="stdntspara">Lorem ipsum dolor sit amet, sed
                        do eiusmod tempor incididunt ut labore et 
                        dolore magna aliqua. Ut minim veniam,
                        exercitationullamco laboris nisi ut  </p>
                    </div>
                    <div class="col-md-12"> <a href="#" class="stdnmainbtn"><img src="images/demo-new-site/icons/login.png"> Read more</a> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a data-slide="prev" href="#media" class="left carousel-control">‹</a> <a data-slide="next" href="#media" class="right carousel-control">›</a> </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="container cntmrgn">
    <hr>
    <div id="custom_carousel" class="carousel slide" data-ride="carousel" data-interval="2500"> 
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <div class="item active">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/canada.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Canada</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/germany.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Germany</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/australia.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Australia</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/hong-kong.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Canada</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/new-zealand.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Canada</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/Singapore.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Canada</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/australia.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Canada</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4"><img src="images/germany.png" class="img-responsive"></div>
              <div class="col-md-8">
                <h4 class="stdywt">Study In Canada</h4>
                <p class="stdntspara">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, labore, magni illum nemo ipsum quod voluptates ab natus nulla possimus incidunt aut neque quaerat mollitia perspiciatis assumenda asperiores consequatur soluta.</p>
                <a href="#" class="rgstrnw">Register Now</a> </div>
            </div>
          </div>
        </div>
        <!-- End Item --> 
      </div>
      <!-- End Carousel Inner -->
      <div class="controls">
        <ul class="nav">
          <li data-target="#custom_carousel" data-slide-to="0"><a href="#"><img src="images/canada-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="1"><a href="#"><img src="images/germany-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="2"><a href="#"><img src="images/australia-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="3"><a href="#"><img src="images/hong-kong-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="4"><a href="#"><img src="images/new-zealand-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="5"><a href="#"><img src="images/Singapore-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="6"><a href="#"><img src="images/australia-2.png" class="img-responsive"><small>Canada</small></a></li>
          <li data-target="#custom_carousel" data-slide-to="7"><a href="#"><img src="images/germany-2.png" class="img-responsive"><small>Canada</small></a></li>
        </ul>
      </div>
    </div>
    <!-- End Carousel --> 
  </div>
</div>
<div class="container-fluid ftrbg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12"><img src="images/demo-new-site/logo.png" width="200" class="img-responsive lgmrngs"></div>
            <div class="col-md-12 col-sm-12">
              <p class="ftrpara">Lorem ipsum dolor sit amet, consectetur sed
                do eiusmod tempor incididunt ut labore et 
                dolore magna aliqua.</p>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('6','','images/demo-new-site/icons/new/f-h.png',1)"><img src="images/demo-new-site/icons/new/f.png" alt="" width="30" height="30" id="6"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('7','','images/demo-new-site/icons/new/g-h.png',1)"><img src="images/demo-new-site/icons/new/g.png" alt="" width="30" height="30" id="7"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('8','','images/demo-new-site/icons/new/s-h.png',1)"><img src="images/demo-new-site/icons/new/s.png" alt="" width="30" height="30" id="8"></a></div>
              <div class="scl ftrgtmrgn"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('9','','images/demo-new-site/icons/new/t-h.png',1)"><img src="images/demo-new-site/icons/new/t.png" alt="" width="30" height="30" id="9"></a></div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Quick Contact</h4>
            </div>
            <!--address-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/location-pin(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp"> Lorem ipsum dolor sit amet consectetur,<br>
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            <!-------phone------>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/phone-receiver.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">+ 91 0123456789</p>
                </div>
              </div>
            </div>
            <!--mail-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/demo-new-site/icons/close-envelope(2).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">toshinri@gmail.com</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Information</h4>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="about.mm" class="ftrlinkp">About us</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Terms and conditions</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="#" class="ftrlinkp">Privacy policy</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="blog.mm" class="ftrlinkp">Blog</a></p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/log-in(1).png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p><a href="contact.mm" class="ftrlinkp">Contact us</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <h4 class="ftrhdr">Latest News</h4>
            </div>
            <!--1-->
            <marquee  behavior="scroll" direction="up" scrolldelay="250" behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/social-twitter-circular-button.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">Lorem ipsum dolor sit amet 
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/social-twitter-circular-button.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">Lorem ipsum dolor sit amet 
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            <!--1-->
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-1 col-sm-1 col-xs-1">
                <div class="row"><img src="images/icon/social-twitter-circular-button.png"></div>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <div class="row">
                  <p class="ftrkp">Lorem ipsum dolor sit amet 
                    adipisicing elit ,sed don</p>
                </div>
              </div>
            </div>
            </marquee>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <h3 class="strsrvc">© 2016 Shinrai. All rights reserved</h3>
</div>
<script>
    $(document).ready(function(ev){
    $('#custom_carousel').on('slide.bs.carousel', function (evt) {
      $('#custom_carousel .controls li.active').removeClass('active');
      $('#custom_carousel .controls li:eq('+$(evt.relatedTarget).index()+')').addClass('active');
    })
});
    </script> 
<script>
   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 70;
			 if ($(window).scrollTop() > navHeight) {
				 $('nav').addClass('fixed');
			 }
			 else {
				 $('nav').removeClass('fixed');
			 }
		});
	});
</script> 
<script>
$(document).ready(function(){
    $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
    });  
});
</script> 
<script>
$(document).ready(function() {
  $('#media').carousel({
    pause: true,
    interval: false,
  });
});
</script> 
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script>
   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 70;
			 if ($(window).scrollTop() > navHeight) {
				 $('nav').addClass('fixed');
			 }
			 else {
				 $('nav').removeClass('fixed');
			 }
		});
	});
</script> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="js/bootstrap.min.js"></script>
</body>
</html>