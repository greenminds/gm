<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
	<head>
	

      <!-- main content start-->
      <div id="page-wrapper">
    <div class="main-page general">
    <form>
      <div class="panel-info rothdr"> Pre-Sales / Opportunities/Leads / Refferals </div>
      <div class="panel-info widget-shadow">
        <h4 class="title2">Refferals </h4>
        <div class="col-md-12 crtbtnbtm">
        <div class="row">
        <div class="col-md-8">
        </div>
        <div class="col-md-4">
                <div class="row">
                <div class="col-md-2"></div>
                  <div class="col-md-2"> <!--<a href="createopportunitie.html" class="btn btn-warning crtbtn" data-tooltip="Create"> <span class="glyphicon glyphicon-plus"></span> </a>--> </div>
                  <div class="col-md-2 col-sm-1 col-xs-4">
                    <div class="dropdown">
                      <button class="btn btn-info dropdown-toggle crtbtn" type="button" data-toggle="dropdown" data-tooltip="Export"><i class="glyphicon glyphicon-export"></i></button>
                      <ul class="dropdown-menu">
                        <li><a href="#">XLS</a></li>
                        <li><a href="#">WORD</a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-1 col-xs-2"> <a href="#" class="btn btn-default" data-tooltip="Refresh"> <span class="glyphicon glyphicon-refresh "></span> </a> </div>
                  <div class="col-md-3 col-sm-1 col-xs-4">
                    <div class="dropdown"> <span class="btn-group">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" data-tooltip="Showing rows"><span class="page-size">25</span> <span class="caret"></span></button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#">10</a></li>
                        <li class="active"><a href="#">25</a></li>
                        <li><a href="#">50</a></li>
                        <li><a href="#">100</a></li>
                      </ul>
                      </span> </div>
                  </div>
                  <div class="col-md-1"></div>
                </div>
              </div>
                      </div>
        </div>
        <div class="tables">
          <div class="table-responsive bs-example">
            <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th class="tblthd">Refered Name</th>
                  <th class="tblthd">Referred on</th>
                  <th class="tblthd">Relation</th>
                  <th class="tblthd">Referred By</th>
                  <th class="tblthd">Status</th>
                  <th class="tblthd">Action</th>
                </tr>
                <tr>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td>
                  <input type="text" class="form-control">
                  </td>
                  <td></td>
                </tr>
                </thead>
                <tbody class="tbdy" id="myTable">
              <c:forEach var="ref" items="${ref}">
										<tr>
											<td>${ref.refferedName}</td>
											<td>${ref.refferedOn}</td>
											<td>${ref.relation}</td>
											<td>${ref.refferedBy}</td>
											<td>${ref.status}</td>
											<td><a href="editopportunity.mm"
												class="glyphicon glyphicon-edit edtbtn"></a>&nbsp; <a
												href="#" class="glyphicon glyphicon-remove edtbtn"></a></td>
										</tr>
									</c:forEach>
                </tbody>
            </table>
            <div class="col-md-12">
      <ul class="pagination pagination-lg pager pull-right" id="myPager"></ul>
      </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
      </form>
    </div>
  </div>
  
</body>
</html>
     