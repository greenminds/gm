function SearchRefund(res,field){
	
	var val = res.value;
	if (val.length > 0)
		AjaxController.RefundBy(val, field, Refundres);

	}

function SearchOnlinepay(res,field)
{
	var val = res.value;
	if (val.length > 0)
		AjaxController.onlinePayBy(val, field, OnlinepayDoc);

	}


function editInvoice(id){
		AjaxController.editInvoice(id, editResult);
}
function editResult(res){
	document.getElementById("editInEmail").value=res.email;
	document.getElementById("editInDate").value=res.date;
	document.getElementById("editInAmount").value=res.amount;
	document.getElementById("editInPaymode").value=res.paymentMode;
	document.getElementById("editInTransaction").value=res.transactionNo;
	document.getElementById("editInCoordinator").value=res.coordinator;
	document.getElementById("editInDescription").value=res.description;
	document.getElementById("invoiceId").value=res.id;
}
	
function editRefunds(id){
	AjaxController.editRefunds(id, editrefundResult);
}
function deleteRefunds(id){
	AjaxController.deleteRefundsBy(id, Refundres1);
}
function approvalRec(id){
	AjaxController.approvalRecBy(id, getApproveDoc1);
}
function approvalInvo(id){
	AjaxController.approvalInvoBy(id, getinvoiceDoc1);
}
function approvalMisc(id){
	AjaxController.approvalMiscBy(id, MiscfeeDoc1);
}

function editrefundResult(res){

document.getElementById("editInEmail").value=res.client;
document.getElementById("editInDate").value=res.date;
document.getElementById("editInAmount").value=res.amount;
document.getElementById("editInPaymode").value=res.payMode;
document.getElementById("editInDescription").value=res.description;
document.getElementById("refundId").value=res.id;
}



function OnlinepayDoc(result) { 
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].date + "</td><td>"
					+ result[i].amount + "</td><td>" + result[i].email
					+ "</td><td>" + result[i].clientId + "</td><td>" + result[i].refNo + "</td><td>" + result[i].payId + "</td><td>" + result[i].reqId + "</td><td>" + result[i].trainId + "</td></tr>"
					
		}
	

	document.getElementById("myTable").innerHTML = res;
	document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:result.length});
	}

function RefundDoc(result) { 
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].client + "</td><td>"
					+ result[i].amount + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].description + "</td><td>" + result[i].payMode 
					res += "<td><a href='#' data-tooltip='Edit' class='actn' data-toggle='modal' data-target='#invcedit'><span class='glyphicon glyphicon-edit edtbtn'></span></a><ul class='actnul'><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn'></span></a></ul></td></tr>";
		}
	
	document.getElementById("myTable").innerHTML = res;
	}
	
function Refundres(result) { 
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].client + "</td><td>"
					+ result[i].amount + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].description + "</td><td>" + result[i].payMode
					res +=  "</td><td><ul class='actnul'><li><a href='editclients.html' data-tooltip='Edit' class='actn'><span class='glyphicon glyphicon-edit edtbtn'  onclick='editRefunds(" + result[i].id + ")' ></span>";
			res += "</a></li>&nbsp;&nbsp;<li><a href='#' data-tooltip='Remove' class='actn'> <span class='glyphicon glyphicon-remove edtbtn' onclick='deleteRefunds("+ result[i].id + ")' ></span></a></li>&nbsp;&nbsp;</a></li></ul></td></tr>";
		
		}	
	document.getElementById("myTable").innerHTML = res;
	document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:result.length});
	
}

function Refundres1(result) { 
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].client + "</td><td>"
					+ result[i].amount + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].description + "</td><td>" + result[i].payMode
					res +=  "</td><td><ul class='actnul'><li><a href='editclients.html' data-tooltip='Edit' class='actn'><span class='glyphicon glyphicon-edit edtbtn'  onclick='editRefunds(" + result[i].id + ")' ></span>";
			res += "</a></li>&nbsp;&nbsp;<li><a href='#' data-tooltip='Remove' class='actn'> <span class='glyphicon glyphicon-remove edtbtn' onclick='deleteRefunds("+ result[i].id + ")' ></span></a></li>&nbsp;&nbsp;</a></li></ul></td></tr>";
		
		}	
	document.getElementById("myTable").innerHTML = res;
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:result.length});
	
}


	function SearchMiscfee(res,field){
		
		var val = res.value;
		if (val.length > 0)
			AjaxController.MiscfeeBy(val, field, MiscfeeDoc);

		}


		function MiscfeeDoc(result) { 
		
		var res = "";
		if (result.length > 0)
			for (var i = 0; i < result.length; i++) {
				res += "<tr><td>" + result[i].id + "</td><td>"
						+ result[i].client + "</td><td>" + result[i].date
						+ "</td><td>" + result[i].feeName + "</td><td>" + result[i].amonut + "</td><td>" + result[i].payMode + "</td><td>" + result[i].transation + "</td><td>" + result[i].description 
						res += "<td><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn' onclick='approvalMisc(" + result[i].id + ")' ></span></a></ul></td></tr>";
			}
		

		document.getElementById("myTable").innerHTML = res;
		document.getElementById("myPager").innerHTML = "";
		
		$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:result.length});
		}


		function MiscfeeDoc1(result) { 
		
		var res = "";
		if (result.length > 0)
			for (var i = 0; i < result.length; i++) {
				res += "<tr><td>" + result[i].id + "</td><td>"
						+ result[i].client + "</td><td>" + result[i].date
						+ "</td><td>" + result[i].feeName + "</td><td>" + result[i].amonut + "</td><td>" + result[i].payMode + "</td><td>" + result[i].transation + "</td><td>" + result[i].description 
						res += "<td><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn' onclick='approvalMisc(" + result[i].id + ")' ></span></a></ul></td></tr>";
			}
		

		document.getElementById("myTable").innerHTML = res;
document.getElementById("myPager").innerHTML = "";
		
		$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
		}




function Searchinvoice(res,field){
	
		var val = res.value;
	if (val.length > 0)
	AjaxController.SearchinvoicetBy(val, field, getinvoiceDoc);

}

function SearchReceipt(res, field){
	
	var val = res.value;
	if (val.length > 0)
		AjaxController.searchReceipt(val, field, getApproveDoc);

}
function getApproveDoc(result) { 
	
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].id + "</td><td>"
					+ result[i].email + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].amount + "</td><td>" + result[i].paymentMode + "</td><td>" + result[i].transactionNo + "</td><td>" + result[i].description + "</td><td>" 
					res += "<td><ul class='actnul'><li><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn' onclick='approvalRec(" + result[i].id + ")' ></span></a></li></ul></td></tr>";
		}
	
	document.getElementById("myTable").innerHTML = res;
	document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:result.length});
}

function getApproveDoc1(result) { 
	
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].id + "</td><td>"
					+ result[i].email + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].amount + "</td><td>" + result[i].paymentMode + "</td><td>" + result[i].transactionNo + "</td><td>" + result[i].description + "</td><td>" 
					res += "<td><ul class='actnul'><li><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn' onclick='approvalRec(" + result[i].id + ")' ></span></a></li></ul></td></tr>";
		}
	
	document.getElementById("myTable").innerHTML = res;
	document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});
}


function pagination(val) {
	
document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:val});
	
	
}
function getinvoiceDoc(result) { 
	
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].id + "</td><td>"
					+ result[i].email + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].amount + "</td><td>" + result[i].paymentMode + "</td><td>" + result[i].transactionNo + "</td><td>" + result[i].description + "</td><td>" + result[i].coordinator 
					res += "<td><a href='#' data-tooltip='Edit' class='actn' data-toggle='modal' data-target='#invcedit'><span class='glyphicon glyphicon-edit edtbtn' onclick='editInvoice(" + result[i].id + ")' ></span></a><ul class='actnul'><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn' onclick='approvalInvo(" + result[i].id + ")' ></span></a></ul></td></tr>";
		}
	
	document.getElementById("myTable").innerHTML = res;
	document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:result.length});

}

function getinvoiceDoc1(result) { 
	
	var res = "";
	if (result.length > 0)
		for (var i = 0; i < result.length; i++) {
			res += "<tr><td>" + result[i].id + "</td><td>"
					+ result[i].email + "</td><td>" + result[i].date
					+ "</td><td>" + result[i].amount + "</td><td>" + result[i].paymentMode + "</td><td>" + result[i].transactionNo + "</td><td>" + result[i].description + "</td><td>" + result[i].coordinator 
					res += "<td><a href='#' data-tooltip='Edit' class='actn' data-toggle='modal' data-target='#invcedit'><span class='glyphicon glyphicon-edit edtbtn' onclick='editInvoice(" + result[i].id + ")' ></span></a><ul class='actnul'><a href='#' data-tooltip='Go to Approval' class='actn'><span class='glyphicon glyphicon-ok edtbtn' onclick='approvalInvo(" + result[i].id + ")' ></span></a></ul></td></tr>";
		
		}
	document.getElementById("myTable").innerHTML = res;
document.getElementById("myPager").innerHTML = "";
	
	$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:3});	
}
function isAmount(txtObj) {
	var Phon = txtObj.value;
	
	for (var i = 0; i < Phon.length; i++) {
		var ch = Phon.substring(i, i + 1);
		if ((ch < "0" || "9" < ch)&& ch != '.') {
			document.getElementById("isAmount").innerHTML ="Please enter the valid Amount!";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false
		}
		else
		{
		document.getElementById("isAmount").innerHTML ="";
		}
	}

}

function isEmail(txtObj) {
	var mail=txtObj.value
	if (mail=="")
	{   
	
		document.getElementById("isEmail").innerHTML = "This field is blank.Please enter Email-ID.";      
		setTimeout((function() {
			txtObj.select();
			txtObj.focus()
		}), 0);
		return false;
	if (mail.indexOf("@") == -1 || mail.indexOf("@") + 1 == mail.length
			|| mail.indexOf(".") == -1 || mail.indexOf(".") + 1 == mail.length
			|| mail.substring(0, 1) == ".") {
		document.getElementById("isEmail").innerHTML ="Invalid Email ID";
		setTimeout((function() {
			txtObj.select();
			txtObj.focus()
		}), 0);
		return false;
	} else {

		var n = mail.indexOf("@")
		var n1 = mail.indexOf(".", n)
		var n2 = mail.lastIndexOf(".", n)
		if (n + 1 == n1 || n - 1 == n2) {
			document.getElementById("isEmail").innerHTML ="Invalid Email ID";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false;
		}
	}

	var k = 0
	for (var j = 0; j < mail.length; j++) {
		if ((mail.substring(j, j + 1) >= 'a' && mail.substring(j, j + 1) <= 'z')
				|| (mail.substring(j, j + 1) >= '0' && mail.substring(j, j + 1) <= '9')
				|| mail.substring(j, j + 1) == '.'
				|| mail.substring(j, j + 1) == '@'
				|| mail.substring(j, j + 1) == '_') {
		} else {
			document.getElementById("isEmail").innerHTML ="Invalid Email ID";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false;
		}
		if (mail.substring(j, j + 1) == ".") {
			if (k + 1 == j && j != 1) {
				document.getElementById("isEmail").innerHTML ="Invalid Email ID";
				setTimeout((function() {
					txtObj.select();
					txtObj.focus()
				}), 0);
				return false;
			}
			k = j
		}
	}

	if (mail.lastIndexOf(".") + 1 == mail.length
			|| mail.lastIndexOf("@") + 1 == mail.length) {
		document.getElementById("isEmail").innerHTML ="Invalid Email ID";
		setTimeout((function() {
			txtObj.select();
			txtObj.focus()
		}), 0);
		return false;
	}

	if (mail.indexOf("@") != mail.lastIndexOf("@")) {
		document.getElementById("isEmail").innerHTML ="Invalid Email ID";
		setTimeout((function() {
			txtObj.select();
			txtObj.focus()
		}), 0);
		return false;
	}
	else
	{
	document.getElementById("isEmail").innerHTML ="";
	}
}
}


function isPayment(txtObj) {

	var str = txtObj.value;
	
	for (var i = 0; i < str.length; i++) {
		var ch = str.substring(i, i + 1);
		if (((ch < "a" || "z" < ch) && (ch < "A" || "Z" < ch)) && (ch < "0" || "9" < ch) && ch != ' ' && ch != '-') {
			document.getElementById("isPayment").innerHTML =" field accepts only alphabets, hyphen,spaces...Please re-enter !!.";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false
		}
		else
		{
		document.getElementById("isPayment").innerHTML ="";
		}
	}
}

function isPayment1(txtObj) {

	var str = txtObj.value;
	
	for (var i = 0; i < str.length; i++) {
		var ch = str.substring(i, i + 1);
		if (((ch < "a" || "z" < ch) && (ch < "A" || "Z" < ch)) && (ch < "0" || "9" < ch) && ch != ' ' && ch != '-') {
			document.getElementById("isPayment1").innerHTML =" field accepts only alphabets, hyphen,spaces...Please re-enter !!.";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false
		}
		else
		{
		document.getElementById("isPayment1").innerHTML ="";
		}
	}
}

function isPayment2(txtObj) {

	var str = txtObj.value;
	
	for (var i = 0; i < str.length; i++) {
		var ch = str.substring(i, i + 1);
		if (((ch < "a" || "z" < ch) && (ch < "A" || "Z" < ch)) && (ch < "0" || "9" < ch) && ch != ' ' && ch != '-') {
			document.getElementById("isPayment2").innerHTML =" field accepts only alphabets, hyphen,spaces...Please re-enter !!.";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false
		}
		else
		{
		document.getElementById("isPayment2").innerHTML ="";
		}
	}
}

function isTransaction(txtObj) {

	var str = txtObj.value;
	
	for (var i = 0; i < str.length; i++) {
		var ch = str.substring(i, i + 1);
		if ((ch < "A" || "Z" < ch) && (ch < "0" || "9" < ch)) {
			document.getElementById("isTransaction").innerHTML =" field accepts only UpperCase ,numbers,spaces, hyphen &...Please re-enter !!.";
			setTimeout((function() {
				txtObj.select();
				txtObj.focus()
			}), 0);
			return false;
		}
		else
		{
		document.getElementById("isTransaction").innerHTML ="";
		}
	}
}
