// JavaScript Document
window.onload = function initMap(){
 //googlemap is the id of the placeholder in map.html
 if(document.getElementById('googlemap')){ 
  //your office coordinates latitude / longitude, if you change the variable name, you will also need to update line 8 and 14
  var office = new google.maps.LatLng(13.035157, 80.216059); 
  // zoom level defines the default scale of the map, the center is aligned to the coordinates from the previous line, the backgroundColor is the colour of the placeholder while the map loads (make it the same colour as your website background
  var mapOptions = { zoom: 12, center:office, backgroundColor: "#252222",scrollwheel: false, };
  // generates map with the options defined above
  var map = new google.maps.Map(document.getElementById("googlemap"), mapOptions);
  // this is the InfoWindow (speech bubble) that contains the logo, it can contain HTML instead of an image
  // adding a style="display:block" to fix the wrong size issue when InfoWindow only contains an image
  
  //the marker. If you do not want the marker to bounce, simply deleted the line that contains animation: google.maps.Animation.BOUNCE and uncomment the one below
  //var marker = new google.maps.Marker({ position:office, map: map}); 
  var marker = new google.maps.Marker({ position:office, map: map, animation: google.maps.Animation.BOUNCE }); 
  //opens the InfoWindow, not needed if InfoWindow is not used
  //infowindow.open(map,marker);
  //move the center of the map left and down to allow space to view the infowindow, not needed if InfoWindow is not used
  map.panBy(10,70);

  //function that makes the marker stop bouncing when clicked (it can be annoying). Delete it if not using the bouncing marker
  google.maps.event.addListener(marker, 'click', function() { 
  if (this.getAnimation() != null) { this.setAnimation(null); } 
  else { this.setAnimation(google.maps.Animation.BOUNCE); } });

  //map customisation
  //colours of the different map elements
  //change the hue hex colour to the main colour of your site and it should look ok
  var colours = [
   { featureType: "administrative", elementType: "all", stylers: [ { saturation: 0 }, { hue: "#000" } ]},
   { featureType: "landscape", elementType: "all", stylers: [ { hue: "#dbfed0" }, { saturation: 0 }, { lightness: 50} ] },
   { featureType: "poi", elementType: "all", stylers: [ { hue: "#9cf680" }, { saturation: -100 } ] },
   { featureType: "road", elementType: "all", stylers: [ { hue: "#fdf1a0" }, { saturation: -100 } ] },
   { featureType: "transit", elementType: "all", stylers: [ { hue: "#000" }, { saturation:-100  } ] },
   { featureType: "water", elementType: "all", stylers: [ { hue: "#333" }, { saturation: -100 }, { lightness: 50 } ] } 
  ];

  //applying colours to map
  var styledMapOptions = { map: map, name: "Map"}
  var myStyledMap = new google.maps.StyledMapType(colours,styledMapOptions);

  //defining new map style
  map.mapTypes.set('colouredMap', myStyledMap);
  map.setMapTypeId('colouredMap');

  //setting up the navigation options as a dropdown with thee options: your custom map colours, satellite view and hybrid view
  map.setOptions({ mapTypeControl: true, mapTypeControlOptions: { style: google.maps.MapTypeControlStyle.DROPDOWN_MENU, mapTypeIds: ['colouredMap', google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID] } });
 }
}