package com.cryonez.dao;

import java.util.List;

import com.cryonez.entity.Category;
import com.cryonez.entity.Country;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;




/**
 * @author s6
 *
 */
/**
 * @author s6
 *
 */
public interface UserDAO {




	
	public User getUser();

	public void save(Opportunities opp);

	public List<Opportunities> getOpportunity();

	public List<Opportunities> getOpportunityById(Long id);

	public List<Opportunities> getSearchVal(String val);

	public List<Opportunities> getStatusVal(boolean val);

	public void deleteOpp(Long id);

	public List<Enquiries> getEnquiries();

	public void update(Opportunities opp);

	public Enquiries getEnquiriesById(Long enqId);

	public List<Refferal> getRefferal();

	public List<Enquiries> getSearchEnquiry(String val);

	public void saveCategory(Category cat);

	public List<Category> getClientCategories();

	public Category getThisCategory(String id);

	public List<Category> getCategoryLikeThis(String val);

	public void saveSources(Source src);

	public List<Source> getSources();

	public Source getThisSource(String id);

	public List<Source> getSorcesBySearchValues(String val);

	public void saveDesignation(Designation dsg);

	public List<Designation> getAllDesignations();

	public Designation getThisDesignation(String id);

	public List<Designation> getDesignationLikeThis(String val);

	public void saveEmployee(Employee emp);

	public List<Employee> getAllEmployees();

	public Employee getThisEmployee(String id);

	public List<Employee> getEmployeesLikeThis(String val);

	public List<Country> getAllCountries();

	public void deleteDoc(Long id);
	   
	
}
