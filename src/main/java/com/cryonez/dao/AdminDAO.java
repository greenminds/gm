package com.cryonez.dao;

import java.util.List;

import com.cryonez.entity.Category;
import com.cryonez.entity.Client;
import com.cryonez.entity.Country;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.Followup;
import com.cryonez.entity.Miscfee;
import com.cryonez.entity.OnlinePay;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Receipt;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.Refund;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;
import com.cryonez.entity.Invoice;
/**
 * @author s6
 *
 */
/**
 * @author s6
 *
 */
public interface AdminDAO {



	public List<Client> getAllClients();
	public List<Employee> getAllEmployee();
	public List<Client> getUserDetail();
	public List<Client> getClientsByStatus(String val);
	
	public List<User> getNotUpAgreeByList(String val);
	public List<User> getfilteragreeaproveByList(String val);
	public List<User> getfilteragreeuploadByList(String val);
	
	public List<Client> getClientsByStatusByField(String val, String field,String type);

	public Client getClientsById(String clientId);
	
	public List<Employee> SearchThisUserStatusInfo(String val, String field);
	
	public List<User> SearchDocuploadlistStatusinfo(String val, String field);
	public List<Employee> getEmpByDesignation(String designation);
	
	public List<User> getfileuploadBylist();
	public List<Miscfee> MiscfeeByListInfo(String val, String field);
	public List<User> getdocumentApprovalByList();
	public List<User> getagreementApprovalByList();
	public List<User> getagreementuploadByList();
	public List<User> AgreeuploadByUserByField(String val, String field, String type);
	public void saveReceiptBy(Receipt receipt);
	public void saveMiscfeeBy(Miscfee miscfee);
	public List<Receipt> getReceiptBy();
	
	public List<Miscfee> getMiscfeeBy();
	public List<Receipt> SearchReceiptByListInfo(String val, String field);
	public List<User> SearchDocApproveByListInfo(String val, String field, String type);
	public List<User> AgreeApproveByListInfo(String val, String field, String type);
	public Client getClientByEmail(String email);
	public List<Invoice> getinvoiceBy();
	public void saveInvoiceBy(Invoice invoicer);
	public List<Invoice> SearchinvoicetByListInfo(String val, String field);
	     
	public void saveRefundBy(Refund refunder);
	
	public List<Refund> getRefundBy();
	
	public List<Refund> RefundByListInfo(String val, String field);
	
	public List<Employee> getCoordinatorByList();
	public void deleteClientById(Client clt);
	public Invoice getInvoiceById(String id);
	public Refund getRefundByIdInfo(String id);
	public Client deleteClientsByIdInfo(String id);
	public void saveClientsBy(Client clientList);
	public Receipt approvalAccRecByIdInfo(String id);
	public Invoice approvalInvoByIdInfo(String id);
	public Miscfee approvalMiscByIdInfo(String id);
	 
	
	public List<OnlinePay> getOnlineByList();
	public List<OnlinePay> onlinePayByListInfo(String val, String field);
	public List<Refund> getRefunderBy();
	public List<Client> getClientsByList();
	public List<Client> SearchClientCoByFieldInfo(String val, String field, String type);
	public List<User> SearchUserFUCoByFieldInfo(String val, String field);
	public List<User> SearchUserAUCoByFieldInfo(String val, String field, String type);
	public List<User> SearchUserDACoByFieldInfo(String val, String field, String type);
	public List<User> SearchUserAACoByFieldInfo(String val, String field, String type);
	public Followup getFollowupByClient(String clientId, String type);
	public void saveFolloup(Followup followup); 
	  
}
