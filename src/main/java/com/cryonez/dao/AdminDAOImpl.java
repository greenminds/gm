package com.cryonez.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.cryonez.entity.Client;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Followup;
import com.cryonez.entity.Miscfee;
import com.cryonez.entity.OnlinePay;
import com.cryonez.entity.Receipt;
import com.cryonez.entity.Refund;
import com.cryonez.entity.User;
import com.cryonez.entity.Invoice;



/**
 * @author ramalingam.a
 *
 */
@SuppressWarnings("unused")
@Component
@Scope("singleton")
public class AdminDAOImpl implements AdminDAO {

	private Logger LOG = Logger.getLogger(AdminDAOImpl.class);
	Session session = null;
	private static HibernateTemplate ht;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.ht = new HibernateTemplate(sessionFactory);
	}

	@Override
	public List<Client> getAllClients() {
		List<Client> clientList = ht.find("from Client c where c.deleted=false");
		return clientList;
	}
	
	/*public List<Refund> getRefundBy() {
		List<Refund> refunder=ht.find("from Refund r where r.deleted=false");
		return refunder;
		}*/
	@Override
	public List<Employee> getAllEmployee() {
		List<Employee> clientList = ht.find("from Employee c");
		return clientList;
	}

	@Override
	public List<User> getfileuploadBylist() {
		boolean status = true;
		List<User> fileuploadList = ht.find("from User c where c.isDocumentuploaded = ?", status);
		return fileuploadList;
	}
	
	public List<User> getdocumentApprovalByList() { 
		boolean status = false;
		List<User> documentAppList = ht.find("from User c where c.isDocumentApprovaled = ?", status);
		return documentAppList;
		
		
			
	}
public List<User> getagreementuploadByList() {
		
		
		boolean status = false;
		List<User> DocuApprovalList = ht.find("from User c where c.iAgreementuploaded = ?", status);
		return DocuApprovalList;
		}
		
	
public List<User> getagreementApprovalByList() {
		
		
		boolean status = false;
		List<User> agreementApprovalList = ht.find("from User c where c.isAgreementApprovaled = ?", status);
		return agreementApprovalList;
		}
		
			
	
	@Override
	public List<Client> getClientsByStatus(String val) {
		List<Client> clientList = null;
		if(val.equalsIgnoreCase("all"))
		clientList =ht.find("from Client c where c.deleted=false ");
		else
	 clientList = ht.find("from Client c where c.deleted = false and c.clientStatus  = ?", val);
	return clientList;
}
		
	@SuppressWarnings("unchecked")
	public List<User> getNotUpAgreeByList(String val) {
		
		if(val.equalsIgnoreCase("Approve"))
		{
			boolean status = true;
			List<User> NotDocAgreeList = ht.find("from User c where c.isDocumentApprovaled = ?", status);
		return NotDocAgreeList;
		}
		else
		{
			boolean status = false;
			List<User> NotDocAgreeList = ht.find("from User c where c.isDocumentApprovaled = ?", status);
			return NotDocAgreeList;
		
		}
		
		
	}
	@Override
	public List<User> getfilteragreeaproveByList(String val) {
		
		if(val.equalsIgnoreCase("Approve"))
		{
			boolean status = true;
			List<User> agreementApprovalList = ht.find("from User c where c.isAgreementApprovaled = ?", status);
		return agreementApprovalList;
		}
		else
		{
			boolean status = false;
			List<User> agreementApprovalList = ht.find("from User c where c.isAgreementApprovaled = ?", status);
			return agreementApprovalList;
		
		}
		
		
	}
	@Override
	public List<User> getfilteragreeuploadByList(String val) {
		
		if(val.equalsIgnoreCase("Approve"))
		{
			boolean status = true;
			List<User> agreementuploadList = ht.find("from User c where c.iAgreementuploaded = ?", status);
		return agreementuploadList;
		}
		
		
		else
		{
			boolean status = false;
			List<User> agreementuploadList = ht.find("from User c where c.iAgreementuploaded = ?", status);
			return agreementuploadList;
		
		}
		
		
	}
	
	@Override
	public List<Client> getClientsByStatusByField(String val, String field,String type) {
		List<Client> clientList = null;
		if(StringUtils.isNotBlank(type))
		{
		if(type.equalsIgnoreCase("all"))
		{
		clientList = ht.find("from Client c where c.deleted=false and c."+field+" like ?", val+"%");
		
		}
		else{
			
			clientList = ht.find("from Client c where c.clientStatus = ? and c."+field+" like ?", type, val+"%" );
		}
		}
		else
		{
			clientList = ht.find("from Client c where c.deleted=false and c."+field+" like ?", val+"%");	
		}
		return clientList;
		
	}
	
	@Override
	public List<Client> SearchClientCoByFieldInfo(String val, String field,String type) {
		List<Client> clientList = null;
		if(StringUtils.isNotBlank(type))
		{
		if(type.equalsIgnoreCase("all"))
		{
			
		clientList = ht.find("from Client c where c.deleted=false and c."+field+" like ?", val);
		
		}
		else
		{
			clientList = ht.find("from Client c where c.clientStatus = ? and c."+field+" like ?", type, val );
		}		
		}
		else
		{
			clientList = ht.find("from Client c where c.deleted=false and c."+field+" like ?", val);	
		}
		return clientList;
		
	}
	
	
	@Override
	public List<User> SearchUserAUCoByFieldInfo(String val, String field,String type) {
		List<User> agreementuploadList = null;
		if(StringUtils.isNotBlank(type))
		{
		if(type.equalsIgnoreCase("pending"))
		{
	
			agreementuploadList = ht.find("from User c where c."+field+" like ?", val);
		}
			else
			{
			agreementuploadList = ht.find("from User c where  c."+field+" like ?", type, val );
			}
		}
		else
		{
			agreementuploadList = ht.find("from User c where c."+field+" like ?", val);	
		}
		return agreementuploadList;
		
	}
	
	

	@Override
	public List<User> SearchUserDACoByFieldInfo(String val, String field,String type) {
		List<User> DocuApprovalList = null;
		if(StringUtils.isNotBlank(type))
		{
		if(type.equalsIgnoreCase("pending"))
		{
			DocuApprovalList = ht.find("from User c where c."+field+" like ?", val);
		
		}
		else
		{
			DocuApprovalList = ht.find("from User c where  c."+field+" like ?", type, val );
		
		}
		}
		else
		{
			DocuApprovalList = ht.find("from User c where c."+field+" like ?", val);	
		}
		return DocuApprovalList;
		
	}
	
	@Override
	public List<User> SearchUserAACoByFieldInfo(String val, String field,String type) {
		List<User> agreementApprovalList = null;
		if(StringUtils.isNotBlank(type))
		{
		if(type.equalsIgnoreCase("pending"))
		{
		
			agreementApprovalList = ht.find("from User c where c."+field+" like ?", val);
		}
			else
			{
			agreementApprovalList = ht.find("from User c where  c."+field+" like ?", type, val );
		
		}
		}
		else
		{
			agreementApprovalList = ht.find("from User c where c."+field+" like ?", val);	
		}
		return agreementApprovalList;
		
	}
	
	public List<User> SearchUserFUCoByFieldInfo(String val, String field ) {
		List<User> fileuploadList = null;
		
			fileuploadList = ht.find("from User c where  c."+field+" like ?", val);
		
		return fileuploadList;
		
	}
	
	@Override
	public List<Employee> SearchThisUserStatusInfo(String val, String field) {
		List<Employee> emp = null;
		emp = ht.find("from Employee c where c."+field+" like ?", val+"%");
    	return emp;
		
	}
	
	public List<User> SearchDocuploadlistStatusinfo(String val, String field) {
		List<User> Doc = null;
		if(StringUtils.isNotBlank(val)){
		Doc = ht.find("from User c where c.isDocumentuploaded=true and c."+field+" like ?", val+"%");
		}
		else
		{
			Doc = ht.find("from User c where c.isDocumentuploaded ");	
		}
		return Doc;
	}

	public List<Receipt> SearchReceiptByListInfo(String val, String field) {
		List<Receipt> reciept = null;
		boolean approv = false;
		if(StringUtils.isNotBlank(val)){
		reciept = ht.find("from Receipt c where c.approval=false and c."+field+" like ?" , val+"%" );
		}
		else
		{
			reciept = ht.find("from Receipt c where c.approval=false");	
		}
		return reciept;
		
	}

	@SuppressWarnings("unchecked")
	public List<Invoice> SearchinvoicetByListInfo(String val, String field) {
		List<Invoice> invoicer = null;
		if(StringUtils.isNotBlank(val)){
		invoicer = ht.find("from Invoice c where c.approval=false and c."+field+" like ?", val+"%");
		}
		else
		{
			invoicer = ht.find("from Invoice c where c.approval=false");	
		}
    	return invoicer;
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> AgreeuploadByUserByField(String val, String field, String type) {
		List<User> UserList = null;
		if(StringUtils.isNotBlank(type))
		{
			
		if(type.equalsIgnoreCase("pending"))
		{
			
			UserList = ht.find("from User c where c.iAgreementuploaded = false and c."+field+" like ?", val+"%" );
		
			
			}
		else
		{
			
			UserList = ht.find("from User c where c.iAgreementuploaded = true and c."+field+" like ?", val+"%" );
		
		}
		}
		else{
			UserList = ht.find("from User c where c.iAgreementuploaded = false and c."+field+" like ?", val+"%" );
				
		}
		return UserList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> SearchDocApproveByListInfo(String val, String field,String type) {
		List<User> UserList = null;
		if(StringUtils.isNotBlank(type))
		{
		
		if(type.equalsIgnoreCase("pending"))
		{
			UserList = ht.find("from User c where c.isDocumentApprovaled =false and c."+field+" like ?", val+"%");
		
		
			}
		else
		{

			UserList = ht.find("from User c where c.isDocumentApprovaled =true and c."+field+" like ?", val+"%");
		
		}
		}
		else
		{
			UserList = ht.find("from User c where c.isDocumentApprovaled =false and c."+field+" like ?", val+"%");	
		}
		return UserList;
		}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> AgreeApproveByListInfo(String val, String field,String type) {
		List<User> UserList = null;
		if(StringUtils.isNotBlank(type))
		{
		
		if(type.equalsIgnoreCase("pending"))
		{
			
			UserList = ht.find("from User c where c.isAgreementApprovaled =false and c."+field+" like ?",  val+"%");
			
			}
		else
		{
			
			UserList = ht.find("from User c where c.isAgreementApprovaled =true and c."+field+" like ?", val+"%");
			
		}
		}
		else
		{
			UserList = ht.find("from User c where c.isAgreementApprovaled =false and c."+field+" like ?",  val+"%");	
		}
		
		return UserList;
	}
	
	public Client getClientsById(String clientId) {
		Client client = null;
		List<Client> clientList = ht.find("from Client c where c.id = ?", Integer.parseInt(clientId) );
		if(clientList != null && clientList.size() > 0)
			client = clientList.get(0);
		return client;
	}

	@Override
	public List<Client> getUserDetail() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	public List<Employee> getEmpByDesignation(String designation) {
		List<Employee> EmpList = null;
		if(StringUtils.isNotBlank(designation))
		EmpList = ht.find("from Employee e where e.emp_designation = ?", designation);
    	return EmpList;
	}

	
	public List<Employee> getCoordinatorByList() {
		List<Employee> coordinators = null;
		coordinators = ht.find("from Employee ");
    	return coordinators;
	}

	public List<User> getfileuploadlist() {
		// TODO Auto-generated method stub
		return null;
	}

	



@Override
public void  saveReceiptBy(Receipt receipt) {
	ht.saveOrUpdate(receipt);
	
}

@Override
public void  saveMiscfeeBy(Miscfee miscfee) {
	ht.saveOrUpdate(miscfee);
	
}

public List<Receipt> getReceiptBy() {
	List<Receipt> receipt=ht.find("From Receipt r where r.approval=false");
	return receipt;
}

public List<Miscfee> getMiscfeeBy() {
	List<Miscfee> miscfee=ht.find("From Miscfee m where m.approval=false");
	return miscfee;
}
public Client getClientByEmail(String email) {
	Client client = null;
	List<Client> clientList=ht.find("From Client c where c.email = ?", email);
	if(clientList != null && clientList.size() > 0){
		client = clientList.get(0);
	}
	return client;
}


@Override
public List<Invoice> getinvoiceBy() {
	List<Invoice> invoicer=ht.find("From Invoice i where i.approval=false");
	return invoicer;
}

@Override
public void saveInvoiceBy(Invoice invoicer) {
	ht.saveOrUpdate(invoicer);
	
}


public List<Refund> RefundByListInfo(String val, String field) {
	List<Refund> refunder = null;
	if(StringUtils.isNotBlank(val)){
	refunder = ht.find("from Refund c where c.deleted=false and c."+field+" like ?", val+"%");
	}
	else
	{
		refunder = ht.find("from Refund c where c.deleted=false and");	
	}
	return refunder;
	
}

@Override
public void  saveRefundBy(Refund refunder) {
ht.saveOrUpdate(refunder); 

}

public void  saveClientsBy(Client clientList) {
ht.saveOrUpdate(clientList); 

}
public List<Refund> getRefundBy() {
List<Refund> refunder=ht.find("from Refund r where r.deleted=false");
return refunder;
}

public List<Refund> getRefunderBy() {
List<Refund> refunder=ht.find("from Refund r where r.deleted=false");
return refunder;
}

public List<Client> getClientsByList() {
List<Client> clientList=ht.find("from Client c where c.deleted=false");
return clientList;
}

public List<Miscfee> MiscfeeByListInfo(String val, String field) {
	List<Miscfee> miscfee = null;
	if(StringUtils.isNotBlank(val)){
	miscfee = ht.find("from Miscfee c where c.approval=false and c."+field+" like ?", val+"%");
	}
	else
	{
		miscfee = ht.find("from Miscfee c where c.approval=false");	
	}
	return miscfee;
	
}

@Override
public void deleteClientById(Client clt) {
	ht.delete(clt);
	
}

@Override
public Invoice getInvoiceById(String id) {
		Invoice invoice = null;
		List<Invoice> invoiceList = ht.find("from Invoice i where i.id = ?", Integer.parseInt(id));
		if(invoiceList != null && invoiceList.size() > 0)
			invoice = invoiceList.get(0);
		return invoice;
		
}

@Override
public Refund getRefundByIdInfo(String id) {
	Refund refunder = null;
	List<Refund> refunderList = ht.find("from Refund i where i.id = ?", Integer.parseInt(id));
	if(refunderList != null && refunderList.size() > 0)
		refunder = refunderList.get(0);
	return refunder;
}


public Receipt approvalAccRecByIdInfo(String id) {
	Receipt reciept1 = null;
	List<Receipt> recieptList = ht.find("from Receipt i where i.id = ?", Integer.parseInt(id));
	if(recieptList != null && recieptList.size() > 0)
		reciept1 = recieptList.get(0);
	return reciept1;
}
public Invoice approvalInvoByIdInfo(String id) {

	Invoice invoicer1 = null;
	List<Invoice> invoicerList = ht.find("from Invoice i where i.id = ?", Integer.parseInt(id));
	if(invoicerList != null && invoicerList.size() > 0)
		invoicer1= invoicerList.get(0);
	return invoicer1;
}


public Miscfee approvalMiscByIdInfo(String id) {
	Miscfee miscfee1 = null;
	List<Miscfee> miscfeeList = ht.find("from Miscfee i where i.id = ?", Integer.parseInt(id));
	if(miscfeeList != null && miscfeeList.size() > 0)
		miscfee1 = miscfeeList.get(0);
	return miscfee1;
}

public Client deleteClientsByIdInfo(String id) {
	Client clientList1 = null;
	List<Client> clientList2 = ht.find("from Client i where i.id = ?", Integer.parseInt(id));
	if(clientList2 != null && clientList2.size() > 0)
		clientList1 = clientList2.get(0);
	return clientList1;
}

@Override
public List<OnlinePay> getOnlineByList() {
	List<OnlinePay> onlinepay=ht.find("From OnlinePay c  ");
	return onlinepay;
}

@Override
public List<OnlinePay> onlinePayByListInfo(String val, String field) {
	List<OnlinePay> onlinepay = null;
	if(StringUtils.isNotBlank(val)){
		onlinepay = ht.find("from OnlinePay c where c."+field+" like ?", val+"%");
	}
	else
	{
		onlinepay = ht.find("from OnlinePay c ");	
	}
	return onlinepay;
}

@SuppressWarnings("unchecked")
@Override
public Followup getFollowupByClient(String clientId, String type) {
	Followup followup = null;
		List<Followup> followupList = ht.find("from Followup f where f.type = ? and f.clientId = ? ", type, Integer.parseInt(clientId));
		if(followupList != null && followupList.size() > 0){
			followup = followupList.get(0);
		}
		return followup;
}

@Override
public void saveFolloup(Followup followup) {
	ht.saveOrUpdate(followup);
}


}





