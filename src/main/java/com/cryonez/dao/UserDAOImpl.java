package com.cryonez.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Component;

import com.cryonez.Service.UserManager;
import com.cryonez.entity.Category;
import com.cryonez.entity.Country;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;

/**
 * @author s6
 *
 */
/**
 * @author s6
 *
 */
@SuppressWarnings("unused")
@Component
@Scope("singleton")
public class UserDAOImpl implements UserDAO {

	private Logger LOG = Logger.getLogger(UserDAOImpl.class);
	Session session = null;
	private static HibernateTemplate ht;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.ht = new HibernateTemplate(sessionFactory);
	}






	
	
	public User getUser() {
		try {
			List<User> user = ht.find("from User u");
			User use = user.get(0);
			return use;
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	@Override
	public void save(Opportunities opp) {
		ht.save(opp);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Opportunities> getOpportunity() {
		List<Opportunities> opp = ht.find("from Opportunities o");
		return opp;
	}

	@Override
	public List<Opportunities> getOpportunityById(Long id) {
		List<Opportunities> opp = ht.find("from Opportunities o where o.id = ?", id);
		return opp;
	}

	@SuppressWarnings("unchecked")
	public List<Opportunities> getSearchVal(String val) {
		if(val != null && val.equalsIgnoreCase("all"))
			return ht
					.find("from Opportunities o");
		else
		return ht
				.find("from Opportunities o where o.email LIKE? or o.firstName LIKE ? or o.phoneNumber LIKE? or o.assignedTo LIKE ?",
						"%" + val + "%", "%" + val + "%", "%" + val + "%", "%" + val + "%");

	}

	@Override
	public List<Opportunities> getStatusVal(boolean val) {
		return ht.find("from Opportunities o where o.status = ?", val);
	}

	@Override
	public void deleteOpp(Long id) {
		ht.delete(ht.find("from Opportunities o where o.id = ?", id).get(0));
	}

	@Override
	public List<Enquiries> getEnquiries() {
		return ht.find("from Enquiries e ");
	}

	@Override
	public void update(Opportunities opp) {
		ht.saveOrUpdate(opp);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Enquiries getEnquiriesById(Long enqId) {
		return (Enquiries) ht.find("from Enquiries e where e.id = ?",enqId).get(0);
	}

	@Override
	public List<Refferal> getRefferal() {
		return ht.find("from Refferal r");
	}

	@Override
	public List<Enquiries> getSearchEnquiry(String val) {
		return ht.find("From Enquiries e where e.name LIKE ? or e.email LIKE ? or e.phoneNumber LIKE ? or e.date LIKE ? or e.message LIKE ?","%"+ val+"%","%"+ val+"%","%"+ val+"%","%"+ val+"%","%"+ val+"%");
	}

	@Override
	public void saveCategory(Category cat) {
		ht.saveOrUpdate(cat);
		
	}

	@Override
	public List<Category> getClientCategories() {
		List<Category> cats=ht.find("From Category");
		return cats;
	}

	@Override
	public Category getThisCategory(String id) {
		Category catg=null;
		try{
		catg=(Category) ht.find("From Category w where w.cat_id=?",id).get(0);
		}catch(Exception e){
			LOG.error("Error while fetching the Single category");
		}
		return catg;
	}

	
	@Override
	public List<Category> getCategoryLikeThis(String val) {
		
		return ht.find("From Category w where w.cat_id LIKE ? or w.description LIKE ? ","%"+ val+"%","%"+ val+"%");
		
		 }

	@Override
	public void saveSources(Source src) {
		ht.saveOrUpdate(src);
		
	}

	@Override
	public List<Source> getSources() {
		return ht.find("From Source ");
	}

	@Override
	public Source getThisSource(String id) {
		
		Source src=	 (Source)ht.find("From Source w where w.src_id=?",id).get(0);
		  return src;
	}

	@Override
	public List<Source> getSorcesBySearchValues(String val) {
		System.out.println();
		List<Source> src= ht.find("From  Source w where w.src_id LIKE ? or w.description LIKE ? ","%"+ val+"%","%"+ val+"%");
	return src;
	}

	@Override
	public void saveDesignation(Designation dsg) {
		ht.saveOrUpdate(dsg);
		
	}

	@Override
	public List<Designation> getAllDesignations(){
		List<Designation> dsg=ht.find("From Designation");
		return dsg;
		
	}

	@Override
	public Designation getThisDesignation(String id) {
		return (Designation) ht.find("From Designation w where w.des_id= ? ",id).get(0);
	}

	@Override
	public List<Designation> getDesignationLikeThis(String val) {
		
		List<Designation> dsg=ht.find("From  Designation w where w.des_id LIKE ? or w.description LIKE ? or w.level LIKE ? ","%"+val+"%","%"+val+"%","%"+val+"%");
		return dsg;
	}

	@Override
	public void saveEmployee(Employee emp) {
		ht.saveOrUpdate(emp);
		
	}

	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> emp=ht.find("From Employee");
		return emp;
	}

	@Override
	public Employee getThisEmployee(String id) {
		Employee emp=null;
		try{
			emp=(Employee) ht.find("From Employee w where w.e_id= ?",id).get(0);
		}catch(Exception e){
			LOG.error("Error while fetching the Employee details");
		}
		
		 return emp;
	}

	@Override
	public List<Employee> getEmployeesLikeThis(String val) {
		List<Employee> emps=ht.find("From Employee w where w.e_id LIKE ? OR w.emp_firstname LIKE ? OR w.emp_email LIKE ? OR w.emp_mobile LIKE ? OR w.emp_phone LIKE ? ","%"+val+"%","%"+val+"%","%"+val+"%","%"+val+"%","%"+val+"%");
		return emps;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Country> getAllCountries() {
		List<Country> ctr=ht.find("From Country");
		return ctr;
	}
	
	@Override
	public void deleteDoc(Long id){
		ht.delete(ht.find("from MyDocuments my where my.id=?",id).get(0));
	}
}
