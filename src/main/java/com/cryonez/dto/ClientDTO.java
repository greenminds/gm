package com.cryonez.dto;


public class ClientDTO {

	private int id;
	private String email;
	private String phoneNumber;
	private String salutation;
	private String firstName;
	private String middleName;
	private String lastName;
	private String dob;
	private String passportNo;
	private String passportExpDate;
	private String birthCountry;
	private String residenCountry;
	private String citizenshipCountry;
	private String prefferedLanguage;
	private String dateOfJoin;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String country;
	private int zipcode;
	private String mobileNumber;
	private String phoneNumber1;
	private String alternateEmail;
	private String fax;
	private String contactMethod;
	private String specialInstructions;
	private String otherEmail;
	
	private String program;
	private String visaPosts;
	private String fileNo;
	private String fileNoOther;
	private String fileStatus;
	private String appSubmissionDate;
	private String medExpDate;
	private String interviewDate;
	private String interviewTime;
	private String interviewLocation;
	private String addInfo;
	private String medSentBy;
	private String medBillNo;
	private String visaSentBy;
	private String visaWaybillNo;
	
	private String prIssueDate;
	private String prValidityDate;
	private String landingDate;
	private String citizenshipDate;
	private String employer;
	private String employerLocation;
	private String lmoNumber;
	private String wpIssueDate;
	private String wpInterviewDate;
	private String wpExtGrantDate;
	private String wpInterviewWaiverReciptDate;
	private String tempRenExt;
	private String arrivalDate;
	private String departureDate;

	private String sales;
	private String relationshipManager;
	private String processingOfficer;
	private String processingConsultant;
	
	private String clientCategory;
	private String clientStatus;
	private String source;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	/**
	 * @return the salutation
	 */
	public String getSalutation() {
		return salutation;
	}
	/**
	 * @param salutation the salutation to set
	 */
	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the middleName
	 */
	public String getMiddleName() {
		return middleName;
	}
	/**
	 * @param middleName the middleName to set
	 */
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * @return the dob
	 */
	public String getDob() {
		return dob;
	}
	/**
	 * @param dob the dob to set
	 */
	public void setDob(String dob) {
		this.dob = dob;
	}
	/**
	 * @return the passportNo
	 */
	public String getPassportNo() {
		return passportNo;
	}
	/**
	 * @param passportNo the passportNo to set
	 */
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	/**
	 * @return the passportExpDate
	 */
	public String getPassportExpDate() {
		return passportExpDate;
	}
	/**
	 * @param passportExpDate the passportExpDate to set
	 */
	public void setPassportExpDate(String passportExpDate) {
		this.passportExpDate = passportExpDate;
	}
	/**
	 * @return the birthCountry
	 */
	public String getBirthCountry() {
		return birthCountry;
	}
	/**
	 * @param birthCountry the birthCountry to set
	 */
	public void setBirthCountry(String birthCountry) {
		this.birthCountry = birthCountry;
	}
	/**
	 * @return the residenCountry
	 */
	public String getResidenCountry() {
		return residenCountry;
	}
	/**
	 * @param residenCountry the residenCountry to set
	 */
	public void setResidenCountry(String residenCountry) {
		this.residenCountry = residenCountry;
	}
	/**
	 * @return the citizenshipCountry
	 */
	public String getCitizenshipCountry() {
		return citizenshipCountry;
	}
	/**
	 * @param citizenshipCountry the citizenshipCountry to set
	 */
	public void setCitizenshipCountry(String citizenshipCountry) {
		this.citizenshipCountry = citizenshipCountry;
	}
	/**
	 * @return the prefferedLanguage
	 */
	public String getPrefferedLanguage() {
		return prefferedLanguage;
	}
	/**
	 * @param prefferedLanguage the prefferedLanguage to set
	 */
	public void setPrefferedLanguage(String prefferedLanguage) {
		this.prefferedLanguage = prefferedLanguage;
	}
	/**
	 * @return the dateOfJoin
	 */
	public String getDateOfJoin() {
		return dateOfJoin;
	}
	/**
	 * @param dateOfJoin the dateOfJoin to set
	 */
	public void setDateOfJoin(String dateOfJoin) {
		this.dateOfJoin = dateOfJoin;
	}
	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the zipcode
	 */
	public int getZipcode() {
		return zipcode;
	}
	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}
	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}
	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	/**
	 * @return the phoneNumber1
	 */
	public String getPhoneNumber1() {
		return phoneNumber1;
	}
	/**
	 * @param phoneNumber1 the phoneNumber1 to set
	 */
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}
	/**
	 * @return the alternateEmail
	 */
	public String getAlternateEmail() {
		return alternateEmail;
	}
	/**
	 * @param alternateEmail the alternateEmail to set
	 */
	public void setAlternateEmail(String alternateEmail) {
		this.alternateEmail = alternateEmail;
	}
	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	/**
	 * @return the contactMethod
	 */
	public String getContactMethod() {
		return contactMethod;
	}
	/**
	 * @param contactMethod the contactMethod to set
	 */
	public void setContactMethod(String contactMethod) {
		this.contactMethod = contactMethod;
	}
	/**
	 * @return the specialInstructions
	 */
	public String getSpecialInstructions() {
		return specialInstructions;
	}
	/**
	 * @return the otherEmail
	 */
	public String getOtherEmail() {
		return otherEmail;
	}
	/**
	 * @param otherEmail the otherEmail to set
	 */
	public void setOtherEmail(String otherEmail) {
		this.otherEmail = otherEmail;
	}
	/**
	 * @param specialInstructions the specialInstructions to set
	 */
	public void setSpecialInstructions(String specialInstructions) {
		this.specialInstructions = specialInstructions;
	}
	/**
	 * @return the program
	 */
	public String getProgram() {
		return program;
	}
	/**
	 * @param program the program to set
	 */
	public void setProgram(String program) {
		this.program = program;
	}
	/**
	 * @return the visaPosts
	 */
	public String getVisaPosts() {
		return visaPosts;
	}
	/**
	 * @param visaPosts the visaPosts to set
	 */
	public void setVisaPosts(String visaPosts) {
		this.visaPosts = visaPosts;
	}
	/**
	 * @return the fileNo
	 */
	public String getFileNo() {
		return fileNo;
	}
	/**
	 * @param fileNo the fileNo to set
	 */
	public void setFileNo(String fileNo) {
		this.fileNo = fileNo;
	}
	/**
	 * @return the fileNoOther
	 */
	public String getFileNoOther() {
		return fileNoOther;
	}
	/**
	 * @param fileNoOther the fileNoOther to set
	 */
	public void setFileNoOther(String fileNoOther) {
		this.fileNoOther = fileNoOther;
	}
	/**
	 * @return the fileStatus
	 */
	public String getFileStatus() {
		return fileStatus;
	}
	/**
	 * @param fileStatus the fileStatus to set
	 */
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	/**
	 * @return the appSubmissionDate
	 */
	public String getAppSubmissionDate() {
		return appSubmissionDate;
	}
	/**
	 * @param appSubmissionDate the appSubmissionDate to set
	 */
	public void setAppSubmissionDate(String appSubmissionDate) {
		this.appSubmissionDate = appSubmissionDate;
	}
	/**
	 * @return the medExpDate
	 */
	public String getMedExpDate() {
		return medExpDate;
	}
	/**
	 * @param medExpDate the medExpDate to set
	 */
	public void setMedExpDate(String medExpDate) {
		this.medExpDate = medExpDate;
	}
	/**
	 * @return the interviewDate
	 */
	public String getInterviewDate() {
		return interviewDate;
	}
	/**
	 * @param interviewDate the interviewDate to set
	 */
	public void setInterviewDate(String interviewDate) {
		this.interviewDate = interviewDate;
	}
	/**
	 * @return the interviewTime
	 */
	public String getInterviewTime() {
		return interviewTime;
	}
	/**
	 * @param interviewTime the interviewTime to set
	 */
	public void setInterviewTime(String interviewTime) {
		this.interviewTime = interviewTime;
	}
	/**
	 * @return the interviewLocation
	 */
	public String getInterviewLocation() {
		return interviewLocation;
	}
	/**
	 * @param interviewLocation the interviewLocation to set
	 */
	public void setInterviewLocation(String interviewLocation) {
		this.interviewLocation = interviewLocation;
	}
	/**
	 * @return the addInfo
	 */
	public String getAddInfo() {
		return addInfo;
	}
	/**
	 * @param addInfo the addInfo to set
	 */
	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}
	/**
	 * @return the medSentBy
	 */
	public String getMedSentBy() {
		return medSentBy;
	}
	/**
	 * @param medSentBy the medSentBy to set
	 */
	public void setMedSentBy(String medSentBy) {
		this.medSentBy = medSentBy;
	}
	/**
	 * @return the medBillNo
	 */
	public String getMedBillNo() {
		return medBillNo;
	}
	/**
	 * @param medBillNo the medBillNo to set
	 */
	public void setMedBillNo(String medBillNo) {
		this.medBillNo = medBillNo;
	}
	/**
	 * @return the visaSentBy
	 */
	public String getVisaSentBy() {
		return visaSentBy;
	}
	/**
	 * @param visaSentBy the visaSentBy to set
	 */
	public void setVisaSentBy(String visaSentBy) {
		this.visaSentBy = visaSentBy;
	}
	/**
	 * @return the visaWaybillNo
	 */
	public String getVisaWaybillNo() {
		return visaWaybillNo;
	}
	/**
	 * @param visaWaybillNo the visaWaybillNo to set
	 */
	public void setVisaWaybillNo(String visaWaybillNo) {
		this.visaWaybillNo = visaWaybillNo;
	}
	/**
	 * @return the prIssueDate
	 */
	public String getPrIssueDate() {
		return prIssueDate;
	}
	/**
	 * @param prIssueDate the prIssueDate to set
	 */
	public void setPrIssueDate(String prIssueDate) {
		this.prIssueDate = prIssueDate;
	}
	/**
	 * @return the prValidityDate
	 */
	public String getPrValidityDate() {
		return prValidityDate;
	}
	/**
	 * @param prValidityDate the prValidityDate to set
	 */
	public void setPrValidityDate(String prValidityDate) {
		this.prValidityDate = prValidityDate;
	}
	/**
	 * @return the landingDate
	 */
	public String getLandingDate() {
		return landingDate;
	}
	/**
	 * @param landingDate the landingDate to set
	 */
	public void setLandingDate(String landingDate) {
		this.landingDate = landingDate;
	}
	/**
	 * @return the citizenshipDate
	 */
	public String getCitizenshipDate() {
		return citizenshipDate;
	}
	/**
	 * @param citizenshipDate the citizenshipDate to set
	 */
	public void setCitizenshipDate(String citizenshipDate) {
		this.citizenshipDate = citizenshipDate;
	}
	/**
	 * @return the employer
	 */
	public String getEmployer() {
		return employer;
	}
	/**
	 * @param employer the employer to set
	 */
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	/**
	 * @return the employerLocation
	 */
	public String getEmployerLocation() {
		return employerLocation;
	}
	/**
	 * @param employerLocation the employerLocation to set
	 */
	public void setEmployerLocation(String employerLocation) {
		this.employerLocation = employerLocation;
	}
	/**
	 * @return the lmoNumber
	 */
	public String getLmoNumber() {
		return lmoNumber;
	}
	/**
	 * @param lmoNumber the lmoNumber to set
	 */
	public void setLmoNumber(String lmoNumber) {
		this.lmoNumber = lmoNumber;
	}
	/**
	 * @return the wpIssueDate
	 */
	public String getWpIssueDate() {
		return wpIssueDate;
	}
	/**
	 * @param wpIssueDate the wpIssueDate to set
	 */
	public void setWpIssueDate(String wpIssueDate) {
		this.wpIssueDate = wpIssueDate;
	}
	/**
	 * @return the wpInterviewDate
	 */
	public String getWpInterviewDate() {
		return wpInterviewDate;
	}
	/**
	 * @param wpInterviewDate the wpInterviewDate to set
	 */
	public void setWpInterviewDate(String wpInterviewDate) {
		this.wpInterviewDate = wpInterviewDate;
	}
	/**
	 * @return the wpExtGrantDate
	 */
	public String getWpExtGrantDate() {
		return wpExtGrantDate;
	}
	/**
	 * @param wpExtGrantDate the wpExtGrantDate to set
	 */
	public void setWpExtGrantDate(String wpExtGrantDate) {
		this.wpExtGrantDate = wpExtGrantDate;
	}
	/**
	 * @return the wpInterviewWaiverReciptDate
	 */
	public String getWpInterviewWaiverReciptDate() {
		return wpInterviewWaiverReciptDate;
	}
	/**
	 * @param wpInterviewWaiverReciptDate the wpInterviewWaiverReciptDate to set
	 */
	public void setWpInterviewWaiverReciptDate(String wpInterviewWaiverReciptDate) {
		this.wpInterviewWaiverReciptDate = wpInterviewWaiverReciptDate;
	}
	/**
	 * @return the tempRenExt
	 */
	public String getTempRenExt() {
		return tempRenExt;
	}
	/**
	 * @param tempRenExt the tempRenExt to set
	 */
	public void setTempRenExt(String tempRenExt) {
		this.tempRenExt = tempRenExt;
	}
	/**
	 * @return the arrivalDate
	 */
	public String getArrivalDate() {
		return arrivalDate;
	}
	/**
	 * @param arrivalDate the arrivalDate to set
	 */
	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	/**
	 * @return the departureDate
	 */
	public String getDepartureDate() {
		return departureDate;
	}
	/**
	 * @param departureDate the departureDate to set
	 */
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}
	public String getSales() {
		return sales;
	}
	public void setSales(String sales) {
		this.sales = sales;
	}
	public String getRelationshipManager() {
		return relationshipManager;
	}
	public void setRelationshipManager(String relationshipManager) {
		this.relationshipManager = relationshipManager;
	}
	public String getProcessingOfficer() {
		return processingOfficer;
	}
	public void setProcessingOfficer(String processingOfficer) {
		this.processingOfficer = processingOfficer;
	}
	public String getProcessingConsultant() {
		return processingConsultant;
	}
	public void setProcessingConsultant(String processingConsultant) {
		this.processingConsultant = processingConsultant;
	}
	public String getClientCategory() {
		return clientCategory;
	}
	public void setClientCategory(String clientCategory) {
		this.clientCategory = clientCategory;
	}
	public String getClientStatus() {
		return clientStatus;
	}
	public void setClientStatus(String clientStatus) {
		this.clientStatus = clientStatus;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
	
	
	
}
