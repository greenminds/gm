package com.cryonez.dto;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public class EmployeeDTO {


	
	private Long id;
	private String e_id;
	private String emp_id;
	private String emp_firstname;
	private String emp_lastname;
	private String emp_email;
	private String emp_password;
	private String emp_mobile;
	private String emp_designation;
	private String emp_phone;
	private String emp_address;
	private String emp_city;
	private String emp_state;
	private String emp_country; 
	private String emp_pincode;
	private boolean isactive;
	private String createddate;
	private String updateddate;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	public String getEmp_firstname() {
		return emp_firstname;
	}
	public void setEmp_firstname(String emp_firstname) {
		this.emp_firstname = emp_firstname;
	}
	public String getEmp_lastname() {
		return emp_lastname;
	}
	public void setEmp_lastname(String emp_lastname) {
		this.emp_lastname = emp_lastname;
	}
	public String getEmp_email() {
		return emp_email;
	}
	public void setEmp_email(String emp_email) {
		this.emp_email = emp_email;
	}
	public String getEmp_password() {
		return emp_password;
	}
	public void setEmp_password(String emp_password) {
		this.emp_password = emp_password;
	}
	public String getEmp_mobile() {
		return emp_mobile;
	}
	public void setEmp_mobile(String emp_mobile) {
		this.emp_mobile = emp_mobile;
	}
	public String getEmp_phone() {
		return emp_phone;
	}
	public void setEmp_phone(String emp_phone) {
		this.emp_phone = emp_phone;
	}
	public String getEmp_address() {
		return emp_address;
	}
	public void setEmp_address(String emp_address) {
		this.emp_address = emp_address;
	}
	public String getEmp_city() {
		return emp_city;
	}
	public void setEmp_city(String emp_city) {
		this.emp_city = emp_city;
	}
	public String getEmp_state() {
		return emp_state;
	}
	public void setEmp_state(String emp_state) {
		this.emp_state = emp_state;
	}
	public String getEmp_country() {
		return emp_country;
	}
	public void setEmp_country(String emp_country) {
		this.emp_country = emp_country;
	}
	public String getEmp_pincode() {
		return emp_pincode;
	}
	public void setEmp_pincode(String emp_pincode) {
		this.emp_pincode = emp_pincode;
	}
	public boolean isIsactive() {
		return isactive;
	}
	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	public String getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}
	public String getEmp_designation() {
		return emp_designation;
	}
	public void setEmp_designation(String emp_designation) {
		this.emp_designation = emp_designation;
	}
	public String getE_id() {
		return e_id;
	}
	public void setE_id(String e_id) {
		this.e_id = e_id;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", e_id=" + e_id + ", emp_id=" + emp_id
				+ ", emp_firstname=" + emp_firstname + ", emp_lastname="
				+ emp_lastname + ", emp_email=" + emp_email + ", emp_password="
				+ emp_password + ", emp_mobile=" + emp_mobile
				+ ", emp_designation=" + emp_designation + ", emp_phone="
				+ emp_phone + ", emp_address=" + emp_address + ", emp_city="
				+ emp_city + ", emp_state=" + emp_state + ", emp_country="
				+ emp_country + ", emp_pincode=" + emp_pincode + ", isactive="
				+ isactive + ", createddate=" + createddate + ", updateddate="
				+ updateddate + "]";
	}
	
	
	
	
	


	}

