package com.cryonez.util;


import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

public class MailManager {


	@Autowired
	private VelocityEngine velocityEngine;

	private String fromAddress;

	private String fromName;

	private String appUrl;

	private int emailPort;

	private String emailHost;


	public VelocityEngine getVelocityEngine() {
		return velocityEngine;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}


	public void sendEmail(final String subject, final Map<String, Object> data, final String template,
			final String toName, final String toAddress) {
		data.put("appUrl", appUrl);
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", false);
		mailProperties.put("mail.smtp.starttls.enable", true);
		//**********
		mailProperties.put("mail.smtp.ssl.trust", emailHost);
		//*******
		mailSender.setJavaMailProperties(mailProperties);
		mailSender.setHost(emailHost);
		mailSender.setPort(emailPort);
		mailSender.setProtocol("smtp");

		mailSender.setUsername(fromAddress);
		mailSender.setPassword("12@password");
//		mailSender.setPassword("Master6611");

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(toAddress);
		mailMessage.setFrom(fromAddress);
		mailMessage.setSubject("iAvailable registration Code");

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				 message.setFrom(fromAddress, fromName);

				message.addTo(toAddress, toName);
				message.setSubject(subject);

				String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/" + template
						+ ".vm", data);
				message.setText(text, true);
			}
		};
		mailSender.send(preparator);
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	/**
	 * @return the emailPort
	 */
	public int getEmailPort() {
		return emailPort;
	}

	/**
	 * @param emailPort
	 *            the emailPort to set
	 */
	public void setEmailPort(int emailPort) {
		this.emailPort = emailPort;
	}

	/**
	 * @return the emailHost
	 */
	public String getEmailHost() {
		return emailHost;
	}

	/**
	 * @param emailHost
	 *            the emailHost to set
	 */
	public void setEmailHost(String emailHost) {
		this.emailHost = emailHost;
	}

}