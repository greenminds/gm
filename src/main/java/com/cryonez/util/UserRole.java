package com.cryonez.util;

public enum UserRole {
	ROLE_ADMIN("admin"), ROLE_USER("user");
	
	UserRole(String description){
		this.description = description;
	}
	
	private String name;
	private String description;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
