package com.cryonez.web;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.print.attribute.standard.Severity;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cryonez.Service.UserManager;
import com.cryonez.entity.Client;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.MyDocuments;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.User;
import com.cryonez.entity.UserExperience;
import com.cryonez.util.MailManager;

/**
 * @author s6
 *
 */
/**
 * @author s6
 *
 */
@Controller
public class UserController {
	private Logger LOG = Logger.getLogger(UserManager.class);

	@Autowired
	UserManager userManager;
	@Autowired
	AjaxController ajaxController;

	@Autowired
	MailManager mailManager;

	@Autowired
	AdminController adminController;

	@Autowired(required = false)
	@Qualifier("authManager")
	AuthenticationManager authManager;

	@Autowired
	private HttpSession session;

	@RequestMapping(value = "homePage.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doHomePage(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		String enq1 = (String) session.getAttribute("enq1");
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		
		} else {
			model.addAttribute("enq", "false");
			//return "redirect:homepage.mm";
		}

		if (StringUtils.isNotBlank(enq1) && enq1.equals("true")) {
			model.addAttribute("enq1", enq1);
			session.setAttribute("enq1", "false");
		} else {
			model.addAttribute("enq1", "false");
		}
		
		//return null;
	}

	/*
	 * public void doHomePage(ModelMap model,HttpServletRequest req,
	 * 
	 * @RequestParam(value = "hiddenValue", required = false) String
	 * hiddenValue,
	 * 
	 * @RequestParam(value = "enq", required = false) String enq) {
	 * 
	 * if(StringUtils.isNotBlank(enq) && enq.equalsIgnoreCase("true"))
	 * model.addAttribute("enq", "true"); enq="false"; //else
	 * //model.addAttribute("enq", "false"); //
	 * model.addAttribute("hiddenValue", hiddenValue);
	 * 
	 * }
	 */

	@RequestMapping(value = "enquiry.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doEnquiry(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "profession", required = false) String profession,
			@RequestParam(value = "message", required = false) String message,
			@RequestParam(value = "hidden", required = false) String hidden) {
		Enquiries enq = new Enquiries();

		enq.setName(firstName);
		enq.setPhoneNumber(phoneNumber);
		enq.setEmail(email);
		enq.setLastName(lastName);
		enq.setMessage(message);
		enq.setProfession(profession);

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);
		enq.setDate(reportDate);
		userManager.insertEnquiry(enq);
		session.setAttribute("enq", "true");
		if (hidden.equalsIgnoreCase("homePage")) {
			return "redirect:homePage.mm";
		} else if (hidden.equalsIgnoreCase("expressentry")) {
			return "redirect:expressentry.mm";
		}

		else if (hidden.equalsIgnoreCase("qubecskiled")) {
			return "redirect:qubecskiled.mm";
		} else if (hidden.equalsIgnoreCase("familyspons")) {
			return "redirect:familysponsorship.mm";
		} else if (hidden.equalsIgnoreCase("business")) {
			return "redirect:businessimmigration.mm";
		}
		 else if(hidden.equalsIgnoreCase("career")){
		 return "redirect:career.mm";
		 }

		return "redirect:provincialnominee.mm";

	}

	/*
	 * @RequestMapping(value = "enquiry.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public String doEnquiry(ModelMap model,
	 * HttpServletRequest req,HttpSession session,
	 * 
	 * @RequestParam(value = "firstName", required = false) String firstName,
	 * 
	 * @RequestParam(value = "phoneNumber", required = false) String
	 * phoneNumber,
	 * 
	 * @RequestParam(value = "email", required = false) String email,
	 * 
	 * @RequestParam(value = "lastName", required = false) String lastName,
	 * 
	 * @RequestParam(value = "profession", required = false) String profession,
	 * 
	 * @RequestParam(value = "message", required = false) String message,
	 * 
	 * @RequestParam(value="hidden",required=false)String hidden) {
	 * 
	 * Enquiries enq = new Enquiries();
	 * 
	 * enq.setName(firstName); enq.setPhoneNumber(phoneNumber);
	 * enq.setEmail(email); enq.setLastName(lastName); enq.setMessage(message);
	 * enq.setProfession(profession);
	 * 
	 * DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss"); Date today =
	 * Calendar.getInstance().getTime(); String reportDate = df.format(today);
	 * enq.setDate(reportDate); userManager.insertEnquiry(enq); //boolean
	 * enquiry=false;
	 * 
	 * session.setAttribute("enq", "true"); if(hidden.equals("expressentry")){
	 * 
	 * return "redirect:expressentry.mm"; } // String view
	 * ="Enquiry submitted "; // model.addAttribute("view", view);
	 * 
	 * return "redirect:homePage.mm";
	 * 
	 * 
	 * }
	 */
	@RequestMapping(value = "onEnquiry.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doOnEnquiry1(HttpServletRequest req, HttpSession session,
			@RequestParam(value = "fName", required = false) String fName,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "emailId", required = false) String emailId,
			@RequestParam(value = "lName", required = false) String lName) {

		Enquiries enq1 = new Enquiries();

		enq1.setName(fName);
		enq1.setPhoneNumber(mobile);
		enq1.setEmail(emailId);
		enq1.setLastName(lName);

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);
		enq1.setDate(reportDate);

		userManager.insertEnquiry(enq1);
		if (enq1 != null) {

		}
		session.setAttribute("enq1", "true");
		return "redirect:homePage.mm#show_load";

	}

	@RequestMapping(value = "/register.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doRegister(ModelMap model, HttpServletRequest request, HttpSession session) {
		
		

	}

	@RequestMapping(value = "/dummi.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doDummi(ModelMap model, HttpServletRequest request, HttpSession session) {

		
	}

	/*@RequestMapping(value = "signUpSuccess.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doAddIUser(HttpServletRequest request, ModelMap map, HttpSession session,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "email", required = false) String userName,
			@RequestParam(value = "profession", required = false) String profession,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "confirmPassword", required = false) String confirmPassword,
			@RequestParam(value = "phonenumber", required = false) String phoneNumber) {
		if (firstName != "" || userName != "" || password != "") {
			User userFromDb = null;
			userFromDb = userManager.verifyEmail(userName);
			Long returnId = (long) (int) (Math.random() * 90000) + 10000;

			UUID token = UUID.randomUUID();
			if (userFromDb == null) {
				User audi = new User();
				// UserCheck aud = new UserCheck();
				// UserAddress uad = new UserAddress();
				// Coordinators co = new Coordinators();
				Opportunities opp = new Opportunities();

				audi.setAccountType("user");
				audi.setfName(firstName);
				audi.setPassword(password);
				audi.setActive(false);
				audi.setProfession(profession);
				audi.setUserName(userName);
				audi.setEmail(userName);
				audi.setToken(token);
				audi.setAccountType("user");
				audi.setMobile(phoneNumber);
				audi.setUserId(returnId);
				userManager.insertUser(audi);
				session.setAttribute("audi", "true");
				// aud.setEmailAddress(userName);
				// aud.setUserId(returnId);
				// aud.setUserName(userName);
				// aud.setPassword(password);
				// userManager.insertUser(aud);
				//
				// uad.setUserId(returnId);
				// userManager.insertUserAddress(uad);
				//
				// co.setUserId(returnId);
				// userManager.insertCordinator(co);
				//
				opp.setClientStatus(false);
				opp.setEmail(userName);
				opp.setFirstName(firstName);
				opp.setPhoneNumber(phoneNumber);
				userManager.insertOpportunities(opp);

				System.setProperty("java.net.preferIPv4Stack", "true");
				try {
					Map<String, Object> data = new HashMap<String, Object>();
					data.put("firstName", firstName);
					data.put("name", userName);
					data.put("token", token);
					data.put("SellerRegistration", audi.getEmail());
					String template = null;
					template = "welcomeUser";

					String subject = "Welcome User";
					String toName = firstName;
					String toAddress = userName;
					mailManager.sendEmail(subject, data, template, toName, toAddress);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return "redirect:login.mm";

	}
*/

	@RequestMapping(value = "signUpSuccess.mm", method = { RequestMethod.GET,
			RequestMethod.POST })
	public String doAddIUser(
			HttpServletRequest request,
			ModelMap map,
			HttpSession session,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "email", required = false) String userName,
			@RequestParam(value = "profession", required = false) String profession,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "confirmPassword", required = false) String confirmPassword,
			@RequestParam(value = "phonenumber", required = false) String phoneNumber) {
		if (firstName != "" || userName != "" || password != "") {
			User userFromDb = null;
			userFromDb = userManager.verifyEmail(userName);
			Long returnId = (long) (int) (Math.random() * 90000) + 10000;

			UUID token = UUID.randomUUID();
			if (userFromDb == null) {
				User audi = new User();
				Opportunities opp = new Opportunities();

				audi.setAccountType("user");
				audi.setfName(firstName);
				audi.setPassword(password);
				audi.setActive(false);
				audi.setProfession(profession);
				audi.setUserName(userName);
				audi.setEmail(userName);
				audi.setToken(token);
				audi.setMobile(phoneNumber);
				audi.setIsDocumentuploaded(false);
				audi.setIsDocumentApprovaled(false);
				audi.setiAgreementuploaded(false);
				audi.setIsAgreementApprovaled(false);
				audi.setUserId(returnId);
				userManager.insertUser(audi);
				Client client = new Client();
				client.setEmail(userName);
				client.setPhoneNumber(audi.getMobile());
				client.setFirstName(audi.getfName());
				client.setUserId(audi.getId());
				userManager.saveClient(client);
				session.setAttribute("audi", "true");

				opp.setClientStatus(false);
				opp.setEmail(userName);
				opp.setFirstName(firstName);
				opp.setPhoneNumber(phoneNumber);
				userManager.insertOpportunities(opp);

				System.setProperty("java.net.preferIPv4Stack", "true");
				try {
					Map<String, Object> data = new HashMap<String, Object>();
					data.put("firstName", firstName);
					data.put("name", userName);
					data.put("token", token);
					data.put("SellerRegistration", audi.getEmail());
					String template = null;
					template = "welcomeUser";

					String subject = "Welcome User";
					String toName = firstName;
					String toAddress = userName;
					mailManager.sendEmail(subject, data, template, toName,
							toAddress);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return "redirect:login.mm";

	}

	@RequestMapping(value = "/userActivation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView doSellerActivation(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "token", required = true) UUID token, HttpSession session) {

		User user = null;
		user = userManager.getUserByToken(token);
		if (user != null) {

			user.setActive(true);
			userManager.mergeUser(user);
		} else
			return new ModelAndView("redirect:login.mm");

		return new ModelAndView("redirect:login.mm");
	}

	@RequestMapping(value = "login.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doLoginDemo(ModelMap model, HttpServletRequest req,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		
		String audi = (String) session.getAttribute("audi");

		if (StringUtils.isNotBlank(audi) && audi.equals("true")) {
			model.addAttribute("audi", audi);
			session.setAttribute("audi", "false");
		} else {
			model.addAttribute("audi", "false");
		}

	}
	/*@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		String enq1 = (String) session.getAttribute("enq1");
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}*/

	private boolean isAuthenticated(String username, String password) {
		Authentication requ = new UsernamePasswordAuthenticationToken(username, password);
		Authentication resu = authManager.authenticate(requ);
		SecurityContextHolder.getContext().setAuthentication(resu);
		return resu.isAuthenticated();

	}

	@RequestMapping(value = "verifyLogin.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doUserLogins(HttpServletResponse resp, ModelAndView modelview, ModelMap model,
			HttpServletRequest request, @RequestParam(value = "email", required = false) String username,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "logoutparm", required = false) String logoutparm,
			@RequestParam(value = "checkbox", required = false) String no, HttpSession session) {

		UserDetails details = userManager.getActiveUser(username);

		if (details != null && StringUtils.isNotBlank(details.getPassword()) && details.getPassword().equals(password)) {
			User user = userManager.getUser(username);
			isAuthenticated(username, password);
			String uName = SecurityContextHolder.getContext().getAuthentication().getName();

			user = userManager.getUser(uName);
			session.setAttribute("user", user);
			String active = user.getAccountType();

			if (active.equals("admin")) {
				return "redirect:adminDashboard.mm";

			}
			try {
				if (no.equals("no")) {
					return "redirect:dashBoard.mm";
				} else {
					Cookie con = new Cookie("cookieName", username);
					System.out.println("cookie is " + con.toString());
					con.setMaxAge(600 * 600);
					con.setPath("/");
					resp.addCookie(con);
					return "redirect:dashBoard.mm";

				}
			} catch (Exception e) {
				return "redirect:dashBoard.mm";
			}
		}
		Cookie[] cookies = request.getCookies();
		String value = "";
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equals("cookieName")) {
					value = c.getValue();
					c.setMaxAge(0);
					c.setPath("/");
					resp.addCookie(c);
				}

			}
		}
		return "redirect:login.mm?error=1";

	}

	@RequestMapping(value = "/forgotPassword.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doForgotPassword(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "valid.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void dovalid(ModelMap model, @RequestParam(value = "token", required = false) UUID token) {
		User user = null;
		user = userManager.getUserByToken(token);
		model.addAttribute("email", user.getEmail());
		model.addAttribute("token", token);
	}

	@RequestMapping(value = "/valid1.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doPasswordCheck(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "token", required = false) UUID token,
			@RequestParam(value = "check", required = false) String check) {
		User userFromDB = userManager.getEmailAddress(email);

		UUID token1 = userFromDB.getToken();
		String flag = null;
		userFromDB.setPassword(password);
		userManager.mergeUser(userFromDB);
		return "redirect:resetPassword.mm";
	}

	@RequestMapping(value = "resetPassword.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doresetpassword() {
		System.out.println("inside resetPassword");

	}

	@RequestMapping(value = "forgotMailSuccess.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doforgotpassword(ModelMap map) {

	}

	@RequestMapping(value = "/logout.mm", method = RequestMethod.GET)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:login.mm";
	}

	@RequestMapping(value = "/dashBoard.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doIndex(ModelMap model, HttpServletRequest request, HttpSession session) {

		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		return null;
	}

	@RequestMapping(value = "/refer.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doRefer(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		return null;
	}

	@RequestMapping(value = "referNow.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doReferNow(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "refferedName", required = false) String refferedName,
			@RequestParam(value = "refferedMname", required = false) String refferedMname,
			@RequestParam(value = "refferedLname", required = false) String refferedLname,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber,
			@RequestParam(value = "relation", required = false) String relation) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		Long refId = user.getUserId();
		String fname = user.getfName();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);

		Refferal ref = new Refferal();
		ref.setRefferedOn(reportDate);
		ref.setRefferedBy(fname);
		ref.setReffId(refId);
		ref.setEmailAddress(email);
		ref.setPhoneNumber(phoneNumber);
		ref.setRefferedName(refferedName);
		ref.setRefferedLname(refferedLname);
		ref.setRefferedMname(refferedMname);
		ref.setRelation(relation);

		userManager.insertrefferal(ref);

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("firstName", refferedName);
		data.put("name", email);

		data.put("SellerRegistration", email);
		String template = null;
		template = "refer";

		String subject = "Welcome User";
		String toName = refferedName;
		String toAddress = email;
		mailManager.sendEmail(subject, data, template, toName, toAddress);

		return "redirect:refer.mm";
	}

	@RequestMapping(value = "/myInfo.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doMyInfo(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());

		Long userId = user.getUserId();
		List<UserExperience> userExp = userManager.getExpDetails(userId);

		model.addAttribute("userExp", userExp);
		model.addAttribute("user", user);
		
		//System.out.println(user.getBloodRelation());
		
		String use1=(String) session.getAttribute("use1");
			
			if(StringUtils.isNotBlank(use1)&& use1.equals("true")){
				model.addAttribute("use1", use1);
				session.setAttribute("use1","false");
			}else{
				model.addAttribute("use1", "false");
			}
				
				/*String audi = (String) session.getAttribute("audi");

		if (StringUtils.isNotBlank(audi) && audi.equals("true")) {
			model.addAttribute("audi", audi);
			session.setAttribute("audi", "false");
		} else {
			model.addAttribute("audi", "false");
		}*/
				
				
		return null;
	}

	@RequestMapping(value = "submitInfo.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doSubmitInfo(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "middleName", required = false) String middleName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "mobile", required = false) String mobile,
			@RequestParam(value = "dob", required = false) String dob,
			@RequestParam(value = "passport", required = false) String passport,
			@RequestParam(value = "passportExpiry", required = false) String passportExpiry,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "zipCode", required = false) String zipCode,
			@RequestParam(value = "highestEdu", required = false) String highestEdu,
			@RequestParam(value = "nameDegree", required = false) String nameDegree,
			@RequestParam(value = "nameUniversity", required = false) String nameUniversity,
			@RequestParam(value = "english", required = false) String english,
			@RequestParam(value = "french", required = false) String french,
			@RequestParam(value = "type", required = false) String type,
			@RequestParam(value = "spouseName", required = false) String spouseName,
			@RequestParam(value = "spouseEdu", required = false) String spouseEdu,
			@RequestParam(value = "spouseDob", required = false) String spouseDob,
			@RequestParam(value = "spouseEnglish", required = false) String spouseEnglish,
			@RequestParam(value = "spouseFrench", required = false) String spouseFrench,
			@RequestParam(value = "Stand", required = false) String Stand,
			@RequestParam(value = "Standa", required = false) String Standa,
			@RequestParam(value = "Standard", required = false) String Standard,
			@RequestParam(value = "profilePic") MultipartFile profilePic) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		Long userExp = user.getUserId();

		byte[] photo = null;
		if (profilePic.getSize() != 0) {

			try {
				photo = profilePic.getBytes();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		user.setAddress(address);
		user.setCity(city);
		user.setCountry(country);
		user.setDob(dob);
		user.setEnglish(english);
		user.setfName(firstName);
		user.setFrench(french);
		user.setHighestEdu(highestEdu);
		user.setlName(lastName);
		user.setMaritalStatus(type);
		user.setmName(middleName);
		user.setMobile(mobile);
		user.setNameDegree(nameDegree);
		user.setNameUniversity(nameUniversity);
		user.setNameUniversity(nameUniversity);
		user.setPassportExpiry(passportExpiry);
		user.setPassportNo(passport);
		user.setPinCode(zipCode);

		user.setSpouseDob(spouseDob);
		user.setSpouseEdu(spouseEdu);
		user.setSpouseEnglish(spouseEnglish);
		user.setSpouseFrench(spouseFrench);
		user.setSpouseName(spouseName);
		user.setState(state);
		user.setBloodRelation(Stand);
		user.setJobOffer(Standa);
		user.setStudyExp(Standard);
		String formPostUrl = "https://sandbox.citruspay.com/sslperf/paogateway";
		String secret_key = "255b3c6f6422b4fcb43871656773441d3b14ce71";
		String access_Key = "SO40UIZE0HXJYXJ1GJ2T";
		String returnUrl = "http://localhost:8080/ROOT/paymentconfirm.mm";
		model.addAttribute("formPostUrl", formPostUrl);
		model.addAttribute("secret_key", secret_key);
		model.addAttribute("returnUrl", returnUrl);

		if (photo != null && !photo.equals("")) {
			user.setProfilePic(photo);
		}

		userManager.updateUser(user);
		session.setAttribute("use1", "true");

		return "redirect:myInfo.mm";
	}

	@RequestMapping(value = "/myDocuments.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doMyDocuments(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		Long userId = user.getUserId();
		List<MyDocuments> userDoc = userManager.getDocDetails(userId);
		model.addAttribute("userDoc", userDoc);
		
		String mydoc = (String) session.getAttribute("mydoc");

		if (StringUtils.isNotBlank(mydoc) && mydoc.equals("true")) {
			model.addAttribute("mydoc", mydoc);
			session.setAttribute("mydoc", "false");
		} else {
			model.addAttribute("mydoc", "false");
		}

	
		
		return null;
	}

	@RequestMapping(value = "/updateDocument.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doUpdateDocuments(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "docName", required = false) String docName,
			@RequestParam(value = "document") MultipartFile document) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}
		User user = userManager.getUser(SecurityContextHolder.getContext().getAuthentication().getName());
		String email = user.getEmail();
		Long userId = user.getUserId();

		byte[] photo = null;
		if (document.getSize() != 0) {

			try {
				photo = document.getBytes();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		List<MyDocuments> docment=userManager.getClientDocNames(docName);
		if(docment.size()>0){
			System.out.println("enter");
			//String rst="true";
			session.setAttribute("mydoc", "true");
			
		}
		else{
		MyDocuments doc = new MyDocuments();
		doc.setFileName(docName);
		doc.setUserName(email);
		doc.setUserid(userId);
		doc.setFiles(photo);
		userManager.insertDocuments(doc);
		}
		/*List<MyDocuments> userDoc = userManager.getDocDetails(userId);
		if(userDoc.size()>0 )
		{
			for(MyDocuments userDocs:userDoc ){
				String docname=userDocs.getFileName();
				if (docName!=null && docName!=docname){
					
					MyDocuments doc = new MyDocuments();
					doc.setFileName(docName);
					doc.setUserName(email);
					doc.setUserid(userId);
					doc.setFiles(photo);
					userManager.insertDocuments(doc);
					
				}
				else{
					String errorr="File name already exists";
					model.addAttribute("errorr",errorr);
					return errorr;
				}
		}
		
		}*/
		//model.addAttribute(errorr, errorr);
		return "redirect:myDocuments.mm";
	
	}
	// @RequestMapping(value = "/myInfo.mm", method = { RequestMethod.GET,
	// RequestMethod.POST })
	// public void doAssessmentProfile(ModelMap model, HttpServletRequest
	// request, HttpSession session) {
	// User user = new User();
	// model.addAttribute("user", user);
	//
	// }
	//
	// @RequestMapping(value="/submitInfo.mm",method={ RequestMethod.GET,
	// RequestMethod.POST})
	// public String doSubmitAssessment(ModelMap model, HttpServletRequest
	// request, HttpSession session,@ModelAttribute("user")User user ){
	//
	// userManager.updateUser(user);
	// return "redirect:assessmentProfile.mm";
	// }

	
	@RequestMapping(value = "/deleteDocument.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doDdeleteDocs(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "deleteDocId", required = false) Long id) {
		try {
			userManager.deleteDoc(id);
			return "redirect:myDocuments.mm";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
	
	@RequestMapping(value = "/evaluation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doEvaluation(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}

		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addAttribute("user", user);
		return null;
	}

	@RequestMapping(value = "/retainerAgreement.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doRetainerAgreement(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}

		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addAttribute("user", user);
		return null;
	}

	@RequestMapping(value = "/fileDetails.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doFileDetails(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}

		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addAttribute("user", user);
		return null;
	}

	@RequestMapping(value = "/helpDesk.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doHelpDesk(ModelMap model, HttpServletRequest request, HttpSession session) {
		if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) {
			return "redirect:login.mm";
		}

		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addAttribute("user", user);

		return null;

	}

	@RequestMapping(value = "/image.mm", method = { RequestMethod.GET, RequestMethod.POST })
	protected void doImage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "id", required = false) Long id) throws ServletException, IOException {

		byte[] image = null;
		if (id != null) {
			User user = userManager.getImage(id);
			image = user.getProfilePic();

		}
		response.setContentType("image/jpeg");
		ServletOutputStream outputStream = response.getOutputStream();
		if (image != null)
			outputStream.write(image);
		outputStream.close();

	}

	@RequestMapping(value = "/about.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAbout(ModelMap model, HttpServletRequest request) {

	}
	
	@RequestMapping(value = "/countrymaster.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCountryMaster(ModelMap model, HttpServletRequest request) {

	}
	
	@RequestMapping(value = "/designation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doDesignation(ModelMap model, HttpServletRequest request) {

	}


	@RequestMapping(value = "/australia.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAustralia(ModelMap model, HttpServletRequest request) {

	}

	@RequestMapping(value = "/blog.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doBlog(ModelMap model, HttpServletRequest request) {

	}

	@RequestMapping(value = "/canada.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCanada(ModelMap model, HttpServletRequest request) {

	}

	@RequestMapping(value = "/career.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCareer(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}

	@RequestMapping(value = "/contact.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doContact(ModelMap model, HttpServletRequest request) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		
		} else {
			model.addAttribute("enq", "false");
			//return "redirect:homepage.mm";
		}

	}

	@RequestMapping(value = "contactEnquiry.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doContactEnquiry(HttpServletRequest req,
			@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "message", required = false) String message) {

		Enquiries enq = new Enquiries();

		enq.setName(firstName);
		enq.setPhoneNumber(phoneNumber);
		enq.setEmail(email);
		enq.setMessage(message);

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();
		String reportDate = df.format(today);
		enq.setDate(reportDate);

		userManager.insertEnquiry(enq);
		session.setAttribute("enq", "true");

		return "redirect:contact.mm#show_load";

	}

	@RequestMapping(value = "/newzealand.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doNewzealand(ModelMap model, HttpServletRequest request) {

	}

	@RequestMapping(value = "/service.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doService(ModelMap model, HttpServletRequest request) {

	}

	@RequestMapping(value = "/students.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doStudents(ModelMap model, HttpServletRequest request) {

	}

	@RequestMapping(value = "/provincialnominee.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doProvincialNominee(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}

	@RequestMapping(value = "/qubecskiled.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doQubecSkiled(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}

	@RequestMapping(value = "/familysponsorship.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doFamilySponsorship(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}

	@RequestMapping(value = "/expressentry.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doExpressentry(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}

	@RequestMapping(value = "/businessimmigration.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doBusinessImmigration(ModelMap model, HttpServletRequest req, HttpSession session,
			@RequestParam(value = "hiddenValue", required = false) String hiddenValue) {
		String enq = (String) session.getAttribute("enq");
		
		if (StringUtils.isNotBlank(enq) && enq.equals("true")) {
			model.addAttribute("enq", enq);
			session.setAttribute("enq", "false");
		} else {
			model.addAttribute("enq", "false");
		}
	}

	@RequestMapping(value = "/dummy.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doDummy(ModelMap model, HttpServletRequest request) {

	}
}
