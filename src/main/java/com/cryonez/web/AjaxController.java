package com.cryonez.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.cryonez.Service.AdminManager;
import com.cryonez.Service.UserManager;
import com.cryonez.entity.Category;
import com.cryonez.entity.Client;
import com.cryonez.dto.ClientDTO;
import com.cryonez.dto.userDTO;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.Followup;
import com.cryonez.entity.Miscfee;
import com.cryonez.entity.OnlinePay;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Receipt;
import com.cryonez.entity.Refund;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;
import com.cryonez.entity.UserExperience;
import com.cryonez.entity.Invoice;
import com.cryonez.util.MailManager;

import java.util.Collections;

/**
 * This class is a controller to handle the Ajax requests from the Admin UI. It
 * leverages Direct Web Remoting (DWR) to simplify the Ajax coding.
 * 
 */

@Service
@RemoteProxy(name = "AjaxController")
public class AjaxController {
	private Logger LOG = Logger.getLogger(AjaxController.class);

	@Autowired
	private UserManager userManager;

	@Autowired
	private AdminManager adminManager;

	@Autowired
	private MailManager mailManager;

	@Autowired(required = false)
	@Qualifier("authManager")
	AuthenticationManager authManager;

	/*
	 * @RemoteMethod public void deleteOpp(Long id){ userManager.deleteOpp(id);
	 * }
	 */
	@RemoteMethod
	public String addClientDetails(HttpSession session, String clientEmail, String phone, String salutation,
			String firstName, String middleName, String lastName, String dob, String passportNo,
			String passportExpDate, String birthCountry, String residencyCountry, String citizenCountry,
			String preferredLanguage, String doj) {
		Client client = adminManager.getClientByEmail(clientEmail);
		if(client == null)
			client = new Client();
		client.setEmail(clientEmail);
		client.setPhoneNumber(phone);
		client.setSalutation(salutation);
		client.setFirstName(firstName);
		client.setMiddleName(middleName);
		client.setLastName(lastName);
		client.setDob(dob);
		client.setPassportNo(passportNo);
		client.setPassportExpDate(passportExpDate);
		client.setBirthCountry(birthCountry);
		client.setResidenCountry(residencyCountry);
		client.setCitizenshipCountry(citizenCountry);
		client.setPrefferedLanguage(preferredLanguage);
		client.setDateOfJoin(doj);
		try {

			userManager.saveClient(client);
			session.setAttribute("clientId", client);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error(e);
		}
		return null;

	}

	
	
	@RemoteMethod
	public String addClientAddressDetails(HttpSession session, String address1, String address2, String city,
			String state, String country, String zipcode, String mobile, String phone1, String otherEmail, String fax,
			String contactMethod, String specialInst) {
		Client client = (Client) session.getAttribute("clientId");
		client.setAddress1(address1);
		client.setAddress2(address2);
		client.setCity(city);
		client.setState(state);
		client.setCountry(country);
		StringUtils.isNotBlank(zipcode);
		client.setZipcode(zipcode);
		client.setMobileNumber(mobile);
		client.setPhoneNumber1(phone1);
		client.setOtherEmail(otherEmail);
		client.setFax(fax);
		client.setContactMethod(contactMethod);
		client.setSpecialInstructions(specialInst);

		try {
			userManager.saveClient(client);
			session.setAttribute("clientId", client);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RemoteMethod
	public String addClientFilesDetails(HttpSession session, String program, String visaPosts, String fileNo,
			String fileNoOther, String fileStatus, String appSubmissionDate, String medExpDate, String interviewDate,
			String interviewTime, String interviewLocation, String addInfo, String medSentBy, String medBillNo,
			String visaSentBy, String visaWaybillNo) {
		Client client = (Client) session.getAttribute("clientId");
		client.setProgram(program);
		client.setVisaPosts(visaPosts);
		client.setFileNo(fileNo);
		client.setFileNoOther(fileNoOther);
		client.setFileStatus(fileStatus);
		client.setAppSubmissionDate(appSubmissionDate);
		client.setMedExpDate(medExpDate);
		client.setInterviewDate(interviewDate);
		client.setInterviewTime(interviewTime);
		client.setInterviewLocation(interviewLocation);
		client.setAddInfo(addInfo);
		client.setMedSentBy(medSentBy);
		client.setMedBillNo(medBillNo);
		client.setVisaSentBy(visaSentBy);
		client.setVisaWaybillNo(visaWaybillNo);

		try {
			userManager.saveClient(client);
			session.setAttribute("clientId", client);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RemoteMethod
	public String addClientPRDetails(HttpSession session, String prIssueDate, String prValidityDate,
			String landingDate, String citizenshipDate, String employer, String employerLocation, String lmoNumber,
			String wpIssueDate, String wpInterviewDate, String wpExtGrantDate, String wpInterviewWaiverReciptDate,
			String tempRenExt, String arrivalDate, String departureDate, String saveFourthStepSuccess) {
		Client client = (Client) session.getAttribute("clientId");
		client.setPrIssueDate(prIssueDate);
		client.setPrValidityDate(prValidityDate);
		client.setLandingDate(landingDate);
		client.setCitizenshipDate(citizenshipDate);
		client.setEmployer(employer);
		client.setEmployerLocation(employerLocation);
		client.setLmoNumber(lmoNumber);
		client.setWpIssueDate(wpIssueDate);
		client.setWpInterviewDate(wpInterviewDate);
		client.setWpExtGrantDate(wpExtGrantDate);
		client.setWpInterviewWaiverReciptDate(wpInterviewWaiverReciptDate);
		client.setTempRenExt(tempRenExt);
		client.setArrivalDate(arrivalDate);
		client.setDepartureDate(departureDate);

		try {
			userManager.saveClient(client);
			session.setAttribute("clientId", client);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RemoteMethod
	public String addClientProcessDetails(HttpSession session, String sales, String relationshipManager,
			String processingOfficer, String processingConsultant) {
		Client client = (Client) session.getAttribute("clientId");
		client.setSales(sales);
		client.setRelationshipManager(relationshipManager);
		client.setProcessingOfficer(processingOfficer);
		client.setProcessingConsultant(processingConsultant);
		try {
			userManager.saveClient(client);
			session.setAttribute("clientId", client);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RemoteMethod
	public String addClientSystemDetails(HttpSession session, String clientCategory, String clientStatus, String source) {
		Client client = (Client) session.getAttribute("clientId");
		client.setClientCategory(clientCategory);
		client.setClientStatus(clientStatus);
		User user = userManager.getEmailAddress(client.getEmail());
		if(user != null){
			user.setStatus(clientStatus);
			userManager.saveRadio(user);
		}
		client.setSource(source);
		try {
			userManager.saveClient(client);
			session.setAttribute("clientId", client);
			return "success";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RemoteMethod
	public List<Opportunities> getSearchVal(String val) {
		return userManager.getSearchVal(val);
	}

	/**
	 * This method to validate client is already exist or not
	 * 
	 * @param email
	 * @return
	 */
	@RemoteMethod
	public List<Client> getClients(String val,HttpSession session) {
		List<Client> clientList = adminManager.getClientByStatus(val);
		session.setAttribute("clientList", clientList);
		return clientList;
		
	}
	

	
	@RemoteMethod
	public List<User> getDocApproved(String val, HttpSession session) {
		List<User> DocuApprovalList = adminManager.getNotUpAgreeList(val);
		session.setAttribute("DocuApprovalList", DocuApprovalList);
		return DocuApprovalList;  
	}
	@RemoteMethod
	public List<User> getfilteragreeaprove(String val ,HttpSession session) {
		List<User> agreementApprovalList = adminManager.getfilteragreeaproveList(val);
		session.setAttribute("agreementApprovalList", agreementApprovalList);
		return agreementApprovalList;
			}
	
	@RemoteMethod
	public List<User> getfilteragreeupload( String val, HttpSession session) {
		List<User> agreementuploadList = adminManager.getfilteragreeuploadList(val);
		session.setAttribute("agreementuploadList", agreementuploadList);
		return agreementuploadList;
	}
	
	
	@RemoteMethod
	public Invoice editInvoice(String id) {
		Invoice invoice = adminManager.getInvoiceById(id);
		return invoice;
	}

	@RemoteMethod
	public Refund editRefunds(String id) {
		Refund refunder = adminManager.getRefundById(id);
		return refunder;
	}
	
	@RemoteMethod
	public List<Refund> deleteRefundsBy(String id) {
		Refund refunder1 = adminManager.getRefundById(id);
		refunder1.setDeleted(true);
		adminManager.saveRefund(refunder1);
		List<Refund> refunder = adminManager.getRefunder();
		return refunder; 
				
	}
	
	
		 


	
	@RemoteMethod
	public List<Receipt> approvalRecBy(String id) {
		Receipt reciept1 = adminManager.approvalAccRecById(id);
		reciept1.setApproval(true);
		adminManager.saveReceipt(reciept1);
		List<Receipt> reciept = adminManager.getReceipt();
		return reciept;
	
	}
	@RemoteMethod
	public List<Invoice> approvalInvoBy(String id) {
		Invoice invoicer1 = adminManager.approvalInvoById(id);
		invoicer1.setApproval(true);
		adminManager.saveinvoice(invoicer1);
		List<Invoice> invoicer = adminManager.getinvoice();
		return invoicer;
		
	}
	@RemoteMethod
	public List<Miscfee> approvalMiscBy(String id) {
		Miscfee miscfee1 = adminManager.approvalMiscById(id);
		miscfee1.setApproval(true);
		adminManager.saveMiscfee(miscfee1);
		List<Miscfee> miscfee = adminManager.getMiscfee();
		return miscfee;
		
	}
	
	@RemoteMethod
	public List<Client> getClientsByStatus(String val, String field, String type,HttpSession session) {
		List<Client> clientList = adminManager.getClientByStatusByField(val, field, type);
	
		session.setAttribute("clientList", clientList);
		return clientList;
	
	
	}
	
	@RemoteMethod
	public List<Client> SearchClientCoBy(String val, String field, String type,HttpSession session) {
		List<Client> clientList = adminManager.SearchClientCoByField(val, field, type);
	
		session.setAttribute("clientList", clientList);
		return clientList;
	
	
	}
	@RemoteMethod
	public List<User> SearchUserFUCoBy(String val, String field, HttpSession session) {
		List<User> fileuploadList = adminManager.SearchUserFUCoByField(val, field );
	
		session.setAttribute("fileuploadList", fileuploadList);
		return fileuploadList;
	
	
	}
	
	@RemoteMethod
	public List<User> SearchUserAUCoBy(String val, String field, String type,HttpSession session) {
		List<User> agreementuploadList = adminManager.SearchUserAUCoByField(val, field, type);
	
		session.setAttribute("agreementuploadList", agreementuploadList);
		return agreementuploadList;
	
	
	}
	
	
	@RemoteMethod
	public List<User> SearchUserDACoBy(String val, String field, String type,HttpSession session) {
		List<User> DocuApprovalList = adminManager.SearchUserDACoByField(val, field, type);
	
		session.setAttribute("DocuApprovalList", DocuApprovalList);
		return DocuApprovalList;
	
	
	}
	
	@RemoteMethod
	public List<User> SearchUserAACoBy(String val, String field, String type,HttpSession session) {
		List<User> agreementApprovalList = adminManager.SearchUserAACoByField(val, field, type);
	
		session.setAttribute("agreementApprovalList", agreementApprovalList);
		return agreementApprovalList;
	
	
	}
	
	@RemoteMethod
	public List<OnlinePay> onlinePayBy(String val, String field ) {
		List<OnlinePay> online = adminManager.onlinePayByList(val, field);
		return online;
	}

	
	
	@RemoteMethod
	public List<Refund> RefundBy(String val, String field ) {
		List<Refund> refunder = adminManager.RefundByList(val, field);
		return refunder;
	}


	@RemoteMethod
	public List<Miscfee> MiscfeeBy(String val, String field ) {
		List<Miscfee> miscfee = adminManager.MiscfeeByList(val, field);
		return miscfee;
	}
	@RemoteMethod
	public List<Client> deleteClientsBy(String id)  {
		Client clientList1 = adminManager.deleteClientsById(id);
		clientList1.setDeleted(true);
		adminManager.saveClients(clientList1);
		List<Client> clientList = adminManager.getClientsBy();
		return clientList;
	}
	@RemoteMethod
	public List<Employee> SearchThisUser(String val, String field ) {
		List<Employee> EmpList = adminManager.SearchThisUserStatus(val, field);
		return EmpList;
	}
	
	@RemoteMethod
	public List<User> SearchDocuploadlist( String val, String field ,HttpSession session) {
		List<User> fileuploadList = adminManager.SearchDocuploadlistStatus(val, field);
		session.setAttribute("fileuploadList", fileuploadList);
		return fileuploadList;
	}
	
	/*@RemoteMethod
	public List<User> deleteFileUpload(HttpSession session, String userId){
		List<User> userList = (List<User>) session.getAttribute("fileuploadList");
		boolean isDelete = false;
		List<User> newList = new ArrayList<>
		if(userList != null && userList.size() > 0){
			for(User usr: userList){
				if(usr.getUserId() == Integer.parseInt(userId))
					isDelete = true;  
			}
		}
		return adminManager.deleteFileUpload(userId);
	}
	*/
	/*public List<Receipt> searchReceiptBy(String val, String field ) {
		List<Receipt> reciept = adminManager.SearchReceiptByList(val, field);
		return reciept;
	}*/
	@RemoteMethod
	public List<Receipt> searchReceipt(String val, String field) {
		List<Receipt> reciept = adminManager.SearchReceiptByList(val, field);
		return reciept;
	}
	@RemoteMethod
	public List<Invoice> SearchinvoicetBy(String val, String field ) {
		List<Invoice> invoicer = adminManager.SearchinvoicetByList(val, field);
		return invoicer;
	}
	
	
	@RemoteMethod
	public List<User> AgreeuploadBy(String val, String field, String type, HttpSession session) {
		List<User> UserList = adminManager.AgreeuploadByUser(val, field, type);
		session.setAttribute("agreementuploadList", UserList);
		return UserList;
	}

	@RemoteMethod
	public List<User> SearchDocApproveBy(String val, String field, String type, HttpSession session) {	
		List<User> UserList = adminManager.SearchDocApproveByList(val, field, type);
		session.setAttribute("DocuApprovalList", UserList);
		return UserList;
	}
	
	@RemoteMethod
	public List<User> AgreeApproveBy(String val, String field, String type ,HttpSession session) {
		List<User> UserList = adminManager.AgreeApproveByList(val, field, type);
		session.setAttribute("agreementApprovalList", UserList);
		return UserList;
	}
	
	@RemoteMethod
	public String enqtoClient(String email, String name, String phone) {
		Date dte = new Date();

		if (!isValidEmailAddress(email))
			return "Email not valid";
		User user = userManager.getUser(email);
		if (user != null)
			return "User already exist";
		else {
			Client client = new Client();
			client.setEmail(email);
			client.setFirstName(name);
			client.setPhoneNumber(phone);
			client.setDateOfJoin(dte.toString());
			userManager.saveClient(client);
			user = new User();
			user.setEmail(email);
			user.setUserName(email);
			user.setActive(true);
			userManager.saveRadio(user);
		}
		return "success";

	}

	@RemoteMethod
	public List<Opportunities> getStatusVal(boolean val) {
		return userManager.getStatusVal(val);

	}

	@RemoteMethod
	public List<Enquiries> getSearchEnquiry(String val) {
		return userManager.getSearchEnquiry(val);
	}

	//

	//
	//

	//
	private boolean isAuthenticated(String username, String password) {

		Authentication request = new UsernamePasswordAuthenticationToken(username, password);
		Authentication result = authManager.authenticate(request);
		SecurityContextHolder.getContext().setAuthentication(result);
		return result.isAuthenticated();
	}

	//
	/*
	 * @RemoteMethod public void deleteOpp(Long id) {
	 * 
	 * userManager.deleteOpp(id); }
	 */

	//
	@RemoteMethod
	public String login(String username, String password) {
		if (StringUtils.isBlank(username)) {
			return "Please enter username";
		}
		if (!isValidEmailAddress(username)) {
			return "Email not valid";
		}
		if (StringUtils.isBlank(password))
			return "Please enter password";

		User user = userManager.getUser(username);
		if (user == null)
			return "Not registered User, Please register";
		else if (!user.getActive())
			return "Your account not activated, please check your email";
		else if (!user.getPassword().equals(password))
			return "Invalid password";
		else

			return "success";

	}

	@RemoteMethod
	public boolean verifyEmail(String email) {
		boolean b = userManager.checkEmail(email);
		if (b) {
			return true;
		}
		return false;
	}

	//
	@RemoteMethod
	public boolean checkEmail(String email) {

		boolean b = userManager.checkEmail(email);
		if (b) {
			return true;
		}
		return false;
	}

	@RemoteMethod
	public boolean checkPassport(String pal) {
		boolean b = userManager.checkPassport(pal);
		if (b) {

			return true;
		}
		return false;
	}

	@RemoteMethod
	public void saveExperience(String companyList, String designationsList, String fromDates, String toDates,
			String roles) {

		User user = userManager.getActiveUser(SecurityContextHolder.getContext().getAuthentication().getName());
		Long userId = user.getUserId();
		UserExperience userExp = (UserExperience) userManager.checkCompanyName(userId, companyList);
		if (userExp == null)
			userExp = new UserExperience();

		userExp.setUserId(userId);
		userExp.setCompanyName(companyList);
		userExp.setDesignation(designationsList);
		userExp.setFromPeriod(fromDates);
		userExp.setToPeriod(toDates);
		userExp.setRole(roles);
		userManager.saveUserExperience(userExp);
	}

	@RemoteMethod
	public void saveQuestion(String qs) {
		User user = userManager.getActiveUser(SecurityContextHolder.getContext().getAuthentication().getName());
		user.setBloodRelation(qs);
		userManager.saveRadio(user);
	}

	@RemoteMethod
	public void saveQuestion2(String qs) {
		User user = userManager.getActiveUser(SecurityContextHolder.getContext().getAuthentication().getName());
		user.setJobOffer(qs);
		userManager.saveRadio(user);
	}

	@RemoteMethod
	public void saveQuestion3(String qs) {
		User user = userManager.getActiveUser(SecurityContextHolder.getContext().getAuthentication().getName());
		user.setStudyExp(qs);
		userManager.saveRadio(user);
	}

	@RemoteMethod
	public boolean checkRadio() {
		User user = userManager.getActiveUser(SecurityContextHolder.getContext().getAuthentication().getName());
		String bloodRelation = user.getBloodRelation();
		if (bloodRelation == "1$stand") {
			return true;
		}
		return false;
	}

	@RemoteMethod
	public boolean emailidCheck(String email) {

		boolean b = userManager.checkEmail(email);
		if (b) {
			return true;
		}

		return false;
	}

	public static boolean isValidEmailAddress(String str) {

		String at = "@";
		String dot = ".";
		int lat = str.indexOf(at);
		int lstr = str.length();
		if (str.indexOf(at) == -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(at, (lat + 1)) != -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(dot, (lat + 2)) == -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(" ") != -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		return true;
	}

	@RemoteMethod
	public String getValidationByEmailAddress(String email) {
		String error = null;
		if (!isValidEmailAddress(email)) {
			error = "Not valied Email Address";
			return error;
		}
		User userFromDb = userManager.getEmailAddress(email);
		if (userFromDb != null) {
			error = "Email Address not available";
			return error;
		}
		error = "";
		return error;
	}

	@RemoteMethod
	public boolean emailCheck1(String email) {

		boolean b = userManager.checkEmail(email);
		if (b) {
			return true;
		}

		return false;
	}

	@RemoteMethod
	public void forgotPassword(String email) {
		String template = null;
		User user = userManager.getEmailAddress(email);
		UUID token = user.getToken();
		final Map<String, Object> data = new HashMap<String, Object>();

		data.put("token", token);
		data.put("email", email);
		template = "resetpassword";

		String subject = "Reset Password";
		String toName = email;
		final String toAddress = email;
		mailManager.sendEmail(subject, data, template, toName, toAddress);

	}

	@RemoteMethod
	public String getUserLoginCookie(HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		String value = "";
		String vals;
		String password = "";
		if (cookies != null) {
			for (Cookie c : cookies) {
				if (c.getName().equals("cookieName")) {
					value = c.getValue();
					password = userManager.getPassword(value);
					if (password != "") {
						vals = value + "_" + password;
						return vals;
					}
					return null;
				}
			}
		}
		return null;

	}

	@RemoteMethod
	public Category getThisCategory(String id) {

		return userManager.getThisCategory(id);
	}

	@RemoteMethod
	public void deletThisCatg(String id) {
		Category catg = userManager.getThisCategory(id);
		catg.setIsactive(false);
		userManager.saveCategory(catg);
	}

	@RemoteMethod
	public List<Category> getCategoryLikeThis(String val) {

		List<Category> catgs = userManager.getCategoryLikeThis(val);

		return catgs;

	}

	@RemoteMethod
	public Source editThisSource(String id) {
		Source src = userManager.getThisSource(id);
		return src;

	}

	@RemoteMethod
	public void deletThisSource(String id) {
		Source src = userManager.getThisSource(id);
		src.setIsactive(false);
		userManager.saveSources(src);
	}

	@RemoteMethod
	public List<Source> getSearchValues(String val) {
		System.out.println();
		List<Source> sources = userManager.getSorcesBySearchValues(val);

		return sources;
	}

	@RemoteMethod
	public Designation getThisDesignation(String id) {
		Designation dsg = userManager.getThisDesignation(id);
		return dsg;
	}

	@RemoteMethod
	public void deletThisDesignation(String id) {
		Designation dsg = userManager.getThisDesignation(id);
		dsg.setIsactive(false);
		userManager.saveDesignation(dsg);
	}

	@RemoteMethod
	public List<Designation> getDesignationLikeThis(String val) {
		List<Designation> dsg = userManager.getDesignationLikeThis(val);
		return dsg;
	}

	@RemoteMethod
	public void ChangeEmployeeStatus(String id) {
		Employee emp = userManager.getThisEmployee(id);
		emp.setIsactive(false);

		userManager.saveEmployee(emp);
	}

	@RemoteMethod
	public List<Employee> getThisSearchValue(String val) {
		List<Employee> emps = userManager.getEmployeesLikeThis(val);
		return emps;

	}

	@RemoteMethod
	public List<String> setoerderAmount(String amount) {
		List<String> res = new ArrayList<String>();
		String orderAmount = amount;
		String vanityUrl = "paogateway";
		String merchantTxnId = String.valueOf(System.currentTimeMillis());
		String currency = "INR";
		String data = vanityUrl + orderAmount + merchantTxnId + currency;
		String secret_key = "255b3c6f6422b4fcb43871656773441d3b14ce71";

		try {
			javax.crypto.Mac mac = javax.crypto.Mac.getInstance("HmacSHA1");
			mac.init(new javax.crypto.spec.SecretKeySpec(secret_key.getBytes(), "HmacSHA1"));
			byte[] hexBytes = new org.apache.commons.codec.binary.Hex().encode(mac.doFinal(data.getBytes()));
			String securitySignature = new String(hexBytes, "UTF-8");
			res.add(orderAmount);

			res.add(securitySignature);
			res.add(merchantTxnId);
			res.add(currency);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;

	}

	@RemoteMethod
	public String saveProfileFollowup(HttpSession session, String profileNote1, String profileNote2, String profileNote3, String profileDate1, String profileDate2,
			String profileDate3, boolean callYou1, boolean profileReceipt1, String clientId, String type) {
		if(StringUtils.isNotBlank(clientId)) {
			Followup followup = adminManager.getFollowupByClientId(clientId, type);
			if(followup == null){
				followup = new Followup();
				followup.setClientId(Integer.parseInt(clientId));
			}
			followup.setDate1(profileDate1);
			followup.setDate2(profileDate2);
			followup.setDate3(profileDate3);
			followup.setNotes1(profileNote1);
			followup.setNotes2(profileNote2);
			followup.setNotes3(profileNote3);
			followup.setCallYou(callYou1);
			followup.setProfileReceived(profileReceipt1);
			followup.setType(type);
			adminManager.saveFollowup(followup);
		}else
			return "error";
				return "success";
	}
	
}


