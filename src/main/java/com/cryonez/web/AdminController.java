package com.cryonez.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cryonez.Service.AdminManager;
import com.cryonez.Service.UserManager;
import com.cryonez.dto.ClientDTO;
import com.cryonez.dto.userDTO;
import com.cryonez.entity.Category;
import com.cryonez.entity.Client;
import com.cryonez.entity.Country;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.Miscfee;
import com.cryonez.entity.OnlinePay;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Receipt;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.Refund;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;
import com.cryonez.entity.Invoice;

@Controller
public class AdminController {

	@Autowired
	UserManager userManager;

	@Autowired
	AdminManager adminManager;

	@RequestMapping(value = "/adminDashboard.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doDashboard(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/addClient.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doAddClient(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "prePop", required = false) String email) {

		Client client = new Client();
		if (StringUtils.isNotBlank(email))
			client = adminManager.getClientByEmail(email);
		model.addAttribute("clt", client);
		List<Country> countryList = userManager.getCountryList();
		model.addAttribute("countries", countryList);
		List<Employee> salempList = adminManager.geEmpListByDesignation("SALES");
		model.addAttribute("salemp", salempList);
		List<Employee> relatoinmanList = adminManager.geEmpListByDesignation("RELATIONSHIP MANAGER");
		model.addAttribute("relatoinmanList", relatoinmanList);
		List<Employee> processingemp = adminManager.geEmpListByDesignation("PROCESSING OFFICER");
		model.addAttribute("processingemp", processingemp);
		List<Employee> processingConsultantemp = adminManager.geEmpListByDesignation("PROCESSING CONSULTANT");
		model.addAttribute("processingConsultantemp", processingConsultantemp);
		List<Employee> processingempcor = adminManager.geEmpListByDesignation("PROCESSING COORDINATOR");
		model.addAttribute("processingempcor", processingempcor);
		
		return null;
	}

	@RequestMapping(value = "createClientSheet.mm", method = RequestMethod.GET)
	public void doCreateIUserSheet(HttpServletResponse response, ModelMap model,HttpSession session) {
		System.out.println();
		List<Client> clientList1 = (List<Client> )session.getAttribute("clientList");
		Workbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("ClientsList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("ClientId");
		header.createCell(1).setCellValue("Name");
		header.createCell(2).setCellValue("Email");
		header.createCell(3).setCellValue("Contact");
		header.createCell(4).setCellValue("Join Date");
		header.createCell(5).setCellValue("Coordinator");
		int rowCount = 1;

		for (Client clientList : clientList1) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(clientList.getId());
			data.createCell(1).setCellValue(clientList.getFirstName());
			data.createCell(2).setCellValue(clientList.getEmail());
			data.createCell(3).setCellValue(clientList.getPhoneNumber());
			data.createCell(4).setCellValue(clientList.getDateOfJoin());
			data.createCell(5).setCellValue(clientList.getLastName());

		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "ClientsList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createEmployeeSheet.mm", method = RequestMethod.GET)
	public void doCreateEmployeeSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<Employee> empList = (List<Employee>) session.getAttribute("emp");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("EmployeeList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("empId");
		header.createCell(1).setCellValue("FirstName");
		header.createCell(2).setCellValue("Email");
		header.createCell(3).setCellValue("Mobile");
		int rowCount = 1;

		for (Employee emp : empList) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(emp.getId());
			data.createCell(1).setCellValue(emp.getEmp_firstname());
			data.createCell(2).setCellValue(emp.getEmp_email());
			data.createCell(3).setCellValue(emp.getEmp_mobile());

		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "EmployeeList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createOnlinePaymentSheet.mm", method = RequestMethod.GET)
	public void docreateOnlinePaymentSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<OnlinePay> onlinepayment = (List<OnlinePay>) session.getAttribute("onlinepay");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("OnlinePaymentList.xlsx");

		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("id");
		header.createCell(1).setCellValue("date");
		header.createCell(2).setCellValue("amount");
		header.createCell(3).setCellValue("email");
		header.createCell(4).setCellValue("clientId");
		header.createCell(5).setCellValue("refNo");
		header.createCell(6).setCellValue("payId");
		header.createCell(7).setCellValue("reqId");
		header.createCell(8).setCellValue("trainId");
		int rowCount = 1;

		for (OnlinePay onlinepay : onlinepayment) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(onlinepay.getId());
			data.createCell(1).setCellValue(onlinepay.getDate());
			data.createCell(2).setCellValue(onlinepay.getAmount());
			data.createCell(3).setCellValue(onlinepay.getEmail());
			data.createCell(4).setCellValue(onlinepay.getClientId());
			data.createCell(5).setCellValue(onlinepay.getRefNo());
			data.createCell(6).setCellValue(onlinepay.getPayId());
			data.createCell(7).setCellValue(onlinepay.getReqId());
			data.createCell(8).setCellValue(onlinepay.getTrainId());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "OnlinePaymentList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createReceiptSheet.mm", method = RequestMethod.GET)
	public void doCreateReceiptSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<Receipt> receipter = (List<Receipt>) session.getAttribute("reciept");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("ReceiptList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Client");
		header.createCell(2).setCellValue("Date");
		header.createCell(3).setCellValue("Amount");
		header.createCell(4).setCellValue("Mode");
		header.createCell(5).setCellValue("TR.ID");
		header.createCell(6).setCellValue("Description");
		header.createCell(7).setCellValue("Coordinators");
		int rowCount = 1;

		for (Receipt reciept : receipter) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(reciept.getId());
			data.createCell(1).setCellValue(reciept.getEmail());
			data.createCell(2).setCellValue(reciept.getDate());
			data.createCell(3).setCellValue(reciept.getAmount());
			data.createCell(4).setCellValue(reciept.getPaymentMode());
			data.createCell(5).setCellValue(reciept.getTransactionNo());
			data.createCell(6).setCellValue(reciept.getDescription());
			data.createCell(7).setCellValue(reciept.getCoordinator());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "ReceiptList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}

	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createInvoiceSheet.mm", method = RequestMethod.GET)
	public void doCreateInvoiceSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<Invoice> invo = (List<Invoice>) session.getAttribute("invoicer");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("InvoiceList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Client");
		header.createCell(2).setCellValue("Date");
		header.createCell(3).setCellValue("Amount");
		header.createCell(4).setCellValue("Mode");
		header.createCell(5).setCellValue("TR.ID");
		header.createCell(6).setCellValue("Description");
		header.createCell(7).setCellValue("Coordinators");
		int rowCount = 1;

		for (Invoice invoicer : invo) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(invoicer.getId());
			data.createCell(1).setCellValue(invoicer.getEmail());
			data.createCell(2).setCellValue(invoicer.getDate());
			data.createCell(3).setCellValue(invoicer.getAmount());
			data.createCell(4).setCellValue(invoicer.getPaymentMode());
			data.createCell(5).setCellValue(invoicer.getTransactionNo());
			data.createCell(6).setCellValue(invoicer.getDescription());
			data.createCell(7).setCellValue(invoicer.getCoordinator());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "InvoiceList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}

	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createMiscfeeSheet.mm", method = RequestMethod.GET)
	public void doCreateMiscfeeSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<Miscfee> misc = (List<Miscfee>) session.getAttribute("miscfee");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("MiscfeeList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Client");
		header.createCell(2).setCellValue("Date");
		header.createCell(3).setCellValue("Fee Name");
		header.createCell(4).setCellValue("Amount");
		header.createCell(5).setCellValue("Pay Mode");
		header.createCell(6).setCellValue("TR.ID");
		header.createCell(7).setCellValue("Description");

		int rowCount = 1;

		for (Miscfee miscfee : misc) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(miscfee.getId());
			data.createCell(1).setCellValue(miscfee.getClient());
			data.createCell(2).setCellValue(miscfee.getDate());
			data.createCell(3).setCellValue(miscfee.getFeeName());
			data.createCell(4).setCellValue(miscfee.getAmonut());
			data.createCell(5).setCellValue(miscfee.getPayMode());
			data.createCell(6).setCellValue(miscfee.getTransation());
			data.createCell(7).setCellValue(miscfee.getDescription());
			
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "MiscfeeList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createRefundSheet.mm", method = RequestMethod.GET)
	public void doCreateRefundSheet1(HttpSession session, HttpServletResponse response, ModelMap model) { 
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<Refund> refund = (List<Refund>) session.getAttribute("refunder");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("RefundList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Client");
		header.createCell(1).setCellValue("Amount");
		header.createCell(2).setCellValue("Date");
		header.createCell(3).setCellValue("Description");
		header.createCell(4).setCellValue("PayMode");
		int rowCount = 1;

		for (Refund refunder : refund) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(refunder.getClient());
			data.createCell(1).setCellValue(refunder.getAmount());
			data.createCell(2).setCellValue(refunder.getDate());
			data.createCell(3).setCellValue(refunder.getDescription());
			data.createCell(4).setCellValue(refunder.getPayMode());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "RefundList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
		
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createAgreementUploadSheet.mm", method = RequestMethod.GET)
	public void doCreateAgreementUploadSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<User> agreementupload = (List<User>) session.getAttribute("agreementuploadList");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("agreementuploadList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Name");
		header.createCell(2).setCellValue("Email");
		header.createCell(3).setCellValue("Contact");
		header.createCell(4).setCellValue("Coordinators");
		int rowCount = 1;

		for (User agreementuploadList : agreementupload) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(agreementuploadList.getUserId());
			data.createCell(1).setCellValue(agreementuploadList.getfName());
			data.createCell(2).setCellValue(agreementuploadList.getEmail());
			data.createCell(3).setCellValue(agreementuploadList.getMobile());
			data.createCell(4).setCellValue(agreementuploadList.getCoordinator());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "agreementuploadList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createFileUploadSheet.mm", method = RequestMethod.GET)
	public void doCreateFileUploadSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<User> fileupload = (List<User>)session.getAttribute("fileuploadList");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("fileuploadList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Name");
		header.createCell(2).setCellValue("Email");
		header.createCell(3).setCellValue("Contact");
		header.createCell(4).setCellValue("Coordinators");
		int rowCount = 1;

		for (User fileuploadList : fileupload) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(fileuploadList.getUserId());
			data.createCell(1).setCellValue(fileuploadList.getfName());
			data.createCell(2).setCellValue(fileuploadList.getEmail());
			data.createCell(3).setCellValue(fileuploadList.getMobile());
			data.createCell(4).setCellValue(fileuploadList.getCoordinator());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "fileuploadList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
	
	
	@SuppressWarnings("unchecked") 
	@RequestMapping(value = "createAgreementApprovalSheet.mm", method = RequestMethod.GET)
	public void doCreateAgreementApprovalSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<User> agreementApproval = (List<User>) session.getAttribute("agreementApprovalList");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("agreementApprovalList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Name");
		header.createCell(2).setCellValue("Email");
		header.createCell(3).setCellValue("Contact");
		header.createCell(4).setCellValue("Coordinators");
		int rowCount = 1;

		for (User agreementApprovalList : agreementApproval) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(agreementApprovalList.getUserId());
			data.createCell(1).setCellValue(agreementApprovalList.getfName());
			data.createCell(2).setCellValue(agreementApprovalList.getEmail());
			data.createCell(3).setCellValue(agreementApprovalList.getMobile());
			data.createCell(4).setCellValue(agreementApprovalList.getCoordinator());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "agreementApprovalList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "createDocumentApprovalSheet.mm", method = RequestMethod.GET)
	public void doCreateDocumentApprovalSheet(HttpSession session, HttpServletResponse response, ModelMap model) {
		System.out.println();
		// List<EmployeeDTO> clientList = adminManager.getAllEmployee();
		List<User> DocuApproval = (List<User>) session.getAttribute("DocuApprovalList");
		Workbook wb = new XSSFWorkbook();

		Sheet sheet = wb.createSheet("agreementApprovalList.xlsx");

		Row header = sheet.createRow(0);

		header.createCell(0).setCellValue("Id");
		header.createCell(1).setCellValue("Name");
		header.createCell(2).setCellValue("Email");
		header.createCell(3).setCellValue("Contact");
		header.createCell(4).setCellValue("Coordinators");
		int rowCount = 1;

		for (User DocuApprovalList : DocuApproval) {
			Row data = sheet.createRow(rowCount);
			rowCount++;
			data.createCell(0).setCellValue(DocuApprovalList.getUserId());
			data.createCell(1).setCellValue(DocuApprovalList.getfName());
			data.createCell(2).setCellValue(DocuApprovalList.getEmail());
			data.createCell(3).setCellValue(DocuApprovalList.getMobile());
			data.createCell(4).setCellValue(DocuApprovalList.getCoordinator());
		}
		response.setContentType("application/vnd.ms-excel");
		response.addHeader("Content-disposition", "attachment; filename=" + "DocuApprovalList.xlsx");

		try {
			wb.write(response.getOutputStream());
			wb.close();
		} catch (IOException e) {
			System.err.println("Inside Catch");
			e.printStackTrace();
		}

	}
	
	
	
	@RequestMapping(value = "/addclientdocument.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAddclientdocument(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/agreementapproval.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAgreementapproval(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<userDTO> agreementApprovalList = new ArrayList<userDTO>();
		agreementApprovalList = adminManager.getagreementApprovalList();
		model.addAttribute("agreementApprovalList", agreementApprovalList);
		session.setAttribute("agreementApprovalList", agreementApprovalList);
		
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	@RequestMapping(value = "/agreementupload.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAgreementupload(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<userDTO> agreementuploadList = new ArrayList<userDTO>();
		agreementuploadList = adminManager.getagreementuploadlList();
		model.addAttribute("agreementuploadList", agreementuploadList);
		session.setAttribute("agreementuploadList", agreementuploadList);
		
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);

	}

	@RequestMapping(value = "/approvaclientdocumtnel.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doApprovaclientdocumtnel(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/clientfileupload.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doClientfileupload(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<userDTO> fileuploadList = new ArrayList<userDTO>();
		fileuploadList = adminManager.getfileuploadlist();
		model.addAttribute("fileuploadList", fileuploadList);
		session.setAttribute("fileuploadList", fileuploadList);
		
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	@RequestMapping(value = "/clients.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doClients(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<Client> clientList = new ArrayList<Client>();
		clientList = adminManager.getAllClients();
		session.setAttribute("clientList", clientList);
		model.addAttribute("clientList", clientList);
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);

	}

	@RequestMapping(value = "/createinvoice.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCreateinvoice(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/createopportunitie.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCreateopportunitie(ModelMap model, HttpServletRequest request, HttpSession session) {
		model.addAttribute("opp", new Opportunities());
		List<Employee> emp = userManager.getAllEmployees();
		model.addAttribute("dsg", emp);
		List<Category> categories = userManager.getClientCategories();
		model.addAttribute("cats", categories);
		List<Source> sources = userManager.getSources();
		model.addAttribute("src", sources);

		List<Country> ctr = userManager.getAllCountries();
		model.addAttribute("ctr", ctr);
	}

	@RequestMapping(value = "/opportunities.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doOpportunities(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<Opportunities> opp = userManager.getOpportunity();
		model.addAttribute("opp", opp);

	}

	// @RequestMapping(value = "/opportunities.mm", method = {
	// RequestMethod.GET, RequestMethod.POST })
	// public void doOpportunities(){
	//
	// }

	@RequestMapping(value = "/createsimplemail.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCreatesimplemail(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/documentapproval.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doDocumentapproval(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<userDTO> DocuApprovalList = new ArrayList<userDTO>();
		DocuApprovalList = adminManager.getdocumentApprovalList();
		model.addAttribute("DocuApprovalList", DocuApprovalList);
		session.setAttribute("DocuApprovalList", DocuApprovalList);
		
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	/*
	 * @RequestMapping(value = "/agreementapproval.mm", method = {
	 * RequestMethod.GET, RequestMethod.POST }) public void
	 * doagreementapproval(ModelMap model, HttpServletRequest request,
	 * HttpSession session) { List<userDTO> agreementApprovalList = new
	 * ArrayList<userDTO>(); agreementApprovalList =
	 * adminManager.getagreementApprovalList();
	 * model.addAttribute("agreementApprovalList", agreementApprovalList);
	 * 
	 * 
	 * 
	 * }
	 */
	@RequestMapping(value = "/editInvoice.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doEditInvoice(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("invoicer") Invoice invoicer) {
	System.out.println();
	adminManager.saveinvoice(invoicer);
	session.setAttribute("invoicer", invoicer);
	return "redirect:accountinvoice.mm";
	}
	
	/*@RequestMapping(value = "/editclients.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doEditclients(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "clientId", required = true) String clientId) {

		Client client = adminManager.getClientById(clientId);
		model.addAttribute("client", client);

	}*/

	@RequestMapping(value = "/editclients.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doEditclients(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "clientId", required = true) String clientId) {
	/*	if (SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser")
				|| SecurityContextHolder.getContext().getAuthentication().getName().equals("")) 
			return "redirect:login.mm";*/
		
		model.addAttribute("clientId", clientId);
		User user = userManager.getUserDetails(SecurityContextHolder.getContext().getAuthentication().getName());
		model.addAttribute("user", user);
		Client client = adminManager.getClientById(clientId);
		model.addAttribute("clt", client);
		List<Country> countryList = userManager.getCountryList();
		model.addAttribute("countries", countryList);
		List<Employee> salempList = adminManager.geEmpListByDesignation("SALES");
		model.addAttribute("salemp", salempList);
		List<Employee> relatoinmanList = adminManager.geEmpListByDesignation("RELATIONSHIP MANAGER");
		model.addAttribute("relatoinmanList", relatoinmanList);
		List<Employee> processingemp = adminManager.geEmpListByDesignation("PROCESSING OFFICER");
		model.addAttribute("processingemp", processingemp);
		List<Employee> processingConsultantemp = adminManager.geEmpListByDesignation("PROCESSING CONSULTANT");
		model.addAttribute("processingConsultantemp", processingConsultantemp);
		List<Employee> processingempcor = adminManager.geEmpListByDesignation("PROCESSING COORDINATOR");
		model.addAttribute("processingempcor", processingempcor);
		

	}
	
	@RequestMapping(value = "/createrefunds.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddrefunds(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("refunder") Refund refunder) {

		adminManager.saveRefund(refunder);
		return "redirect:refunds.mm";
	}
	

	@RequestMapping(value = "/editRefunds.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doEditRefunds(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("refunder") Refund refunder) {

		adminManager.saveRefund(refunder);
		return "redirect:refunds.mm";
	}
	
	

	@RequestMapping(value = "/refunds.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doaRefund(ModelMap model, HttpServletRequest request, HttpSession session) {

		List<Refund> refunder = adminManager.getRefund();
		model.addAttribute("refunder", refunder);
		
		session.setAttribute("refunder", refunder);
	}

	
	
	/*
	 * @RequestMapping(value = "/employee.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public void doAddEmployee(ModelMap model,
	 * HttpServletRequest request, HttpSession session, @RequestParam(value =
	 * "addemploy", required = true) String addemploy) {
	 * 
	 * 
	 * Client client = adminManager.getClientById(addemploy);
	 * model.addAttribute("emp", emp);
	 * 
	 * }
	 */

	@RequestMapping(value = "/enquiries.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doenquiries(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<Enquiries> enq = userManager.getEnquiries();
		model.addAttribute("enq", enq);
	}

	
	@RequestMapping(value = "/editenquiries.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doEditenquiries(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "enqId", required = false) Long enqId) {
		try {
			if (enqId != null) {
				Enquiries enq = userManager.getEnquiriesById(enqId);
				model.addAttribute("enq", enq);
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@RequestMapping(value = "/editopportunity.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doEditopportunity(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "oppId", required = false) Long id) {
		try {
			model.addAttribute("opp", new Opportunities());
			List<Opportunities> oppor = userManager.getOpportunityById(id);
			if (!oppor.isEmpty()) {
				Opportunities opp = oppor.get(0);
				model.addAttribute("opp", opp);
			}
			List<Country> ctr = userManager.getAllCountries();
			model.addAttribute("ctr", ctr);
			List<Designation> dsg = userManager.getAllDesignations();
			model.addAttribute("dsg", dsg);
			List<Category> categories = userManager.getClientCategories();
			model.addAttribute("cats", categories);
			List<Source> sources = userManager.getSources();
			model.addAttribute("src", sources);

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@RequestMapping(value = "/deleteopportunity.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doDeleteopportunity(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "deleteOppId", required = false) Long id) {
		try {
			userManager.deleteOpp(id);
			return "redirect:opportunities.mm";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value = "/inactiveclients.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doInactiveclients(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	/*
	 * @RequestMapping(value = "/reciept.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public void doReceipt(ModelMap model,
	 * HttpServletRequest request, HttpSession session) {
	 * 
	 * }
	 */
	@RequestMapping(value = "/reciept.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doReceipt(ModelMap model, HttpServletRequest request, HttpSession session) {

		List<Receipt> reciept = adminManager.getReceipt();
		model.addAttribute("reciept", reciept);
	
		session.setAttribute("reciept", reciept);
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	@RequestMapping(value = "/miscfee.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doMiscfee(ModelMap model, HttpServletRequest request, HttpSession session) {

		List<Miscfee> miscfee = adminManager.getMiscfee();
		 model.addAttribute("miscfee", miscfee);
		session.setAttribute("miscfee", miscfee);
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	/*
	 * @RequestMapping(value = "/createreciept.mm", method = {
	 * RequestMethod.GET, RequestMethod.POST }) public String
	 * doCreateReceipt(ModelMap model, HttpServletRequest request, HttpSession
	 * session, @RequestParam(value = "email", required = false) String email,
	 * @RequestParam(value = "assProgram", required = false) String assProgram,
	 * @RequestParam(value = "date", required = false) String date,
	 * @RequestParam(value = "amount", required = false) String amount,
	 * 
	 * @RequestParam(value = "paymentMode", required = false) String
	 * paymentMode, @RequestParam(value = "transactionNo", required = false)
	 * String transactionNo, @RequestParam(value = "description", required =
	 * false) String description, @RequestParam(value = "promoCode", required =
	 * false) String promoCode, @ModelAttribute("reciept") Receipt reciept) {
	 * 
	 * Receipt reciept = new Receipt(); reciept.setAmount(amount);
	 * reciept.setAssProgram(assProgram); reciept.setDate(date);
	 * reciept.setDescription(description); reciept.setEmail(email);
	 * reciept.setPaymentMode(paymentMode); reciept.setPromoCode(promoCode);
	 * reciept.setTransactionNo(transactionNo);
	 * adminManager.saveReceipt(reciept); return "redirect:reciept.mm";
	 * 
	 * }
	 */

	@RequestMapping(value = "/createreciept.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddNewUserDetails(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("reciept") Receipt reciept) {

		adminManager.saveReceipt(reciept);
		return "redirect:reciept.mm";
	}

	@RequestMapping(value = "/createmisc.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddMiscfee(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("miscfee") Miscfee miscfee) {

		adminManager.saveMiscfee(miscfee);
		return "redirect:miscfee.mm";
	}

	@RequestMapping(value = "/accountinvoice.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doinvoice(ModelMap model, HttpServletRequest request, HttpSession session) {

		List<Invoice> invoicer = adminManager.getinvoice();
		model.addAttribute("invoicer", invoicer);
		session.setAttribute("invoicer", invoicer);
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	@RequestMapping(value = "/installment.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doInstallment(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
	}

	/*
	 * @RequestMapping(value = "/accountinvoice.mm", method = {
	 * RequestMethod.GET, RequestMethod.POST }) public void
	 * doAccountinvoice(ModelMap model, HttpServletRequest request, HttpSession
	 * session) {
	 * 
	 * }
	 */

	/*
	 * @RequestMapping(value = "/miscfee.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public void doMiscfee(ModelMap model,
	 * HttpServletRequest request, HttpSession session) {
	 * 
	 * }
	 */

	/*
	 * @RequestMapping(value = "/refunds.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public void doRefunds(ModelMap model,
	 * HttpServletRequest request, HttpSession session) {
	 * 
	 * }
	 */

	@RequestMapping(value = "/onlinepayments.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doOnlinepayments(ModelMap model, HttpServletRequest request, HttpSession session) {
		List<Employee> coordinators = adminManager.getCoordinatorList();
		model.addAttribute("coordinators", coordinators);
		
		List<OnlinePay> onlinepay = adminManager.getOnlinePay();
		model.addAttribute("onlinepay", onlinepay);
		
		session.setAttribute("onlinepay", onlinepay);
	}

	@RequestMapping(value = "/invoice.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddInvoicer(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("invoicer") Invoice invoicer) {

		adminManager.saveinvoice(invoicer);
		return "redirect:accountinvoice.mm";
	}

	/*
	 * @RequestMapping(value = "/invoice.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public void doInvoice(ModelMap model,
	 * HttpServletRequest request, HttpSession session) {
	 * 
	 * }
	 */

	@RequestMapping(value = "/refferals.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doRefferals(ModelMap model, HttpServletRequest request, HttpSession session) {
		try {
			List<Refferal> ref = userManager.getRefferal();
			model.addAttribute("ref", ref);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@RequestMapping(value = "/saveOpp.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doSaveOpp(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("opp") Opportunities opp) {
		try {
			userManager.save(opp);
			return "redirect:opportunities.mm";
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	@RequestMapping(value = "/updateOpp.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doUpdateOpp(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("opp") Opportunities opp) {
		try {
			userManager.update(opp);
			return "redirect:opportunities.mm";
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

	@RequestMapping(value = "/clientcategories.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doClientCategorie(ModelMap model, HttpServletRequest request, HttpSession session) {
		Category cat = new Category();
		List<Category> categories = userManager.getClientCategories();
		model.addAttribute("category", cat);
		model.addAttribute("cats", categories);
	}

	@RequestMapping(value = "/addclientcategories.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doAddClientCategorie(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("category") Category cat) {
		userManager.saveCategory(cat);
		return "redirect:clientcategories.mm";
	}

	@RequestMapping(value = "/source.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doSource(ModelMap model, HttpServletRequest request, HttpSession session) {
		Source src = new Source();
		model.addAttribute("source", src);
		List<Source> sources = userManager.getSources();
		model.addAttribute("src", sources);
	}

	@RequestMapping(value = "/addsource.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddSource(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("source") Source src) {

		src.setIsactive(true);
		userManager.saveSources(src);
		return "redirect:source.mm";
	}

	@RequestMapping(value = "/assignto.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doassign(ModelMap model, HttpServletRequest request, HttpSession session) {

		model.addAttribute("designation", new Designation());
		List<Designation> dsgn = userManager.getAllDesignations();
		model.addAttribute("dsgn", dsgn);
	}

	@RequestMapping(value = "/addassignto.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddassignto(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("designation") Designation dsg) {
		dsg.setIsactive(true);
		userManager.saveDesignation(dsg);
		return "redirect:assignto.mm";
	}

	/* View user page */
	@RequestMapping(value = "/userdetails.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doUserDetails(ModelMap model, HttpServletRequest request, HttpSession session) {

		List<Employee> emp = userManager.getAllEmployees();
		// model.addAttribute("emp", emp);
		session.setAttribute("emp", emp);
	}

	@RequestMapping(value = "/role.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doRole(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/accesscontrol.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAccessControl(ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	/* To Load the CreateUser Page */
	@RequestMapping(value = "/createnewuser.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doCreateNewUserDetails(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "empid", required = false) String id) {

		Employee emp = new Employee();

		if (StringUtils.isNotBlank(id)) {
			emp = userManager.getThisEmployee(id);
		}

		List<Designation> dsg = userManager.getAllDesignations();
		model.addAttribute("dsg", dsg);

		/*
		 * model.addAttribute("emp", emp); List<Country> ctr =
		 * userManager.getAllCountries(); model.addAttribute("ctr", ctr);
		 */

		List<Country> countryList = userManager.getCountryList();
		model.addAttribute("countries", countryList);

	}

	/* To Save the User */
	@RequestMapping(value = "/addnewuser.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doaddNewUserDetails(ModelMap model, HttpServletRequest request, HttpSession session,
			@ModelAttribute("emp") Employee emp) {
		emp.setIsactive(true);
		userManager.saveEmployee(emp);
		return "redirect:userdetails.mm";
	}
}
