package com.cryonez.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table
public class Designation implements Serializable{

	
	private static final long serialVersionUID = 1L;
	private Long id;
	private String des_id;
	private String level;
	private String description;
	private boolean isactive;
	private boolean isassignable;
	private String createddate;
	private String updateddate;
	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isIsactive() {
		return isactive;
	}
	public void setIsactive(boolean isactive) {
		this.isactive = isactive;
	}
	public String getCreateddate() {
		return createddate;
	}
	public void setCreateddate(String createddate) {
		this.createddate = createddate;
	}
	public String getUpdateddate() {
		return updateddate;
	}
	public void setUpdateddate(String updateddate) {
		this.updateddate = updateddate;
	}
	public String getDes_id() {
		return des_id;
	}
	public void setDes_id(String des_id) {
		this.des_id = des_id;
	}
	public boolean isIsassignable() {
		return isassignable;
	}
	public void setIsassignable(boolean isassignable) {
		this.isassignable = isassignable;
	}
	
}
