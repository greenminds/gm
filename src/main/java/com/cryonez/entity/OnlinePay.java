package com.cryonez.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table

public class OnlinePay implements Serializable  {

	private int id;
	private String date;
	private String amount;;
	private String email;
	private String clientId;
	private String refNo;
	private String payId;
	private String reqId;
	private String trainId;
	
	@Id
	@GeneratedValue
	
	
	public int getId() {
		return id;
	}
	@Override
	public String toString() {
		return "OnlinePay [id=" + id + ", date=" + date + ", amount=" + amount + ", email=" + email + ", clientId="
				+ clientId + ", refNo=" + refNo + ", payId=" + payId + ", reqId=" + reqId + ", trainId=" + trainId
				+ "]";
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	public String getPayId() {
		return payId;
	}
	public void setPayId(String payId) {
		this.payId = payId;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getTrainId() {
		return trainId;
	}
	public void setTrainId(String trainId) {
		this.trainId = trainId;
	}
	 
	
}
