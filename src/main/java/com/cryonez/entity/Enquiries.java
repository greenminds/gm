package com.cryonez.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "enquiries")
public class Enquiries implements Serializable{

	private Long enqId;
	private String name;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String date;
	private String message;
	private String profession;
	
	@Id
	@GeneratedValue
	public Long getEnqId() {
		return enqId;
	}
	public void setEnqId(Long enqId) {
		this.enqId = enqId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Enquiries [enqId=" + enqId + ", name=" + name + ", email=" + email + ", phoneNumber=" + phoneNumber
				+ ", date=" + date + ", message=" + message + "]";
	}
	
	
	
	
}
