package com.cryonez.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "refferal")
public class Refferal {

	private Long reffId;
	private String refferedName;
	private String refferedLname;
	private String refferedMname;
	private String emailAddress;
	private String phoneNumber;
	private String refferedOn;
	private String relation;
	private String refferedBy;

	private boolean status;

	@Id
	@GeneratedValue
	public Long getReffId() {
		return reffId;
	}

	public void setReffId(Long reffId) {
		this.reffId = reffId;
	}

	public String getRefferedName() {
		return refferedName;
	}

	public void setRefferedName(String refferedName) {
		this.refferedName = refferedName;
	}

	public String getRefferedOn() {
		return refferedOn;
	}

	public void setRefferedOn(String refferedOn) {
		this.refferedOn = refferedOn;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getRefferedBy() {
		return refferedBy;
	}

	public void setRefferedBy(String refferedBy) {
		this.refferedBy = refferedBy;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getRefferedLname() {
		return refferedLname;
	}

	public void setRefferedLname(String refferedLname) {
		this.refferedLname = refferedLname;
	}

	public String getRefferedMname() {
		return refferedMname;
	}

	public void setRefferedMname(String refferedMname) {
		this.refferedMname = refferedMname;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Refferal [reffId=" + reffId + ", refferedName=" + refferedName + ", refferedOn=" + refferedOn
				+ ", relation=" + relation + ", refferedBy=" + refferedBy + ", status=" + status + ",phoneNumber ="
				+ phoneNumber + ",emailAddress=" + emailAddress + ",refferedMname=" + refferedMname + ",refferedLname="
				+ refferedLname + "]";
	}

}
