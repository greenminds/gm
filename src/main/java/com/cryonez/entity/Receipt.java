package com.cryonez.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table
public class Receipt implements Serializable {

	private int id;
	private String amount;
	private String assProgram;;
	private String date;
	private String description;
	private String email;
	private String paymentMode;
	private String promoCode;
	private String transactionNo;
	private String coordinator; 
	private boolean isApproval;
	@Id
	@GeneratedValue
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getAmount() {
		return amount;
	}
	
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getAssProgram() {
		return assProgram;
	}
	public void setAssProgram(String assProgram) {
		this.assProgram = assProgram;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getTransactionNo() {
		return transactionNo;
	}
	public void setTransactionNo(String transactionNo) {
		this.transactionNo = transactionNo;
	}
	
	@Override
	public String toString() {
		return "Employee [id=" + id + ", amount=" + amount + ", assProgram=" + assProgram
				+ ", date=" + date + ", description="
				+ description + ", email=" + email + ", paymentMode="
				+ paymentMode + ", promoCode=" + promoCode
				+ ", transactionNo=" + transactionNo + " , coordinator=" + coordinator + "]";
	}
	public String getCoordinator() {
		return coordinator;
	}
	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	public boolean isApproval() {
		return isApproval;
	}

	public void setApproval(boolean isApproval) {
		this.isApproval = isApproval;
	}
	
		}
