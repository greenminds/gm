package com.cryonez.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings({ "serial", "deprecation" })
@Entity
@Table(name = "user")
public class User implements Serializable, UserDetails {

	private int id;
	private long userId;
	private String userName;
	private String email;
	private String accountType;
	private Boolean isDocumentuploaded;
	private Boolean iAgreementuploaded;
	private Boolean isAgreementApprovaled;
	private Boolean isDocumentApprovaled;
	private String status;
	private String fName;
	private String mName;
	private String lName;
	private String mobile;
	private String profession;
	private String password;
	private String confirmPassword;
	private String dob;
	private String passportNo;
	private String passportExpiry;
	private String address;
	private String country;
	private String state;
	private String city;
	private String pinCode;
	private byte[] profilePic;
	private String highestEdu;
	private String nameDegree;
	private String nameUniversity;
	private String english;
	private String french;
	private String companyName;
	private String designation;
	private String dateFrom;
	private String dateTo;
	private String role;
	private String maritalStatus;
	private String bloodRelation;
	private String jobOffer;
	private String studyExp;
	private String spouseName;
	private String spouseEdu;
	private String spouseDob;
	private String spouseEnglish;
	private String spouseFrench;
	private UUID token;
	private Boolean active;
	private String coordinator; 

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccountType() {
		return accountType;
	}
	

	
	
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getPassportNo() {
		return passportNo;
	}

	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}

	public String getPassportExpiry() {
		return passportExpiry;
	}

	public void setPassportExpiry(String passportExpiry) {
		this.passportExpiry = passportExpiry;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public byte[] getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(byte[] profilePic) {
		this.profilePic = profilePic;
	}

	public String getHighestEdu() {
		return highestEdu;
	}

	public void setHighestEdu(String highestEdu) {
		this.highestEdu = highestEdu;
	}

	public String getNameDegree() {
		return nameDegree;
	}

	public void setNameDegree(String nameDegree) {
		this.nameDegree = nameDegree;
	}

	public String getNameUniversity() {
		return nameUniversity;
	}

	public void setNameUniversity(String nameUniversity) {
		this.nameUniversity = nameUniversity;
	}

	public String getEnglish() {
		return english;
	}

	public void setEnglish(String english) {
		this.english = english;
	}

	public String getFrench() {
		return french;
	}

	public void setFrench(String french) {
		this.french = french;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getBloodRelation() {
		return bloodRelation;
	}

	public void setBloodRelation(String bloodRelation) {
		this.bloodRelation = bloodRelation;
	}

	public String getJobOffer() {
		return jobOffer;
	}

	public void setJobOffer(String jobOffer) {
		this.jobOffer = jobOffer;
	}

	public String getStudyExp() {
		return studyExp;
	}

	public void setStudyExp(String studyExp) {
		this.studyExp = studyExp;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getSpouseEdu() {
		return spouseEdu;
	}

	public void setSpouseEdu(String spouseEdu) {
		this.spouseEdu = spouseEdu;
	}

	public String getSpouseDob() {
		return spouseDob;
	}

	public void setSpouseDob(String spouseDob) {
		this.spouseDob = spouseDob;
	}

	public String getSpouseEnglish() {
		return spouseEnglish;
	}

	public void setSpouseEnglish(String spouseEnglish) {
		this.spouseEnglish = spouseEnglish;
	}

	public String getSpouseFrench() {
		return spouseFrench;
	}

	public void setSpouseFrench(String spouseFrench) {
		this.spouseFrench = spouseFrench;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {

		GrantedAuthority auth = new GrantedAuthorityImpl("User");
		Collection<GrantedAuthority> authorities = new LinkedList<GrantedAuthority>();
		authorities.add(auth);
		return authorities;
	}

	@Override
	@Transient
	public String getUsername() {
		return userName;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return active;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return active;
	}

	@Override
	@Transient
	public boolean isEnabled() {
		return active;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return active;
	}

/*	@Override
	public String toString() {

		return "User [id=" + id + ", userId=" + userId + ", userName=" + userName + ", email=" + email
				+ ", accountType=" + accountType + ",isDocumentuploaded=" +isDocumentuploaded+",iAgreementuploaded=" +iAgreementuploaded+",isAgreementApprovaled=" +isAgreementApprovaled+",isDocumentApprovaled=" + isDocumentApprovaled +", status=" + status + ", fName=" + fName + ", mName=" + mName
				+ ", lName=" + lName + ", mobile=" + mobile + ", profession=" + profession + ", password=" + password
				+ ", confirmPassword=" + confirmPassword + ", dob=" + dob + ", passportNo=" + passportNo
				+ ", passportExpiry=" + passportExpiry + ", address=" + address + ",country=" + country + ",state="
				+ state + ",city=" + city + "," + "pinCode=" + pinCode + ",profilePic=" + profilePic + ",highestEdu="
				+ highestEdu + ",nameDegree=" + nameDegree + ",nameUniversity=" + nameUniversity + ",english="
				+ english + ",french=" + french + ",companyName=" + companyName + ",designation=" + designation + ","
				+ "dateFrom=" + dateFrom + ",dateTo=" + dateTo + ",role=" + role + ",maritalStatus=" + maritalStatus
				+ ",bloodRelation=" + bloodRelation + ",jobOffer=" + jobOffer + ",studyExp=" + studyExp + ",coordinator=" + coordinator + "]";

	}
*/
	

	public Boolean getIsAgreementApprovaled() {
		return isAgreementApprovaled;
	}

	public void setIsAgreementApprovaled(Boolean isAgreementApprovaled) {
		this.isAgreementApprovaled = isAgreementApprovaled;
	}

	public Boolean getIsDocumentApprovaled() {
		return isDocumentApprovaled;
	}

	public void setIsDocumentApprovaled(Boolean isDocumentApprovaled) {
		this.isDocumentApprovaled = isDocumentApprovaled;
	}

	

	public Boolean getIsDocumentuploaded() {
		return isDocumentuploaded;
	}

	public void setIsDocumentuploaded(Boolean isDocumentuploaded) {
		this.isDocumentuploaded = isDocumentuploaded;
	}

	public Boolean getiAgreementuploaded() {
		return iAgreementuploaded;
	}

	public void setiAgreementuploaded(Boolean iAgreementuploaded) {
		this.iAgreementuploaded = iAgreementuploaded;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	
}
