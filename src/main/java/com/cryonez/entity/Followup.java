package com.cryonez.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Followup {

	private int id;
	private int clientId;
	private String notes1;
	private String date1;
	private String type;
	private String notes2;
	private String date2;
	private String notes3;
	private String date3;
	private boolean callYou;
	private boolean profileReceived;
	private String coordinatorId;
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the clientId
	 */
	public int getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the notes1
	 */
	public String getNotes1() {
		return notes1;
	}
	/**
	 * @param notes1 the notes1 to set
	 */
	public void setNotes1(String notes1) {
		this.notes1 = notes1;
	}
	/**
	 * @return the date1
	 */
	public String getDate1() {
		return date1;
	}
	/**
	 * @param date1 the date1 to set
	 */
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	/**
	 * @return the notes2
	 */
	public String getNotes2() {
		return notes2;
	}
	/**
	 * @param notes2 the notes2 to set
	 */
	public void setNotes2(String notes2) {
		this.notes2 = notes2;
	}
	/**
	 * @return the date2
	 */
	public String getDate2() {
		return date2;
	}
	/**
	 * @param date2 the date2 to set
	 */
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	/**
	 * @return the notes3
	 */
	public String getNotes3() {
		return notes3;
	}
	/**
	 * @param notes3 the notes3 to set
	 */
	public void setNotes3(String notes3) {
		this.notes3 = notes3;
	}
	/**
	 * @return the date3
	 */
	public String getDate3() {
		return date3;
	}
	/**
	 * @param date3 the date3 to set
	 */
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	/**
	 * @return the callYou
	 */
	public boolean isCallYou() {
		return callYou;
	}
	/**
	 * @param callYou the callYou to set
	 */
	public void setCallYou(boolean callYou) {
		this.callYou = callYou;
	}
	/**
	 * @return the profileReceived
	 */
	public boolean isProfileReceived() {
		return profileReceived;
	}
	/**
	 * @param profileReceived the profileReceived to set
	 */
	public void setProfileReceived(boolean profileReceived) {
		this.profileReceived = profileReceived;
	}
	/**
	 * @return the coordinatorId
	 */
	public String getCoordinatorId() {
		return coordinatorId;
	}
	/**
	 * @param coordinatorId the coordinatorId to set
	 */
	public void setCoordinatorId(String coordinatorId) {
		this.coordinatorId = coordinatorId;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	

}
