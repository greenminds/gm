package com.cryonez.entity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table
public class Miscfee implements Serializable
{
	private int id;
	private String client;
	private String date;
	private String feeName;
	private String amonut;
	private String payMode;
	private String transation;
	private String description;
	private boolean isApproval;
	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public String getAmonut() {
		return amonut;
	}
	public void setAmonut(String amonut) {
		this.amonut = amonut;
	}
	public String getPayMode() {
		return payMode;
	}
	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}
	public String getTransation() {
		return transation;
	}
	public void setTransation(String transation) {
		this.transation = transation;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ",client=" + client + ",date=" + date + ",feeName=" + feeName +",amonut="+ amonut +  ", payMode=" + payMode + 
				",transation="+ transation +" description="+ description +"]";
	}
	public boolean isApproval() {
		return isApproval;
	}
	public void setApproval(boolean isApproval) {
		this.isApproval = isApproval;
	}
}
