package com.cryonez.Service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import com.cryonez.dao.AdminDAO;
import com.cryonez.dto.ClientDTO;
import com.cryonez.dto.EmployeeDTO;
import com.cryonez.dto.userDTO;
import com.cryonez.entity.Client;
import com.cryonez.entity.Country;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Followup;
import com.cryonez.entity.Miscfee;
import com.cryonez.entity.OnlinePay;
import com.cryonez.entity.Receipt;
import com.cryonez.entity.Refund;
import com.cryonez.entity.User;
import com.cryonez.entity.Invoice;
//import com.cryonez.entity.invoice;

/**
 * @author ramalingam.a
 * @param <UserDTO>
 * 
 */
public class AdminManagerImpl<UserDTO> implements AdminManager {

	@Autowired
	AdminDAO adminDao;

	@Override
	public List<Client> getAllClients() {
	//	List<ClientDTO> clientDtoList = new ArrayList<ClientDTO>();
		/*List<Client> clientList = adminDao.getAllClients();
		if(clientList != null && clientList.size() > 0)
			for(Client clt: clientList){
				ClientDTO cltDto = new ClientDTO();
				cltDto.setId(clt.getId());
				cltDto.setEmail(clt.getEmail());
				cltDto.setFirstName(clt.getFirstName());
				cltDto.setMobileNumber(clt.getMobileNumber());
				cltDto.setDateOfJoin(clt.getDateOfJoin());
				clientDtoList.add(cltDto);
			}
*/		return adminDao.getAllClients();

	}

	public List<EmployeeDTO> getAllEmployee() {
		List<EmployeeDTO> clientDtoList = new ArrayList<EmployeeDTO>();
		List<Employee> clientList = adminDao.getAllEmployee();
		if(clientList != null && clientList.size() > 0)
			for(Employee clt: clientList){
				EmployeeDTO cltDto = new EmployeeDTO();
				cltDto.setId(clt.getId());
				cltDto.setEmp_email(clt.getEmp_email());
				cltDto.setEmp_firstname(clt.getEmp_firstname());
				cltDto.setEmp_mobile(clt.getEmp_mobile());
		
				clientDtoList.add(cltDto);
			}
		return clientDtoList;

	}
	
	
	/*public List<userDTO> getagreementApprovalList() {
	List<userDTO>agreementApprovalDtoList = new ArrayList<userDTO>();
	List<User> agreementApprovalList = adminDao.getagreementApprovalByList();
	if(agreementApprovalList != null && agreementApprovalList.size() > 0)
		for(User ult: agreementApprovalList){
			userDTO userDto = new userDTO();
			userDto.setUserId(ult.getUserId());
			userDto.setfName(ult.getfName());
			userDto.setlName(ult.getlName());
			userDto.setEmail(ult.getEmail());
			userDto.setMobile(ult.getMobile());
	
			agreementApprovalDtoList.add(userDto);
		}
	return agreementApprovalDtoList;

}
	
	public List<userDTO> getagreementuploadList() {
	List<userDTO>agreementuploadDtoList = new ArrayList<userDTO>();
	List<User> agreementuploadList = adminDao.getagreementuploadByList();
	if(agreementuploadList != null && agreementuploadList.size() > 0)
		for(User ult: agreementuploadList){
			userDTO userDto = new userDTO();
			userDto.setUserId(ult.getUserId());
			userDto.setfName(ult.getfName());
			userDto.setlName(ult.getlName());
			userDto.setEmail(ult.getEmail());
			userDto.setMobile(ult.getMobile());
	
			agreementuploadDtoList.add(userDto);
		}
	return agreementuploadDtoList;

}*/
	
	
	
	public List<User> getfileuploadlist() {
	//	List<userDTO> fileuploadDtoList = new ArrayList<userDTO>();
	//	List<User> fileuploadList = adminDao.getfileuploadBylist();
		/*if(fileuploadList != null && fileuploadList.size() > 0)
			for(User ult: fileuploadList){
				userDTO userDto = new userDTO();
				userDto.setUserId(ult.getUserId());
				userDto.setfName(ult.getfName());
				userDto.setlName(ult.getlName());
				userDto.setEmail(ult.getEmail());
				userDto.setMobile(ult.getMobile());
		
				fileuploadDtoList.add(userDto);
			}
		*/return adminDao.getfileuploadBylist();

	}
	
	@Override
	public List<Client> getClientByStatus(String val) {
		List<Client> clientList = adminDao.getClientsByStatus(val);
		return clientList;
		
		// TODO Auto-generated method stub
		}
	@Override
	public List<User> getNotUpAgreeList(String val) {
		List<User> DocuApprovalList = adminDao.getNotUpAgreeByList(val);
		return DocuApprovalList;
		
		// TODO Auto-generated method stub
		}
	@Override
	public List<User> getfilteragreeaproveList(String val) {
		List<User> agreementApprovalList = adminDao.getfilteragreeaproveByList(val);
		return agreementApprovalList;
		
		// TODO Auto-generated method stub
		}
	@Override
	public List<User> getfilteragreeuploadList(String val) {
		List<User> agreementuploadList = adminDao.getfilteragreeuploadByList(val);
		return agreementuploadList;
		
		// TODO Auto-generated method stub
		}
	
	@Override
	public List<Client> getClientByStatusByField(String val, String field, String type) {
		List<Client> clientList = adminDao.getClientsByStatusByField(val, field, type);
		return clientList;
		
	}
	
	@Override
	public List<Client> SearchClientCoByField(String val, String field, String type) {
		List<Client> clientList = adminDao. SearchClientCoByFieldInfo(val, field, type);
		return clientList;
		
	}
	

	@Override
	public List<User> SearchUserAUCoByField(String val, String field, String type) {
		List<User> agreementuploadList = adminDao. SearchUserAUCoByFieldInfo(val, field, type);
		return agreementuploadList;
		}
	
	@Override
	public List<User> SearchUserDACoByField(String val, String field, String type) {
		List<User> agreementuploadList = adminDao. SearchUserDACoByFieldInfo(val, field, type);
		return agreementuploadList;
		}
	
	
	@Override
	public List<User> SearchUserAACoByField(String val, String field, String type) {
		List<User> agreementApprovalList = adminDao. SearchUserAACoByFieldInfo(val, field, type);
		return agreementApprovalList;
		}
	@Override
	public List<User> SearchUserFUCoByField(String val, String field ) {
		List<User> fileuploadList = adminDao. SearchUserFUCoByFieldInfo(val, field);
		return fileuploadList;
		
	}
	
	public List<Employee>SearchThisUserStatus(String val, String field) {
		List<Employee> emp = adminDao.SearchThisUserStatusInfo(val, field);
		return emp;
		
	}
	public List<User>SearchDocuploadlistStatus(String val, String field) {
		List<User> Doc = adminDao.SearchDocuploadlistStatusinfo(val, field);
		return Doc;
		
	}
	public List<Receipt>SearchReceiptByList(String val, String field) {
		List<Receipt> reciept = adminDao.SearchReceiptByListInfo(val, field);
		return reciept;
		
	}
	public List<Invoice>SearchinvoicetByList(String val, String field) {
		List<Invoice> invoicer = adminDao.SearchinvoicetByListInfo(val, field);
		return invoicer;
		
	}
	
	@Override
	public List<User> AgreeuploadByUser(String val, String field, String type) {
		List<User> UserList = adminDao.AgreeuploadByUserByField(val, field, type);
		return UserList;
		
	}
	
	@Override
	public List<User> SearchDocApproveByList(String val, String field, String type) {
		List<User> UserList = adminDao.SearchDocApproveByListInfo(val, field, type);
		return UserList;
		
	}

	@Override
	public List<User> AgreeApproveByList(String val, String field, String type) {
		List<User> UserList = adminDao.AgreeApproveByListInfo(val, field, type);
		return UserList;
		
	}

	
	@Override
	public Client getClientById(String clientId) {
		Client client = adminDao.getClientsById(clientId);
		return client;
	}


	@Override
	public List<Employee> geEmpListByDesignation(String designation) {
		List<Employee> empList = adminDao.getEmpByDesignation(designation);
		return empList;
	}
	
	@Override
	public List<Employee> getCoordinatorList() {
		List<Employee> coordinators = adminDao.getCoordinatorByList();
		return coordinators;
	}
	
	@Override
	public List<User> getdocumentApprovalList() {
		List<User> empList = adminDao.getdocumentApprovalByList();
		return empList;
	}

	@Override
	public List<User> getagreementApprovalList() {
		List<User> empList = adminDao.getagreementApprovalByList();
		return empList;
	}

	@Override
	public  List<User> getagreementuploadlList() {
		List<User> empList = adminDao.getagreementuploadByList();
		return empList;
	}

	@Override
	public void saveReceipt(Receipt receipt1) {
		adminDao.saveReceiptBy(receipt1);
	}

	public void saveMiscfee(Miscfee miscfee1) {
		adminDao.saveMiscfeeBy(miscfee1);
	}
	@Override
	public List<Receipt> getReceipt() {
		
		return adminDao.getReceiptBy();
	}
	
	@Override
	public List<Refund> getRefunder() {
		
		return adminDao.getRefunderBy();
	}
	
	@Override
	public List<Client> getClientsBy() {
		
		return adminDao.getClientsByList();
	}
	
	@Override
	public List<Miscfee> getMiscfee() {
		
		return adminDao.getMiscfeeBy();
	}
	@Override
	public Client getClientByEmail(String email) {
		
		return adminDao.getClientByEmail(email);
	}

	
	@Override
	public List<Invoice> getinvoice() {
		
		return adminDao.getinvoiceBy();

	}

	@Override
	public void saveinvoice(Invoice invoicer1) {
		adminDao.saveInvoiceBy(invoicer1);
	}

	@Override
	public List<Refund> getRefund() {
		
		return adminDao.getRefundBy();
	}

	@Override
	public void saveRefund(Refund refunder1) {
		adminDao.saveRefundBy(refunder1);
	}
	
	@Override
	public void saveClients(Client clientList1) {
		adminDao.saveClientsBy(clientList1);
	}
	public List<Refund> RefundByList(String val, String field) {
		List<Refund> refunder = adminDao.RefundByListInfo(val, field);
		return refunder;
		
	}
	
	public List<Miscfee> MiscfeeByList(String val, String field) {
		List<Miscfee> miscfee = adminDao.MiscfeeByListInfo(val, field);		
	return miscfee;
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List deleteClientById(HttpSession session, String clientId) {
		Client client = new Client();
		client.setId(Integer.parseInt(clientId));
		List<Client> clientList = null;
		clientList =  (List<Client>) session.getAttribute("clientList");
		boolean isDelete = false;
		List<Client> newList = new ArrayList<Client>();
		if(clientList != null && clientList.size() > 0){
			for(Client clt: clientList){
				if(clt.getId() == Integer.parseInt(clientId)){
					adminDao.deleteClientById(clt);
					isDelete = true;
				}
				else
					newList.add(clt);
			}
		}
		if(isDelete){
			session.setAttribute("clientList", newList);
			return newList;
		}
		return clientList;
}

	@Override
	public Invoice getInvoiceById(String id) {
return adminDao.getInvoiceById(id);
	}

	public List deleteFileUpload(HttpSession session, String userId) {
		
		User user = new User();
		user.setId(Integer.parseInt(userId));
		List<User> userList = null;
		/*userList =  (List<User>) session.getAttribute("clientList");
		boolean isDelete = false;
		List<Client> newList = new ArrayList<Client>();
		if(userList != null && userList.size() > 0){
			for(User clt: userList){
				if(clt.getId() == Integer.parseInt(userId)){
					isDelete = true;
				}
				else
					newList.add(clt);
			}
		}
		if(isDelete){
			session.setAttribute("clientList", newList);
			return newList;
		}*/
		return userList;
	}

	@Override
	public List deleteFileUpload(String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Refund getRefundById(String id) {
		return adminDao.getRefundByIdInfo(id);
	}
	
	@Override
	public Receipt approvalAccRecById(String id) {
		return adminDao.approvalAccRecByIdInfo(id);
	}
	
	@Override
	public Invoice approvalInvoById(String id) {
		return adminDao.approvalInvoByIdInfo(id);
	}
	
	@Override
	public Miscfee approvalMiscById(String id) {
		return adminDao.approvalMiscByIdInfo(id);
	}
	
	@Override
	public Client deleteClientsById(String id) {
		return adminDao.deleteClientsByIdInfo(id);
	}

	@Override
public List<OnlinePay> getOnlinePay() {
		
		return adminDao.getOnlineByList();
	
	} 

	@Override
	public List onlinePayByList(String val, String field) {
		List<OnlinePay> online = adminDao.onlinePayByListInfo(val, field);
		return online;
		
	}

	@Override
	public Followup getFollowupByClientId(String clientId,String type) {
		
		return adminDao.getFollowupByClient(clientId, type);
	}

	@Override
	public void saveFollowup(Followup followup) {
		adminDao.saveFolloup(followup);
	}

	
}
