package com.cryonez.Service;

import java.util.List;

import javax.servlet.http.HttpSession;

import com.cryonez.dto.ClientDTO;
import com.cryonez.dto.EmployeeDTO;
import com.cryonez.dto.userDTO;
import com.cryonez.entity.Client;
import com.cryonez.entity.Country;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Followup;
import com.cryonez.entity.Miscfee;
import com.cryonez.entity.OnlinePay;
import com.cryonez.entity.Receipt;
import com.cryonez.entity.Refund;
import com.cryonez.entity.User;
import com.cryonez.entity.Invoice;

/**
 * @author s6
 * @param <UserDTO>
 *
 */

public interface AdminManager<UserDTO> {

	public List<ClientDTO> getAllClients();
	
	// public List<ClientDTO> getClientByStatus(String val);
	public List<Client> getClientByStatus(String val);
	
	public List<User> getNotUpAgreeList(String val);

	public List<User> getfilteragreeaproveList(String val);
	
	public List<User> getfilteragreeuploadList(String val);
	
	public List<Client> getClientByStatusByField(String val, String field, String type);

	public Client getClientById(String clientId);
	
	public Client deleteClientsById(String id);
	public List<EmployeeDTO> getAllEmployee();

	public List<Employee> SearchThisUserStatus(String val, String field);

	public List<Employee> geEmpListByDesignation(String string);

	public List<Miscfee>MiscfeeByList(String val, String field);

	public List<User> getfileuploadlist();

	public List<User> getdocumentApprovalList(); 
	public List<User> getagreementApprovalList(); 
	public List<User> getagreementuploadlList(); 

	public List<User> SearchDocuploadlistStatus(String val, String field);

	public List<User> AgreeuploadByUser(String val, String field, String type);

	public void saveReceipt(Receipt receipt1);
	
	public void saveMiscfee(Miscfee miscfee1);

	public List<Receipt> getReceipt();

	public List<Receipt> SearchReceiptByList(String val, String field);

	public List<User> SearchDocApproveByList(String val, String field, String type);

	public List<User> AgreeApproveByList(String val, String field, String type);

	public Client getClientByEmail(String email);

	public List<Invoice> getinvoice();

	public void saveinvoice(Invoice invoicer1);

	public List<Invoice> SearchinvoicetByList(String val, String field);

	public List<Miscfee> getMiscfee();

	
	
	
 
	public void saveRefund(Refund refunder1);
	
	
	public List<Refund> getRefund();
	
	
	public List<Refund>RefundByList(String val, String field);

	public List<Employee> getCoordinatorList();

	public List<Client> deleteClientById(HttpSession session, String clientId);

	public Invoice getInvoiceById(String id);

	public List<User> deleteFileUpload(String userId);

	public Refund getRefundById(String id);

	public void saveClients(Client clientList1);

	public Receipt approvalAccRecById(String id);

	public Invoice approvalInvoById(String id);

	public Miscfee approvalMiscById(String id);

	public List<OnlinePay> getOnlinePay();

	public List<OnlinePay> onlinePayByList(String val, String field);

	public List<Refund> getRefunder();

	public List<Client> getClientsBy();

	public List<Client> SearchClientCoByField(String val, String field, String type);

	public List<User> SearchUserFUCoByField(String val, String field );

	public List<User> SearchUserAUCoByField(String val, String field, String type);

	public List<User> SearchUserDACoByField(String val, String field, String type);

	public List<User> SearchUserAACoByField(String val, String field, String type);

	public Followup getFollowupByClientId(String clientId, String type);

	public void saveFollowup(Followup followup);  
	 
	
	
 
	
	
	
	
	
	
	
	


}
