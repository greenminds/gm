package com.cryonez.Service;

import java.util.List;
import java.util.UUID;

import com.cryonez.entity.Category;
import com.cryonez.entity.Client;
import com.cryonez.entity.Country;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.MyDocuments;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;
import com.cryonez.entity.UserExperience;


/**
 * @author s6
 *
 */
/**
 * @author s6
 *
 */
public interface UserManager {

//	User getUser(String username);


	   
	public void saveCategory(Category cat);

	public List<Category> getClientCategories();

	public Category getThisCategory(String id);

	public List<Category> getCategoryLikeThis(String val);

	public void saveSources(Source src);

	public List<Source> getSources();

	public Source getThisSource(String id);

	public List<Source> getSorcesBySearchValues(String val);

	public void saveDesignation(Designation dsg);

	public List<Designation> getAllDesignations();

	public Designation getThisDesignation(String id);

	public List<Designation> getDesignationLikeThis(String val);

	public void saveEmployee(Employee emp);

	public List<Employee> getAllEmployees();

	public Employee getThisEmployee(String id);

	public List<Employee> getEmployeesLikeThis(String val);

	public List<Country> getAllCountries();

	public User getUser();

	public void save(Opportunities opp);
//
	public List<Opportunities> getOpportunity();
//
	public List<Opportunities> getOpportunityById(Long id);
//
	public void deleteOpp(Long id);
//
	public List<Opportunities> getSearchVal(String val);
//
	public List<Opportunities> getStatusVal(boolean val);
//
	public List<Enquiries> getEnquiries();
//
	public void update(Opportunities opp);
//
	public Enquiries getEnquiriesById(Long enqId);
//
	public List<Refferal> getRefferal();
public List<Enquiries> getSearchEnquiry(String val);

	public boolean checkEmail(String email);
	public User getUserByToken(UUID token);
	public User getActiveUser(String username);
	public User getUser(String username);
	public void insertUser(User audi);
	public void mergeUser(User user);

	public String getPassword(String value);
	public User getUserDetails(String name);
	public void updateUser(User user);

	public void insertEnquiry(Enquiries enq);
	public void insertrefferal(Refferal ref);
	public Refferal getReferalDetails(String fname);
	public void insertOpportunities(Opportunities opp);

	public void insertDocuments(MyDocuments doc);

	public boolean checkPassport(String pal);

	public UserExperience getUserExp(Long userExp);

	public void updateUserExp(UserExperience userExperience);

	public void saveUserExperience(UserExperience userExp);

	public UserExperience checkCompanyName(Long userId, String companyList);

	public List<UserExperience> getExpDetails(Long userId);

	public void saveRadio(User user);

	public List<MyDocuments> getDocDetails(Long userId);

	public User getImage(Long id);

	public List<Country> getCountryList();
	


	public User getEmailAddress(String email);

	public User verifyEmail(String email);

	public void saveClient(Client client);

	public List<MyDocuments> getClientDocNames(String docName);

	public void deleteDoc(Long id);




	

}
