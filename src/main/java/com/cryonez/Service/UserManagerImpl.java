package com.cryonez.Service;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.cryonez.dao.UserDAO;
import com.cryonez.entity.Category;
import com.cryonez.entity.Client;
import com.cryonez.entity.Country;
import com.cryonez.entity.Designation;
import com.cryonez.entity.Employee;
import com.cryonez.entity.Enquiries;
import com.cryonez.entity.MyDocuments;
import com.cryonez.entity.Opportunities;
import com.cryonez.entity.Refferal;
import com.cryonez.entity.Source;
import com.cryonez.entity.User;
import com.cryonez.entity.UserExperience;

/**
 * @author s6
 *
 */
/**
 * @author s6
 *
 */
public class UserManagerImpl implements UserManager {
	
	
	
	private Logger LOG = Logger.getLogger(UserManager.class);
	Session session = null;
	private static HibernateTemplate ht;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.ht = new HibernateTemplate(sessionFactory);
		session = sessionFactory.openSession();
	}


	@Autowired
	UserDAO userDao;

	@Override
	public User getUser() {
		// TODO Auto-generated method stub
		return userDao.getUser();
	}



	@Override
	public void saveCategory(Category cat) {
		userDao.saveCategory(cat);
		
	}

	@Override
	public List<Category> getClientCategories() {
		
		return userDao.getClientCategories();
	}

	@Override
	public Category getThisCategory(String id) {
		
		return userDao.getThisCategory(id);
	}

	@Override
	public List<Category> getCategoryLikeThis(String val) {
		List<Category> catgs=userDao.getCategoryLikeThis(val);
		return catgs;
	}

	@Override
	public void saveSources(Source src) {
		
		userDao.saveSources(src);
		}

	@Override
	public List<Source> getSources() {
		
		return userDao.getSources();
	}

	@Override
	public Source getThisSource(String id) {
		
		return userDao.getThisSource(id);
	}

	@Override
	public List<Source> getSorcesBySearchValues(String val) {
		System.out.println();
		return userDao.getSorcesBySearchValues(val);
	}

	@Override
	public void saveDesignation(Designation dsg) {
		userDao.saveDesignation(dsg);
		
	}

	@Override
	public List<Designation> getAllDesignations() {
		
		return userDao.getAllDesignations();
	}

	@Override
	public Designation getThisDesignation(String id) {
		
		return userDao.getThisDesignation(id);
	}

	@Override
	public List<Designation> getDesignationLikeThis(String val) {
		List<Designation> dsg=userDao.getDesignationLikeThis(val);
		return dsg;
	}

	@Override
	public void saveEmployee(Employee emp) {
	userDao.saveEmployee(emp);
		
	}

	@Override
	public List<Employee> getAllEmployees() {
		
		return userDao.getAllEmployees();
	}

	@Override
	public Employee getThisEmployee(String id) {
		
		return userDao.getThisEmployee(id);
	}

	@Override
	public List<Employee> getEmployeesLikeThis(String val) {
		
		return userDao.getEmployeesLikeThis(val);
	}

	@Override
	public List<Country> getAllCountries() {
		
		return userDao.getAllCountries();
	}

	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkPassport(String pal) {
		List<String> list = ht.find("FROM User u where u.passportNo =?", pal);

		if (list.size() > 0) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("rawtypes")
	public boolean checkEmail(String email) {
		List list = ht.find("FROM User u where u.email =? ", email);
		if (list.size() > 0) {
			return true;
		}
		return false;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public UserExperience checkCompanyName(Long userId, String companyList) {
		UserExperience userExp = null;
		List<UserExperience> uelist = ht.find("FROM UserExperience ue where ue.userId =? and ue.companyName =?",userId,companyList);
		if(uelist.size()>0){
			userExp = uelist.get(0);
		}
		return userExp;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<UserExperience> getExpDetails(Long userId) {
		
		List<UserExperience> userExp=ht.find("FROM UserExperience ue where ue.userId =?",userId);
		return userExp;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<MyDocuments> getDocDetails(Long userId) {
		
		List<MyDocuments> userDoc = ht.find("FROM MyDocuments my where my.userid=?",userId) ;
		
		return userDoc;
	}
	
	@SuppressWarnings("unchecked")
	public User getUserByToken(UUID token) {
		User user = null;

		List<User> users = ht.find("FROM User u WHERE u.token = ?", token);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;

	}

	@SuppressWarnings("unchecked")
	public User getActiveUser(String username) {
		User user = null;
		Boolean str = true;
		List<User> users = ht.find("FROM User u WHERE u.active = ? and u.userName = ?", str, username);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;
	}

	@SuppressWarnings("unchecked")
	public User getUser(String username) {
		User user = null;
		List<User> users = ht.find("FROM User u WHERE u.userName = ?", username);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;
	}

	public void insertUser(User audi) {
		ht.save(audi);
	}

	public void mergeUser(User user) {
		ht.update(user);
	}

	public void updateUser(User user) {
		ht.saveOrUpdate(user);
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getPassword(String value) {
		List<User> users = ht.find("FROM User u WHERE u.userName =?", value);
		String pass=null;
		if (users != null && !users.isEmpty()) {
			pass=users.get(0).getPassword();
		}

		return pass;
	}

	@Override
	public User getUserDetails(String name) {
		User user = null;
			user = (User) ht.find("FROM User u where u.userName= ?", name).get(0);

		return user;

	}

	@Override
	public Refferal getReferalDetails(String fname) {
		Refferal refferal = null;
		try {
			refferal = (Refferal) ht.find("FROM Refferal r where r.refferedBy =?", fname).get(0);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return refferal;
	}

	
	@Override
	public void insertEnquiry(Enquiries enq) {
		ht.save(enq);

	}

	@Override
	public void insertrefferal(Refferal ref) {
		ht.save(ref);

	}

	
	 @Override
	 public void insertOpportunities(Opportunities opp) {
	 ht.saveOrUpdate(opp);
	
	 }

	@Override
	public void insertDocuments(MyDocuments doc) {
		ht.save(doc);

	}

	@Override
	public UserExperience getUserExp(Long userExp) {
		UserExperience userExpe=null;
			try{
				userExpe=(UserExperience) ht.find("SELECT UserExperience ue WHERE ue.userId=?",userExp);
			}catch(Exception e){
				
			}
		return userExpe;
	}
	
	@Override
	public User getImage(Long id) {
		List<User> products = ht.find("FROM User p WHERE p.userId=?", id);
		User product = products.get(0);
		return product;
	}
	



	@Override
	public void updateUserExp(UserExperience userExperience) {
		ht.saveOrUpdate(userExperience);
		
	}

	@Override
	public void saveUserExperience(UserExperience userExp) {
		ht.saveOrUpdate(userExp);
		
	}

	@Override
	public void saveRadio(User user) {
		ht.saveOrUpdate(user);
		
	}





	@Override
	public void save(Opportunities opp) {
		userDao.save(opp);
	}

	@Override
	public List<Opportunities> getOpportunity() {
		List<Opportunities> opp=userDao.getOpportunity();
		return opp;
		
	}

	@Override
	public List<Opportunities> getOpportunityById(Long id) {
		List<Opportunities> opp=userDao.getOpportunityById(id);
		return opp;
	}

	@Override
	public void deleteOpp(Long id) {
		userDao.deleteOpp(id);
	}

	@Override
	public List<Opportunities> getSearchVal(String val) {
		return userDao.getSearchVal(val);
	}

	@Override
	public List<Opportunities> getStatusVal(boolean val) {
		return userDao.getStatusVal(val);
	}

	@Override
	public List<Enquiries> getEnquiries() {
		
		return userDao.getEnquiries();
	}

	@Override
	public void update(Opportunities opp) {
		 userDao.update(opp);
	}

	@Override
	public Enquiries getEnquiriesById(Long enqId) {
		 return userDao.getEnquiriesById(enqId);
	}

	@Override
	public List<Refferal> getRefferal() {
		return userDao.getRefferal();
	}

	@Override
	public List<Enquiries> getSearchEnquiry(String val) {
		return userDao.getSearchEnquiry(val);
	}



	@Override
	public List<Country> getCountryList() {
		return userDao.getAllCountries();
	}



	@SuppressWarnings("unchecked")
	@Override
	public User getEmailAddress(String email) {
	User user=null;
	List<User> userList = ht.find("from User u where u.email=? ",email);
	if(userList != null && userList.size() > 0)
		user = userList.get(0);
		return user;
	}



	@Override
	public User verifyEmail(String email) {
		List list = ht.find("FROM User u where u.email =? ", email);
		if (list.size() > 0) {
			return (User)list.get(0);
		}
		return null;
		}




	@Override
	public void saveClient(Client client) {
		ht.saveOrUpdate(client);
		
	}

	@Override
	public void deleteDoc(Long id){
		userDao.deleteDoc(id);
	}
	
@SuppressWarnings("unchecked")
	
	public List<MyDocuments> getClientDocNames(String docname) {
		
		List<MyDocuments> userDoc =ht.find("From MyDocuments my where fileName=?",docname);
		
		/*String query="select fileName from Mydocuments";
		List<MyDocuments> userDoc = (List<MyDocuments>) session.createQuery(query);*/
		
		return userDoc;
	}
	
}
